#include "Sandbox.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.IsInitImGui = 0,
	.Name = "Sandbox",
    };
    application_create(appSettings);

    Layer sandboxLayer = {
	.Name = "Sandbox Layer",
	.OnAttach = sandbox_on_attach,
	.OnUpdate = sandbox_on_update,
	.OnUIRender = sandbox_on_ui_render,
	.OnEvent = sandbox_on_event,
	.OnDestoy = sandbox_on_destroy
    };

    application_push_layer(sandboxLayer);
}
