#include "Sandbox.h"

#include <cglm/cglm.h>
#include <glad/glad.h>

#include <Utils/SimpleStandardLibrary.h>
#include <Graphics/Renderer.h>
#include <Graphics/ModelRenderer.h>
#include <Graphics/KeyCodes.h>
#include <Graphics/RuntimeCamera.h>
#include <Graphics/SimpleWindow.h>
#include <UI/Suif.h>
#include <string.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Graphics/KeyCodes.h>
#include <Graphics/RuntimeCamera.h>

#define ImVec2(x, y) (ImVec2) {x, y}
#define ImVec4(x, y, z, w) (ImVec4) {x, y, z, w}

static SimpleWindow* CurrentWindow = NULL;
static RuntimeCamera CurrentCamera;

static Shader FramebufferShader;

static Texture2D DefaultTexture;
/*Base UI Textures*/
static Texture2D Refresh;
static FpsCounter Fps;

static RuntimeCamera OrthoCamera;

v4 FrontColor = { 0, 0, 1, 1 };
v4 RightColor = { 1, 0, 0, 1 };
v4 UpColor    = { 0, 1, 0, 1 };
v4 RayColor   = { 0.8f, 0.8f, 0, 1 };

CubeMap Cubemap;

static StaticModel* SceneModels = NULL;
static TransformComponent* ModelTransforms = NULL;
u32 SkyboxID;
LightsData LightData;

static f32 Timestep = 0.0f;

void
get_display_size(i32* width, i32* height)
{
    window_get_size(CurrentWindow, width, height);
}

void
sandbox_on_attach()
{
    //vsnprintf

    CurrentWindow = application_get_window();


    WS = wide_string(L"Русский язык");
    WS2 = wide_string(L"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя");
    WS3 = wide_string(L"Б012390А");
    ButtonPlus = wide_string(L"Добавить");
    ButtonMinus = wide_string(L"Уменьшить");
    ButtonText = wide_string(L"Сведения о программе");
    SliderText = wide_string(L"Слайдер");
    CheckBoxLabel = wide_string(L"Текст для чекбокса");

    //WavFile file = wav_reader_read(asset_sound("SimpleSound.wav"));
    //SoundComponent sc = sound_component_new();
    //getchar();

    //window_disable_cursor(window);
    window_set_vsync(2);

    glEnable(GL_MULTISAMPLE);

    RuntimeCameraSettings orthoSettings = (RuntimeCameraSettings) {
	.Type = CameraType_OrthographicSpectator,

	//Spectator
	.Position = { 0.0f, 0.0f, -2.0f },
	.SensitivityHorizontal = 0.35f,
	.SensitivityVertical = 0.45f,
	.Zoom = 1,

	.Left = 0,
	.ORight = CurrentWindow->Width,
	.Bot = 0,
	.Top = CurrentWindow->Height,
	.Near = 0.1f,
	.Far = 1000.0f
    };
    //NOTE(): we always use default declared in runtime_camera_update
    OrthoCamera = runtime_camera_create(orthoSettings);
    renderer_init();

}

void
sandbox_on_update(f32 timestep)
{
    Timestep = timestep;

    fps_counter_update(&Fps);
    runtime_camera_update(&CurrentCamera);

    framebuffer_bind(viewport_get_current_framebuffer());
    {
	renderer_clear(v4_new(0.1f, 0.1f, 0.1f, 1.0f));

	// Пропускаем 1 фрейм
	SuiInstance* instance = sui_get_instance();
	sui_backend_new_frame();
	sui_begin_frame((SuiFrameInfo){.Timestep = timestep});
	{
	    sui_frontend_usage();
	    sui_auto_layout();
	    // sui_process_input();

	    sui_backend_draw();
	}
	sui_end_frame();
    }
    framebuffer_unbind();

    framebuffer_transfer(viewport_get_current_framebuffer()->RendererId,
			 viewport_get_draw_framebuffer()->RendererId,
			 CurrentWindow->Width,
			 CurrentWindow->Height);

    renderer_flush(viewport_get_draw_framebuffer()->ColorAttachments[0]);
}

void
sandbox_on_ui_render()
{
    dockspace_on_ui();

    //bool p_open = true;
    //igShowDemoWindow(&p_open);

    ImGuiIO* io = igGetIO();

    ImGuiWindowFlags windowFlags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoMove;
    ImVec2 windowPosition = ImVec2(550, 120);
    igSetNextWindowPos(windowPosition, ImGuiCond_Always, ImVec2(0,0));
    igSetNextWindowBgAlpha(0.35f); // Transparent background
    static bool isOverlayVisible = true;
    if (igBegin("GameDemoWindow", &isOverlayVisible, windowFlags))
    {
	igText("Frames: %d, Latency: %f, Vsync: %d", Fps.Fps, 1000 * Timestep, CurrentWindow->VsyncLevel);
    }
    igEnd();

}

void
sandbox_on_event(Event* event)
{
    sui_backend_event(event);

	switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }

    /* switch (event->Category) */
    /* { */

    /* case KeyCategory: */
    /* { */
    /*	if (event->Type != KeyPressed) */
    /*	    break; */

    /*	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event; */
    /*	if (keyEvent->KeyCode == KEY_ESCAPE) */
    /*	{ */
    /*	    application_close(); */
    /*	    event->IsHandled = 1; */
    /*	} */

    /*	break; */
    /* } */

    /* case  EventCategory_Window: */
    /* { */
    /*	if (event->Type == WindowShouldBeClosed) */
    /*	{ */
    /*	    application_close(); */
    /*	} */
    /*	else if (event->Type == WindowResized) */
    /*	{ */
    /*	    WindowResizedEvent* wrEvent = (WindowResizedEvent*) event; */
    /*	    OrthoCamera.ORight = wrEvent->Width; */
    /*	    OrthoCamera.Top = wrEvent->Height; */
    /*	    runtime_camera_update_projection(&OrthoCamera); */
    /*	    runtime_camera_update(&OrthoCamera); */
    /*	    //GWARNING("R, T: %lu %lu\n", OrthoCamera.ORight, OrthoCamera.Top); */
    /*	    //printf("R, T: %u %u\n", OrthoCamera.ORight, OrthoCamera.Top); */
    /*	} */
    /*	break; */
    /* } */

    /* case EventCategory_MouseCategory: */
    /* { */
    /*	if (event->Type == MouseScrolled) */
    /*	{ */
    /*	    MouseScrolledEvent* mevent = (MouseScrolledEvent*)event; */
    /*	    RuntimeCamera* runtimeCamera = world_render_get_camera(); */

    /*	    //Place this code in spectator */
    /*	    const f32 lowerThreshold = 0.1f; */
    /*	    if (runtimeCamera->Zoom >= lowerThreshold) */
    /*	    { */
    /*		f32 val = 1.0f; */
    /*		if (mevent->YOffset < 0.0f) */
    /*		{ */
    /*		    val *= -1; */
    /*		} */
    /*		runtimeCamera->Zoom -= 0.25 * val; */
    /*	    } */

    /*	    runtimeCamera->Zoom = MINMAX(runtimeCamera->Zoom, lowerThreshold, 100.0f); */
    /*	    runtime_camera_update_projection(runtimeCamera); */
    /*	    runtime_camera_update(runtimeCamera); */

    /*	    event->IsHandled = 1; */
    /*	} */
    /*	break; */
    /* } */
    /* } */

    /* if (event->Type == KeyPressed) */
    /* { */
    /*	KeyPressedEvent* keyPressedEvent = (KeyPressedEvent*) event; */
    /*	switch (keyPressedEvent->KeyCode) */
    /*	{ */
    /*	case KEY_P: */
    /*	{ */
    /*	    if (CurrentCamera.Type & CameraType_Orthographic) */
    /*	    { */
    /*		runtime_camera_set_perspective(&CurrentCamera); */
    /*	    } */
    /*	    else */
    /*	    { */
    /*		runtime_camera_set_orthograhic(&CurrentCamera); */
    /*	    } */
    /*	    break; */
    /*	} */

    /*	case KEY_E: */
    /*	{ */
    /*	    FlagSwitchDefine(renderer_enable_wireframe_mode(RendererType_Renderer3D), */
    /*			     renderer_enable_fill_mode(RendererType_Renderer3D)); */

    /*	    FlagSwitchDefine( */
    /*		renderer_enable_wireframe_mode(RendererType_RendererModel), */
    /*		renderer_enable_fill_mode(RendererType_RendererModel)); */
    /*	    break; */
    /*	} */

    /*	case KEY_F: */
    /*	{ */
    /*	    FlagSwitchDefine(renderer_enable_face_culling(RendererType_Renderer3D), renderer_disable_face_culling(RendererType_Renderer3D)); */
    /*	    break; */
    /*	} */

    /*	case KEY_G: */
    /*	{ */
    /*	    FlagSwitchDefine(renderer_enable_depth_testing(RendererType_Renderer3D), renderer_disable_depth_testing(RendererType_Renderer3D)); */
    /*	    break; */
    /*	} */

    /*	case KEY_M: */
    /*	{ */
    /*	    FlagSwitchDefine(window_disable_cursor(CurrentWindow), */
    /*			     window_enable_cursor(CurrentWindow)); */
    /*	    break; */
    /*	} */

    /*	case KEY_J: */
    /*	{ */
    /*	    //FlagSwitchDefine(glEnable(GL_MULTISAMPLE), glDisable(GL_MULTISAMPLE)); */
    /*	    break; */
    /*	} */

    /*	} */
    /* } */
    /* else if (event->Type == MouseMoved) */
    /* { */
    /*	MouseMovedEvent* mevent = (MouseMovedEvent*) event; */
    /*	runtime_camera_update_view(&CurrentCamera); */
    /* } */
}

void
sandbox_on_destroy()
{
}


#if 0
if (ui_overlay_begin("OverlayStrID", ImVec2(450, 150)))
{
    igColorEdit4("Front", FrontColor.V, ImGuiColorEditFlags_None);
    igColorEdit4("Right", RightColor.V, ImGuiColorEditFlags_None);
    igColorEdit4("Up"   , UpColor.V   , ImGuiColorEditFlags_None);

    igText("Front %f %f %f", CurrentCamera.Front.X, CurrentCamera.Front.Y, CurrentCamera.Front.Z);
    igText("Right %f %f %f", CurrentCamera.Right.X, CurrentCamera.Right.Y, CurrentCamera.Right.Z);
    igText("Up %f %f %f", CurrentCamera.Up.X, CurrentCamera.Up.Y, CurrentCamera.Up.Z);

    f32 dot = v3_dot(CurrentCamera.Up, CurrentCamera.Right);
    igText("Dot Up,Right: %f, %f, %f", dot, rad(dot), deg(dot));

    igText("Orientation: %f %f %f %f", CurrentCamera.Orientation.X, CurrentCamera.Orientation.Y, CurrentCamera.Orientation.Z, CurrentCamera.Orientation.W);
    igText("Position: %f %f %f", CurrentCamera.Position.X, CurrentCamera.Position.Y, CurrentCamera.Position.Z);

    igText("Yaw: %f Pitch: %f\n", CurrentCamera.Yaw, CurrentCamera.Pitch);
}
ui_overlay_end();
#endif


#if 0
if (igBegin("Camera", &open, ImGuiWindowFlags_None))
{
    igText("CameraType: %s", camera_type_to_string(CurrentCamera.Type));

    igSliderFloat("ZoomLevel", &CurrentCamera.Zoom, 0.1f, 10.0f, "%0.1f", ImGuiSliderFlags_None);
    igSliderFloat("Speed", &CurrentCamera.Speed, 0.1f, 10.0f, "%0.1f", ImGuiSliderFlags_None);
    igSliderFloat3("Position", CurrentCamera.Position.V, -25.f, 25.f, "%0.1f", ImGuiSliderFlags_None);

    f32 fov = deg(CurrentCamera.Fov);
    igDragFloat("Fov", &fov,
		1.0f, 0.0f, 150.0f, "%0.1f", ImGuiSliderFlags_None);
    CurrentCamera.Fov = rad(fov);
}
igEnd();
#endif
