#ifndef VULKAN_SANDBOX_H
#define VULKAN_SANDBOX_H

#include <Engine.h>

void vulkan_sandbox_on_attach();
void vulkan_sandbox_on_attach_finished();
void vulkan_sandbox_on_update(f32 timestep);
void vulkan_sandbox_on_ui_render();
void vulkan_sandbox_on_event(Event* event);
void vulkan_sandbox_on_destroy();

#endif // VULKAN_SANDBOX_H
