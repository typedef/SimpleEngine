#include "VulkanSandbox.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 0,
	.IsFrameRatePrinted = 0,
	.Name = "VulkanSandbox",
    };
    application_create(appSettings);

    Layer vulkanSandboxLayer = {0};
    vulkanSandboxLayer.Name = "VulkanSandbox Layer";
    vulkanSandboxLayer.OnAttach = vulkan_sandbox_on_attach;
    vulkanSandboxLayer.OnUpdate = vulkan_sandbox_on_update;
    vulkanSandboxLayer.OnUIRender = vulkan_sandbox_on_ui_render;
    vulkanSandboxLayer.OnEvent = vulkan_sandbox_on_event;
    vulkanSandboxLayer.OnDestoy = vulkan_sandbox_on_destroy;

    application_push_layer(vulkanSandboxLayer);
}
