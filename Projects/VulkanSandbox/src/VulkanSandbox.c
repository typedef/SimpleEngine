#include "VulkanSandbox.h"

#include <InputSystem/KeyCodes.h>
#include <Graphics/SimpleWindow.h>
#include <Math/SimpleMath.h>
#include <UI/Sui.h>
#include <UI/SuiDemo.h>
#include <Editor/EditorViewport.h>
#include "SimpleCompiler.h"
//
//nothing for today
static SimpleWindow* CurrentWindow = NULL;
//NOTE(typedef): Temporary
static EcsWorld* World;
static Renderer3dStatic gRenderer3dStatic;

// NOTE() Demo   related
CameraComponent gCameraComponent;
StaticModel gStaticModel;
LightDataUbo gLightDataUbo;

void
vulkan_sandbox_on_attach()
{
    CurrentWindow = application_get_window();
    window_set_vsync(1);

    asset_manager_create();
    //editor_viewport_activate();

    CameraComponentCreateInfo createInfo = {
	.IsCameraMoved = 1,

	.Settings = {
	    .Type = CameraType_PerspectiveSpectator,
	    .Position = v3_new(0, 0, 1.0f),
	    .Front = v3_new(0, 0, -1),
	    .Up    = v3_new(0, 1,  0),
	    .Right = v3_new(1, 0,  0),
	    .Yaw = 0,
	    .Pitch = 0,
	    .PrevX = 0,
	    .PrevY = 0,
	    .SensitivityHorizontal = 0.35f,
	    .SensitivityVertical = 0.45f
	},

	.Perspective = {
	    .Near = 0.001f,
	    .Far = 1000.0f,
	    .AspectRatio = CurrentWindow->AspectRatio,
	    // NOTE(typedef): in degree
	    .Fov = 90
	},

	.Spectator = {
	    .Speed = 5.0f,
	    .Zoom = 0
	}

    };

    gCameraComponent =
	camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);

    //BUG(typedef): Not cleaned texture
#if 1
    //gStaticModel = static_model_load(asset_model("Retro computer/Retro computer.glb"));
    gStaticModel = static_model_load_assimp(asset_model("Retro computer/Retro computer.glb"));
    //vguard(0);
#else
    gStaticModel = static_model_cube_create();
#endif

    gLightDataUbo = (LightDataUbo) {
	.DirectionalLightsCount = 1,
	.DirectionalLights = {
	    [0] = {
		.Base = {
		    .Ambient = v3_new(0.75, 0.75, 0.75),
		    .Diffuse = v3_new(0.75, 0.75, 0.75),
		    .Specular = v3_new(0.75, 0.75, 0.75),
		},
		.Color = v3_new(1, 1, 1),
		.Direction = v3_new(0, -1.0f, 0)
	    }
	},
	.PointLightsCount = 0,
	.FlashLightsCount = 0

    };

#if 1
    Renderer3dStaticSettings settings = {
	.MaxCount = 100000,
	.pCamera = &gCameraComponent.Base
    };
    vsr_3d_static_create(&gRenderer3dStatic, &settings);
#endif

#if 1

    VsaTexture* pTex0 = (VsaTexture*) asset_manager_load_asset(AssetType_Texture, asset_texture("BathRoom.jpg"));
    VsaTexture* pTex1 = (VsaTexture*) asset_manager_load_asset(AssetType_Texture, asset_texture("Bed.jpeg"));

#endif

    { // Default Ecs Preparation
	//World = ecs_world_create();
	//ecs_world_register_component(World, CameraComponent);

	/* EntityId entity = ecs_world_entity(World, "MainCamera"); */
	/* ecs_world_entity_add_component(World, entity, CameraComponent, cameraComponent); */
    }
    //scene_load(&gAssetManager);
}

void
vulkan_sandbox_on_update(f32 timestep)
{

    EditorViewportUpdate evu = (EditorViewportUpdate) {
	.Camera = &gCameraComponent,
	.Timestep = timestep
    };
    editor_viewport_on_update(evu);

#if 1
    i32 imageIndex = vsr_get_image_index();
    vsr_3d_static_draw_model(&gRenderer3dStatic,
			     imageIndex,
			     gStaticModel,
			     gLightDataUbo,
			     gCameraComponent.Settings.Position);
#endif

}

void
vulkan_sandbox_on_ui_render()
{
    //sui_demo_of_frontend_usage();
    static WideString ltr = {.Buffer = L"Позиция", .Length = 7};
    static i32 isVisible = 1;
    if (sui_panel_begin(&ltr, &isVisible, SuiPanelFlags_WithOutHeader))
    {
	WideString format = wide_string_news(L"Камера: %f %f %f");
	v3 pos = gCameraComponent.Settings.Position;
	sui_text(&format, pos.X, pos.Y, pos.Z);
    }
    sui_panel_end();
}

void
vulkan_sandbox_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	if (event->Type != EventType_KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}
	break;
    }

    case EventCategory_Window:
    {
	if (event->Category == EventCategory_Window)
	{
	    switch (event->Type)
	    {

	    case EventType_WindowShouldBeClosed:
		application_close();
		event->IsHandled = 1;
		break;

	    case EventType_WindowResized:
	    case EventType_WindowMaximized:
		GINFO("Resized or Maximized!\n");
		vsa_set_window_resized();
		break;
	    }
	}

    }

    }
}

void
vulkan_sandbox_on_destroy()
{
    GINFO("Sandbox Destroy!\n");

    asset_manager_destroy();

#if 1
    vsr_3d_static_destroy(&gRenderer3dStatic);
#endif
}
