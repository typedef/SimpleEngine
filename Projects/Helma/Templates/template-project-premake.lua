workspace "Engine"
architecture "x64"
startproject "SandboxApp"

configurations
{
  "Debug",
  "Release",
  "Dist"
}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include dirs
IncludeDirs = {}
IncludeDirs["GLFW"] = "Dependencies/GLFW/include"
IncludeDirs["GLAD"] = "Dependencies/glad/include"
IncludeDirs["CGLM"] = "Dependencies/CGLM/include"
IncludeDirs["CIMGUI"] = "Dependencies/cimgui/src"
IncludeDirs["CGLTF"] = "Dependencies/cgltf/src/"
IncludeDirs["MINIAUDIO"] = "Dependencies/MiniAudio/src/"
IncludeDirs["STB"] = "Dependencies/stb/src/"

group "Dependencies"
include "Dependencies/GLFW"
include "Dependencies/glad"
include "Dependencies/cimgui"
include "Dependencies/cgltf"
include "Dependencies/MiniAudio"
include "Dependencies/stb"

project "Engine"
    location "Engine"
    cdialect "C99"
    kind "StaticLib"
    language "C"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")

    files
    {
      "%{prj.name}/src/**.h",
      "%{prj.name}/src/**.c",
    }

    defines
    {
      "_CRT_SECURE_NO_WARNINGS",
      "GLFW_INCLUDE_NONE",
      "CIMGUI_DEFINE_ENUMS_AND_STRUCTS",
      "_GNU_SOURCE",
    }

    includedirs
    {
      "%{prj.name}/src",
      "%{IncludeDirs.GLFW}",
      "%{IncludeDirs.GLAD}",
      "%{IncludeDirs.CGLM}",
      "%{IncludeDirs.CIMGUI}",
      "%{IncludeDirs.CGLTF}",
      "%{IncludeDirs.MINIAUDIO}",
      "%{IncludeDirs.STB}"
    }

    filter "system:linux"
      defines { "LINUX_PLATFORM" }

    filter "system:windows"
      defines { "WINDOWS_PLATFORM" }

    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"

project "${ProjectName}"
    location "Projects/${ProjectName}"
    kind "ConsoleApp"
    language "C"
    cdialect "C99"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")

    files
    {
      "Projects/%{prj.name}/src/**.h",
      "Projects/%{prj.name}/src/**.c"
    }

    defines
    {
      "GLFW_INCLUDE_NONE",
      "CIMGUI_DEFINE_ENUMS_AND_STRUCTS",
      "_GNU_SOURCE"
    }

    includedirs
    {
      "Engine/src",
      "Dependencies",
      "%{IncludeDirs.GLFW}",
      "%{IncludeDirs.GLAD}",
      "%{IncludeDirs.CGLM}",
      "%{IncludeDirs.CIMGUI}",
      "%{IncludeDirs.CGLTF}",
      "%{IncludeDirs.MINIAUDIO}",
      "%{IncludeDirs.STB}"
    }

    links
    {
       "Engine",
       "uuid",
       "glad",
       "GLFW",
       "cimgui",
       "cgltf",
       "stb",
       "MiniAudio"
    }

    -- ${ProjectName}
    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"

    filter "system:linux"
      defines { "LINUX_PLATFORM" }
      links
      {
	"GL", "GLU",
	"X11","dl",
	"Xinerama", "Xcursor", "m",
	"Xxf86vm", "Xrandr", "pthread", "Xi",
	"stdc++"
      }

    filter "system:windows"
      defines { "WINDOWS_PLATFORM" }
