#include "Helma.h"

#include <string.h>

#include "ProjectAnalizer.h"
#include "TemplateCreator.h"
#include <Graphics/KeyCodes.h>
#include <Utils/Types.h>
#include <Utils/SimpleStandardLibrary.h>

static SimpleWindow* CurrentWindow;

void
helma_on_attach()
{
    CurrentWindow = application_get_window();

#if 0
    char* result = helma_generate_enum_to_string("InputText");
#endif
    /* char* result = helma_generate_enum_to_string(file_read_string("/home/bies/Data/programming/C/SimpleEnginePrivate/Game/src/Game/Core/AI/ActivityType.h")); */
    /* printf("Result: %s\n", result); */

#if 0
    const char* root = "/home/bies/Data/programming/C/SimpleEnginePrivate/";

    {
	TimeState timeState;
	profiler_start(&timeState);
	LinkedFile* linkedFile = simple_parse(root);
	profiler_end(&timeState);
	GINFO("Time: %ld ms\n", profiler_get_milliseconds(&timeState));
	vassert_break();
    }

#endif

}

void
helma_on_attach_finished()
{
}

void
helma_on_update(f32 timestep)
{
}

char InputText[KB(100)];
char DestText[KB(100)];
char* EnumToStringTime = NULL;

static void
enum_to_string_ui()
{
    static bool enumToStringVisible = true;
    if (igBeginTabItem("EnumToString", &enumToStringVisible, ImGuiTabItemFlags_None))
    {
	i32 width = 650;
	i32 height = 750;
	i32 btnWidth = 50;

	igInputTextMultiline("##source", InputText, ARRAY_COUNT(InputText), ImVec2(width, height), ImGuiInputTextFlags_None, NULL, NULL);

	igSameLine(width + 25, 50);

	static i32 isBtnPressed = 0;
	static TimeState timeState;
	if (igButton("=>", ImVec2(btnWidth, 50)))
	{
	    profiler_start(&timeState);

	    if (string_length(InputText) > 0)
	    {
		char* result = helma_generate_enum_to_string(InputText);
		if (result != NULL)
		{
		    memcpy(DestText, result, string_length(result) + 1);
		    string_builder_free(result);
		}
	    }
	    profiler_end(&timeState);

	    EnumToStringTime = profiler_get_string(&timeState);
	    isBtnPressed = 1;
	}

	igSameLine(width + 25 + btnWidth + 50, 50);

	igInputTextMultiline("##dest", DestText, ARRAY_COUNT(DestText), ImVec2(width, height), ImGuiInputTextFlags_None, NULL, NULL);

	igText("Input Length: %d", string_length(InputText));
	igSameLine(width + 25 + btnWidth + 50, 50);
	igText("Dest Length: %d", string_length(DestText));

	if (isBtnPressed)
	    igText("Process in %s", EnumToStringTime);

	igEndTabItem();
    }
}

static void
parse_structs_ui()
{
    static bool parseStructsVisible = true;
    if (igBeginTabItem("ParseStructs", &parseStructsVisible, ImGuiTabItemFlags_None))
    {
	static char buf[512];
	igInputText("File Path", buf, ARRAY_COUNT(buf), ImGuiInputTextFlags_None, NULL, NULL);
	ImVec2 size;
	igGetItemRectSize(&size);
	igSameLine(size.x + 25, 50);

	static TimeState timeState;
	static TypeInfo* typenames = NULL;
	static char* timeString = NULL;
	if (igButton("Get Structs", ImVec2(0,0)))
	{
	    if (string_length(buf) > 0 && path_is_directory_exist(buf))
	    {
		profiler_start(&timeState);
		typenames = simple_parse_types(buf);
		profiler_end(&timeState);
		timeString = profiler_get_string(&timeState);
	    }
	}

	igText("Typenames (%d):", array_count(typenames));
	igSameLine(0, 150);
	igText("Proccess time: %s", timeString);
	i32 i, count = array_count(typenames);
	for (i = 0; i < count; ++i)
	{
	    TypeInfo info = typenames[i];
	    igText("%s %s", struct_type_to_string(info.Type), info.Name);
	}


	igEndTabItem();
    }
}

static void
parser_ui()
{
    static bool parserVisible = true;
    if (igBeginTabItem("Parser", &parserVisible, ImGuiTabItemFlags_None))
    {
	static char buf[512];
	igInputText("File Path", buf, ARRAY_COUNT(buf), ImGuiInputTextFlags_None, NULL, NULL);
	ImVec2 size;
	igGetItemRectSize(&size);
	igSameLine(size.x + 25, 50);

	static TimeState timeState;
	static LinkedFile* linkedFile = NULL;
	static char* timeString = NULL;
	if (igButton("Parse Dirs", ImVec2(0,0)))
	{
	    if (string_length(buf) > 0 && path_is_directory_exist(buf))
	    {
		profiler_start(&timeState);
		linkedFile = simple_parse(buf);
		profiler_end(&timeState);
		timeString = profiler_get_string(&timeState);
	    }
	}

	igEndTabItem();
    }
}

void
helma_on_ui_render()
{
    dockspace_on_ui();

    static bool p_open = true;
    igShowDemoWindow(&p_open);

    static bool enumOpened = true;
    if (igBegin("Enum", &enumOpened, ImGuiWindowFlags_None))
    {
	if (igBeginTabBar("SomeTabID123", ImGuiTabBarFlags_None))
	{
	    enum_to_string_ui();
	    parse_structs_ui();
	    parser_ui();
	}

	igEndTabBar();

    }
    igEnd();

    static bool isProjectCreatorOpened = true;
    if (igBegin("Project Creator", &isProjectCreatorOpened, ImGuiTabBarFlags_None))
    {
	igText("SimpleUserInterfaceSandbox: %s", path_get_current_directory());

#define bufLength 128
	static char buf[bufLength];
	igInputText("Project Name: ", buf, bufLength, ImGuiInputTextFlags_None,  NULL, NULL);
	static ProjectInfo info = { 0 };
	static i32 isProjectCreated = -1;
	igCheckbox("Use Close Application Handler", (bool*) &info.Snippets.CloseApplicationEventHandler);
	if (igButton("Generate", (ImVec2){0,0}))
	{
	    size_t actualLength = string_length(buf);
	    if (actualLength > 0)
	    {
		info.DirectoryWhereCreate = path_get_current_directory();
		info.ProjectName = buf;
		info.ProjectNameLength = actualLength;
		isProjectCreated = project_create(info);
	    }
	}

	if (isProjectCreated == 0)
	{
	    igText("Project is already exist!");
	}
    }
    igEnd();

}

void
helma_on_event(Event* event)
{
    switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case  EventCategory_Window:
    {
	if (event->Type == WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
helma_on_destroy()
{
}
