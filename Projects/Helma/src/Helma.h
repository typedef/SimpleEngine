#ifndef HELMA_H
#define HELMA_H

#include <Engine.h>

void helma_on_attach();
void helma_on_attach_finished();
void helma_on_update(f32 timestep);
void helma_on_ui_render();
void helma_on_event(Event* event);
void helma_on_destroy();

#endif
