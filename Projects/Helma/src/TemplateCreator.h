#ifndef TEMPLATE_CREATOR_H
#define TEMPLATE_CREATOR_H

#include <Utils/Types.h>

typedef struct ProjectCodeSnippets
{
    i32 CloseApplicationEventHandler;
} ProjectCodeSnippets;

typedef struct ProjectInfo
{
    const char* DirectoryWhereCreate;
    const char* ProjectName;
    size_t ProjectNameLength;
    ProjectCodeSnippets Snippets;
} ProjectInfo;

i32 project_create(ProjectInfo info);


#endif
