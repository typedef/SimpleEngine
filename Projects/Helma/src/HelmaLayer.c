#include "Helma.h"

#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.Name = "Helma",
    };
    application_create(appSettings);

    Layer helmaLayer;
    helmaLayer.Name = "Helma Layer";
    helmaLayer.OnAttach = helma_on_attach;
    helmaLayer.OnUpdate = helma_on_update;
    helmaLayer.OnUIRender = helma_on_ui_render;
    helmaLayer.OnEvent = helma_on_event;
    helmaLayer.OnDestoy = helma_on_destroy;

    application_push_layer(helmaLayer);
}
