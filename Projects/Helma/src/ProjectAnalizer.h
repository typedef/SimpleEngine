#ifndef PROJECT_ANALIZER_H
#define PROJECT_ANALIZER_H

#include <Utils/Types.h>

typedef struct IElement IElement;
typedef struct FileLink FileLink;

typedef struct LinkedFile
{
    IElement* File;
    FileLink* Links; // FilesIncluded
} LinkedFile;

LinkedFile* simple_parse(const char* rootPath);

typedef enum StructType
{
    StructType_Struct = 0,
    StructType_Enum
} StructType;
force_inline const char*
struct_type_to_string(StructType type)
{
    switch (type)
    {
    case StructType_Struct: return "struct";
    case StructType_Enum: return "enum";
    }

    vassert_break();
    return "";
}

typedef struct TypeInfo
{
    StructType Type;
    char* Name;
} TypeInfo;
TypeInfo* simple_parse_types(const char* root);


#endif
