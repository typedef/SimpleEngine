#include "SimpleTest.h"
//#include "UI/ui.h"

#include "UTests/Test.h"
#include "UTests/SelfTest/UTest_SelfTest.h"
#include "UTests/Math/UTest_BaseMath.h"
#include "UTests/Math/UTest_M4.h"
#include "UTests/Utils/UTest_String.h"
#include "UTests/Utils/UTest_StringBuilder.h"
#include "UTests/Utils/UTest_Path.h"
#include "UTests/Utils/UTest_Array.h"
#include "UTests/Utils/UTest_HashTable.h"
#include "UTests/Utils/UTest_JsonParser.h"
#include "UTests/Utils/UTest_RingQueue.h"

#include <Utils/SimpleStandardLibrary.h>

#include "ToggleButton.h"

#define ImVec2(x, y) (ImVec2) {x, y}
#define ImVec4(x, y, z, w) (ImVec4) {x, y, z, w}

/*
Point* pqArray = NULL;
		    Point*   f32      Point
priority_queue_push(pqArray, priority, point);
priority_queue_pop(pqArray, priority, point);
*/
SimpleWindow* CurrentWindow;
static ImFont** g_Fonts = NULL;

void
simple_test_on_attach()
{
    CurrentWindow = application_get_window();

    window_set_vsync(5);

    //ProfilingTest profilingTest;
    //profiling_test_init(&profilingTest);

    //NOTE(typedef): this test works, is latest
    m4_test();
    array_test();
    utest_self_test();
    string_test();
    hash_test();
    //BUG(typedef): it's crashing app due to its hdr problems
    string_builder_test();

#if 0
    json_parser_test();
    priority_queue_test();
    m4_test();

    path_test();
    base_math_test();
    v2_test();
    scene_test();
    serializer_test();
    //memory_allocator_test();

    GSUCCESS("Run all test's!\n");
#endif

    g_Fonts = ui_get_fonts();
}

void
simple_test_on_event(Event* event)
{
    if (event->Category == KeyCategory && event->Type == KeyPressed)
    {
	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}
    }
    else if (event->Category == EventCategory_Window && event->Type == WindowShouldBeClosed)
    {
	application_close();
    }
}

static void
memory_block_popup(MemoryBlock* block, i32 show)
{
    const char* File;
    i32 Line;
    i64 AllocatedSize;
    void* Address;

    if (show)
	igOpenPopup_Str("my_select_popup",ImGuiPopupFlags_MouseButtonLeft);
    igSameLine(0, 0);

    if (igBeginPopup("my_select_popup", ImGuiWindowFlags_Popup))
    {
	igText("File: %s",  block->File);
	igText("Line: %d",  block->Line);
	igText("AllocatedSize: %ld", block->AllocatedSize);
	igText("Address: %ld", (size_t)block->Address);
	igEndPopup();
    }
}

static MemoryBlock* Max = NULL;
static MemoryBlock* Min = NULL;
static const char** Files = NULL;
static i32* Lines = NULL;

static bool
lines_item_getter(void* data, int idx, const char** outText)
{
    GWARNING("LALAAAAAAAAAAAAAAAAAA\n");

    i32* ptr = (i32*)data;

    char buf[64];
    sprintf(buf, "%d", ptr[idx]);
    *outText = istring(buf);
    return true;
}

static void
memory_debugger()
{
    {
	//Memory Debug
	i32 totalSize = memory_helper_get_allocated_size();
	MemoryBlock** memoryBlocks = memory_helper_get_memory_blocks();

	if (!Max)
	    Max = array_filter(memoryBlocks, item->AllocatedSize > filteredItem->AllocatedSize);
	if (!Min)
	    Min = array_filter(memoryBlocks, item->AllocatedSize < filteredItem->AllocatedSize);

	if (!Files)
	{
	    MemoryBlock** distinctFiles = array_distinct(memoryBlocks, string_compare(item->File, distinctItem->File));
	    array_push(Files, "All");
	    array_foreach(distinctFiles, array_push(Files, item->File););
	    array_free(distinctFiles);
	}
	if (!Lines)
	{
	    MemoryBlock** distinctLines = array_distinct(memoryBlocks, item->Line == distinctItem->Line);

	    GERROR("DistinctLines COUNT: %d\n", array_count(distinctLines));
	    array_foreach(distinctLines, GERROR("%d\n", item->Line););

	    array_push(Lines, -1);
	    array_foreach(distinctLines, array_push(Lines, item->Line););

	    array_free(distinctLines);
	}

	//GLOG("%ld\n", (size_t)MemoryAllocationList);
	static bool isMemoryAllocationsDebugVisible = true;
	if (igBegin("MemoryAllocationsDebug", &isMemoryAllocationsDebugVisible, ImGuiWindowFlags_None))
	{
	    static i32 currentFileItem = 0;
	    static i32 currentLineItem = 0;
	    igCombo_Str_arr("File", &currentFileItem, Files, array_count(Files), 3);
	    //igCombo_Str_arr("Line", &currentLineItem, Lines, array_count(Lines), 3);
	    igCombo_FnBoolPtr("Linessss", &currentLineItem, lines_item_getter, (void*)Lines, currentLineItem, 3);

	    igText("Max allocated block: %f mb\n", TOMB(Max->AllocatedSize));
	    igText("Min allocated block: %d bytes\n", Min->AllocatedSize);

	    ImVec2 winSize; igGetWindowSize(&winSize); //GERROR("%f %f\n", winSize.x, winSize.y);
	    f32 pixelsInByte = f32(Max->AllocatedSize) / winSize.x;
	    DO_ONES(GERROR("PixelsInByte: %f\n", pixelsInByte));

	    i32 currentLineWidth = 0;
	    i32 i, count = array_count(memoryBlocks);
	    for (i = 0; i < count; ++i)
	    {
		MemoryBlock* block = memoryBlocks[i];

		if (currentFileItem != 0 && !string_compare(block->File, Files[currentFileItem]))
		{
		    continue;
		}

		char buf[64];
		memset(buf, '\0', 64*sizeof(char));
		sprintf(buf, "%ld", (size_t)block->Address);
		if (igButton(buf, ImVec2(block->AllocatedSize/*180*/, 30)))
		{
		}

		//GERROR("currentLineWidth: %d\n", currentLineWidth);
		currentLineWidth += (block->AllocatedSize + 10);
		f32 thresholdX = (i < (count - 1)) ? (winSize.x - memoryBlocks[i + 1]->AllocatedSize) : winSize.x;
		if (currentLineWidth < thresholdX)
		{
		    igSameLine(0,10);
		}
		else
		{
		    currentLineWidth = 0;
		}

		if (igIsItemHovered(ImGuiHoveredFlags_RectOnly))
		{
		    memory_block_popup(block, 1);
		}

		/*
		  CIMGUI_API void igGetWindowContentRegionMin(ImVec2 *pOut);
		  CIMGUI_API void igGetWindowContentRegionMax(ImVec2 *pOut);
		*/
	    }
	    //ImDrawList* draw_list = ImGui::GetWindowDrawList();
	}
	igEnd();
    }
}

static i32 selectedIndex = -1;
const char* selectedFileName = NULL;

static void
test_on_ui()
{
    static bool isDemoOpen = 1;
    static bool isTestPanelOpen = 1;
    static char* testText = "No text";
    static FunctionResultData* results = NULL;

    const char** fileNames = test_get_filenames();
    i32 length = array_len(fileNames);

    if (igBegin("Test's", &isTestPanelOpen, ImGuiWindowFlags_None))
    {
	i32 state = 0;
	igToggleButton("E", ImVec2(25, 25), &state);
	TestInfo testInfo = test_get_info();
	igSameLine(35.0f, 20);
	igText("S: %d E: %d TestCount: %d", testInfo.SuccessCount, testInfo.ErrorsCount, testInfo.Count);
	for (i32 i = 0; i < length; i++)
	{
	    const char* filename = fileNames[i];
	    if (igCollapsingHeader_BoolPtr(filename, NULL, ImGuiWindowFlags_NoCollapse))
	    {
		FileInfo* fileInfo = file_get_info(filename);
		if (!fileInfo || !fileInfo->Functions)
		{
		    continue;
		}

		i32 f, functionsCount = array_len(fileInfo->Functions);
		for (f = 0; f < functionsCount; ++f)
		{
		    const char* functionName = fileInfo->Functions[f];
		    FunctionResult* functionResult =  file_info_get_function_result(fileInfo, functionName);
		    if (!functionName || !functionResult)
		    {
			continue;
		    }

		    if ((state == 1 && functionResult->IsSuccess) || (state == 2 && !functionResult->IsSuccess))
		    {
			continue;
		    }

		    f32 redVal;
		    f32 greenVal;
		    f32 blueVal;

		    if (functionResult->IsSuccess)
		    {
			redVal = 0.3f;
			greenVal = 0.8f;
			blueVal = 0.6f;
		    }
		    else
		    {
			redVal = 1.0f;
			greenVal = 0.5f;
			blueVal = 0.5f;
		    }

		    igPushID_Int(1);
		    if (f == selectedIndex && selectedFileName != NULL && string_compare(filename, selectedFileName))
		    {
			static f32 selectedRedVal   = 0.9f;
			static f32 selectedGreenVal = 0.5f; // 0.3f that awesome red color
			static f32 selectedBluesVal = 0.1f;

			igPushStyleColor_Vec4(ImGuiCol_Button, ImVec4(selectedRedVal, selectedGreenVal, selectedBluesVal, 1.0f));
			igPushStyleColor_Vec4(ImGuiCol_ButtonHovered, ImVec4(selectedRedVal, selectedGreenVal + 0.1f, selectedBluesVal + 0.1f, 1.0f));
			igPushStyleColor_Vec4(ImGuiCol_ButtonActive, ImVec4(selectedRedVal, selectedGreenVal + 0.2f, selectedBluesVal + 0.2f, 1.0f));
		    }
		    else
		    {
			igPushStyleColor_Vec4(ImGuiCol_Button, ImVec4(redVal, greenVal, blueVal, 1.0f));
			igPushStyleColor_Vec4(ImGuiCol_ButtonHovered, ImVec4(redVal, greenVal + 0.1f, blueVal + 0.1f, 1.0f));
			igPushStyleColor_Vec4(ImGuiCol_ButtonActive, ImVec4(redVal, greenVal + 0.2f, blueVal + 0.2f, 1.0f));
		    }

		    if (igButton(functionName, ImVec2(0, 0)))
		    {
			selectedIndex = f;
			selectedFileName = filename;

			window_set_title(CurrentWindow, functionName);
			results = functionResult->Results;
		    }

		    igPopStyleColor(3);
		    igPopID();
		}
	    }
	}
    }
    igEnd();

    if (igBegin("Test Results", NULL, ImGuiWindowFlags_None))
    {
	igPushFont(g_Fonts[1]);

	if (results != NULL)
	{
	    i32 i, count = array_count(results);
	    for(i = 0; i < count; ++i)
	    {
		FunctionResultData funcResultData = results[i];

		if (funcResultData.IsSeparated)
		    igSeparator();

		if (funcResultData.NestingLevel)
		{
		    char* sb = NULL;
		    for (i32 n = 0; n < funcResultData.NestingLevel; ++n)
		    {
			string_builder_appends(sb, "    ");
		    }
		    igText("%s%s [Resulted: %d]", sb, funcResultData.Message, funcResultData.ReturnCode);
		    string_builder_free(sb);
		}
		else
		{
		    igText("%s [Resulted: %d]", funcResultData.Message, funcResultData.ReturnCode);
		}
	    }
	}

	igPopFont();
    }
    igEnd();
}

void
simple_test_on_ui()
{
    static bool opt_fullscreen = 1;
    static bool newItem = 0;
    static bool openItem = 0;
    static bool opt_padding = 0;
    static bool p_open = 1;
    static ImGuiDockNodeFlags dockspace_flags = ImGuiDockNodeFlags_None;

    ImGuiWindowFlags window_flags = ImGuiWindowFlags_MenuBar | ImGuiWindowFlags_NoDocking;
    ImGuiWindowClass class;
    if (opt_fullscreen)
    {
	ImGuiViewport* viewport = igGetMainViewport();
	igSetNextWindowPos(viewport->Pos, ImGuiCond_None, ImVec2_(0, 0));
	igSetNextWindowSize(viewport->Size, ImGuiCond_None);
	igSetNextWindowViewport(viewport->ID);
	igPushStyleVar_Float(ImGuiStyleVar_WindowRounding, 0.0f);
	igPushStyleVar_Float(ImGuiStyleVar_WindowBorderSize, 0.0f);
	window_flags |= ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove;
	window_flags |= ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoNavFocus;
    }
    else
    {
	dockspace_flags &= ~ImGuiDockNodeFlags_PassthruCentralNode;
    }


    if (dockspace_flags & ImGuiDockNodeFlags_PassthruCentralNode)
	window_flags |= ImGuiWindowFlags_NoBackground;

    if (!opt_padding)
    {
	igPushStyleVar_Vec2(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
    }

    igBegin("DockSpace Demo", &p_open, window_flags);

    if (!opt_padding)
	igPopStyleVar(1);

    if (opt_fullscreen)
	igPopStyleVar(2);

    // DockSpace
    ImGuiIO* io = igGetIO();
    if (io->ConfigFlags & ImGuiConfigFlags_DockingEnable)
    {
	ImGuiID dockspace_id = igGetID_Str("MyDockSpace");
	igDockSpace(dockspace_id, ImVec2(0.0f, 0.0f), dockspace_flags, &class);
    }

    if (igBeginMenuBar())
    {
	if (igBeginMenu("File", 1))
	{
	    // Disabling fullscreen would allow the window to be moved to the front of other windows,
	    // which we can't undo at the moment without finer window depth/z control.
	    if (igMenuItem_Bool("Open", "Ctrl + O", 0, 1))
	    {
		GWARNING("Ctrl O\n");
	    }
	    if (igMenuItem_Bool("New", "Ctrl+N", 0, 1))
	    {
		GWARNING("Ctrl N\n");
	    }
	    igSeparator();

	    igSeparator();

	    if (igMenuItem_Bool("Close", NULL, false, 0))
		p_open = false;

	    igEndMenu();
	}

	igEndMenuBar();
    }
    igEnd();

#if 0
    {    // NOTE(bies): we can get exaples from demo
	static bool isDemoOpen = true;
	igShowDemoWindow(&isDemoOpen);
    }
#endif

#if 1
    //memory_debugger();
    test_on_ui();
#endif
}

void
simple_test_on_update(f32 timestep)
{
}

void
simple_test_on_destroy()
{
}
