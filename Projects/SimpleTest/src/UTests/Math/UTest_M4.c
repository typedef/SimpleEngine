#include "UTest_M4.h"

#include "UTests/Test.h"
#include <Math/SimpleMath.h>
#include <cglm/cglm.h>

void
cglm_get_transform(vec3 position, vec2 size, f32 angle, mat4 transform)
{
    vec3 scaleVec = { size[0], size[1], 1.0f };
    mat4 translation = GLM_MAT4_IDENTITY_INIT;
    mat4 rotation = GLM_MAT4_IDENTITY_INIT;
    mat4 scale = GLM_MAT4_IDENTITY_INIT;

    glm_translate(translation, position);
    glm_rotate_z(rotation, glm_rad(angle), rotation);
    glm_scale(scale, scaleVec);

    glm_mat4_mulN((mat4*[]) {&translation, &rotation, &scale}, 3, transform);
}

void
perspective_creation_test()
{
    f32 near = 0.1f,
	far = 1000.0f,
	aspect = 1920.0f / 1080.0f,
	fov = 45.0f;

    m4 pers = perspective(near, far, aspect, fov);
    mat4 glmPers;
    glm_perspective(fov, aspect, near, far, glmPers);

    m4 glmAsM4 = m4_double_array(glmPers);
    GERROR("HERE!\n");
    M4_Equal(pers, glmAsM4);
    M4_Value(pers);
    M4_Value(glmAsM4);
}

void
m4_look_at_test()
{
    v3 eye = v3_new(5.0f, 4.0f, 0.0f);
    v3 center = v3_new(0.0f, 0.0f, 1.0f);
    v3 up = v3_new(0.0f, 1.0f, 0.0f);

    m4 perspective = m4_look_at(eye, center, up);
    mat4 cglm;
    glm_lookat(eye.V, center.V, up.V, cglm);

    M4_Equal(perspective, m4_double_array(cglm));
    M4_Value(perspective);
    M4_Value(m4_double_array(cglm));
}

void
camera_view_matrix_test()
{
    //Camera camera;
    //v3 position = v3_new(2.5f, 1.5f, 1.0f);
    //v3 rotationAxis = v3_new(0.0f, 0.0f, 1.0f);
    //camera_recalculate_view(&camera, position, rotationAxis, 0.0f);
}

void
m4_transform_test()
{
    f32 angle   = 0.0f;
    v3 xyz      = v3_new(1.0f, 1.0f, 1.0f);
    v3 size     = v3_new(1.5f, 1.0f, 1.0f);
    v3 rotation = v3_new(0.0f, 0.0f, angle);

    mat4 cglmTransform = GLM_MAT4_IDENTITY_INIT;
    cglm_get_transform(xyz.V, size.V, angle, cglmTransform);
    m4 transform = m4_transform(xyz, rotation, size);
    m4 cglmM4 = m4_double_array(cglmTransform);

    M4_Equal(transform, cglmM4);
    M4_Value(transform);
    M4_Value(cglmM4);
}

void
m4_translate_test()
{
    v3 translate = v3_new(1.0f, 0.5f, 0.0f);

    {
	mat4 cglm = GLM_MAT4_IDENTITY_INIT;
	m4 translation = m4_translate_identity(translate);
	glm_translate(cglm, translate.V);

	M4_Value(translation);
	M4_Value(m4_double_array(cglm));
	M4_Equal(translation, m4_double_array(cglm));
    }
}

void
m4_apply_transform_test()
{
    f32 angle = 0.0f;
    v3 xyz      = v3_new(1.0f, 1.0f, 1.0f);
    v3 size     = v3_new(1.5f, 1.0f, 1.0f);
    v3 rotation = v3_new(0.0f, 0.0f, angle);

    m4 transform = m4_transform(xyz, rotation, size);
    mat4 cglmTransform = GLM_MAT4_IDENTITY_INIT;
    cglm_get_transform(xyz.V, size.V, angle, cglmTransform);

    v3 transformedV3 = m4_mul_v3(transform, xyz);
    vec3 cglmTransformedVec3;
    glm_mat4_mulv3(cglmTransform, xyz.V, 1.0f, cglmTransformedVec3);

    v3 glmV3 = v3_array(cglmTransformedVec3);
    V3_Is_Equal(glmV3, transformedV3);

    V3_Value(glmV3);
    V3_Value(transformedV3);
}

void
m4_layout_test()
{
    mat4 cglmIdentity = GLM_MAT4_IDENTITY_INIT;
    m4 identity = m4_identity();

    M4_Value_Array(cglmIdentity);
    M4_Value(identity);

    {
	TEST_NEXT_SEPARATED();
	M4_Equal(m4_double_array(cglmIdentity), identity);
    }

    {
	TEST_NEXT_SEPARATED();
	V4_Value(identity.V[0]);
	V4_Value(identity.V[1]);
	V4_Value(identity.V[2]);
	V4_Value(identity.V[3]);
    }
}

void
m4_rotate_x_test()
{
    f32 radians = rad(90);
    m4 identity = m4_identity();
    m4 rotatedX = m4_rotate_x(identity, radians);

    mat4 identityCGLM = GLM_MAT4_IDENTITY_INIT;
    mat4 cglm;
    glm_rotate_x(identityCGLM, radians, cglm);

    m4 m4cglm = m4_double_array(cglm);
    M4_Equal(rotatedX, m4cglm);
    M4_Value(rotatedX);
    M4_Value(m4cglm);
}

void
m4_rotate_y_test()
{
    f32 radians = rad(90);
    m4 identity = m4_identity();
    m4 rotatedY = m4_rotate_y(identity, radians);

    mat4 identityCGLM = GLM_MAT4_IDENTITY_INIT;
    mat4 cglm;
    glm_rotate_y(identityCGLM, radians, cglm);

    m4 m4cglm = m4_double_array(cglm);
    M4_Equal(rotatedY, m4cglm);
    M4_Value(rotatedY);
    M4_Value(m4cglm);
}

void
m4_rotate_z_test()
{
    f32 radians = rad(90);
    m4 identity = m4_identity();
    m4 rotatedZ = m4_rotate_z(identity, radians);

    mat4 identityCGLM = GLM_MAT4_IDENTITY_INIT;
    mat4 cglm;
    glm_rotate_z(identityCGLM, radians, cglm);

    m4 m4cglm = m4_double_array(cglm);
    M4_Equal(rotatedZ, m4cglm);
    M4_Value(rotatedZ);
    M4_Value(m4cglm);
}

void
m4_make_rotation_matrix_test()
{
    v3 axis = v3_new(1.0, 0.7f, .4f);
    f32 radians = rad(90);

    {
	v3 axisNormalized = v3_normalize(axis);
	vec3 axisn;
	glm_vec3_normalize_to(axis.V, axisn);
	V3_Value(axisNormalized);
	V3_Value(v3_array(axisn));
	V3_Equal(axisNormalized, v3_array(axisn));
	TEST_NEXT_SEPARATED();
    }

    {
	v3 a = v3_new(1.0f, 0.7f, 0.3f);
	vec3 b;
	glm_vec3_scale(a.V, 2.0f, b);
	V3_Value(v3_mulv(a, 2.0f));
	V3_Value(v3_array(b));
	V3_Equal(v3_mulv(a, 2.0f), v3_array(b));
	TEST_NEXT_SEPARATED();
    }

    {
	m4 modifiedByVec = m4_identity();
	mat4 glmModifiedByVec = GLM_MAT4_IDENTITY_INIT;
	v3 a = v3_new(1.0f, 0.7f, 0.3f);
	modifiedByVec.V[0] = v3_mulv_as_v4(a, a.X);
	modifiedByVec.V[1] = v3_mulv_as_v4(a, a.Y);
	modifiedByVec.V[2] = v3_mulv_as_v4(a, a.Z);
	glm_vec3_scale(a.V, a.X, glmModifiedByVec[0]);
	glm_vec3_scale(a.V, a.Y, glmModifiedByVec[1]);
	glm_vec3_scale(a.V, a.Z, glmModifiedByVec[2]);

	M4_Equal(modifiedByVec, m4_double_array(glmModifiedByVec));
	M4_Value(modifiedByVec);
	M4_Value_Array(glmModifiedByVec);
	TEST_NEXT_SEPARATED();
    }

    m4 rotationMatrix = m4_rotation_matrix(axis, radians);

    mat4 glm = GLM_MAT4_IDENTITY_INIT;
    glm_rotate_make(glm, radians, axis.V);

    M4_Value(rotationMatrix);
    M4_Value(m4_double_array(glm));
    M4_Equal(rotationMatrix, m4_double_array(glm));
    TEST_NEXT_SEPARATED();
}

void
m4_rotate_test()
{
    v3 axis = v3_new(1.0, 0.7f, .4f);
    f32 radians = rad(90);
    m4 identity = m4_identity();

    m4 rotated = m4_rotate(identity, axis, radians);

    mat4 cglm = GLM_MAT4_IDENTITY_INIT;
    glm_rotate(cglm, radians, axis.V);

    m4 m4cglm = m4_double_array(cglm);
    M4_Equal(rotated, m4cglm);
    M4_Value(rotated);
    M4_Value(m4cglm);
}

void
m4_creation_test()
{
    {
	m4a data = {
	    1.0, 0.0f, 0.0f, 0.0f,
	    0.0, 1.0f, 0.0f, 0.0f,
	    0.0, 0.0f, 1.0f, 0.0f,
	    0.0, 0.0f, 0.0f, 1.0f
	};
	m4 m = m4_double_array(data);
	M4_Value(m);
	M4_Equal(m, m4_identity());
    }

    {
	m4a data = {
	    1.0, 0.1f, 0.2f, 0.3f,
	    0.7, 1.9f, 0.9f, 0.9f,
	    0.0, 0.0f, 1.0f, 0.0f,
	    0.7, 0.0f, 0.0f, 1.0f
	};

	M4_Value(m4_double_array(data));
    }
}

void
m4_mul_test()
{
    m4a dataa = {
	    1.9f, 0.7f, 0.7f, 0.3f,
	    0.8f, 2.9f, 1.f, 0.7f,
	    0.7f, 0.7f, .1f, 1.f,
	    0.7f, 1.7f, 0.f, .7f
    };
    m4a datab = {
	    1.9f, 0.7f, 0.7f, 0.3f,
	    0.8f, 2.9f, 1.f, 0.7f,
	    0.7f, 0.7f, .1f, 1.f,
	    0.7f, 1.7f, 0.f, .7f
    };
    m4 a = m4_double_array(dataa),
       b = m4_double_array(datab);

    m4 result = m4_mul(a, b);

    mat4 glm;
    glm_mat4_mul(a.M, b.M, glm);

    M4_Value(result);
    M4_Value(m4_double_array(glm));
    M4_Equal(result, m4_double_array(glm));
}

void
m4_test()
{
    TEST(perspective_creation_test());
    TEST(m4_look_at_test());

    TEST(m4_transform_test());
    TEST(m4_apply_transform_test());
    TEST(m4_translate_test());
    TEST(m4_layout_test());
    TEST(m4_rotate_x_test());
    TEST(m4_rotate_y_test());
    TEST(m4_rotate_z_test());
    TEST(m4_make_rotation_matrix_test());
    TEST(m4_rotate_test());
    TEST(m4_creation_test());
    TEST(m4_mul_test());
}
