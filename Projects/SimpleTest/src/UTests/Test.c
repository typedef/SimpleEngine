#include "Test.h"
#include <assert.h>
#include <stdlib.h>

static i8 g_IsInitialized = 0;
static i8 g_IsProfilingInitialized = 0;

static i32 g_NextNestingLevel = 0;
static i32 g_IsNextSeparated = 0;

static TestTable g_TestTable;
static ProfilingTest* g_ProfilingTest;

const char* g_CurrentFunction;
static TestInfo g_TestInfo;

void*
alloc_delegate(size_t n, i32 line, const char* file)
{
    return malloc(n);
}

void
free_delegate(void* data, i32 line, const char* file)
{
    free(data);
}

/////////////////////////////////////////
//          TEST INTERNING             //
/////////////////////////////////////////
static char** TestInternStrings = NULL;
static char*
test_intern_string(const char* input)
{
    i32 i, count = array_count(TestInternStrings);
    for (i = 0; i < count; ++i)
    {
	char* internStr = TestInternStrings[i];
	if (string_compare(internStr, input))
	{
	    return internStr;
	}
    }

    char* allocatedString = string(input);
    array_push_w_alloc(TestInternStrings, alloc_delegate, free_delegate, allocatedString);
    return allocatedString;
}
/////////////////////////////////////////
//         END TEST INTERNING          //
/////////////////////////////////////////

static FunctionResultData
function_result_data_create(i8 nestingLevel, i8 isSeparated, i8 returnCode, const char* message)
{
    FunctionResultData functionResultData = {
	.NestingLevel = nestingLevel,
	.IsSeparated = isSeparated,
	.ReturnCode = returnCode,
	.Message = message
    };

    return functionResultData;
}

force_inline void
reset_global_flags()
{
}

FunctionResult*
file_info_get_function_result(FileInfo* fileInfo, const char* functionName)
{
    vassert_not_null(fileInfo);

    i32 i, functionsCount = array_len(fileInfo->Functions);
    for (i = 0; i < functionsCount; ++i)
    {
	if (string_compare(fileInfo->Functions[i], functionName))
	{
	    return &fileInfo->Results[i];
	}
    }

    return NULL;
}

void
file_info_add_function_result(FileInfo* fileInfo, FunctionResult* functionResult, i32 code, const char* message)
{
    if (functionResult != NULL)
    {
	//1 error means test not successed
	if (!code)
	    functionResult->IsSuccess = code;
    }
    else
    {
	if (code)
	{
	    ++g_TestInfo.SuccessCount;
	}
	else
	{
	    ++g_TestInfo.ErrorsCount;
	}

	FunctionResult result = { .Codes = NULL, .Results = NULL, .IsSuccess = code };
	array_push_w_alloc(fileInfo->Functions, alloc_delegate, free_delegate, g_CurrentFunction);
	array_push_w_alloc(fileInfo->Results, alloc_delegate, free_delegate, result);

	functionResult = &fileInfo->Results[array_count(fileInfo->Results) - 1];

	g_TestInfo.Count++;
    }

    FunctionResultData functionResultData =
	function_result_data_create(g_NextNestingLevel, g_IsNextSeparated, code, message);

    array_push_w_alloc(functionResult->Results, alloc_delegate, free_delegate, functionResultData);

    // TODO(bies): remove this in the future
    if (code != -2)
    {
	array_push_w_alloc(functionResult->Codes, alloc_delegate, free_delegate, code);
    }

    //reset_global_flags
    g_NextNestingLevel = 0;
    g_IsNextSeparated = 0;
}

FileInfo*
test_table_get_file_info(TestTable* testTable, const char* filename)
{
    i32 i, filesCount = array_count(testTable->Filenames);

    for (i = 0; i < filesCount; i++)
    {
	if (string_compare(testTable->Filenames[i], filename))
	{
	    return &testTable->Infos[i];
	}
    }

    return NULL;
}

void
test_table_add_file_info(TestTable* testTable, const char* filename, i8 code, const char* message)
{
    FileInfo* fileInfo = test_table_get_file_info(testTable, filename);
    if (fileInfo == NULL)
    {
	array_push_w_alloc(testTable->Filenames, alloc_delegate, free_delegate, filename);

	FileInfo tempFileInfo = (FileInfo) {
	    .Functions = NULL,
	    .Results = NULL
	};
	array_push_w_alloc(testTable->Infos, alloc_delegate, free_delegate, tempFileInfo);
	i32 ind = array_count(testTable->Infos) - 1;
	fileInfo = &testTable->Infos[ind];
    }

    FunctionResult* functionResult = file_info_get_function_result(fileInfo, g_CurrentFunction);
    file_info_add_function_result(fileInfo, functionResult, code, message);
}

void
test(i8 code, const char* filePath, const char* message)
{
    vassert_not_null(g_CurrentFunction);
    vassert_not_null(message);
    vassert_not_null(filePath);

    if (!g_IsInitialized)
    {
	g_IsInitialized = 1;
	g_TestTable.Filenames = NULL;
	g_TestTable.Infos = NULL;
	g_TestInfo.Count = 0;
	g_TestInfo.SuccessCount = 0;
	g_TestInfo.ErrorsCount = 0;
    }

    char* filename = path_get_filename(filePath);
    vassert_not_null(filename);
    char* ifilename = test_intern_string(filename);
    vassert_not_null(ifilename);
    memory_free(filename);

    test_table_add_file_info(&g_TestTable, ifilename, code, message);
}

void
test_set_function(const char* function)
{
    g_CurrentFunction = function;
}

void
_test_next_as_child(i32 nestingLevel)
{
    g_NextNestingLevel = nestingLevel;
}

void
_test_next_separated()
{
    g_IsNextSeparated = 1;
}

i32
get_next_nesting_level()
{
    return g_NextNestingLevel;
}

FileInfo*
file_get_info(const char* filename)
{
    vassert_not_null(filename);

    FileInfo* value = test_table_get_file_info(&g_TestTable, filename);
    return value;
}

const char**
test_get_filenames()
{
    return g_TestTable.Filenames;
}

TestInfo
test_get_info()
{
    return g_TestInfo;
}
