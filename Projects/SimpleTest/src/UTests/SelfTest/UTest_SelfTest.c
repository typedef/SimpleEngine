#include "UTest_SelfTest.h"

#include "UTests/Test.h"

static void
failure_test()
{
    Condition(1 == 0);
}

static void
success_test()
{
    Condition(1 == 1);
}

void
utest_self_test()
{
    TEST(failure_test());
    TEST(success_test());
}
