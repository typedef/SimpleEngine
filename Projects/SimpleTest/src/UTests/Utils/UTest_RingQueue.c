#include "UTest_RingQueue.h"

#include "UTests/Test.h"
#include <Utils/SimpleStandardLibrary.h>

// Just save some queue tests, rewrite this
void
default_test()
{
    i32* rq = (i32*) ring_queue_new(10, sizeof(i32));
    for (i32 i = 1; i <= 17; ++i)
    {
	if (i == 10 || i == 11 || i == 12 || i == 14)
	{
	    ring_queue_pop(rq);
	}
	ring_queue_push(rq, i);
    }

    for (i32 i = 0; i < ring_queue_count(rq); ++i)
    {
	I32_Value(rq[i]);
	printf("rq[%d]: %d\n", i, rq[i]);
    }

    TEST_NEXT_SEPARATED();
    TEST_NEXT_AS_CHILD_N(2);

    for (i32 i = 0; i < ring_queue_count(rq); ++i)
    {
	i32 v = ring_queue_pop(rq);
	printf("pop(rq): %d\n", v);
	I32_Value(v);
    }


    i32 insertIndex = ring_queue_insert_index(rq);
    for (i32 i = 0; i < ring_queue_count(rq); ++i)
    {
	printf("rq[%d]: %d %s\n", i, rq[i], i == insertIndex ? "<-" : "  ");
    }

    printf("Count: %d, InsertIndex: %d\n", ring_queue_count(rq), ring_queue_insert_index(rq));
}

void
ring_queue_test()
{
    TEST(default_test());
}
