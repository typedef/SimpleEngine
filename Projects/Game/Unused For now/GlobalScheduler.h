#ifndef GLOBAL_SCHEDULER_H
#define GLOBAL_SCHEDULER_H

#include "../GameTypes.h"


/*
Day Routine Starts From [06:50 .. 23:50]
  Routine:
  ~Sleep (last sleeping part)
  ~Wake Up (this should be a switch from SleepRoutine)

*/

void scheduler_table_add_person(SchedulerTable* schedulerTable, PersonID personID, ModuleType moduleType);
void scheduler_table_remove_person(SchedulerTable* schedulerTable, PersonID personID);
i32 global_scheduler_get_possible_index_of_time(GlobalScheduler* globalScheduler, GameTime eventTime);
i32 global_scheduler_get_index_of_time(GlobalScheduler* globalScheduler, GameTime eventTime);
void global_scheduler_calculate_current_index(GlobalScheduler* globalScheduler, GameTime currentTime);
void global_scheduler_set_next_index(GlobalScheduler* globalScheduler);
void global_scheduler_update(VirtualWorld* world, GlobalScheduler* globalScheduler, GameTime currentTime);
void global_scheduler_register(GlobalScheduler* globalScheduler, GameTime eventTime, PersonID id, ModuleType moduleType);
void global_scheduler_default_worker(GlobalScheduler* globalScheduler, PersonID id);
void global_scheduler_destroy(GlobalScheduler* scheduler);


#endif
