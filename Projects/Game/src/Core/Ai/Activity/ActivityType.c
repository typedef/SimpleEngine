#include "ActivityType.h"

#include <Utils/Types.h>

const char*
activity_type_to_string(ActivityType activityType)
{
    switch (activityType)
    {
    case ActivityType_Idle:             return "Idle";

    case ActivityType_Sleep_Begin:      return "Sleep_Begin";
    case ActivityType_Sleep_FallASleep: return "Sleep_FallASleep";
    case ActivityType_Sleep:            return "Sleep";
    case ActivityType_Sleep_WakeUp:     return "Sleep_WakeUp";
    case ActivityType_Sleep_End:        return "Sleep_End";

    case ActivityType_Toilet:           return "Toilet";

    case ActivityType_Food:             return "Food";

    case ActivityType_Bath:             return "Bath";

    case ActivityType_Fun:              return "Fun";
    case ActivityType_Fun_Find:         return "Fun_Find";
    case ActivityType_Fun_Talk:         return "Fun_Talk";
    case ActivityType_Fun_Phone_Talk:   return "Fun_Phone_Talk";

    case ActivityType_Instincts:        return "Fun_Phone_Talk";
    case ActivityType_Ambitions:        return "Fun_Phone_Talk";

    case ActivityType_Work0:            return "Work0";
    case ActivityType_HomeSweetHome:    return "HomeSweetHome";

    case ActivityType_Phone_Begin:      return "Phone_Begin";
    case ActivityType_Phone_Talk:       return "Phone_Talk";
    case ActivityType_Phone_Talk_Fun:   return "Phone_Talk_Fun";
    case ActivityType_Phone_End:        return "Phone_End";

    }

    vassert(0 && "Wront activity type");
    return "Unknown";
}
