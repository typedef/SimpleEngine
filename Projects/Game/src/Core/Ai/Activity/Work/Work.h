void
work0_activity_update(AiData ad)
{
    const f32 WorkPointStep = 100.0f / (16.0f * 60 * 60);

    ad.Needs->Food    -= ad.WorldTimestep * WorkPointStep;
    ad.Needs->Toilet  -= ad.WorldTimestep * WorkPointStep;
    ad.Needs->Social  -= ad.WorldTimestep * WorkPointStep;
    ad.Needs->Energy  -= ad.WorldTimestep * WorkPointStep;
    ad.Needs->Hygiene -= ad.WorldTimestep * WorkPointStep;
    ad.Needs->Fun     -= ad.WorldTimestep * WorkPointStep;
}
