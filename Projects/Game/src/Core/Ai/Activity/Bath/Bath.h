void
bath_activity_update(AiData ad)
{
    const f32 HygienePointStep = 100.0f / (0.5f * 60 * 60);

    ad.Needs->Hygiene += ad.WorldTimestep * HygienePointStep;
    ad.Needs->Fun     += ad.WorldTimestep * HygienePointStep;
}
