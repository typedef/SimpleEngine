void
food_activity_update(AiData ad)
{
    const f32 FoodPointStep    = 100.0f / (0.3f * 60 * 60);

    ad.Needs->Food += ad.WorldTimestep * FoodPointStep;
}
