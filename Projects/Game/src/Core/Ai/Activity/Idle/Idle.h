void
idle_activity_update(AiData ad)
{
    const f32 IdlePointStep = 100.0f / (8.0f * 60 * 60);

    ad.Needs->Food      -= ad.WorldTimestep * IdlePointStep;
    ad.Needs->Toilet    -= ad.WorldTimestep * IdlePointStep;
    ad.Needs->Social    -= ad.WorldTimestep * IdlePointStep;
    ad.Needs->Energy    -= ad.WorldTimestep * IdlePointStep;
    ad.Needs->Hygiene   -= ad.WorldTimestep * IdlePointStep;
    ad.Needs->Fun       -= ad.WorldTimestep * IdlePointStep;
    ad.Needs->Instincts += ad.WorldTimestep * IdlePointStep;
    ad.Needs->Ambitions -= ad.WorldTimestep * IdlePointStep;
}
