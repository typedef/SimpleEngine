void
fun_activity_update(AiData ad)
{
    const f32 FunPointStep = 100.0f / (0.5f * 60 * 60);

    ad.Needs->Fun += ad.WorldTimestep * FunPointStep;
}

void
fun_talk_activity_update(AiData ad)
{
    const f32 FunPointStep = 100.0f / (0.25f * 60 * 60);

    ad.Needs->Fun += ad.WorldTimestep * FunPointStep;
}

void
fun_phone_talk_activity_update(AiData ad)
{
    const f32 FunPointStep = 100.0f / (0.5f * 60 * 60);

    ad.Needs->Fun += ad.WorldTimestep * FunPointStep;
}
