void
sleep_activity_update(AiData ad)
{
    const f32 SleepPointStep   = 100.0f / (8.0f * 60 * 60);

    ad.Needs->Energy  += ad.WorldTimestep * SleepPointStep;
    ad.Needs->Hygiene -= ad.WorldTimestep * (SleepPointStep * 4);
    ad.Needs->Fun      = 50.0f;
}
