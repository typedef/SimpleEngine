#include "Needs.h"

Needs
needs_create()
{
    Needs needs = {
	.Food      = NEEDS_MAX,
	.Toilet    = NEEDS_MAX,
	.Social    = NEEDS_MAX,
	.Energy    = NEEDS_MAX,
	.Hygiene   = NEEDS_MAX,
	.Fun       = NEEDS_MAX,
	.Instincts = NEEDS_MAX / 2,
	.Ambitions = NEEDS_MAX / 2
    };

    return needs;
}

void
needs_update(Needs* needs)
{
    needs->Food      = MINMAX(needs->Food     , 0, NEEDS_MAX);
    needs->Toilet    = MINMAX(needs->Toilet   , 0, NEEDS_MAX);
    needs->Social    = MINMAX(needs->Social   , 0, NEEDS_MAX);
    needs->Energy    = MINMAX(needs->Energy   , 0, NEEDS_MAX);
    needs->Hygiene   = MINMAX(needs->Hygiene  , 0, NEEDS_MAX);
    needs->Fun       = MINMAX(needs->Fun      , 0, NEEDS_MAX);
    needs->Instincts = MINMAX(needs->Instincts, 0, NEEDS_MAX);
    needs->Ambitions = MINMAX(needs->Ambitions, 0, NEEDS_MAX);
}
