#ifndef AI_CONTEXT_H
#define AI_CONTEXT_H

#include <Core/Ai/Module/Module.h>

/*
  DOCS(typedef): Во внешний мир от активностей мы кидаем только этот делегат
*/
typedef void (*ActivityDelegate)(AiData activityData);

void ai_context_init();

ActivityDelegate ai_context_get_activity(ActivityType type);
Module ai_context_get_module(ModuleType type);

#endif // AI_CONTEXT_H
