#ifndef NEEDS_H
#define NEEDS_H

#include <Utils/Types.h>

#define NEEDS_MAX 100.0f
//BUG(typedef): mb we dont need this
#define NeedsPercent(p) ((100.0f / NEEDS_MAX) * p)

typedef struct Needs
{
    /* Initialize range 0 .. NEEDS_MAX */
    f32 Food;
    f32 Toilet;
    f32 Social;
    f32 Energy;
    f32 Hygiene;
    f32 Fun;
    f32 Instincts;
    f32 Ambitions;
} Needs;

Needs needs_create();
void needs_update(Needs* needs);

#endif // NEEDS_H
