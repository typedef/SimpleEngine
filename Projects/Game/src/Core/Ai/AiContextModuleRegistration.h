#include "Module/Modules/FoodModule.h"
#include "Module/Modules/FunModule.h"
#include "Module/Modules/HygieneModule.h"
#include "Module/Modules/IdleModule.h"
#include "Module/Modules/PhoneModule.h"
#include "Module/Modules/SleepModule.h"
#include "Module/Modules/ToiletModule.h"
#include "Module/Modules/WorkModule.h"

void
ai_context_module_register(ModuleType type, Module (*moduleCreate)(ModuleOnInterruptDelegate interrupt))
{
    //BUG(typedef): we are here
    RegisteredModules[type] = moduleCreate(ContextData.OnInterruptDelegate);
}

void
ai_context_module_register_all()
{
    ai_context_module_register(ModuleType_Idle, idle_module_create);
    ai_context_module_register(ModuleType_Go, idle_module_create);
    ai_context_module_register(ModuleType_Sleep,   sleep_module_create);
    ai_context_module_register(ModuleType_Toilet,  toilet_module_create);
    ai_context_module_register(ModuleType_Food,    food_module_create);
    ai_context_module_register(ModuleType_Hygiene, hygiene_module_create);

    ai_context_module_register(ModuleType_Work, work_module_create);
    ai_context_module_register(ModuleType_Fun,   fun_module_create);

    ai_context_module_register(ModuleType_Instincts,   fun_module_create);
    ai_context_module_register(ModuleType_Ambitions,   fun_module_create);

    ai_context_module_register(ModuleType_Phone, phone_module_create);

    for (i32 i = ModuleType_Idle; i < ModuleType_Count; ++i)
    {
	Module module = RegisteredModules[i];
	//GERROR("%s\n", module_type_to_string(i));

	vassert(module.OnUpdate && module.OnInterrupt && "Module registration failed, not all modules registered!!!");
    }
}
