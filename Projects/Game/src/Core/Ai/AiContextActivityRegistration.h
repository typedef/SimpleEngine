#include "Activity/Bath/Bath.h"
#include "Activity/Food/Food.h"
#include "Activity/Fun/Fun.h"
#include "Activity/Go/Go.h"
#include "Activity/Idle/Idle.h"
#include "Activity/Sleep/Sleep.h"
#include "Activity/Toilet/Toilet.h"
#include "Activity/Work/Work.h"

void
ai_context_activity_register(ActivityType type, ActivityDelegate delegate)
{
    vassert_not_null(delegate);
    RegisteredActivities[type] = delegate;
}

void
ai_context_activity_register_all()
{
    // ActivityType_Idle
    ai_context_activity_register(ActivityType_Idle, idle_activity_update);

    ai_context_activity_register(ActivityType_Go_Walk, go_walk_activity_update);

    // ActivityType_Sleep
    ai_context_activity_register(ActivityType_Sleep_Begin, sleep_activity_update);
    ai_context_activity_register(ActivityType_Sleep_FallASleep, sleep_activity_update);
    ai_context_activity_register(ActivityType_Sleep, sleep_activity_update);
    ai_context_activity_register(ActivityType_Sleep_WakeUp, sleep_activity_update);
    ai_context_activity_register(ActivityType_Sleep_End, sleep_activity_update);

    // ActivityType_Toilet
    ai_context_activity_register(ActivityType_Toilet, toilet_activity_update);

    // ActivityType_Food
    ai_context_activity_register(ActivityType_Food, food_activity_update);

    // ActivityType_Bath0
    ai_context_activity_register(ActivityType_Bath, bath_activity_update);

    // ActivityType_Fun
    ai_context_activity_register(ActivityType_Fun_Find      , idle_activity_update);
    ai_context_activity_register(ActivityType_Fun           , fun_activity_update);
    ai_context_activity_register(ActivityType_Fun_Talk      , fun_talk_activity_update);
    ai_context_activity_register(ActivityType_Fun_Phone_Talk, fun_phone_talk_activity_update);

    ai_context_activity_register(ActivityType_Instincts, fun_activity_update);
    ai_context_activity_register(ActivityType_Ambitions, fun_activity_update);

    // ActivityType_Phone
    ai_context_activity_register(ActivityType_Phone_Begin   , idle_activity_update);
    ai_context_activity_register(ActivityType_Phone_Talk    , idle_activity_update);
    ai_context_activity_register(ActivityType_Phone_Talk_Fun, fun_phone_talk_activity_update);
    ai_context_activity_register(ActivityType_Phone_End     , idle_activity_update);

    // NOTE(bies): Placeholders for now
    ai_context_activity_register(ActivityType_Work0, work0_activity_update);
    ai_context_activity_register(ActivityType_HomeSweetHome, work0_activity_update);

    for (i32 i = ActivityType_Idle; i < ActivityType_Count; ++i)
    {
	ActivityDelegate activityDelegate = RegisteredActivities[i];
	if (activityDelegate == NULL)
	{
	    printf("Activity Not Registered: %s\n", activity_type_to_string((ActivityType)i));
	    vassert(0 && "Activity registration failed, not all activity type registered!!!");
	}
    }
}
