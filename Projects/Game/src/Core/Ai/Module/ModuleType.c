#include "ModuleType.h"

char*
module_type_to_string(ModuleType moduleType)
{
    switch (moduleType)
    {
    case ModuleType_Sleep:           return "Sleep Module";
    case ModuleType_Toilet:          return "Toilet Module";
    case ModuleType_Food:            return "Food Module";
    case ModuleType_Hygiene:         return "Hygiene Module";
    case ModuleType_Fun:             return "Fun Module";
    case ModuleType_Instincts:       return "Instincts Module";
    case ModuleType_Ambitions:       return "Ambitions Module";

    case ModuleType_Idle:            return "Idle Module";
    case ModuleType_Work:            return "Work Module";

    case ModuleType_Phone:           return "Phone Module";

    default: return "Unknown";
    }
}

ActivityType
module_type_get_default_activity(ModuleType type)
{
    switch (type)
    {

    case ModuleType_Idle: return ActivityType_Idle;
    case ModuleType_Sleep: return ActivityType_Sleep;
    case ModuleType_Toilet: return ActivityType_Toilet;
    case ModuleType_Food: return ActivityType_Food;
    case ModuleType_Hygiene: return ActivityType_Bath;

    case ModuleType_Work: return ActivityType_Work0;
    case ModuleType_Fun: return ActivityType_Fun_Find;
    case ModuleType_Phone: return ActivityType_Phone_Begin;

    }

    vassert(0 && "Wrong module type!");
    return ActivityType_Idle;
}

static i32
_sleep_module_check(ModuleType type)
{
    return 0;
}

static i32
_toilet_module_check(ModuleType type)
{
    return 0;
}

static i32
_food_module_check(ModuleType type)
{
    return 0;
}

static i32
_hygiene_module_check(ModuleType type)
{
    return 0;
}

static i32
_fun_module_check(ModuleType type)
{
    switch (type)
    {
    case ModuleType_Phone:
	return 1;
    }

    return 0;
}

static i32
_instincts_module_check(ModuleType type)
{
    return 0;
}

static i32
_ambitions_module_check(ModuleType type)
{
    switch (type)
    {
    case ModuleType_Work:
	return 1;
    }

    return 0;
}

i32
module_type_can_be_module_handler(ModuleType type, ModuleType urgent)
{
    if (type == urgent)
	return 1;

    switch (urgent)
    {

    case ModuleType_Sleep:
	return _sleep_module_check(type);

    case ModuleType_Toilet:
	return _toilet_module_check(type);

    case ModuleType_Food:
	return _food_module_check(type);

    case ModuleType_Hygiene:
	return _hygiene_module_check(type);

    case ModuleType_Fun:
	return _fun_module_check(type);

    case ModuleType_Instincts:
	return _instincts_module_check(type);

    case ModuleType_Ambitions:
	return _ambitions_module_check(type);

    }

    vassert_break();
}
