#ifndef MODULE_TYPE_H
#define MODULE_TYPE_H

#include <Utils/Types.h>
#include <Core/Ai/Activity/ActivityType.h>


typedef enum ModuleType
{
    ModuleType_Idle = 0,

    ModuleType_Go,

    ModuleType_Sleep,
    ModuleType_Toilet,
    ModuleType_Food,
    ModuleType_Hygiene,
    ModuleType_Fun,
    ModuleType_Instincts,
    ModuleType_Ambitions,

    ModuleType_Work,

    ModuleType_Phone,

    // This thing always equals to enum Count, and it is const value
    ModuleType_Count
} ModuleType;

#define ModuleType(x) ((ModuleType)x)

char* module_type_to_string(ModuleType moduleType);
ActivityType module_type_get_default_activity(ModuleType type);

/*
  NOTE(bies):
  Может ли данный тип обрабатывать необходимый модуль.
  Например, если персонажу было скучно (сработал UrgentModule _Fun)
  и в качестве развлечения было выбрано разговаривать по телефону (_Phone).
  То при следующий проверке мы увидим, что модуль _Fun обрабатывается в _Phone
  и мы можем не менять _Phone -> _Fun, что избавит нас от циклической смены модулей.
*/
i32 module_type_can_be_module_handler(ModuleType type, ModuleType urgent);

#define module_type_is_interruptible(type)			\
    ({ (type < ModuleType_NonInterruptible_Begin)		\
	    || (type > ModuleType_NonInterruptible_End); })


#endif
