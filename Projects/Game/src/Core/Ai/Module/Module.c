#include "Module.h"

#include <Core/Ai/AiContext.h>

Module
module_create(ModuleSettings set)
{
    Module module = (Module) {
	.Type            = set.Type,
	.CurrentActivity = set.CurrentActivity,
	.OnUpdate        = set.OnUpdate,
	.OnInterrupt     = set.OnInterrupt
    };

    return module;
}

i32
module_get_priority(ModuleType type)
{
    // DOCS(bies): smaller = better
    switch (type)
    {
    case ModuleType_Idle:            return 100;
    case ModuleType_Sleep:           return 30;
    case ModuleType_Toilet:          return 10;
    case ModuleType_Food:            return 20;
    case ModuleType_Hygiene:         return 40;
    case ModuleType_Work:            return 40;
    }

    return 100;
}

void
module_update(Module* module, AiData aiData)
{
    //NOTE(): remove this
    module->OnUpdate(module, aiData);

    ActivityDelegate activityDelegate = ai_context_get_activity(module->CurrentActivity);
    vassert(activityDelegate != NULL && "Can't find activity update function, you probably forget to register some activity!");

    activityDelegate(aiData);
}
