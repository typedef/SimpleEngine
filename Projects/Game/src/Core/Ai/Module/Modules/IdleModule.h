void
idle_module_update(Module* module, AiData aiData)
{
    //Empty
}

Module
idle_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Idle,
	.CurrentActivity = ActivityType_Idle,
	.OnUpdate = idle_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);
    return module;
}
