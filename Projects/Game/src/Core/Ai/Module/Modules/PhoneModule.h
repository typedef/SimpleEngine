void
phone_module_update(Module* module, AiData aiData)
{
    module->OnInterrupt(module, aiData);
}

Module
phone_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Phone,
	.CurrentActivity = ActivityType_Phone_Begin,
	.OnUpdate = phone_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);
    return module;
}
