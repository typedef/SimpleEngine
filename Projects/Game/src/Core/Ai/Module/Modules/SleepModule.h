void
sleep_module_update(Module* module, AiData aiData)
{
    switch (module->CurrentActivity)
    {
    case ActivityType_Sleep_Begin:
	module->CurrentActivity = ActivityType_Sleep_FallASleep;
	break;
    case ActivityType_Sleep_FallASleep:
	if (aiData.Needs->Energy >= NeedsPercent(12))
	    module->CurrentActivity = ActivityType_Sleep;
	break;
    case ActivityType_Sleep:
	if (aiData.Needs->Energy >= NeedsPercent(97))
	    module->CurrentActivity = ActivityType_Sleep_WakeUp;
	break;
    case ActivityType_Sleep_WakeUp:
	if (aiData.Needs->Energy >= NeedsPercent(95))
	    module->CurrentActivity = ActivityType_Sleep_End;
	break;
    case ActivityType_Sleep_End:
	module->OnInterrupt(module, aiData);
	break;
    }
}

Module
sleep_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Sleep,
	.CurrentActivity = ActivityType_Sleep_Begin,
	.OnUpdate = sleep_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);

    return module;
}
