void
hygiene_module_update(Module* module, AiData aiData)
{
    if (aiData.Needs->Hygiene >= NeedsPercent(95))
	module->OnInterrupt(module, aiData);
}

Module
hygiene_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Hygiene,
	.CurrentActivity = ActivityType_Bath,
	.OnUpdate = hygiene_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);
    return module;
}
