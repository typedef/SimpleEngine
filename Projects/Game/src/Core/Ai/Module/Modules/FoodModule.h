void
food_module_update(Module* module, AiData aiData)
{
    if (aiData.Needs->Food >= NeedsPercent(85))
	module->OnInterrupt(module, aiData);
}

Module
food_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Phone,
	.CurrentActivity = ActivityType_Phone_Begin,
	.OnUpdate = food_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);

    return module;
}
