void
work_module_update(Module* module, AiData aiData)
{
    // NOTE(typedef): for now doing nothing
    module->OnInterrupt(module, aiData);
}

Module
work_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Work,
	.CurrentActivity = ActivityType_Work0,
	.OnUpdate = work_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);

    return module;
}
