void
toilet_module_update(Module* module, AiData aiData)
{
    if (aiData.Needs->Toilet >= NeedsPercent(95))
	module->OnInterrupt(module, aiData);
}

Module
toilet_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Toilet,
	.CurrentActivity = ActivityType_Toilet,
	.OnUpdate = toilet_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);

    return module;
}
