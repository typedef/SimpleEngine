void
fun_module_update(Module* module, AiData aiData)
{
    module->OnInterrupt(module, aiData);
}

Module
fun_module_create(ModuleOnInterruptDelegate interrupt)
{
    ModuleSettings set = {
	.Type = ModuleType_Fun,
	.CurrentActivity = ActivityType_Fun_Find,
	.OnUpdate = fun_module_update,
	.OnInterrupt = interrupt
    };

    Module module = module_create(set);

    return module;
}
