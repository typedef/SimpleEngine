#ifndef MODULE_H
#define MODULE_H

#include <Utils/Types.h>

#include <Core/Ai/AiData.h>
#include <Core/Ai/Module/ModuleType.h>
#include <Core/Ai/Activity/ActivityType.h>

struct Module;

typedef void (*ModuleOnUpdateDelegate) (struct Module* module, AiData needs);
typedef void (*ModuleOnInterruptDelegate)(struct Module* module, AiData aiData);

typedef struct ModuleSettings
{
    ModuleType Type;
    ActivityType CurrentActivity;
    ModuleOnUpdateDelegate OnUpdate;
    ModuleOnInterruptDelegate OnInterrupt;
} ModuleSettings;

typedef struct Module
{
    ModuleType Type;
    ActivityType CurrentActivity;

    // NOTE(typedef): some delegates
    ModuleOnUpdateDelegate OnUpdate;
    ModuleOnInterruptDelegate OnInterrupt;
} Module;

Module module_create(ModuleSettings set);
i32 module_get_priority(ModuleType type);
void module_update(Module* module, AiData aiData);
void module_register_all();

#endif // MODULE_H
