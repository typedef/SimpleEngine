#include "GameWorld.h"

#include <Utils/SimpleStandardLibrary.h>
#include <Core/Ai/AiContext.h>
#include <Core/Components/AllComponents.h>

void
temp_game_time_printer(GameTime* time)
{
    printf("\r");
    if (time->Seconds < 10)
    {
	printf("%d:%d:%d%d %d.%d.%d",
	       time->Hours, time->Minutes, 0, time->Seconds,
	       time->Day, time->Month, time->Year);
    }
    else
    {
	printf("%d:%d:%d %d.%d.%d",
	       time->Hours, time->Minutes, time->Seconds,
	       time->Day, time->Month, time->Year);
    }

    fflush(stdout);
}

void
_game_world_ecs_init(GameWorldSettings set)
{
    ecs_world_register_component(set.pEcsWorld, MindComponent);
}

void
_game_population_init(GameWorldSettings set)
{
    EntityId firstPersonExisted = ecs_world_entity(set.pEcsWorld, "First Person");

    MindComponentSettings mindSet = {
	.CurrentEntity = firstPersonExisted,
    };
    MindComponent mind = mind_create(mindSet);

    ecs_world_entity_add_component(set.pEcsWorld, firstPersonExisted, MindComponent, mind);
}

GameWorld
game_world_create(GameWorldSettings set)
{
    ai_context_init();
    _game_world_ecs_init(set);
    _game_population_init(set);

    GameWorld gameWorld = {
	.TimeMultiplier = set.TimeMultiplier,
	.CurrentDate = set.StartDate,
	.pEcsWorld = set.pEcsWorld
    };

    GameTimeSecondsChangedDelegate delegate;
    if (!game_time_subsribe_seconds_delegate(temp_game_time_printer))
    {
	GERROR("Can't subscribe seconds delegate!\n");
	vassert_break();
    }

    return gameWorld;
}

void
game_world_update(GameWorld* gameWorld, f32 timestep)
{
    // NOTE(typedef): updating GameWorld time
    gameWorld->WorldTimestep = gameWorld->TimeMultiplier * timestep;
    const f32 toMs = 1000;
    game_time_add_ms(&gameWorld->CurrentDate, gameWorld->WorldTimestep * toMs);

    // EcsQuery query = ecs_get_query(TransformComponent, StaticModelComponent);

    MindComponent* mind = GetComponentRecordData(MindComponent);
    i32 i, count = array_count(mind);
    for (i = 0; i < count; ++i)
    {
	MindComponent* pMind = &mind[i];
	mind_update(pMind);
    }


}


void
game_world_destroy(GameWorld gameWorld)
{
    printf("\n");
}
