#include "Game.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.Name = "GameDemo",
    };
    application_create(appSettings);

    Layer gameLayer;
    gameLayer.Name = "Game Demo Layer";
    gameLayer.OnAttach = game_on_attach;
    gameLayer.OnUpdate = game_on_update;
    gameLayer.OnUIRender = game_on_ui_render;
    gameLayer.OnEvent = game_on_event;
    gameLayer.OnDestoy = game_on_destroy;

    application_push_layer(gameLayer);
}
