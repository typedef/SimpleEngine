#include "Game.h"

#include <Utils/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <EntitySystem/SimpleEcs.h>

// Game Demo Related
#include <Core/GameWorld/GameWorld.h>
#include <Core/Ai/Module/ModuleType.h>
#include <Core/Ai/Activity/ActivityType.h>
#include <Core/Components/Ai/MindComponent.h>

static GameWorld sGameWorld;

void
game_on_attach()
{
#if LEGACY == 1
    VirtualWorldConfiguration configuration = (VirtualWorldConfiguration) {
	.FamiliesCount = 1,
	    .TimeMultiplier = 8,
	    .StartDate = game_time_new_full(8, 27, 30, 31, 12, 1980),
	    .CurrentPopulationConfig = population_config()
    };
    VirtWorld = virtual_world_create(configuration);
    virtual_world_update(VirtWorld, timestep);
    scene_update();
#endif // LEGACY

    scene_create();
    EcsWorld* pEcsWorld = scene_get_world();

    CameraComponentCreateInfo createInfo = {
	.IsMainCamera = 1,
	.Base = 0,
	.Settings = camera_component_settings_new(
	    CameraType_PerspectiveSpectator),
	.Perspective = createInfo.Perspective,
	.Orthographic = createInfo.Orthographic,
	.Spectator = createInfo.Spectator
    };
    CameraComponent cameraComponent =
	camera_component_new_ext(createInfo);
    EntityId camera = ecs_entity("MainCamera");
    ecs_world_entity_add_component(pEcsWorld, camera, CameraComponent, cameraComponent);

    scene_update_pointer_main_camera();

    GameWorldSettings set = {
	.TimeMultiplier = 8,
	.StartDate = game_time_new_full(8, 27, 30, 31, 03, 1985),
	.pEcsWorld = pEcsWorld
    };
    sGameWorld = game_world_create(set);
}

void
game_on_update(f32 timestep)
{
    scene_update();
    game_world_update(&sGameWorld, timestep);
}

void
game_on_ui_render()
{

    static bool mindVisible = true;
    if (igBegin("Mind", &mindVisible, ImGuiWindowFlags_None))
    {
	MindComponent* mind = GetComponentRecordData(MindComponent);
	igText("CurrentEntity: %d", mind->CurrentEntity);

	igText("Needs:");
	igText("Food: %0.3f", mind->Needs.Food);
	igText("Toilet: %0.3f", mind->Needs.Toilet);
	igText("Social: %0.3f", mind->Needs.Social);
	igText("Energy: %0.3f", mind->Needs.Energy);
	igText("Hygiene: %0.3f", mind->Needs.Hygiene);
	igText("Fun: %0.3f", mind->Needs.Fun);
	igText("Instincts: %0.3f", mind->Needs.Instincts);
	igText("Ambitions: %0.3f", mind->Needs.Ambitions);

	igText("[Current Module]");
	igText("Type: %s", module_type_to_string(mind->CurrentModule.Type));
	igText("CurrentActivity: %s", module_type_to_string(mind->CurrentModule.CurrentActivity));

    }
    igEnd();

    static bool timeVisible = true;
    if (igBegin("Time", &timeVisible, ImGuiWindowFlags_None))
    {
	char buf[65];
	game_time_to_string(buf, sGameWorld.CurrentDate);
	igText("TimeMultiplier: %d", sGameWorld.TimeMultiplier);
	igText("Time: %s", buf);
	igText("WorldTimestep: %0.3f ms", sGameWorld.WorldTimestep * 1000);
    }
    igEnd();


}

void
game_on_event(Event* event)
{
    switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Category == EventCategory_Window)
	{
	    switch (event->Type)
	    {

	    case WindowShouldBeClosed:
		application_close();
		event->IsHandled = 1;
		break;
	    }
	}
    }

    }

}

void
game_on_destroy()
{
    game_world_destroy(sGameWorld);
}



/*

  System Oriented Design

update()
{
    ai_system_update():
    for (i32 i = 0; i < minds.Count; ++i)
    {
	minds[i].Update();
    }

    optimization ...
    let playerFrustum = GetPlayerFrustum();
    worldBuildings = world_building_update(playerFrustum);
    assetsTable = assets_table_update(playerFrustum);
    asset_system_set_visibility(worldBuildings, assetsTable, playerFrustrum);

    // Optimized worldBuilding and assets, get only what is visible for player.
    physics_system_update() depends on RigidBodyComponent, SoftBodyComponent;
    animation_system_update() depends on AnimationComponent;
    graphics_system_update() depends on ModelComponent;


}


some_player_code()
{
    let model: Model = GetPlayerComponent<ModelComponent>();
    foreach (let mesh in model.Meshes)
    {
	if (mesh.IsLeg())
	{
	    mesh.SetDamagedTexture();
	}
    }
}















 */
