#ifndef ECS_TESTING_H
#define ECS_TESTING_H

typedef struct AComponent
{
    i32 A;
    i32 B;
} AComponent;
typedef struct BComponent
{
    i32 Data1;
    i32 Data2;
    i64 Data3;
    f64 Float;
} BComponent;

static void
get_components()
{
    EcsQuery query = ecs_get_query(AComponent, TransformComponent, BComponent);
    for (i32 i = 0; i < 1 /* array_count(query.Entities) */; ++i)
    {
	EntityRecord* record = query.Entities[i];
	AComponent* acomponent = (AComponent*) hash_get(record->Data, query.Ids[0]);
	TransformComponent* tcomponent = hash_get(record->Data, query.Ids[1]);
	BComponent* bcomponent = hash_get(record->Data, query.Ids[2]);

	GINFO("Acom: %d %d\n", acomponent->A, acomponent->B);
	GINFO("Bcom: %d %d %d %f\n", bcomponent->Data1, bcomponent->Data2, bcomponent->Data3, bcomponent->Float);
	m4_print(tcomponent->Matrix, "Transform");
    }
}

static void
for_performance_testing()
{
    AComponent aComponent = {
	.A = 123,
	.B = 28
    };
    TransformComponent tc = transform_component_new(v3_new(4.5, 1.9, 3), v3_new(0,0,0), v3_new(1,1,1));
    GINFO("Transform Size: %ld\n", sizeof(tc));

    // Registering components part
    if (!ecs_world_register_component(world, TransformComponent))
    {
	GERROR("Cant register TransformComponent!\n");
    }
    if (!ecs_world_register_component(world, AComponent))
    {
	GERROR("Cant register AComponent!\n");
    }
    if (!ecs_world_register_component(world, BComponent))
    {
	GERROR("Cant register BComponent!\n");
    }

    // New Entity
    EntityId newEntity = ecs_entity();
    profiler_calc(
	ecs_entity_add_component(newEntity, AComponent, aComponent);
	ecs_entity_add_component(newEntity, TransformComponent, tc);
	);

    i32 neededCount = 0;
    for (i32 i = 0; i < 50000; ++i)
    {
	EntityId entt = ecs_entity();
	BComponent bComp = { 7, 8, 19, 3.14 };

	if (i % 3 == 0)
	    ecs_entity_add_component(entt, AComponent, aComponent);
	if (i % 7 == 0)
	{
	    ecs_entity_add_component(entt, TransformComponent, tc);
	}
	if (i % 17 == 0)
	{
	    ecs_entity_add_component(entt, BComponent, bComp);
	}
    }
    GWARNING("Needed count: %d\n", neededCount);

#if 0
    {
	ComponentId componentId = ecs_component_get_id_by_name("AComponent");
	ComponentRecord tcr =
	    world->CStorage.Records[GetComponentAsArrayIndex(componentId)];
	AComponent* iter = (AComponent*) tcr.Data;
	i32 i, count = array_count(iter);
	for (i = 0; i < count; ++i)
	{
	    AComponent item = iter[i];
	    GINFO("Acom: %d %d %lld\n", item.A, item.B, &iter[i]);
	}
    }
#endif

    profiler_calc(
	EcsQuery query1 = ecs_get_query_w_cache(AComponent, TransformComponent, BComponent)
	);

    profiler_calc(
	EcsQuery query2 = ecs_get_query(AComponent, TransformComponent, BComponent)
	);

    void funcForCheck()
    {
	for (i32 i = 0; i < 500; ++i)
	{
	    EntityId entt = ecs_entity();
	    BComponent bComp = { 7, 8, 19, 3.14 };

	    ecs_entity_add_component(entt, AComponent, aComponent);
	    ecs_entity_add_component(entt, TransformComponent, tc);
	    ecs_entity_add_component(entt, BComponent, bComp);
	}
    }

    profiler_calc(funcForCheck());

}


#endif // ECS_TESTING_H
