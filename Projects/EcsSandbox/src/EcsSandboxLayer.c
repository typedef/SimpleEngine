#include "EcsSandbox.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.IsInitImGui = 1,
	.Name = "EcsSandbox",
    };
    application_create(appSettings);

    Layer ecsSandboxLayer = {0};
    ecsSandboxLayer.Name = "EcsSandbox Layer";
    ecsSandboxLayer.OnAttach = ecs_sandbox_on_attach;
    ecsSandboxLayer.OnUpdate = ecs_sandbox_on_update;
    ecsSandboxLayer.OnUIRender = ecs_sandbox_on_ui_render;
    ecsSandboxLayer.OnEvent = ecs_sandbox_on_event;

    application_push_layer(ecsSandboxLayer);
}
