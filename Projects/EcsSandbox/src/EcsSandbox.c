#include "EcsSandbox.h"

#include <Graphics/KeyCodes.h>
#include <Graphics/Renderer.h>
#include <Graphics/ModelRenderer.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Editor/Components/StaticModelComponentPanel.h>
#include <InputSystem/KeyBinding.h>
#include <Localization/SimpleLocalization.h>
#include <Localization/SimpleLocalizationCodeGen.h>

#define TEST_LOCAL
#if defined(TEST_LOCAL)
#include <Localization/Enums/LocalGroup.h>
#include <Localization/Enums/EngineUi.h>
#include <Localization/Enums/GamePersonNames.h>
#include <Localization/Enums/GameSecondNames.h>
#endif

static f32 Timestep = 0.0f;

// Place somewhere else
static Texture2D DefaultTexture;
static CubeMap Cubemap;

static i32 IsFlag = 0;
void
signle_key_binding_callback()
{
    GWARNING("Single key binding works!\n");
    IsFlag = !IsFlag;
}


void register_key_bindings();

void
ecs_sandbox_on_attach()
{
    window_set_vsync(2);
    asset_manager_init();
    register_key_bindings();
    input_init(application_get()->Window->GlfwWindow);

    //TODO(typedef): remove this with scene loading
    /* ViewportSettings viewportSettings = { */
    /*	.AspectRatio = window->AspectRatio */
    /* }; */
    /* scene_create(viewportSettings); */
    /* CameraComponent* MainCamera = scene_get_main_camera(); */
    /* scene_init_renderers(MainCamera); */


    SimpleLocalization local =
	simple_localization_new(
	    asset_localization(
		/* "ru_RU.slf" */
		"file.slfb"
		));

    SimpleLocalizationData localData =
	simple_localization_load_data(local, asset_localization("ru_RU.slf"));

#if !defined(TEST_LOCAL)
    i32 l, localizedCount = array_count(localData.LocalizedValues);
    for (l = 0; l < localizedCount; ++l)
    {
	GINFO("[%d] %s\n", l, local.GroupNames[l]);
	WideString* wstr = localData.LocalizedValues[l];
	array_foreach(wstr,
		      GWARNING("[%d] ", i/* , sl.Keys[l][i] */);
		      wide_string_print_line(item);
	    );
    }
#endif

#if !defined(TEST_LOCAL)
    simple_localization_code_gen_generate(local, localData);
#endif

#if defined(TEST_LOCAL)
    simple_localization_set(local, localData);
    WideString wstr = simple_localization_get(LocalGroup_EngineUi, EngineUi_StopButton);
    GINFO("INFO!\n");
    wide_string_print_line(wstr);
#endif
}

void
ecs_sandbox_on_update(f32 timestep)
{
    Timestep = timestep;

    key_binding_process();

    scene_update();

    if (IsFlag)
	renderer_clear(v4_new(0.1237,0.55412,0.1241,1));

}

void
ecs_sandbox_on_ui_render()
{
    static bool isKeyBindingPanelOpened = true;
    if (igBegin("KeyBinding", &isKeyBindingPanelOpened, ImGuiWindowFlags_None))
    {
	BindingElement* els = key_binding_get();
	for (i32 i = 0; i < array_count(els); ++i)
	{
	    /* i32 KeyCode; */
	    /* i32 Action; */
	    /* i32 Modificator; */

	    BindingElement el = els[i];
	    char buf[64];
	    binding_element_format(buf, el);
	    igText("%s", buf);
	}
    }
    igEnd();

    EcsWorld* world = scene_get_world();
    if (world != NULL)
    {

	EntityRecord* selectedEntity = scene_get_selected_entity();
	if (selectedEntity != NULL)
	{
	    if (HasComponent(selectedEntity->Id, StaticModelComponent))
	    {
		StaticModelComponent* smc = GetComponent(selectedEntity->Id, StaticModelComponent);
		static_model_component_panel(smc);
	    }

	    if (HasComponent(selectedEntity->Id, TransformComponent))
	    {
		TransformComponent* tc = GetComponent(selectedEntity->Id, TransformComponent);
		transform_component_panel(tc);
	    }

	    if (HasComponent(selectedEntity->Id, CameraComponent))
	    {
		CameraComponent* cc = GetComponent(selectedEntity->Id, CameraComponent);
		static bool opened = true;
		if (igBegin("CameraComponentPanel", &opened, ImGuiWindowFlags_None))
		{
		    if (igButton("Perspective", ImVec2(0, 0)))
		    {
			camera_component_set_perspective(cc);
		    }
		    if (igButton("Orthographic", ImVec2(0, 0)))
		    {
			//Spectator
			SimpleWindow* window = application_get()->Window;
			GINFO("%f %f\n", (f32) window->Width, (f32) window->Height);
			OrthographicSettings ortho = {
			    .Near = 0.1f,
			    .Far = 1000.0f,
			    .Left = 0.0f,
			    .Right = (f32) window->Width,
			    .Bot = 0.0f,
			    .Top = (f32) window->Height,
			};
			//cc->Settings.Front = v3_new(0, 0, -1);
			//cc->Settings.Right = v3_new(1, 0, 0);
			//cc->Settings.Up = v3_new(0, 1, 0);
			cc->Orthographic = ortho;
			//cc->Settings.Position = v3_new(0.0f, 0.0f, -5.0f);

			camera_component_set_orthograhic(cc);
			camera_component_update(cc);
		    }

		    if (cc->Settings.Type == CameraType_PerspectiveSpectator)
		    {

			igSeparator();
			igText("Perspective: ");
			igText("Near: %f", cc->Perspective.Near);
			igText("Far: %f", cc->Perspective.Far);
			igText("AspectRatio: %d", cc->Perspective.AspectRatio);
			igText("Fov: %d", cc->Perspective.Fov);
			igSeparator();

		    }
		    else if (cc->Settings.Type == CameraType_OrthographicSpectator)
		    {

			igSeparator();
			igText("Orthographic: ");
			igText("Near: %f", cc->Orthographic.Near);
			igText("Far: %f", cc->Orthographic.Far);
			igText("Left: %d", cc->Orthographic.Left);
			igText("Right: %d", cc->Orthographic.Right);
			igText("Bot: %d", cc->Orthographic.Bot);
			igText("Top: %d", cc->Orthographic.Top);
			igSeparator();

		    }
		    else
		    {
			vguard(0);
		    }


		    igSeparator();
		    igText("Settings:");
		    igText("Type: ", camera_type_to_string(cc->Settings.Type));
		    igText("Sensitive(h, v): %f %f",
			   cc->Settings.SensitivityHorizontal,
			   cc->Settings.SensitivityVertical);
		    igText("Yaw: %0.2f", cc->Settings.Yaw);
		    igText("Pitch: %0.2f", cc->Settings.Pitch);
		    igText("PrevX: %0.2f", cc->Settings.PrevX);
		    igText("PrevY: %0.2f", cc->Settings.PrevY);
		    igTextV3("Position", cc->Settings.Position);
		    igTextV3("Front", cc->Settings.Front);
		    igTextV3("Up", cc->Settings.Up);
		    igTextV3("Right", cc->Settings.Right);
		    igSeparator();

		    /* igText("Position: %0.2f %0.2f %0.2f", cc->Settings.Position.X, cc->Settings.Position.Y, cc->Settings.Position.Z); */
		    /* igText("Front: %0.2f %0.2f %0.2f", cc->Settings.Position.X, cc->Settings.Position.Y, cc->Settings.Position.Z); */
		    /* igText("Up: %0.2f %0.2f %0.2f", cc->Settings.Up.X, cc->Settings.Up.Y, cc->Settings.Up.Z); */
		    /* igText("Right: %0.2f %0.2f %0.2f", cc->Settings.Right.X, cc->Settings.Right.Y, cc->Settings.Right.Z); */

		    /* CameraType Type; */
		    /* f32 SensitivityHorizontal; */
		    /* f32 SensitivityVertical; */
		    /* f32 Yaw;   // x */
		    /* f32 Pitch; // y */
		    /* f32 PrevX; */
		    /* f32 PrevY; */
		    /* v3  Position; */
		    /* v3  Front; */
		    /* v3  Up; */
		    /* v3  Right; */

		    static bool isMainCamera = false;
		    if (igCheckbox("Is Main Camera: ", &isMainCamera))
		    {
			cc->IsMainCamera = isMainCamera;
			if (isMainCamera)
			{
			    scene_update_pointer_main_camera();
			}
		    }

		    igDragFloat3("Position", cc->Settings.Position.V,
				 1.0f, -5.0, 5.0f, "%0.2f", ImGuiSliderFlags_None);
		}

		igEnd();
	    }


	    if (HasComponent(selectedEntity->Id, SkyboxComponent))
	    {
		SkyboxComponent* sc = GetComponent(selectedEntity->Id, SkyboxComponent);
		static bool opened = true;
		if (igBegin("SkyboxComponentPanel", &opened, ImGuiWindowFlags_None))
		{
		    igText("CubemapId (Read Only for now): %d",
			   sc->Cubemap);
		}

		igEnd();
	    }

	    {
		static bool opened = true;
		if (igBegin("Entity Base Panel", &opened, ImGuiWindowFlags_None))
		{

#define nameBufLength 128
		    static char nameBuf[nameBufLength];
		    static i32 initNameBuf = 1;
		    if (initNameBuf)
		    {
			memset(nameBuf, 0, nameBufLength);
			memcpy(nameBuf, selectedEntity->Name, string_length(selectedEntity->Name));
		    }
		    if (igInputText("Entity Name", nameBuf, nameBufLength, ImGuiInputTextFlags_None, NULL, NULL))
		    {
			char* allocatedName = string(nameBuf);
			memory_free(selectedEntity->Name);
			selectedEntity->Name = allocatedName;
		    }
		    if (igButton("Change", ImVec2(0,0)))
		    {
		    }

		}

		igEnd();
	    }
	}

	static bool entitiesPanelVisible = true;
	if (igBegin("Entities", &entitiesPanelVisible, ImGuiWindowFlags_None))
	{

	    if (igButton("+", ImVec2(0, 0)))
	    {
		ecs_world_entity(world, "Default");
		scene_set_selected_entity(NULL);
	    }

	    ImGuiID popupId = 1337;
	    static EntityRecord* actionEntity = NULL;
	    EntityId id, recordsCount = array_count(world->EStorage.Records);
	    for (id = 0; id < recordsCount; ++id)
	    {
		EntityRecord* eRec = &world->EStorage.Records[id];
		if (eRec->Id == -1)
		    continue;

		if (igButton(eRec->Name, ImVec2(0,0)))
		{
		    scene_set_selected_entity(eRec);
		}

		if (igIsItemClicked(ImGuiMouseButton_Right))
		{
		    igOpenPopup_ID(popupId, ImGuiPopupFlags_None);
		    actionEntity = eRec;
		}
	    }

	    if (igBeginPopupEx(popupId, ImGuiWindowFlags_None))
	    {

		if (igButton("TransformComponent", ImVec2(0,0)))
		{
		    TransformComponent tc = transform_component_new(
			v3_new(0,0,0), v3_new(0,0,0), v3_new(1,1,1));
		    ecs_world_entity_add_component(world, actionEntity->Id, TransformComponent, tc);
		}

		if (igButton("StaticModelComponent_Cube", ImVec2(0,0)))
		{
		    if (!HasComponent(actionEntity->Id, TransformComponent))
		    {
			TransformComponent tc =
			    transform_component_new(
				v3_new(0,0,0), v3_new(0,0,0), v3_new(1,1,1));
			ecs_world_entity_add_component(
			    world, actionEntity->Id, TransformComponent, tc);
		    }

		    StaticModelComponent smc =
			static_model_component_create_cube();
		    ecs_world_entity_add_component(world, actionEntity->Id, StaticModelComponent, smc);

		}

		if (igButton("CameraComponent", ImVec2(0,0)))
		{
		    CameraComponent cc =
			camera_component_new(CameraType_PerspectiveSpectator);
		    ecs_world_entity_add_component(world, actionEntity->Id, CameraComponent, cc);
		}

		if (igButton("SkyboxComponent", ImVec2(0,0)))
		{
		    SkyboxComponent sc =
			skybox_component_new(asset_cubemap("skybox"));
		    ecs_world_entity_add_component(world, actionEntity->Id, SkyboxComponent, sc);
		}


		igEndPopup();
	    }

	}
	igEnd();

    }

}

static i32
not_key_ignore(KeyType type)
{
    if (type < 340 || type > 347)
    {
	return 1;
    }

    return 0;
}

void
ecs_sandbox_on_event(Event* event)
{
    switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;

	if (not_key_ignore(keyEvent->KeyCode))
	{
	    BindingElement el = {
		.KeyCode = keyEvent->KeyCode,
		.Action = keyEvent->Action,
		.Modificator = keyEvent->Modificator
	    };
	    key_binding_add(el);
	}

	switch (keyEvent->KeyCode)
	{

	case KEY_ESCAPE:
	{
	    application_close();
	    event->IsHandled = 1;
	}
	break;

	case KeyType_Space:
	{
	    if (viewport_panel_is_movement_enabled())
	    {
		viewport_panel_disable_movement();
	    }
	    else
	    {
		viewport_panel_enable_movement();
	    }
	}
	break;

	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
register_key_bindings()
{

    {
	BindingElement bind = {
	    .KeyCode = KeyType_S,
	    .Action = ActionType_Press,
	    .Modificator = ModType_Control
	};
	BindingElement* binds = NULL;
	array_push(binds, bind);

	KeyBindingCreationRecord saveRec = {
	    .Major = {
		.KeyCode = KeyType_X,
		.Action = ActionType_Press,
		.Modificator = ModType_Control
	    },
	    .Minor = minor_binding_create(wide_string(L"Сохранить сцену"),
					  scene_save,
					  binds)
	};

	key_binding_register(saveRec);
    }

    {
	BindingElement bind = {
	    .KeyCode = KeyType_O,
	    .Action = ActionType_Press,
	    .Modificator = ModType_Control
	};
	BindingElement* binds = NULL;
	array_push(binds, bind);

	KeyBindingCreationRecord openRec = {
	    .Major = {
		.KeyCode = KeyType_X,
		.Action = ActionType_Press,
		.Modificator = ModType_Control
	    },
	    .Minor = minor_binding_create(wide_string(L"Открыть сцену"),
					  scene_load,
					  binds)
	};

	key_binding_register(openRec);
    }

    {
	KeyBindingCreationRecord openRec = {
	    .Major = {
		.KeyCode = KeyType_O,
		.Action = ActionType_Press,
		.Modificator = ModType_Control
	    },
	    .Minor = minor_binding_create(wide_string(L"Тестирование одиночного keybinding'а"),
					  signle_key_binding_callback,
					  NULL)
	};

	key_binding_register(openRec);
    }

}
