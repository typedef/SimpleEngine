#include "UiSandbox.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.IsInitImGui = 0,
	.Name = "UiSandbox",
    };
    application_create(appSettings);

    Layer uiSandboxLayer = {0};
    uiSandboxLayer.Name = "UiSandbox Layer";
    uiSandboxLayer.OnAttach = ui_sandbox_on_attach;
    uiSandboxLayer.OnUpdate = ui_sandbox_on_update;
    uiSandboxLayer.OnUIRender = ui_sandbox_on_ui_render;
    uiSandboxLayer.OnEvent = ui_sandbox_on_event;
    uiSandboxLayer.OnDestoy = ui_sandbox_on_destroy;

    application_push_layer(uiSandboxLayer);
}
