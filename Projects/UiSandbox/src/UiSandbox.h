#ifndef UI_SANDBOX_H
#define UI_SANDBOX_H

#include <Engine.h>

void ui_sandbox_on_attach();
void ui_sandbox_on_update(f32 timestep);
void ui_sandbox_on_ui_render();
void ui_sandbox_on_event(Event* event);
void ui_sandbox_on_destroy();

#endif // UI_SANDBOX_H
