#include "UiSandbox.h"

#include <string.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Graphics/KeyCodes.h>
#include <Graphics/RuntimeCamera.h>

static SimpleWindow* CurrentWindow = NULL;
static CameraComponent gUiCamera;
static RuntimeCamera OrthoCamera;
WideString WS;
WideString WS2;
WideString WS3;
WideString ButtonPlus;
WideString ButtonMinus;
WideString ButtonText;
WideString SliderText;
WideString CheckBoxLabel;

#include "SuiBackend.h"
//#include <UI/SuiUtils.h>

#define InputBufferLength 128
static i32 InputBuffer[InputBufferLength];

void
get_display_size(i32* width, i32* height)
{
    window_get_size(CurrentWindow, width, height);
}

void
ui_sandbox_on_attach()
{
    CurrentWindow = application_get()->Window;

    window_set_vsync(3);
    input_init(application_get()->Window->GlfwWindow);

    WS = wide_string(L"Русский язык");
    WS2 = wide_string(L"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯабвгдеёжзийклмнопрстуфхцчшщъыьэюя");
    WS3 = wide_string(L"Б012390А");
    ButtonPlus = wide_string(L"Добавить");
    ButtonMinus = wide_string(L"Уменьшить");
    ButtonText = wide_string(L"Сведения о программе");
    SliderText = wide_string(L"Слайдер");
    CheckBoxLabel = wide_string(L"Текст для чекбокса");
    memcpy(InputBuffer, L"Привет, мир! Hello, world! Текст для ввода", 43 * sizeof(wchar));

    f32 aspectRatio = (f32) CurrentWindow->Width / CurrentWindow->Height;

    gUiCamera = camera_component_new(CameraType_PerspectiveSpectator);
    // this should be TransformComponent
    gUiCamera.Settings.Position = v3_new(0.0f, 0.0f, -20.0f);
    gUiCamera.Spectator = (SpectatorSettings) {
	.Speed = 5.0f,
	.Zoom = 0
    };
    gUiCamera.Perspective = (PerspectiveSettings) {
	.Near = 0.01f,
	.Far = 4000.0f,
	.AspectRatio = aspectRatio,
	.Fov = rad(90),
    };
    gUiCamera.IsMainCamera = 1;
    gUiCamera.IsCameraMoved = 1;

    camera_component_set_spectator(&gUiCamera);
    camera_component_set_orthograhic(&gUiCamera);

    renderer_init();
    viewport_register_layer(&gUiCamera);

    // BUG(typedef): Sui Backend Part
    // NOTE(typedef): actual renderer
    RendererSettings uiRendererSettings = {
	.MaxGlyphsCount = 1000,
	.MaxRectsCount = 1000,
	.MaxLinesCount = 1000,
	.Camera = &gUiCamera,
	.LegacyCamera = &OrthoCamera
    };
    ui_renderer_init(uiRendererSettings);

    // wide_string_hash_table_test();
}

/* void */
/* sui_frontend_usage() */
/* { */
/*     static i32 i = 0; */
/*     static f32 value = 350.0f; */
/*     static v2 minMax = { 0, 500 }; */

/*     static v2 size = (v2) {0,0}; */
/*     //static v2 size = {50,50}; */
/*     //GINFO("Size: "); v2_print(size); */

/*     static i32 isPanelVisible = 1; */
/*     if (sui_panel_begin(&ButtonPlus, &isPanelVisible)) */
/*     { */
/*	if (sui_button(&ButtonPlus, v2_new(190, 50))) */
/*	{ */
/*	    size.X += 25; */
/*	    value = MINMAX(value + 5, minMax.X, minMax.Y); */
/*	    GINFO("Size: "); v2_print(size); */
/*	} */

/*     } */
/*     sui_panel_end(); */

/*     if (sui_panel_begin(&ButtonMinus, &isPanelVisible)) */
/*     { */
/*	if (sui_button(&ButtonPlus, v2_new(190, 50))) */
/*	{ */
/*	    size.X += 25; */
/*	    value = MINMAX(value + 5, minMax.X, minMax.Y); */
/*	    GINFO("Size: "); v2_print(size); */
/*	} */

/*     } */
/*     sui_panel_end(); */

/*     if (sui_panel_begin(&ButtonText, &isPanelVisible)) */
/*     { */
/*	if (sui_button(&ButtonPlus, v2_new(190, 50))) */
/*	{ */
/*	    size.X += 25; */
/*	    value = MINMAX(value + 5, minMax.X, minMax.Y); */
/*	    GINFO("Size: "); v2_print(size); */
/*	} */

/*     } */
/*     sui_panel_end(); */
/* } */

void
ui_sandbox_on_update(f32 timestep)
{
    camera_component_update(&gUiCamera);
    runtime_camera_update(&OrthoCamera);

    framebuffer_bind(viewport_get_current_framebuffer());
    {
	renderer_clear(v4_new(0.1f, 0.1f, 0.1f, 1.0f));

	// Пропускаем 1 фрейм
	{
	    SuiInstance* instance = sui_get_instance();
	    sui_backend_new_frame();
	    sui_begin_frame((SuiFrameInfo){.Timestep = timestep});
	    {
		//sui_frontend_usage();
		sui_auto_layout();
		// sui_process_input();

		sui_backend_draw();
	    }
	    sui_end_frame();
	}
    }
    framebuffer_unbind();

    Application* app = application_get();
    framebuffer_transfer(viewport_get_current_framebuffer()->RendererId,
			 viewport_get_draw_framebuffer()->RendererId,
			 app->Window->Width,
			 app->Window->Height);

    renderer_flush(viewport_get_draw_framebuffer()->ColorAttachments[0]);
}

void
ui_sandbox_on_ui_render()
{
}

void
ui_sandbox_on_event(Event* event)
{
    sui_backend_event(event);

    switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
ui_sandbox_on_destroy()
{

}
