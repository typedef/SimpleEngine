/* void */
/* sui_backend_new_frame() */
/* { */
/*     SuiInstance* suiInstance = sui_get_instance(); */
/*     suiInstance->Monitor.DisplaySize = window_get_size_v2(CurrentWindow); */

/*     // DOCS(typedef): We need convert coordinates to OpenGL friendly */
/*     suiInstance->InputData.MousePositionChanged = 0; */
/*     v2 newMousePosition = input_get_cursor_position_v2(); */
/*     v2 prevMousePosition = suiInstance->InputData.MousePosition; */
/*     if (v2_not_equal_epsilon(newMousePosition, prevMousePosition, 0.1f)) */
/*     { */
/*	suiInstance->InputData.PrevMousePosition = */
/*	    suiInstance->InputData.MousePosition; */
/*	suiInstance->InputData.MousePosition = newMousePosition; */
/*	suiInstance->InputData.MousePositionChanged = 1; */
/*     } */
/* } */

/* void */
/* sui_backend_draw_panel(SuiPanel suiPanel, f32 zOrderValue) */
/* { */
/*     SuiStyle* suiStyle = sui_get_style(); */

/*     // DOCS(typedef): Draw panel */
/*     ui_renderer_draw_rect(suiPanel.Position, suiPanel.Size, suiPanel.Color); */

/*     // DOCS(typedef): Draw header */
/*     SuiStyleItem suiStyleItem = */
/*	suiStyle->Items[SuiStyleItemType_Panel_Header]; */
/*     v4 headerColor = v4_new(0.5, 0.5, 0.1, 1.0f); */
/*     v2 headerSize = v2_new( */
/*	suiStyleItem.Size.Min.Width * suiPanel.Size.Width, */
/*	suiStyleItem.Size.Min.Height); */
/*     ui_renderer_draw_rect(suiPanel.Position, headerSize, suiPanel.HeaderColor); */

/*     v4 clr; */
/*     if (zOrderValue > 0.2f) */
/*     { */
/*	clr = v4_new(zOrderValue, zOrderValue, .554f, 1.0f); */
/*     } */
/*     else */
/*     { */
/*	clr = v4_new(zOrderValue, 0.7f, zOrderValue, 1.0f); */
/*     } */
/*     ui_renderer_draw_text( */
/*	suiPanel.Label, */
/*	v3_v2v(v2_sub_y(suiPanel.Position, headerSize.Height), zOrderValue), headerSize, */
/*	clr); */

/*     v2 margin = v2_new(15.0f, 15.0f); */

/*     // DOCS(typedef): Draw widgets */
/*     DO_ONES(GINFO("Widgets Cnt: %d\n", array_count(suiPanel.Widgets))); */
/*     v4 btnColor = v4_new(0.1, 0.253, 0.134, 1); */
/*     for (i32 w = 0; w < array_count(suiPanel.Widgets); ++w) */
/*     { */
/*	SuiWidget suiWidget = suiPanel.Widgets[w]; */
/*	/\* ui_renderer_draw_rect( *\/ */
/*	/\*     v2_new(suiWidget.Position.Width + margin.Width, suiWidget.Position.Height - margin.Height), *\/ */
/*	/\*     suiWidget.Size, *\/ */
/*	/\*     btnColor); *\/ */

/*     } */
/* } */

/* void */
/* sui_backend_draw(Renderer2d* renderer) */
/* { */
/*     SuiDrawData* drawData = sui_get_draw_data(); */
/*     SuiInstance* suiInstance = sui_get_instance(); */
/*     //SuiLayoutData* suiLayoutData = sui_get_layout_data(); */

/*     /\* DO_ONES(v2_print(suiLayoutData->CurrentPosition)); *\/ */
/*     /\* DO_ONES(v2_print(suiLayoutData->RowSize)); *\/ */
/*     /\* ui_renderer_draw_rect(suiLayoutData->CurrentPosition, suiLayoutData->RowSize, v4_new(0.3, 0.2,0.1, 1.0)); *\/ */

/*     //GINFO("Panels count: %d\n", array_count(suiInstance->Panels)); */
/*     SuiPanelKeyValue* panelsTable = suiInstance->PrevFrameCache.PanelsTable; */
/*     i64 panelsCount = array_count(suiInstance->PrevFrameCache.Panels); */
/*     for (i32 p = 0; p < panelsCount; ++p) */
/*     { */
/*	i32 ind = suiInstance->PrevFrameCache.Panels[p]; */
/*	SuiPanel suiPanel = panelsTable[ind].Value; */
/*	DO_ONES(GINFO("Panel[%d] id = %ld\n", p, suiPanel.Id)); */
/*	DO_ONES(GINFO("Panel[%d] ", p); v2_print(suiPanel.Position)); */
/*	DO_ONES(GINFO("Panel[%d] ", p); v2_print(suiPanel.Size)); */

/*	const f32 someStep = 0.25f; */
/*	if (wide_string_cequals(*suiPanel.Label, L"Добавить", 8)) */
/*	{ */
/*	    sui_backend_draw_panel(renderer, suiPanel, 0.0f); */
/*	} */
/*	else */
/*	{ */
/*	    sui_backend_draw_panel(renderer, suiPanel, p*someStep); */
/*	} */
/*     } */

/*     // BUG: comment active panel separate rendering */
/*     /\* i32 activePanelInd = suiInstance->PrevFrameCache.ActivePanelInd; *\/ */
/*     /\* if (activePanelInd != -1) *\/ */
/*     /\* { *\/ */
/*     /\*	SuiPanel lastSuiPanel = panelsTable[activePanelInd].Value; *\/ */
/*     /\*	sui_backend_draw_panel(lastSuiPanel); *\/ */
/*     /\* } *\/ */
/*     /\* { *\/ */
/*     /\*	GINFO("HEHRHRHREHREH!\n\n"); *\/ */
/*     /\* } *\/ */


/*     /\*BIG FONT: w bigger e smaller */
/*       SMALL FONT: w smaller e bigger*\/ */
/*     //f32 w, e; */
/*     //ui_renderer_flush(w = 0.4, e = 0.25); */

/* } */

/* void */
/* sui_backend_event(Event* event) */
/* { */
/*     SuiInputData* suiInputData = sui_get_input_data(); */

/*     SuiKeyState suiKeyState; */
/*     switch (event->Type) */
/*     { */
/*     case KeyRealeased: */
/*     case MouseButtonReleased: */
/*	suiKeyState = SuiKeyState_Released; */
/*	break; */

/*     case KeyPressed: */
/*     case MouseButtonPressed: */
/*	suiKeyState = SuiKeyState_Pressed; */
/*	break; */

/*     default: */
/*	suiKeyState = SuiKeyState_None; */
/*	break; */
/*     } */

/*     switch (event->Category) */
/*     { */

/*     case KeyCategory: */
/*     { */
/*	KeyReleasedEvent* kre = (KeyReleasedEvent*) event; */

/*	if (event->Type == CharTyped) */
/*	{ */
/*	    CharEvent* ce = (CharEvent*) event; */
/*	    suiInputData->CharTyped = ce->Char; */
/*	    //GINFO("%d\n", ce->Char); */
/*	    break; */
/*	} */

/*	switch (kre->KeyCode) */
/*	{ */
/*	case KeyType_Tab: */
/*	    suiInputData->KeyData[SuiKeys_Tab].State = suiKeyState; */
/*	    break; */

/*	case KeyType_Backspace: */
/*	    suiInputData->KeyData[SuiKeys_Backspace].State = suiKeyState; */
/*	    break; */
/*	case KeyType_Delete: */
/*	    suiInputData->KeyData[SuiKeys_Delete].State = suiKeyState; */
/*	    break; */

/*	case KeyType_Left: */
/*	    suiInputData->KeyData[SuiKeys_LeftArrow].State = suiKeyState; */
/*	    break; */
/*	case KeyType_Right: */
/*	    suiInputData->KeyData[SuiKeys_RightArrow].State = suiKeyState; */
/*	    break; */
/*	case KeyType_Up: */
/*	    suiInputData->KeyData[SuiKeys_UpArrow].State = suiKeyState; */
/*	    break; */
/*	case KeyType_Down: */
/*	    suiInputData->KeyData[SuiKeys_DownArrow].State = suiKeyState; */
/*	    break; */
/*	case KeyType_Home: */
/*	    suiInputData->KeyData[SuiKeys_Home].State = suiKeyState; */
/*	    break; */
/*	case KeyType_End: */
/*	    suiInputData->KeyData[SuiKeys_End].State = suiKeyState; */
/*	    break; */

/*	} */

/*	break; */
/*     } */

/*     case EventCategory_MouseCategory: */
/*     { */
/*	if (event->Type != MouseButtonPressed && event->Type != MouseButtonReleased) */
/*	{ */
/*	    break; */
/*	} */

/*	MouseButtonEvent* mbe = (MouseButtonEvent*) event; */
/*	switch (mbe->MouseCode) */
/*	{ */
/*	case MouseButtonType_Left: */
/*	    suiInputData->KeyData[SuiKeys_Mouse_Left_Button].State = suiKeyState; */
/*	    break; */

/*	case MouseButtonType_Right: */
/*	    suiInputData->KeyData[SuiKeys_Mouse_Right_Button].State = suiKeyState; */
/*	    break; */

/*	case MouseButtonType_Middle: */
/*	    suiInputData->KeyData[SuiKeys_Mouse_Middle_Button].State = suiKeyState; */
/*	    break; */
/*	} */
/*	break; */

/*     } */

/*     break; */
/*     } */

/* } */
