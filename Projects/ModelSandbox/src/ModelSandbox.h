#ifndef MODEL_SANDBOX_H
#define MODEL_SANDBOX_H

#include <Engine.h>

void model_sandbox_on_attach();
void model_sandbox_on_attach_finished();
void model_sandbox_on_update(f32 timestep);
void model_sandbox_on_ui_render();
void model_sandbox_on_event(Event* event);
void model_sandbox_on_destroy();

#endif // MODEL_SANDBOX_H
