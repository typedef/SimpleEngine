#include "ModelSandbox.h"

#include <glad/glad.h>

#include <Graphics/KeyCodes.h>
#include <Graphics/Renderer.h>
#include <Graphics/RuntimeCamera.h>
#include <Graphics/ModelRenderer.h>
#include <Model/StaticModel.h>
#include <Utils/SimpleStandardLibrary.h>

static NativeWindow* CurrentWindow = NULL;
static Texture2D DefaultTexture;
static Texture2D Refresh;
static FpsCounter Fps;
// NOTE(typedef): LEGACY
static RuntimeCamera CurrentCamera;
// NOTE(typedef): New
static CameraComponent MainCamera;
static CubeMap Cubemap;

static v4 FrontColor = { 0, 0, 1, 1 };
static v4 RightColor = { 1, 0, 0, 1 };
static v4 UpColor    = { 0, 1, 0, 1 };
static v4 RayColor   = { 0.8f, 0.8f, 0, 1 };

static StaticModel* SceneModels = NULL;
static TransformComponent* ModelTransforms = NULL;
static LightsData LightData;

static f32 Timestep = 0.0f;

void register_lights();

void
model_sandbox_on_attach()
{
    CurrentWindow = application_get_window();
    f32 aspectRatio = (f32) CurrentWindow->Width / CurrentWindow->Height;

    window_set_vsync(2);

    glEnable(GL_MULTISAMPLE);

#define TYPE 1
    RuntimeCameraSettings settings = (RuntimeCameraSettings) {
#if TYPE == 1
	.Type = CameraType_PerspectiveSpectator,
#elif TYPE == 2
	.Type = CameraType_PerspectiveArcball,
#else
	.Type = CameraType_OrthographicSpectator,
#endif

	//Projection
	.Near = 0.01f,
	.Far = 5000.0f,
	.AspectRatio = aspectRatio,
	.FovAngle = 90.0f,

	//Spectator
	.Position = { 0.0f, 2.5f, 5.5f },
	.Yaw = 0,
	.Pitch = 0,
	.Speed = 5,
	.SensitivityHorizontal = 0.35f,
	.SensitivityVertical = 0.45f,
	.Zoom = 0
    };
    //NOTE(): we always use default declared in runtime_camera_update
    CurrentCamera = runtime_camera_create(settings);

    MainCamera = camera_component_new(CameraType_PerspectiveSpectator);
    MainCamera.Settings.SensitivityHorizontal = 0.35f;
    MainCamera.Settings.SensitivityVertical = 0.45f;
    MainCamera.Settings.Position = v3_new(0.0f, 2.5f, 5.5f);
    MainCamera.Perspective = (PerspectiveSettings) {
	.Near = 0.01f,
	.Far = 4000.0f,
	.AspectRatio = CurrentWindow->AspectRatio,
	.Fov = rad(90),
    };
    MainCamera.Spectator = (SpectatorSettings) {
	.Speed = 5.0f,
	.Zoom = 0
    };
    camera_component_set_perspective(&MainCamera);
    camera_component_update(&MainCamera);

    shader_debug(0);
    renderer_set_viewport(CurrentWindow->Width, CurrentWindow->Height);
    renderer_init();
    renderer2d_init(&MainCamera);
    //renderer3d_init(&CurrentCamera);
    renderer_enable_depth_testing(RendererType_Renderer3D);
    renderer_enable_blending(RendererType_Renderer3D);
    renderer_enable_face_culling(RendererType_Renderer3D);

    Texture2DSettings textureSettings = {
	.IsFlipOnLoad = 1,
	.FilterType = TextureFilterType_Linear
    };
    DefaultTexture = texture2d_create(
	//For debug only
	//asset_texture("default/white_texture.png")
	//For release
	asset_texture("default/white.png")
	, textureSettings);

    Fps = fps_counter_create(window_get_short_time);

    //NOTE(bies): model loading, comment for now, fix quat_rot first!
    const char* assetModelPath =
#if 1
	//asset_model("Duck/Duck.gltf");
	//asset_model("Monkey/Suzanne.gltf");
	//asset_model("Corset/Corset.gltf");
	//asset_model("Complex/Complex.gltf");
	//asset_model("Anime/Anime.gltf");
	//asset_model("Sponza/Sponza.gltf"); // have problems
	//asset_model("FlightHelmet/FlightHelmet.gltf");

	"/home/bies/MidData/Assets/Models/Blender Models/SimplePuppet1/Character Running.gltf";
	//asset_model("AnimatedModel/AnimatedModel.gltf");
#else
    asset_model("SheenChair/SheenChair.gltf");
    //asset_model("CubeTextured/BoxTextured.gltf");
    //asset_model("RedBox/Box.gltf");
#endif

    StaticModel model = static_model_load(&DefaultTexture, assetModelPath);
    array_push(SceneModels, model);

    TransformComponent tc =
	transform_component_new(v3_new(1, 1, 1),
				v3_new(0, 0, 0),
				v3_new(1, 1, 1));
    array_push(ModelTransforms, tc);

    renderer3d_model_init(&MainCamera);

#if 0
    Cubemap = cube_map_create_from_single_file(asset_cubemap("skybox3/skybox.png"));
#else
    Cubemap = cube_map_create(asset_cubemap("skybox"));
#endif

    renderer3d_cubemap_init(&MainCamera);

    register_lights();

    // here bug
    viewport_register_layer(&MainCamera);
    viewport_set_active();

    /*Other resource loading*/
    Refresh = texture2d_create(
	asset_texture("BaseUI/Refresh.png"),
	(Texture2DSettings) { .IsFlipOnLoad = 0, .FilterType = TextureFilterType_Linear });

    scene_basic_create_grid(v4_new(0.3, 0.3, 0.3, 1.0), 0.001f, -15.0f, 15.0f, 0.2f);
}

void
model_sandbox_on_attach_finished()
{
    viewport_set_active();
}

void
model_sandbox_on_update(f32 timestep)
{
    Timestep = timestep;

    fps_counter_update(&Fps);

    camera_component_update(&MainCamera);

    framebuffer_bind(viewport_get_current_framebuffer());
    {
	renderer_clear(v4_new(0.1f, 0.1f, 0.1f, 1.0f));

	renderer3d_cubemap_draw(Cubemap.ID);

	scene_basic_render_grid();

	scene_basic_render_camera_orientation(&MainCamera, FrontColor, RightColor, UpColor);

	for (i32 i = 0; i < array_count(SceneModels); ++i)
	{
	    StaticModel model = SceneModels[i];
	    renderer3d_model_draw(model, LightData, ModelTransforms[i].Matrix);
	}

    }
    framebuffer_unbind();

    framebuffer_transfer(viewport_get_current_framebuffer()->RendererId,
			 viewport_get_draw_framebuffer()->RendererId,
			 CurrentWindow->Width,
			 CurrentWindow->Height);

    renderer_flush(viewport_get_draw_framebuffer()->ColorAttachments[0]);
}

void
model_sandbox_on_ui_render()
{
#if 0
    bool p_open = true;
    igShowDemoWindow(&p_open);
#endif

    ImGuiIO* io = igGetIO();

    ImGuiWindowFlags windowFlags = ImGuiWindowFlags_NoDecoration | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoFocusOnAppearing | ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoMove;
    ImVec2 windowPosition = ImVec2(550, 120);
    igSetNextWindowPos(windowPosition, ImGuiCond_Always, ImVec2(0,0));
    igSetNextWindowBgAlpha(0.35f); // Transparent background
    static bool isOverlayVisible = true;
    if (igBegin("GameDemoWindow", &isOverlayVisible, windowFlags))
    {
	igText("Frames: %d, Latency: %f, Vsync: %d", Fps.Fps, 1000 * Timestep, CurrentWindow->VsyncLevel);
    }
    igEnd();

    directional_light_component(&LightData.DirectionalLights[0]);

    static bool isModelPropertiesOpen = true;
    if (igBegin("Models", &isModelPropertiesOpen, ImGuiWindowFlags_None))
    {
	i32 i, count = array_count(SceneModels);
	for (i = 0; i < count; ++i)
	{
	    model_ui(&SceneModels[i], &ModelTransforms[i], &Refresh);
	}
    }
    igEnd();
}

void
model_sandbox_on_event(Event* event)
{
    switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
model_sandbox_on_destroy()
{
}

void
register_lights()
{
    const f32 ambient = 0.25f;
    const f32 diffuse = 0.52f;
    const f32 specular = 0.0f;
    BaseLight baseLight = {
	.Ambient = v3_new(ambient, ambient, ambient),
	.Diffuse = v3_new(diffuse, diffuse, diffuse),
	.Specular = v3_new(specular, specular, specular),
    };
    DirectionalLightComponent dirLight = {
	.Base = baseLight,
	.Color = v3_new(1, 1, 1),
	.Direction = v3_new(-0.02f, -0.29f, -1.0f)
    };

    LightData = (LightsData) {
	.DirectionalLights = NULL,
	.PointLights = NULL,
	.FlashLights = NULL,
    };

    array_push(LightData.DirectionalLights, dirLight);
}
