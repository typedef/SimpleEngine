#include "ModelSandbox.h"
#include <EntryPoint.h>

void
create_user_application()
{
    ApplicationSettings appSettings = {
	.Width = 2000,
	.Height = 1200,
	.IsInitOpenGl = 1,
	.Name = "ModelSandbox",
    };
    application_create(appSettings);

    Layer modelSandboxLayer = {0};
    modelSandboxLayer.Name = "ModelSandbox Layer";
    modelSandboxLayer.OnAttach = model_sandbox_on_attach;
    modelSandboxLayer.OnUpdate = model_sandbox_on_update;
    modelSandboxLayer.OnUIRender = model_sandbox_on_ui_render;
    modelSandboxLayer.OnEvent = model_sandbox_on_event;
    modelSandboxLayer.OnDestoy = model_sandbox_on_destroy;

    application_push_layer(modelSandboxLayer);
}
