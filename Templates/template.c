#include "${ProjectName}.h"

#include <Graphics/KeyCodes.h>

static NativeWindow* CurrentWindow = NULL;

void
${ProjectNameConvLower}_on_attach(NativeWindow* window)
{
    CurrentWindow = window;
}

void
${ProjectNameConvLower}_on_attach_finished()
{

}

void
${ProjectNameConvLower}_on_update(f32 timestep)
{

}

void
${ProjectNameConvLower}_on_ui_render()
{

}

void
${ProjectNameConvLower}_on_event(Event* event)
{
    switch (event->Category)
    {

    case KeyCategory:
    {
	if (event->Type != KeyPressed)
	    break;

	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->KeyCode == KEY_ESCAPE)
	{
	    application_close();
	    event->IsHandled = 1;
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == WindowShouldBeClosed)
	{
	    application_close();
	}
	break;
    }

    }
}

void
${ProjectNameConvLower}_on_destroy()
{

}
