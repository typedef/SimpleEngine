workspace "Engine"
architecture "x64"
startproject "%ProjectName"

configurations
{
  "Debug",
  "Release",
  "Dist"
}

-- Set variable inside /etc/environment
VULKAN_SDK_PATH = os.getenv("VULKAN_SDK")
print("Vulkan SDK Path: " .. VULKAN_SDK_PATH)
assert(VULKAN_SDK_PATH ~= nil, "Can't get Vulkan SDK!")


outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

-- Include dirs
IncludeDirs = {}
IncludeDirs["GLFW"] = "Dependencies/GLFW/include"
IncludeDirs["GLAD"] = "Dependencies/glad/include"
IncludeDirs["CGLM"] = "Dependencies/CGLM/include"
IncludeDirs["CGLTF"] = "Dependencies/cgltf/src/"
IncludeDirs["ASSIMP"] = "Dependencies/assimp/include/"
IncludeDirs["MINIAUDIO"] = "Dependencies/MiniAudio/src/"
IncludeDirs["STB"] = "Dependencies/stb/src/"
IncludeDirs["VULKAN"] = VULKAN_SDK_PATH .. "/include/vulkan"
IncludeDirs["VULKAN_INCLUDE"] = VULKAN_SDK_PATH .. "/include/"

IncludeLibs = {}
IncludeLibs["VULKAN"] = VULKAN_SDK_PATH .. "/lib"
-- print(IncludeDirs.VULKAN)
-- print(IncludeLibs.VULKAN)

group "Dependencies"
 include "Dependencies/GLFW"
 include "Dependencies/glad"
 include "Dependencies/cgltf"
 include "Dependencies/MiniAudio"
 include "Dependencies/stb"
 include "Dependencies/assimp"

project "Engine"
    location "Engine"
    cdialect "C99"
    kind "StaticLib"
    language "C"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")

    files
    {
      "%{prj.name}/src/**.h",
      "%{prj.name}/src/**.c",
    }

    defines
    {
      "_CRT_SECURE_NO_WARNINGS",
      "GLFW_INCLUDE_NONE",
      "_GNU_SOURCE",
    }

    includedirs
    {
      "%{prj.name}/src",
      "%{IncludeDirs.GLFW}",
      "%{IncludeDirs.GLAD}",
      "%{IncludeDirs.CGLM}",
      "%{IncludeDirs.CGLTF}",
      "%{IncludeDirs.ASSIMP}",
      "%{IncludeDirs.MINIAUDIO}",
      "%{IncludeDirs.STB}",
      "%{IncludeDirs.VULKAN}",
      "%{IncludeDirs.VULKAN_INCLUDE}"
    }

    filter "system:linux"
      defines { "LINUX_PLATFORM" }

    filter "system:windows"
      defines { "WINDOWS_PLATFORM" }

    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"

project "%ProjectName"
    location "Projects/%ProjectName"
    kind "ConsoleApp"
    language "C"
    cdialect "C99"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")

    files
    {
      "Projects/%{prj.name}/src/**.h",
      "Projects/%{prj.name}/src/**.c"
    }

    defines
    {
      "GLFW_INCLUDE_NONE",
      "_GNU_SOURCE"
    }

    includedirs
    {
       "Engine/src",
       "Projects/%{prj.name}/src/",
       "Dependencies",
       "%{IncludeDirs.GLFW}",
       "%{IncludeDirs.GLAD}",
       "%{IncludeDirs.CGLM}",
       "%{IncludeDirs.CGLTF}",
       "%{IncludeDirs.ASSIMP}",
       "%{IncludeDirs.MINIAUDIO}",
       "%{IncludeDirs.STB}",
       --"vulkan",
       --"/usr/include/vulkan",
       "%{IncludeDirs.VULKAN}",
       "%{IncludeDirs.VULKAN_INCLUDE}"
    }

    runpathdirs
    {
       "%{IncludeLibs.VULKAN}"
    }

    libdirs
    {
       "%{IncludeLibs.VULKAN}"
    }

    links
    {
       "Engine",
       "vulkan",

       "shaderc_combined",

       -- "glslang",
       -- "OSDependent",
       -- "shaderc_util",
       -- "SPIRV",
       -- "HLSL",
       -- "SPIRV-Tools",
       -- "SPIRV-Tools-opt",
       -- "OGLCompiler",
       -- "MachineIndependent",
       -- "spirv-cross-c",
       -- "spirv-cross-core",
       -- "spirv-cross-cpp",
       -- "spirv-cross-glsl",
       -- "spirv-cross-reflect",
       -- "spirv-cross-util",
       -- "SPIRV-Tools-link",
       -- "SPIRV-Tools-lint",
       -- "SPIRV-Tools-diff",

       -- "VkExtLayer_utils",
       -- "VkLayer_utils",
       -- "VkLayer_khronos_validation",
       "glad",
       "GLFW",
       -- "cimgui",
       "cgltf",
       "assimp",
       "stb",
       "MiniAudio",
       "stdc++"
    }

    -- ${ProjectName}
    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "Speed"

    filter "system:linux"
    defines { "LINUX_PLATFORM" }
      links
      {
	 "X11",
	 "m",
	 "pthread",
	 "stdc++"
      }

    filter "system:windows"
    defines { "WINDOWS_PLATFORM", "_CRT_SECURE_NO_WARNINGS" }
    links {"user32", "gdi32"}
