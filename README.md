# SimpleEngine
SimpleEngine - это просто движок для разработки 3D игр, сильно Work In Progress.

## Get Started

* git clone https://codeberg.org/typedef/SimpleEngine
* cd SimpleEngine
* git clone https://github.com/vezone/Assets assets
* cd Scripts/Windows/
* run script generate_project.bat

## Tools Dependencies
* git
* premake5

## Dependencies For Linux Platform
* gcc


## Dependencies For Windows Platform
* mingw


## Dependencies
* cgltf (https://github.com/jkuhlmann/cgltf)
* cimgui (https://github.com/cimgui/cimgui)
* glad (https://github.com/Dav1dde/glad)
* GLFW (https://github.com/glfw/glfw)
* MiniAudio (https://github.com/mackron/miniaudio)
* stb: stb_image, stb_image_resize, stb_image_write, stb_truetype (https://github.com/nothings/stb)
