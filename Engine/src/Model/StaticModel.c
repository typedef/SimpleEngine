#include "StaticModel.h"

#include <stdio.h>

#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
#include <AssetManager/AssetManager.h>

VsaTextureId
mesh_texture_create(cgltf_texture* texture, const char* path)
{
    if (!texture
	|| !path
	//|| !texture->image->buffer_view
	|| !texture->image->uri)
    {
	return -1;
    }

    char* finalPath = path_combine_directory_and_name(
	path, texture->image->uri);

    if (path == NULL)
    {
	GERROR("Path is wrong: %s\n", path);
	vassert(0 && "Mesh Texture Path is NULL!");
	return -1;
    }

    i64 textureId = asset_manager_load_asset_id(AssetType_Texture, finalPath);

    memory_free(finalPath);

    return textureId;
}

/*
  Allocates indices array, need to be free
*/
u32*
mesh_get_indices(cgltf_primitive primitive)
{
    u32* indices = NULL;

    {
	cgltf_accessor* indicesAccessor = primitive.indices;
	i32 trianglesCount = (i32) indicesAccessor->count / 3;
	indices = (u32*) memory_allocate(indicesAccessor->count * sizeof(u32));

	switch (indicesAccessor->component_type)
	{

	case cgltf_component_type_r_16u:
	{
	    size_t size = sizeof(u16);
	    u16* buffer =
		(u16*) indicesAccessor->buffer_view->buffer->data
		+ indicesAccessor->buffer_view->offset / size
		+ indicesAccessor->offset / size;
	    i32 ind = 0;
	    for (i32 c = 0; c < indicesAccessor->count; ++c)
	    {
		indices[c] = (u32) buffer[ind];
		ind += (i32) (indicesAccessor->stride / size);
	    }
	    break;
	}

	case cgltf_component_type_r_32u:
	{
	    size_t size = sizeof(u32);
	    u32* buffer =
		(u32*) indicesAccessor->buffer_view->buffer->data
		+ indicesAccessor->buffer_view->offset / size
		+ indicesAccessor->offset / size;
	    i32 ind = 0;
	    for (i32 c = 0; c < indicesAccessor->count; ++c)
	    {
		indices[c] = (u32) buffer[ind];
		ind += (i32) (indicesAccessor->stride / size);
	    }
	    break;
	}

	default:
	    vassert(0 && "ComponentType Unknown!!!");
	    break;
	}

	for (i32 i = 0; i < indicesAccessor->count; ++i)
	{
	    printf("%d ", indices[i]);
	    if (i > 0 && i % 10 == 0)
		printf("\n");
	}

	printf("\n");
    }


    return indices;
}

static PbrMetallic
material_pbr_metallic(cgltf_pbr_metallic_roughness pbrMetallic, const char* materialPath)
{
    cgltf_texture* baseColorTexture =
	pbrMetallic.base_color_texture.texture;
    cgltf_texture* metallicTexture =
	pbrMetallic.metallic_roughness_texture.texture;

    PbrMetallic metallic = {
	.BaseColorTexture = mesh_texture_create(baseColorTexture, materialPath),
	.MetallicRoughnessTexture = mesh_texture_create(metallicTexture, materialPath),
	.BaseColor = v4_array_w(pbrMetallic.base_color_factor, 1.0f),
	.MetallicFactor = pbrMetallic.metallic_factor,
	.RoughnessFactor = pbrMetallic.roughness_factor
    };

    return metallic;
}

StaticMesh
static_mesh_load(i32* isModelDownscaled, cgltf_mesh gltfMesh, const char* path)
{
    StaticMesh mesh = {
	.Vertices = NULL,
	.Indices  = NULL,
	.Material = (MeshMaterial) {0},
	.Name = gltfMesh.name != NULL ? string(gltfMesh.name) : "",
	.IsVisible = 1
    };
    vassert_not_null(mesh.Name);

    // Copy From
    v3* meshPositions = NULL;
    v3* meshNormals = NULL;
    v2* meshUVs = NULL;

    GLOG("Mesh name: %s\n", mesh.Name);

#define LoadDataFromAttribute(attribute, code)				\
    {									\
	f32* buffer = (f32*) attribute.data->buffer_view->buffer->data	\
	    + attribute.data->buffer_view->offset / sizeof(f32)		\
	    + attribute.data->offset / sizeof(f32);			\
	i32 ind = 0;							\
	for (i32 c = 0; c < attribute.data->count; ++c)			\
	{								\
	    code;							\
									\
	    ind += (i32) (attribute.data->stride / sizeof(f32));	\
	}								\
    }

    for (i32 p = 0; p < gltfMesh.primitives_count; ++p)
    {
	cgltf_primitive primitive = gltfMesh.primitives[p];
	if (primitive.type != cgltf_primitive_type_triangles)
	{
	    GWARNING("Not a triangle!\n");
	    continue;
	}

	if (primitive.indices != NULL && mesh.Indices == NULL)
	{
	    mesh.IndicesCount = primitive.indices->count;
	    mesh.Indices = mesh_get_indices(primitive);
	}

	/* Positions, Normals */
	for (i32 a = 0; a < primitive.attributes_count; ++a)
	{
	    cgltf_attribute attribute = primitive.attributes[a];
	    switch (attribute.type)
	    {

	    case cgltf_attribute_type_position:
	    {
		//GINFO("Attribute_Position [ name %s, type: %s ]\n", attribute.name, "position", );

		LoadDataFromAttribute(
		    attribute,
		    v3 position = v3_new(buffer[ind + 0], buffer[ind + 1], buffer[ind + 2]);
		    array_push(meshPositions, position);
		    );

		break;
	    }

	    case cgltf_attribute_type_normal:
	    {
		//GINFO("Attribute_Normal [ name %s, type: %s ]\n", attribute.name, "normal");
		LoadDataFromAttribute(
		    attribute,
		    v3 normal = v3_new(buffer[ind + 0], buffer[ind + 1], buffer[ind + 2]);
		    array_push(meshNormals, normal);
		    );

		break;
	    }

	    case cgltf_attribute_type_tangent:
		GINFO("Attribute_Tangent [ name %s, type: %s ]\n", attribute.name, "tangent");
		break;

	    case cgltf_attribute_type_texcoord:
	    {
		//GINFO("Attribute_UV [ name %s, type: %s ]\n", attribute.name, "texcoord");
		LoadDataFromAttribute(
		    attribute,
		    v2 uv = v2_new(buffer[ind + 0], buffer[ind + 1]);
		    array_push(meshUVs, uv););

		break;
	    }

	    case cgltf_attribute_type_color:
		//GINFO("Attribute_Color [ name %s, type: %s ]\n", attribute.name, "color", );
		break;

	    default:
		/*
		  cgltf_attribute_type_invalid
		*/
		GWARNING("Attribute_Default\n");
		break;
	    }
	}

	// primitive.material == NULL for some reason
	if (primitive.material && primitive.material->has_pbr_metallic_roughness)
	{
	    mesh.Material.Name = primitive.material->name ? string(primitive.material->name) : "";
	    mesh.Material.IsMetallicExist = 1;
	    mesh.Material.Metallic =
		material_pbr_metallic(
		    primitive.material->pbr_metallic_roughness,
		    path);
	}
    }

    i32 meshPositionsCount = array_count(meshPositions);
    i32 meshNormalsCount = array_count(meshNormals);
    vassert(meshPositionsCount == meshNormalsCount && "Positions cnt != Normals cnt!");
    i32 isModelBeenDownscaled = *isModelDownscaled;
    i32 needsDownscale = (isModelBeenDownscaled == 1) || array_any_cond(meshPositions, item.X > 100.0f || item.Y > 100.0f || item.Z > 100.0f);
    if (!isModelBeenDownscaled && needsDownscale)
    {
	*isModelDownscaled = needsDownscale;
    }

    GINFO("meshPositionsCount: %d\n", meshPositionsCount);
    GINFO("meshNormalsCount: %d\n", meshNormalsCount);
    GINFO("Mesh UV COUNT: %d\n", array_count(meshUVs));

    for (i32 i = 0; i < meshPositionsCount; ++i)
    {
	MeshVertex vertex;
	//NOTE(typedef): this working weird
	if (0 && needsDownscale)
	{
	    vertex.Position = v3_scale(meshPositions[i], 0.01f);
	}
	else
	{
	    vertex.Position = meshPositions[i];
	}

	vertex.Normal = meshNormals[i];
	if (meshUVs != NULL)
	{
	    vertex.UV = meshUVs[i];
	}
	else
	{
	    vertex.UV = v2_new(0.0f, 0.0f);
	}

	array_push(mesh.Vertices, vertex);
    }

    if (array_count(mesh.Vertices) <= 0)
    {
	vassert(0 && "Smth wrong with mesh!");
    }

    array_free(meshPositions);
    array_free(meshNormals);
    array_free(meshUVs);

    return mesh;
}

StaticModel
static_model_load(const char* path)
{
    cgltf_options loadOptions = { 0 };
    cgltf_data* gltfData = NULL;
    cgltf_result loadResult = cgltf_parse_file(&loadOptions, path, &gltfData);
    if (loadResult != cgltf_result_success)
    {
	GERROR("Loading model error!\n");
	vassert_break();
    }

    loadResult = cgltf_load_buffers(&loadOptions, gltfData, path);
    if (loadResult != cgltf_result_success)
    {
	GERROR("Loading buffers error!\n");
	vassert_break();
    }

    switch (gltfData->file_type)
    {
    case cgltf_file_type_glb:
	break;
    case cgltf_file_type_gltf:
	break;
    default:
	GERROR("StaticModel type incorrect!\n");
	vassert_break();
	break;
    }

    char* aPath = string(path);
    StaticModel model = {
	.Type = ModelType_Loaded,
	.Path = aPath,
	.Name = path_get_name(aPath),
	.Meshes = NULL,
    };


#if 1
    i32 meshesCount = gltfData->meshes_count;
    for (i32 m = 0; m < meshesCount; ++m)
    {
	cgltf_mesh gltfMesh = gltfData->meshes[m];
	i32 downScaled = 0;
	StaticMesh mesh = static_mesh_load(&downScaled, gltfMesh, path);
	array_push(model.Meshes, mesh);
    }

#else

    for (i32 i = 0; i < gltfData->nodes_count; ++i)
    {
	cgltf_node node = gltfData->nodes[i];
	if (node.mesh != NULL)
	{
	    cgltf_mesh gltfMesh = *node.mesh;
	    i32 downScaled = 0;
	    StaticMesh mesh = static_mesh_load(&downScaled, gltfMesh, path);
	    array_push(model.Meshes, mesh);
	}
    }

#endif
    printf("======     StaticModel Loading Info   ======\n");
    printf("Meshes Count: %d\n", array_count(model.Meshes));
    printf("======   StaticModel Loading Info End ======\n");

    cgltf_free(gltfData);

    return model;
}

i64
static_model_get_vertices_count(const StaticModel* pStaticModel)
{
    i64 modelVerticesCount = 0;

    for (i32 i = 0; i < array_count(pStaticModel->Meshes); ++i)
    {
	StaticMesh mesh = pStaticModel->Meshes[i];
	modelVerticesCount += array_count(mesh.Vertices);
    }

    return modelVerticesCount;
}

#include <assimp/cimport.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <assimp/mesh.h>

StaticModel
static_model_load_assimp(const char* path)
{
    StaticModel staticModel = {};

    /*
      NOTE(typedef): This can be usefull for rendering
      aiProcess_SplitLargeMeshes
      aiProcess_ImproveCacheLocality
      #if ENGINE_DEBUG
      aiProcess_ValidateDataStructure
      #endif
      aiProcess_FixInfacingNormals
      aiProcess_OptimizeMeshes
     */
    u32 flag = aiProcess_JoinIdenticalVertices;
    const struct aiScene* pAiScene = aiImportFile(path, flag);
    // aiImportFileExWithProperties
    // cimport
    // aiGetPredefinedLogStream

    if (pAiScene == NULL)
    {
	GERROR("Failed to load the model %s\n", path);
	vguard(0);
    }

    i32 meshesCount = pAiScene->mNumMeshes;
    C_STRUCT aiMesh** meshes = pAiScene->mMeshes;
    for (i32 i = 0; i < meshesCount; ++i)
    {
	struct aiMesh* pMesh = meshes[i];
	GINFO("Mesh name: %s\n", pMesh->mName);
    }

#if 0
    aiMaterial* mat = .....

if(AI_SUCCESS != aiGetMaterialFloat(mat,<material-key>,<where-to-store>)) {
   // handle epic failure here
}
#endif

    aiReleaseImport(pAiScene);


    return staticModel;
}
