#ifndef STATIC_MODEL_H
#define STATIC_MODEL_H

#include <Utils/Types.h>
#include <Utils/cgltf.h>

#include <Graphics/Vulkan/VulkanSimpleApiTypes.h>
// TODO(typedef): free all this memory on destroy

typedef struct VsaTexture VsaTexture;
typedef struct IString IString;

// Put in OpenGLBase.h
typedef enum TextureType
{
    TextureType_Diffuse = 0,
    TextureType_Specular,
    TextureType_Normal
} TextureType;

typedef struct MeshVertex
{
    v3 Position;
    v3 Normal;
    v2 UV;
} MeshVertex;

typedef struct PbrMetallic
{
    // 0 -> null
    VsaTextureId BaseColorTexture;
    VsaTextureId MetallicRoughnessTexture;

    v4 BaseColor;
    f32 MetallicFactor;
    f32 RoughnessFactor;
} PbrMetallic;

typedef struct MeshMaterial
{
    const char* Name;
    i32 IsMetallicExist;
    PbrMetallic Metallic;
} MeshMaterial;

typedef struct StaticMesh
{
    AABB Borders;
    i32 IsVisible;
    const char* Name;
    u32* Indices;
    MeshVertex* Vertices;
    v3 Position;
    size_t IndicesCount;
    MeshMaterial Material;
} StaticMesh;

typedef enum ModelType
{
    ModelType_Loaded = 1 << 0,
    ModelType_Downscaled = 1 << 1,
    ModelType_Generated = 1 << 2,
    ModelType_Generated_Cube = 1 << 3
} ModelType;

typedef struct StaticModel
{
    ModelType Type;
    char* Path;
    const char* Name;
    StaticMesh* Meshes;
} StaticModel;

StaticModel static_model_load(const char* path);
i64 static_model_get_vertices_count(const StaticModel* pStaticModel);

StaticModel static_model_load_assimp(const char* path);

#endif // STATIC_MODEL_H
