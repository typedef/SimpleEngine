#include "StaticModelExt.h"

#include <Graphics/OpenGLBase.h>
#include <Utils/SimpleStandardLibrary.h>

static const f32 vp = 0.5f;
static const f32 dv = 2 * vp;
static v3 DefaultCubePositions[24] = {
    // Front
    //0              1              2                3
    {-vp,vp,vp}, {-vp,-vp,vp}, {vp,-vp,vp}, {vp,vp,vp},

    // Back
    //4               5                6                 7
    {-vp,vp,-vp}, {vp, vp,-vp}, {vp,-vp,-vp}, {-vp,-vp,-vp},

    // Left
    //8               9                10                11
    {-vp,vp,vp}, {-vp,vp,-vp}, {-vp,-vp,-vp}, {-vp,-vp,vp},

    // Right
    //12             13              14               15
    { vp,vp,vp}, {vp,-vp,vp}, {vp,-vp,-vp}, {vp,vp,-vp},

    // Bottom
    //16             17               18               19
    {-vp,-vp,vp}, {-vp,-vp,-vp},{vp,-vp,-vp}, {vp,-vp,vp},

    // Up
    //20              21              22              23
    {-vp,vp,-vp}, {-vp,vp,vp}, {vp,vp,vp}, {vp,vp,-vp}
};
static v3 DefaultCubeNormals[24] = {
    // Front
    //0         1          2          3
    {0, 0, 1}, {0, 0, 1}, {0, 0, 1}, {0, 0, 1},

    // Back
    //4          5           6           7
    {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},

    // Left
    //8           9           10          11
    {-1, 0, 0}, {-1, 0, 0}, {-1, 0, 0}, {-1, 0, 0},

    // Right
    //12        13         14         15
    {1, 0, 0}, {1, 0, 0}, {1, 0, 0}, {1, 0, 0},

    // Bottom
    //16         17          18          19
    {0, -1, 0}, {0, -1, 0}, {0, -1, 0}, {0, -1, 0},

    // Up
    //20        21         22         23
    {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}
};

static v2 DefaultCubeUV[24] = {
    // Front
    //0         1         2         3
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Back
    //4         5         6         7
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Left
    //8         9         10        11
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Right
    //12        13        14        15
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Bottom
    //16        17        18        19
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Up
    //20        21        22        23
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 }
};

static u32 DefaultCubeIndices[36] = {
    // Front - CW
    0, 1, 3, 3, 1, 2,

    // Back - CCW
    4, 7, 5, 5, 7, 6,

    // LEFT - CCW
    8 , 11, 9 , 9 , 11, 10,

    // RIGHT - CW
    12, 13, 15, 15, 13, 14,

    // BOTTOM - CCW
    16, 19, 17, 17, 19, 18,

    // UP    - CW
    20, 21, 23, 23, 21, 22,
};

StaticModel
static_model_cube_create()
{
    StaticModel model = (StaticModel) {
	.Type = ModelType_Generated | ModelType_Generated_Cube,
	    .Name = string("Cube"),
	    .Meshes = NULL
    };

    MeshVertex* vertices = NULL;
    for (i32 v = 0; v < 24; ++v)
    {
	MeshVertex vertex = {
	    .Position = DefaultCubePositions[v],
	    .Normal = DefaultCubeNormals[v],
	    .UV = DefaultCubeUV[v]
	};
	array_push(vertices, vertex);
    }

    StaticMesh mesh = {
	.IsVisible = 1,
	.Name = string("Base Cube"),
	.Indices = DefaultCubeIndices,
	.Vertices = vertices,
	.IndicesCount = 36,
	.Material = {
	    .Name = string("Color"),
	    .IsMetallicExist = 1,
	    .Metallic = {
		.BaseColorTexture = 0,
		.MetallicRoughnessTexture = 0,
		.BaseColor = v4_new(0.9f, 0.9f, 0.9f, 1.0f),
		.MetallicFactor = 0,
		.RoughnessFactor = 0
	    }
	}
    };

    array_push(model.Meshes, mesh);

    return model;
}
