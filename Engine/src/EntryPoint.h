#include "Application/Application.h"

/*
  NOTE(bies):
  Starting main application with 50 Mb stack
*/

extern void create_user_application();

void*
main_thread_function(void* data)
{
    create_user_application();
    application_start();
    application_end();
}

int main(int argc, char** argv)
{
    size_t stackSize = MB(5);
    SimpleThread mainThread = simple_thread_create(main_thread_function, stackSize, NULL);
    simple_thread_attach(&mainThread);

    return 0;
}
