#ifndef VIEWPORT_PANEL_H
#define VIEWPORT_PANEL_H

#include <Utils/Types.h>

typedef struct Event Event;
typedef struct Framebuffer Framebuffer;
typedef struct CameraComponent CameraComponent;

void viewport_register_layer(CameraComponent* camera);

void viewport_set_active();
f32 viewport_width();
f32 viewport_height();
f32 viewport_x();
f32 viewport_y();
i32 viewport_is_active();
i32 viewport_is_movement_enabled();
void viewport_on_attach();
void viewport_on_update(f32 timestep);
void viewport_on_event(Event* event);
void viewport_on_ui();

Framebuffer* viewport_get_current_framebuffer();
Framebuffer* viewport_get_draw_framebuffer();

i8 viewport_panel_is_movement_enabled();
void viewport_panel_enable_movement();
void viewport_panel_disable_movement();

#endif // VIEWPORT_PANEL_H
