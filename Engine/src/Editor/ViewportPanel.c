#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

#include "ViewportPanel.h"

#include <stdbool.h>

#include <Application/Application.h>
#include <Graphics/OpenGLBase.h>
#include <Graphics/Renderer.h>
#include <Graphics/SimpleWindow.h>
#include <InputSystem/SimpleInput.h>
#include <InputSystem/KeyCodes.h>
#include <EntitySystem/Components/AllComponents.h>
#include <Event/Event.h>
//#include <UI/ui.h>
#include <Utils/SimpleStandardLibrary.h>

typedef struct ImVec2
{
    f32 x;
    f32 y;
} ImVec2;
#define ImVec2(x, y) (ImVec2) { x, y }

static i32 ViewportIsActive = 0;
static i32 ViewportMovementEnabled = 1;
static ImVec2 ViewportPosition = ImVec2(0,0);
static ImVec2 ViewportSize = { 0, 0 };
static Framebuffer CurrentFramebuffer;
static Framebuffer DrawFramebuffer;
static SimpleWindow* VieportWindow;

static bool IsViewportFocused = false;
static bool IsViewportHovered = false;

typedef enum OPERATION
{
    ROTATE,
    TRANSLATE,
    SCALE
} OPERATION;
static OPERATION OperationType = TRANSLATE;
static i32 GizmoRotationKeyBinding = KEY_R;
static i32 GizmoTranslationKeyBinding = KEY_T;
static i32 GizmoScaleKeyBinding = KEY_S;
static i32 SnapKeyBinding = KEY_LEFT_CONTROL;

static CameraComponent* ViewportCamera;

/*
  NOTE(typedef): we inject cameracomponent like this for now,
  but in the near future we will be querying this every frame in the main loop
*/
void
viewport_register_layer(CameraComponent* camera)
{
    ViewportCamera = camera;

    Layer layer = {
	.Name = "Viewport Layer",
	.OnAttach = viewport_on_attach,
	.OnUpdate = viewport_on_update,
	.OnEvent = viewport_on_event,
	.OnDestoy = NULL,
    };

    application_push_layer(layer);
}

void
viewport_set_active()
{
    ViewportIsActive = 1;
}

i32
viewport_is_movement_enabled()
{
    return ViewportMovementEnabled;
}

static void
framebuffer_recreate(u32 w, u32 h)
{
    FramebufferSettings framebufferSettings = {
	.Width = w,
	.Height = h,
	.Attachments = { FramebufferType_Rgba, FramebufferType_Depth },
	.Count = 2,
	.SamplesCount = 0
    };
    CurrentFramebuffer = framebuffer_create(framebufferSettings);
    framebufferSettings.SamplesCount = 0;
    DrawFramebuffer = framebuffer_create(framebufferSettings);
}


void
viewport_on_attach()
{
    VieportWindow = application_get_window();

    framebuffer_recreate(VieportWindow->Width, VieportWindow->Height);
#define ImGuizmo_SetOrthographic(x)
    ImGuizmo_SetOrthographic(false);
}

void
viewport_on_update(f32 timestep)
{
    SimpleWindow* window = VieportWindow;
    CameraComponent* camera = ViewportCamera;
    CameraComponentSettings* set = &camera->Settings;

    if (!ViewportIsActive)
	return;

    if (ViewportMovementEnabled)
    {
	input_disable_cursor();

	if (input_is_key_pressed(KeyType_W))
	{
	    camera_component_move(camera, CameraMoveDirectionType_Forward, timestep);
	}
	if (input_is_key_pressed(KeyType_S))
	{
	    camera_component_move(camera, CameraMoveDirectionType_Backward, timestep);
	}
	if (input_is_key_pressed(KeyType_A))
	{
	    camera_component_move(camera, CameraMoveDirectionType_Left, timestep);
	}
	if (input_is_key_pressed(KeyType_D))
	{
	    camera_component_move(camera, CameraMoveDirectionType_Right, timestep);
	}

	f64 x, y;
	input_get_cursor_position(&x, &y);
	if (!f32_equal_epsilon(set->PrevX, x, 0.1f) || !f32_equal_epsilon(set->PrevY, y, 0.1f))
	{
	    //GINFO("Move Rotate: Prev { %f %f } New { %f %f }\n", set->PrevX, set->PrevY, x, y);
	    camera_component_rotate(camera, x, y, timestep);
	}

	if (camera->IsCameraMoved)
	{
	    //GINFO("Move Camera update\n");
	    camera_component_update(camera);
	}
    }
    else
    {
	input_enable_cursor();
    }
}

void
viewport_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->Modificator == MOD_CONTROL)
	{
	    if (keyEvent->KeyCode == GizmoRotationKeyBinding)
	    {
		OperationType = ROTATE;
		event->IsHandled = 1;
	    }
	    else if (keyEvent->KeyCode == GizmoTranslationKeyBinding)
	    {
		OperationType = TRANSLATE;
		event->IsHandled = 1;
	    }
	    else if (keyEvent->KeyCode == GizmoScaleKeyBinding)
	    {
		OperationType = SCALE;
		event->IsHandled = 1;
	    }
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowResized)
	{
	    WindowResizedEvent* wrEvent = (WindowResizedEvent*) event;
	    framebuffer_recreate(wrEvent->Width, wrEvent->Height);
	    renderer_set_viewport(wrEvent->Width, wrEvent->Height);
	}
	break;
    }

    }
}

Framebuffer*
viewport_get_current_framebuffer()
{
    return &CurrentFramebuffer;
}

Framebuffer*
viewport_get_draw_framebuffer()
{
    return &DrawFramebuffer;
}

i8
viewport_panel_is_movement_enabled()
{
    return ViewportMovementEnabled;
}
void
viewport_panel_enable_movement()
{
    ViewportMovementEnabled = 1;
    SimpleWindow* wnd = application_get_window();
    input_set_cursor_position(wnd->Width / 2, wnd->Height / 2);
}
void
viewport_panel_disable_movement()
{
    ViewportMovementEnabled = 0;
}


#if 0
void
viewport_on_ui()
{
    static bool isViewportVisible = 1;
    if (igBegin("Viewport", &isViewportVisible, ImGuiWindowFlags_None))
    {

	{ // do not update mouse coordinate on first load
	    static i32 firstLoad = 0;
	    if (firstLoad)
	    {
		if (igIsWindowFocused(ImGuiFocusedFlags_RootWindow))
		{
		    ViewportIsActive = 1;
		}
		else
		{
		    ViewportIsActive = 0;
		}
	    }
	    firstLoad = 1;
	}

	IsViewportFocused = igIsWindowFocused(ImGuiWindowFlags_None);
	IsViewportHovered = igIsWindowHovered(ImGuiWindowFlags_None);

	if (f32_equal(ViewportSize.x, 0.0f) && f32_equal(ViewportSize.y, 0.0f))
	{
	    igGetContentRegionAvail(&ViewportSize);
	}

	ImVec2 availReg;
	igGetContentRegionAvail(&availReg);
	if (!ImVec2_Equals(ViewportSize, availReg))
	{
	    ImVec2_Assign(ViewportSize, availReg);

	    FramebufferSettings framebufferSettings = {
		.Width = VieportWindow->Width,
		.Height = VieportWindow->Height,
		.Attachments = { FramebufferType_Rgba, FramebufferType_Depth },
		.Count = 2,
		.SamplesCount = 4
	    };
	    CurrentFramebuffer = framebuffer_create(framebufferSettings);
	    framebufferSettings.SamplesCount = 0;
	    DrawFramebuffer = framebuffer_create(framebufferSettings);
	}

	igImage((ImTextureID)DrawFramebuffer.ColorAttachments[0],
		ImVec2_(ViewportSize.x, ViewportSize.y),
		ImVec2_(0, 1), ImVec2_(1, 0),
		ImVec4_(1, 1, 1, 1), ImVec4_(1, 1, 1, 0));

	igGetWindowPos(&ViewportPosition);

	guizmo(SelectedEntityTransform, ViewportCamera);
    }

    igEnd();
}



#endif
