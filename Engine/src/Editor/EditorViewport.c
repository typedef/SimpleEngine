#include "EditorViewport.h"

#include <Event/Event.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <InputSystem/SimpleInput.h>
#include <InputSystem/KeyCodes.h>
#include <Utils/Types.h>
#include <Math/SimpleMath.h>

static i32 gViewportIsActive;
static i32 gViewportMovementEnabled = 1;

void
editor_viewport_activate()
{
    gViewportIsActive = 1;
    input_disable_cursor();
}
void
editor_viewport_disable()
{
    gViewportIsActive = 0;
}

void
editor_viewport_enable_movement()
{
    gViewportMovementEnabled = 1;
}
void
editor_viewport_disable_movement()
{
    gViewportMovementEnabled = 0;
}

void
editor_viewport_on_update(EditorViewportUpdate evUpdate)
{
    CameraComponent* pCamera = evUpdate.Camera;

    if (!gViewportIsActive)
	return;

    if (!gViewportMovementEnabled)
    {
	input_disable_cursor();
    }

    if (input_is_key_pressed(KeyType_W))
    {
	camera_component_move(pCamera, CameraMoveDirectionType_Forward, evUpdate.Timestep);
    }
    else if (input_is_key_pressed(KeyType_S))
    {
	camera_component_move(pCamera, CameraMoveDirectionType_Backward, evUpdate.Timestep);
    }

    if (input_is_key_pressed(KeyType_A))
    {
	camera_component_move(pCamera, CameraMoveDirectionType_Left, evUpdate.Timestep);
    }
    else if (input_is_key_pressed(KeyType_D))
    {
	camera_component_move(pCamera, CameraMoveDirectionType_Right, evUpdate.Timestep);
    }

    if (input_is_key_pressed(KeyType_Q))
    {
	camera_component_move(pCamera, CameraMoveDirectionType_Down, evUpdate.Timestep);
    }
    else if (input_is_key_pressed(KeyType_E))
    {
	camera_component_move(pCamera, CameraMoveDirectionType_Up, evUpdate.Timestep);
    }

    CameraComponentSettings* set = &pCamera->Settings;
    f64 x, y;
    input_get_cursor_position(&x, &y);
    if (!f32_equal_epsilon(set->PrevX, x, 0.1f) || !f32_equal_epsilon(set->PrevY, y, 0.1f))
    {
	//GINFO("Move Rotate: Prev { %f %f } New { %f %f }\n", set->PrevX, set->PrevY, x, y);
	camera_component_rotate(pCamera, x, y, evUpdate.Timestep);
    }

    if (pCamera->IsCameraMoved)
    {
	//GINFO("Move Camera update\n");
	camera_component_update(pCamera);
    }

}

void
editor_viewport_on_event(Event* event)
{
    switch (event->Category)
    {

    case EventCategory_Key:
    {
	KeyPressedEvent* keyEvent = (KeyPressedEvent*) event;
	if (keyEvent->Modificator == ModType_Control)
	{
	    /* if (keyEvent->KeyCode == GizmoRotationKeyBinding) */
	    /* { */
	    /*	OperationType = ROTATE; */
	    /*	event->IsHandled = 1; */
	    /* } */
	    /* else if (keyEvent->KeyCode == GizmoTranslationKeyBinding) */
	    /* { */
	    /*	OperationType = TRANSLATE; */
	    /*	event->IsHandled = 1; */
	    /* } */
	    /* else if (keyEvent->KeyCode == GizmoScaleKeyBinding) */
	    /* { */
	    /*	OperationType = SCALE; */
	    /*	event->IsHandled = 1; */
	    /* } */
	}

	break;
    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowResized)
	{
	    WindowResizedEvent* wrEvent = (WindowResizedEvent*) event;
	    //framebuffer_recreate(wrEvent->Width, wrEvent->Height);
	    //renderer_set_viewport(wrEvent->Width, wrEvent->Height);
	}
	break;
    }

    }
}
