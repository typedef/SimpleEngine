/* #ifndef SUI_H */
/* #define SUI_H */

/* #include <Utils/Types.h> */
/* #include <Event/Event.h> */

/* // BUG: delete this shit??? */
/* typedef struct SuiRect */
/* { */
/*     v2 Position; */
/*     v2 Size; */
/*     v4 Color; */
/* } SuiRect; */
/* typedef struct SuiText */
/* { */
/*     void* Buffer; */
/*     size_t Length; */
/*     i32 IsTemp; */
/*     v2 Position; */
/*     v2 Drawable; */
/* } SuiText; */


/* /\* */
/*   DOCS(typedef): Base Sui Types */
/* *\/ */
/* typedef i64 SuiPanelId; */
/* typedef i64 SuiWidgetId; */
/* #define GetSuiPanelId(label) ((SuiPanelId) ((u64)label)) */
/* #define GetSuiWidgetId(label) ((SuiWidgetId) ((u64)label)) */


/* typedef void (*GetDisplaySizeDelegate)(i32* width, i32* height); */
/* typedef v2 (*GetMousePositionDelegate)(); */



/* typedef struct WideString WideString; */

/* typedef struct SuiSettings */
/* { */
/*     GetDisplaySizeDelegate GetDisplaySize; */
/*     GetMousePositionDelegate GetMousePosition; */
/*     struct RuntimeCamera* Camera; */
/* } SuiSettings; */

/* typedef enum SuiKeys */
/* { */
/*     SuiKeys_Start = 0, */

/*     SuiKeys_Mouse_Left_Button, */
/*     SuiKeys_Mouse_Right_Button, */
/*     SuiKeys_Mouse_Middle_Button, */

/*     SuiKeys_Tab, */
/*     SuiKeys_Backspace, */

/*     SuiKeys_LeftArrow, */
/*     SuiKeys_RightArrow, */
/*     SuiKeys_UpArrow, */
/*     SuiKeys_DownArrow, */
/*     SuiKeys_Home, */
/*     SuiKeys_End, */

/*     SuiKeys_Delete, */

/*     SuiKeys_Count */
/* } SuiKeys; */

/* typedef enum SuiWidgetType */
/* { */
/*     SuiWidgetType_Button = 0, */
/*     SuiWidgetType_ButtonImage, */
/*     SuiWidgetType_ToggleButton, */
/*     SuiWidgetType_Text, */
/*     SuiWidgetType_Slider, */
/*     SuiWidgetType_DropDownList, */
/*     SuiWidgetType_SelectList, */
/*     SuiWidgetType_ColorPicker, */
/*     SuiWidgetType_Count */
/* } SuiWidgetType; */

/* typedef struct SuiWidget */
/* { */
/*     SuiWidgetId Id; */
/*     SuiWidgetType Type; */
/*     WideString* Label; */
/*     v2 Size; */
/*     i32 Integer; */
/*     f32 Float; */
/*     v4 Color; */
/*     u32 TextureId; */
/* } SuiWidget; */

/* typedef enum SuiLayoutType */
/* { */
/*     SuiLayoutType_VerticalStack = 0 */
/* } SuiLayoutType; */

/* typedef struct SuiLayoutData */
/* { */
/*     SuiLayoutType Type; */

/*     v2 CurrentPosition; */
/*     v2 RowSize; */

/* } SuiLayoutData; */


/* typedef struct SuiPanel */
/* { */
/*     SuiPanelId Id; */
/*     /\* DOCS(typedef): If Size == {0, 0}, then not fixed size *\/ */
/*     v2 Size; */
/*     v2 Position; */
/* } SuiPanel; */

/* typedef enum SuiKeyState */
/* { */
/*     SuiKeyState_None = 0, */
/*     SuiKeyState_Pressed, */
/*     SuiKeyState_Released, */
/* } SuiKeyState; */

/* typedef struct SuiKeyData */
/* { */
/*     SuiKeyState State; */
/*     /\* NOTE(typedef): Maybe later try analog values here *\/ */
/* } SuiKeyData; */

/* typedef enum SuiModeType */
/* { */
/*     SuiModeType_Insert = 0, */
/*     SuiModeType_Overwrite */
/* } SuiModeType; */

/* typedef struct SuiDrawData */
/* { */
/*     SuiRect* Rects; */
/*     SuiText* Texts; */
/*     v2 StartPosition; */
/*     v2 NextSize; */
/*     v2 DisplaySize; */

/*     v2 MousePosition; */

/*     i32 ActiveID; */
/*     i32 HotID; */

/*     i32 LastFocusedID; */
/*     v2 LastFocusedPosition; */
/*     v2 LastFocusedSize; */
/*     i32 LeftKeyWasPressed; */

/*     i32 FocusID; */
/*     i32 TextIndexPosition; */
/*     SuiModeType Mode; */

/*     i32 FontMaxHeight; */

/*     /\* Input *\/ */
/*     SuiKeyData KeyData[SuiKeys_Count]; */
/*     i32 CharTyped; */
/* //#define SUI_MAX_CHAR_TO_SAVE 20 */
/* //    i32 CharData[SUI_MAX_CHAR_TO_SAVE]; */
/* } SuiDrawData; */

/* typedef enum SuiColor */
/* { */
/*     SuiColor_Start = 0, */

/*     SuiColor_Button_Foreground, */
/*     SuiColor_Button_Background, */
/*     SuiColor_Button_Hovered, */
/*     SuiColor_Button_Clicked, */
/*     SuiColor_Button_Focused, */

/*     SuiColor_Slider_Foreground, */
/*     SuiColor_Slider_Background, */
/*     SuiColor_Slider_Hovered, */
/*     SuiColor_Slider_Clicked, */
/*     SuiColor_Slider_Control_Foreground, */
/*     SuiColor_Slider_Control_Background, */
/*     SuiColor_Slider_Control_Hovered, */
/*     SuiColor_Slider_Control_Clicked, */

/*     SuiColor_Input_Foreground, */
/*     SuiColor_Input_Background, */
/*     SuiColor_Input_Hovered, */
/*     SuiColor_Input_Clicked, */

/*     SuiColor_Checkbox_Background, */
/*     SuiColor_Checkbox_Hovered, */
/*     SuiColor_Checkbox_Clicked, */
/*     SuiColor_Checkbox_InsideBox, */

/*     SuiColor_Border_Focused, */

/*     SuiColor_Count */
/* } SuiColor; */

/* typedef enum SuiBorder */
/* { */
/*     SuiBorder_Focused = 0, */

/*     SuiBorder_Count */
/* } SuiBorder; */

/* typedef enum SuiSize */
/* { */
/*     SuiSize_Checkbox_Min, */
/*     SuiSize_Checkbox_Offset, */

/*     SuiSize_Count */
/* } SuiSize; */

/* typedef struct SuiStyle */
/* { */
/*     v4 Colors[SuiColor_Count]; */
/*     v2 Borders[SuiBorder_Count]; */
/*     v2 Sizes[SuiSize_Count]; */
/* } SuiStyle; */

/* typedef struct PlatformCallbacks */
/* { */
/*     GetDisplaySizeDelegate GetDisplaySize; */
/*     GetMousePositionDelegate GetMousePosition; */
/* } PlatformCallbacks; */

/* typedef struct SuiWidgetData */
/* { */
/*     SuiPanel Panel; */
/*     SuiWidget* Widgets; */
/*     SuiPanel* Panels; */
/* } SuiWidgetData; */

/* typedef struct SuiInstance */
/* { */
/*     i32 IsInitialized; */
/*     SuiDrawData DrawData; */
/*     SuiLayoutData LayoutData; */
/*     SuiStyle Style; */

/*     SuiWidgetData WidgetData; */

/*     PlatformCallbacks PlatformCallbacks; */
/* } SuiInstance; */

/* void sui_init(SuiSettings settings); */
/* void sui_begin_frame(); */
/* void sui_end_frame(); */

/* i32 sui_panel_begin(WideString* label); */
/* void sui_panel_end(); */

/* u8 sui_button(WideString* name, v2 size); */
/* u8 sui_slider_f32(WideString* name, v2 pos, f32* valuePtr, v2 minMax); */
/* u8 sui_input(i32* str, size_t maxLength, v2 pos); */
/* u8 sui_checkbox_label(WideString* str, i32* check, v2 pos); */
/* void sui_draw(); */

/* void sui_add_key_event(Event* event); */

/* /\* Core Utils *\/ */
/* u8 is_item_hovered(v2 pos, v2 size); */

/* /\* Additional Helpers Functions *\/ */
/* f32 sui_get_text_width_ext(i32* buffer, size_t length); */
/* f32 sui_get_text_width(WideString* str); */
/* f32 sui_get_text_offset(i32* str, size_t len, i32 textIndexPosition); */
/* f32 get_text_max_height(i32* text, i32 maxLength); */


/* /\* */
/*   DOCS(typedef): Public Sui Functions For Back-end */
/* *\/ */
/* SuiDrawData* sui_get_draw_data(); */
/* SuiWidgetData* sui_get_widget_data(); */
/* SuiLayoutData* sui_get_layout_data(); */

/* #endif // SUI_H */
