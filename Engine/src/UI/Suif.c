#include "Suif.h"

#include <stdlib.h>
#include <stdarg.h>

#include <Math/SimpleMath.h>
#include <Utils/SimpleStandardLibrary.h>
#include <InputSystem/SimpleInput.h>
#include <Math/SimpleMathIO.h>

/*
  DOCS(typedef): Global vars
*/
SuiInstance gSuiInstance;

/*
  DOCS(typedef): Base Preprocessors
*/
#define GetDrawData() (&gSuiInstance.DrawData)
#define GetInputData() (&gSuiInstance.InputData)
#define GetStyle() (&gSuiInstance.Style)
#define GetLayoutData() (&gSuiInstance.LayoutData)
#define DefaultGuard(name)			\
    ({						\
	if (!name)				\
	{					\
	    vguard_not_null(name);		\
	}					\
						\
	if (gSuiInstance.IsInitialized)		\
	{					\
	    vguard(gSuiInstance.IsInitialized); \
	}					\
    })
#define WidgetGuard(name)						\
    ({									\
	DefaultGuard(name);						\
									\
	vguard((array_count(gSuiInstance.PrevFrameCache.Panels) > 0) || gSuiInstance.PrevFrameCache.ActivePanelId != -1); \
	vguard(gSuiInstance.PrevFrameCache.CurrentPanelInd != -1);	\
    })

#define IsLayoutNotSet(layout) ({ layout == SuiLayoutType_None; })

#define IsKeyPressed(key)						\
    ({									\
	SuiKeyData keyData = GetInputData()->KeyData[key];		\
	((keyData.IsHandled!=1) && (keyData.State == SuiKeyState_Pressed)); \
    })
#define IsKeyReleased(key)						\
    ({									\
	SuiKeyData keyData = GetInputData()->KeyData[key];		\
	((keyData.IsHandled!=1) && (keyData.State==SuiKeyState_Released)); \
    })
#define HandleKey(key)					\
    ({							\
	GetInputData()->KeyData[key].IsHandled = 1;	\
    })
#define UnHandleKey(key)				\
    ({							\
	GetInputData()->KeyData[key].IsHandled = 0;	\
    })
/*
	 ####################################################
	     DOCS(typedef): Base Sui Types && Functions
	 ####################################################
*/
const char*
sui_state_to_string(SuiState suiState)
{
    switch (suiState)
    {
    case SuiState_None: return "None";
    case SuiState_Hovered: return "Hovered";
    case SuiState_Clicked: return "Clicked";
    case SuiState_Grabbed: return "Grabbed";
    }

    vguard(0);
    return "";
}


void
sui_state_print(SuiState suiState)
{
#if 0
    if (suiState == SuiState_None)
    {
	//GSUCCESS("State: 0 0 0 0\n");
	return;
    }

    GSUCCESS("State: H: %d C: %d G: %d\n",
	     (suiState & SuiState_Hovered),
	     (suiState & SuiState_Clicked),
	     (suiState & SuiState_Grabbed));
#endif
}

f32
sui_apply_offset(f32 fullSize, f32 marginValue, f32 value)
{
    f32 result = 0.0f;
    f32 marginValueAbs = f32_abs(marginValue);
    if (marginValueAbs < 1.0f)
    {
	return value + fullSize * marginValue;
    }
    else
    {
	return value + marginValue;
    }
}

v3
sui_apply_offset_v3(v3 value, v2 fullSize, v2 offset)
{
    value.X = sui_apply_offset(fullSize.X, -offset.X, value.X);
    value.Y = sui_apply_offset(fullSize.X, -offset.Y, value.Y);

    return value;
}

f32
sui_get_text_width(WideString wstr)
{
    f32 textWidth = font_get_string_width(gSuiInstance.pFont, wstr);
    return textWidth;
}

void
sui_get_text_metrics_ext(wchar* buffer, i32 length, f32* width, f32* height)
{
    font_get_string_metrics_ext(gSuiInstance.pFont, buffer, length, width, height);
}

f32
sui_get_char_xoffset(wchar wc)
{
    CharChar value = hash_get(gSuiInstance.pFont->CharTable, (i32) wc);
    return value.XOffset;
}

void
sui_get_text_metrics(WideString wstr, f32* width, f32* height)
{
    font_get_string_metrics(gSuiInstance.pFont, wstr, width, height);
}


/*
	 #####################################
	       DOCS(typedef): Sui Layout
	 #####################################
*/

void
sui_set_layout(SuiLayoutType layoutType)
{
    gSuiInstance.LayoutType = layoutType;
}

SuiLayoutData
sui_get_layout_data(SuiLayoutType layoutType, v3 pos)
{
    SuiLayoutData layoutData = {
	.LayoutType = layoutType,
	.NextPosition = pos,
	.ZOrder = pos.Z - 0.5f,
    };

    return layoutData;
}

v3
sui_layout_get_next_position(SuiLayoutData* pLayoutData)
{
    v3 pos = pLayoutData->NextPosition;
    ++pLayoutData->ZOrder;
    pLayoutData->NextPosition = v3_v2v(pLayoutData->NextPosition.XY, pLayoutData->ZOrder);

    return pos;
}

v3
sui_align_get_position(v2 fullSize, v3 widgetPosition, v2 widgetSize, SuiVerticalAlignType verticalAlign, SuiHorizontalAlignType horizontalAlign, v2 offset)
{
    v3 finalPos = v3_v2v(widgetPosition.XY, widgetPosition.Z - 0.1f);
    finalPos.X += offset.X;
    finalPos.Y += offset.Y;

    f32 freeSpaceX = fullSize.X - widgetSize.X;
    switch (horizontalAlign)
    {

    case SuiHorizontalAlignType_Left:
	// DOCS(typedef): By default it already alinged by left side
	break;

    case SuiHorizontalAlignType_Center:
    {
	f32 freeSpaceXFromEachSide = freeSpaceX / 2;
	finalPos.X += freeSpaceXFromEachSide;
	break;
    }

    case SuiHorizontalAlignType_Right:
    {
	finalPos.X += freeSpaceX;
	break;
    }

    }

    f32 freeSpaceY = fullSize.Y - widgetSize.Y;
    switch (verticalAlign)
    {

    case SuiVerticalAlignType_Top:
	// DOCS(typedef): By default it already alinged by top side
	break;

    case SuiVerticalAlignType_Center:
    {
	f32 freeSpaceYFromEachSide = freeSpaceY * 0.5;
	finalPos.Y -= freeSpaceYFromEachSide;
	break;
    }

    case SuiVerticalAlignType_Bot:
    {
	finalPos.Y -= freeSpaceY;
	break;
    }

    }

    return finalPos;
}

v3
sui_align_get_text_position(WideString* text, v3 widgetPosition, v2 widgetSize, SuiVerticalAlignType verticalAlign, SuiHorizontalAlignType horizontalAlign, f32 marginX)
{
    WideString textValue = *text;
    f32 w, h;
    font_get_string_metrics(gSuiInstance.pFont, textValue, &w, &h);

    f32 firstYOffset = font_get_first_char_y_offset(gSuiInstance.pFont, textValue);

    if ((w > widgetSize.X) && (horizontalAlign != SuiHorizontalAlignType_None))
    {
	GERROR("Some weird widget, with width less then text inside of it!\n");
    }

    if ((h > widgetSize.Y) && (verticalAlign != SuiVerticalAlignType_None))
    {
	GERROR("Some weird widget, with height less then text inside of it!\n");
    }

    if (marginX == 0)
    {
	marginX = 5.0f;
    }

    v3 finalPos = sui_align_get_position(widgetSize,
					 widgetPosition, v2_new(w, h),
					 verticalAlign,
					 horizontalAlign,
					 v2_new(marginX, firstYOffset));

    return finalPos;
}

/*
  NOTE(typedef): el could be not only widget but a panel itself
*/
v3
sui_layout_process_next_position(SuiLayoutData* pSuiLayoutData, v2 elSize)
{
    v3 nextPos = {};

    SuiStyle* pStyle = GetStyle();

    switch (pSuiLayoutData->LayoutType)
    {

    case SuiLayoutType_VerticalStack:
	nextPos = pSuiLayoutData->NextPosition;

	SuiStyleItem item  =
	    pStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];

	//NOTE(): -15.0f Widget.Padding
	pSuiLayoutData->NextPosition = v3_new(
	    pSuiLayoutData->NextPosition.X,
	    pSuiLayoutData->NextPosition.Y - elSize.Height - item.Offset.Padding.Y,
	    pSuiLayoutData->NextPosition.Z);
	break;

    default:
	vguard(0 && "Other layout not supported for now!");
	break;
    }

    return nextPos;
}

void
sui_layout_reset_for_panels(SuiLayoutData* pSuiLayoutData, SuiPanel* pSuiPanel)
{
    SuiStyle* pStyle = GetStyle();

    switch (pSuiLayoutData->LayoutType)
    {

    case SuiLayoutType_VerticalStack:
	SuiStyleItem item =
	    pStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];

	v3 pos = pSuiPanel->Position;
	pSuiLayoutData->NextPosition = v3_new(
	    pos.X + item.Offset.Padding.X,
	    pos.Y - item.Offset.Padding.Y,
	    pos.Z - 0.1);

	if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 0)
	{
	    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
	    pSuiLayoutData->NextPosition.Y -= hdrSize.Y;
	}

	break;

    default:
	vguard(0 && "Other layout not supported for now!");
	break;

    }

}


/*
	 #####################################
	       DOCS(typedef): Sui Style
	 #####################################
*/

void
sui_style_init(SuiStyle* pStyle, Font* pFontInfo)
{
#define PanelSize v2_new(150, 50)

    v4 darkColor = v4_new(0.11, 0.11, 0.11, 1);
    v4 veryDarkColor = v4_new(0.09, 0.09, 0.09, 1);
    v4 ultraDarkColor = v4_new(0.01f, 0.01f, 0.01f, 1);

    SuiStyleItem panel = {
	.Color = {
	    .Background = ultraDarkColor,
	    .Hovered = ultraDarkColor,
	    .Clicked = ultraDarkColor
	},
	.Size = {
	    .Min = PanelSize,
	    .Hovered = PanelSize,
	    .Clicked = PanelSize
	},
	.Offset = {
	    .Padding = v2_new(5.0f, 15.0f)
	}
    };
    pStyle->Items[SuiStyleItemType_Panel] = panel;

    //GERROR("Font.Size: %d\n", pFontInfo->MaxHeight);
    v2 panelHdrPadding = v2_new(0.015, 5);
    SuiStyleItem panelHeader = {
	.Color = {
	    .Background = v4_new(0.02, 0.03, 0.02, 1),
	    .Hovered = v4_new(0.04, 0.06, 0.04, 1),
	    .Clicked = v4_new(0.06, 0.09, 0.06, 1)
	},
	.Size = {
	    .Min = v2_new(1.0f, pFontInfo->MaxHeight + panelHdrPadding.Y),
	    .Hovered = v2_new(1.0f, 0.1f),
	    .Clicked = v2_new(1.0f, 0.1f)
	},
	.Offset = {
	    .Margin = v2_new(5, 0.1f),
	    //BUG(typedef): Use alignment without padding and margin
	    .Padding = panelHdrPadding
	}
    };
    pStyle->Items[SuiStyleItemType_Panel_Header] = panelHeader;

    SuiStyleItem panelHeaderClsBtn = {
	.Color = {
	    .Background = v4_new(0.70f, 0.20f, 0.20f, 1.0f),
	    .Hovered = v4_new(0.70f, 0.20f, 0.20f, 1.0f),
	    .Clicked = v4_new(0.09, 0.06, 0.06, 1.0f)
	},
	.Size = {
	    .Min = v2_new(0.1f, 0.7f),
	},
	.Offset = {
	    .Margin = v2_new(5.0f, panelHdrPadding.Y),
	    .VerticalAlign = SuiVerticalAlignType_Center,
	    .HorizontalAlign = SuiHorizontalAlignType_Right
	}
    };
    pStyle->Items[SuiStyleItemType_Panel_Header_Close_Button] = panelHeaderClsBtn;

    SuiStyleItem panelHeaderClsBtnX = {
	.Color = {
	    .Background = v4_new(1, 1, 1, 1),
	    .Hovered = v4_new(0, 0, 0, 1),
	    .Clicked = v4_new(0, 0, 0, 1)
	},
	.Size = {
	    .Min = v2_new(0.6f, 0.6f),
	},
	.Offset = {
	    .Margin = v2_new(5.0f, 0.07f),
	    .VerticalAlign = SuiVerticalAlignType_Center
	}
    };
    pStyle->Items[SuiStyleItemType_Panel_Header_Close_Button_X] = panelHeaderClsBtnX;

    SuiStyleItem panelBorder = {
	.Color = {
	    .Background = v4_new(0.06, 0.09, 0.06, 1),
	    .Clicked = v4_new(0.36, 0.48, 0.36, 1)
	},
	.Size = {
	    .Min = v2_new(0.5f, 0.5f),
	}
    };
    pStyle->Items[SuiStyleItemType_Panel_Border] = panelBorder;

    /*
      DOCS(typedef): Sui Widgets
    */
    SuiStyleItem btnWidget = {
	.Color = {
	    .Background = v4_new(0.1, 0.153, 0.134, 1),
	    .Hovered = v4_new(0.1, 0.203, 0.134, 1),
	    .Clicked = v4_new(0.1, 0.253, 0.134, 1)
	},
	.Size = {
	    .Min = v2_new(0.5f, 0.5f),
	},
	.Offset = {
	    .VerticalAlign = SuiVerticalAlignType_Top
	}
    };
    pStyle->Items[SuiStyleItemType_Widget_Button] = btnWidget;

    SuiStyleItem sliderF32Widget = {
	.Color = {
	    .Background = v4_new(0.1, 0.153, 0.134, 1),
	    .Hovered = v4_new(0.1, 0.203, 0.134, 1),
	    .Clicked = v4_new(0.1, 0.253, 0.134, 1)
	},
	.Size = {
	    .Min = v2_new(150, 35),
	},
	.Offset = {
	    .HorizontalAlign = SuiVerticalAlignType_Center
	}
    };
    pStyle->Items[SuiStyleItemType_Widget_Slider_F32] = sliderF32Widget;

    SuiStyleItem sliderF32Slider = {
	.Color = {
	    .Background = v4_new(0.02, 0.03, 0.02, 1),
	    .Hovered = v4_new(0.04, 0.06, 0.04, 1),
	    .Clicked = v4_new(0.06, 0.09, 0.06, 1)
	},
	.Size = {
	    .Min = v2_new(0.1f, 1.0f),
	}
    };
    pStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider] = sliderF32Slider;

    f32 checkboxRatio = pFontInfo->MaxHeight;
    v2 checkboxRectSize = v2_new(checkboxRatio, checkboxRatio);
    SuiStyleItem checkboxItem = {
	.Color = {
	    .Background = v4_new(0.02, 0.03, 0.02, 1),
	    .Hovered = v4_new(0.04, 0.06, 0.04, 1),
	    .Clicked = v4_new(0.1, 0.253, 0.134, 1)
	},
	.Size = {
	    .Min = checkboxRectSize,
	}
    };
    pStyle->Items[SuiStyleItemType_Widget_Checkbox] = checkboxItem;

    v2 inputSize = v2_new(10 * pFontInfo->MaxHeight, 1.125f * pFontInfo->MaxHeight);
    SuiStyleItem inputItem = {
	.Color = {
	    .Background = v4_new(0.02, 0.03, 0.02, 1),
	    .Hovered = v4_new(0.04, 0.06, 0.04, 1),
	    .Clicked = v4_new(0.1, 0.253, 0.134, 1)
	},
	.Size = {
	    .Min = inputSize,
	}
    };
    pStyle->Items[SuiStyleItemType_Widget_Input_String] = inputItem;

    v2 dropdownSize = v2_new(10 * pFontInfo->MaxHeight, 1.125f * pFontInfo->MaxHeight);
    SuiStyleItem dropdownItem = {
	.Color = {
	    .Background = v4_new(0.02, 0.03, 0.02, 1),
	    .Hovered = v4_new(0.04, 0.06, 0.04, 1),
	    .Clicked = v4_new(0.1, 0.253, 0.134, 1)
	},
	.Size = {
	    .Min = inputSize,
	}
    };
    pStyle->Items[SuiStyleItemType_Widget_Dropdown] = dropdownItem;


    SuiStyleItem layoutTypeVerticalStack = {
	.Offset = {
	    .Padding = v2_new(15, 15)
	}
    };
    pStyle->Items[SuiStyleItemType_LayoutType_VerticalStack] = layoutTypeVerticalStack;

}

v4
sui_style_get_color_by_state(SuiStyle* style, SuiStyleItemType itemType, SuiState state)
{
    SuiStyleColorItem colorItem = style->Items[itemType].Color;

    switch (state)
    {
    case SuiState_None   : return colorItem.Background;
    case SuiState_Hovered: return colorItem.Hovered;
    case SuiState_Clicked: return colorItem.Clicked;
    case SuiState_Grabbed: return colorItem.Clicked;
    }

    vguard(0);
    return v4_new(0,0,0,0);
}

SuiStyleSizeItem
sui_style_get_size(SuiStyle* style, SuiStyleItemType itemType)
{
    SuiStyleSizeItem sizeItem = style->Items[itemType].Size;
    return sizeItem;
}

SuiStyleOffsetItem
sui_style_get_offset(SuiStyle* style, SuiStyleItemType itemType)
{
    SuiStyleOffsetItem sizeItem = style->Items[itemType].Offset;
    return sizeItem;
}

void
sui_input_init(SuiInputData* pSuiInputData)
{
    pSuiInputData->MousePositionChanged = 0;
    pSuiInputData->LeftKeyWasPressed = 0;
    pSuiInputData->IsInputAlreadyHandled = 0;
    pSuiInputData->Mode = SuiModeType_Insert;
    pSuiInputData->CharTyped = 0;

    memset(pSuiInputData->KeyData, 0, SuiKeys_Count * sizeof(SuiKeyData));
}

/*
  #############################
  DOCS(typedef): Infrastructure
  #############################
*/
void
sui_init(SuiInstanceCreateInfo createInfo)
{
    gSuiInstance = (SuiInstance) { 0 };

    gSuiInstance.IsInitialized = 1;
    gSuiInstance.pFont = createInfo.pFont;
    gSuiInstance.Monitor.DisplaySize = createInfo.DisplaySize;

    { // NOTE(typedef): Init panels and widgets, frame cache
	gSuiInstance.PrevFrameCache.Panels = NULL;
	gSuiInstance.PrevFrameCache.PanelsTable = NULL;
	gSuiInstance.PrevFrameCache.FocusPanelInd = -1;
    }

    { // NOTE(typedef): Init layout
	gSuiInstance.LayoutType = SuiLayoutType_VerticalStack;

	gSuiInstance.PanelLayoutData.LayoutType = SuiLayoutType_None;

	gSuiInstance.PanelLayoutData.ZOrder = 1.0f;
	gSuiInstance.PanelLayoutData.NextPosition = v3_new(15.0f, gSuiInstance.Monitor.DisplaySize.Height, gSuiInstance.PanelLayoutData.ZOrder);
    }

    sui_style_init(&gSuiInstance.Style, gSuiInstance.pFont);
    sui_input_init(GetInputData());
}

i32
sui_begin_frame(SuiFrameInfo suiFrameInfo)
{
    gSuiInstance.FrameInfo = suiFrameInfo;
    //NOTE(typedef): mb safe it for next frame
    gSuiInstance.PrevFrameCache.ActivePanelId = -1;

    SuiDrawData* drawData = GetDrawData();
    drawData->HotID = 0;
    drawData->HotPanelId = 0;

    return 1;
}

static i32
_sui_panel_compare(const void* p1, const void* p2)
{
    i32 p1v = *((i32*) p1);
    i32 p2v = *((i32*) p2);

    i32 cap =
	table_capacity(gSuiInstance.PrevFrameCache.PanelsTable);
    if (p1v > cap || p2v > cap)
    {
	GWARNING("p1v > cap || p2v > cap!\n");
	return 0;
    }

    SuiPanel* pFirstPanel =
	&gSuiInstance.PrevFrameCache.PanelsTable[p1v].Value;
    SuiPanel* pSecondPanel =
	&gSuiInstance.PrevFrameCache.PanelsTable[p2v].Value;

    if (pFirstPanel->Position.Z < pSecondPanel->Position.Z)
    {
	return -1;
    }
    else if (pFirstPanel->Position.Z > pSecondPanel->Position.Z)
    {
	return 1;
    }

    return 0;
}

void
sui_panel_active_behaviour()
{
    // DOCS(typedef): Active panel behaviour (z-sorting)
    if (gSuiInstance.PrevFrameCache.ActivePanelId != -1)
    {
	i32 count = array_count(gSuiInstance.PrevFrameCache.Panels);

	for (i32 i = 0; i < count; ++i)
	{
	    SuiPanel* pSuiPanel = sui_panel_get_by_i(i);

	    if (sui_panel_is_active(pSuiPanel))
	    {
		pSuiPanel->Position.Z = 1.0f;
		continue;
	    }

	    ++pSuiPanel->Position.Z;
	}

	qsort(gSuiInstance.PrevFrameCache.Panels, count, sizeof(i32), _sui_panel_compare);

	f32 zorder = 2.0f;
	for (i32 i = 0; i < count; ++i)
	{
	    SuiPanel* pSuiPanel = sui_panel_get_by_i(i);

	    if (sui_panel_is_active(pSuiPanel) == 0)
	    {
		pSuiPanel->Position.Z = zorder;
		++zorder;
	    }
	}
    }

}

void
sui_end_frame()
{
    // DOCS(typedef): Clean Draw data active & hot ids
    SuiDrawData* drawData = GetDrawData();
    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
	drawData->ActiveID = 0;
	//NOTE(typedef): We can ignore this assign but i want this to be 0
	drawData->HotID = 0;

	drawData->ActivePanelId = 0;
	drawData->HotPanelId = 0;
    }

    /*
      DOCS(typedef): Update panels cache
    */
    //GINFO("Panels->Count: %d\n", array_count(gSuiInstance.PrevFrameCache.Panels));
    //array_clear(gSuiInstance.PrevFrameCache.Panels);

    {
	// DOCS(typedef): Process global input
	if (IsKeyPressed(SuiKeys_Tab))
	{
	    SuiPanel* pPanel = sui_panel_get_next_tab();
	    gSuiInstance.PrevFrameCache.ActivePanelId = pPanel->Id;
	}

	sui_panel_active_behaviour();

	for (i32 i = 0; i < array_count(gSuiInstance.PrevFrameCache.Panels); ++i)
	{
	    SuiPanel* pSuiPanel = sui_panel_get_by_i(i);
	    /* GINFO("pSuiPanel {%lld ", */
	    /*	  pSuiPanel->Id, */
	    /*	  pSuiPanel->Label->Length, */
	    /*	  pSuiPanel->Label->Buffer); */
	    /* wide_string_print_line((*pSuiPanel->Label)); */
	    /* printf(" }\n"); */
	    for (i32 w = 0; w < array_count(pSuiPanel->Widgets); ++w)
	    {
		SuiWidget suiWidget = pSuiPanel->Widgets[w];
		if (suiWidget.TempBuffer.Buffer != NULL)
		{
		    wide_string_destroy(suiWidget.TempBuffer);
		}
	    }
	    array_clear(pSuiPanel->Widgets);
	}

    }


    // DOCS(typedef): Clean Input Data
    SuiInputData* pSuiInputData = GetInputData();
    sui_input_init(pSuiInputData);
}

void
sui_auto_layout()
{

    for (i32 p = 0; p < array_count(gSuiInstance.PrevFrameCache.Panels); ++p)
    {
	i32 ind = gSuiInstance.PrevFrameCache.Panels[p];
	SuiPanel* pSuiPanel =
	    &gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;

	vguard(IsLayoutNotSet(pSuiPanel->LayoutType) && "Layout not set");

	SuiStyle* pSuiStyle = GetStyle();
	SuiStyleItem suiStyleItem =
	    pSuiStyle->Items[SuiStyleItemType_Panel_Header];
	SuiStyleItem verticalStackItem =
	    pSuiStyle->Items[SuiStyleItemType_LayoutType_VerticalStack];

	// DOCS(typedef): Recalc panel size, it should always fit widgets (or it shouldn't maybe make this flag dependent ~~~)
	{
	    /* Resize mech:
	       if (pSuiPanel->Size >= PANEL_MaxSize)
	       {
	       do smth
	       }
	       else
	       {
	       pSuiPanel->Size = CalcWidgetsHeightAndWidth();
	       }

	       Scrollable mech:

	    */
	    i32 widgetsCount = array_count(pSuiPanel->Widgets);
	    v2 neededSize = { 0, 0 };
	    if ((pSuiPanel->Flags & SuiPanelFlags_WithOutHeader) == 0)
	    {
		neededSize = sui_panel_get_header_size(pSuiPanel);
	    }
	    v2 layoutPad = verticalStackItem.Offset.Padding;
	    for (i32 w = 0; w < widgetsCount; ++w)
	    {
		SuiWidget* pSuiWidget = &pSuiPanel->Widgets[w];
		v2 widgetSize = pSuiWidget->Size;
		f32 wwidth = widgetSize.Width + 2 * layoutPad.X;
		if (wwidth > neededSize.Width)
		    neededSize.Width = wwidth;
		neededSize.Height += widgetSize.Height;
	    }

	    // BUG/NOTE(typedef): Bug with offset in procentage
	    neededSize.Height += (widgetsCount + 1) * layoutPad.Y;

	    if (neededSize.Width > pSuiPanel->Size.Width)
	    {
		pSuiPanel->Size.Width = neededSize.Width;
	    }
	    if (neededSize.Height > pSuiPanel->Size.Height)
	    {
		pSuiPanel->Size.Height = neededSize.Height;
	    }
	}

    }

}

i32
sui_is_item_hovered(v3 pos, v2 size)
{
    v2 mousePos = GetInputData()->MousePosition;
    if (mousePos.X >= pos.X
	&& mousePos.X <= (pos.X + size.Width)
	&& mousePos.Y <= pos.Y
	&& mousePos.Y >= (pos.Y - size.Height))
    {
	return 1;
    }

    return 0;
}

static SuiState
sui_is_item_pressed(v3 position, v2 size, SuiWidgetId id)
{
    SuiDrawData* pDrawData = GetDrawData();
    if (pDrawData->ActiveID == 0
	&& pDrawData->HotID == 0
	&& sui_is_item_hovered(position, size))
    {
	//GERROR("HotId: %lld\n", pDrawData->HotID);
	pDrawData->HotID = id;

	if (IsKeyPressed(SuiKeys_Mouse_Left_Button))
	{
	    pDrawData->ActiveID = id;
	    pDrawData->LeftKeyWasPressed = 1;

	    return SuiState_Clicked;
	}

	return SuiState_Hovered;
    }

    return SuiState_None;
}

/*
  ##############################
  DOCS(typedef): Sui Widgets
  ##############################
*/

static void
_sui_click_and_grab_state_machine(
    SuiInputData* pSuiInputData,
    SuiPanelId id,
    SuiState* pState,
    v3 pos,
    v2 size)
{
    // NOTE(): Only for panels
    SuiState state = *pState;

    // DOCS(typedef): Some Header State Logic
    if (state != SuiState_Grabbed
	&& state != SuiState_Clicked)
    {
	*pState = sui_is_item_pressed(pos, size, id);
    }
    else if (*pState == SuiState_Clicked)
    {
	*pState = SuiState_Grabbed;
	pSuiInputData->GrabPosition = v2_v3(pos);
	pSuiInputData->GrabMousePosition = pSuiInputData->MousePosition;
    }

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
	*pState = SuiState_None;
    }
}

v2
sui_panel_get_header_size(SuiPanel* pSuiPanel)
{
    SuiStyle* pSuiStyle = GetStyle();
    v2 hdrMin = sui_style_get_size(pSuiStyle,
				   SuiStyleItemType_Panel_Header).Min;
    v2 headerSize = v2_new(hdrMin.Width * pSuiPanel->Size.Width,
			   hdrMin.Height);

    return headerSize;
}

v3
sui_panel_get_header_position(SuiPanel* pSuiPanel)
{
    v3 hdrPos = v3_v2v(pSuiPanel->Position.XY, pSuiPanel->Position.Z - 0.1f);
    return hdrPos;
}

v3
sui_panel_get_header_text_position(SuiPanel* pSuiPanel)
{
    SuiHorizontalAlignType horizontalAlign = SuiHorizontalAlignType_Left;
    SuiVerticalAlignType verticalAlign = SuiVerticalAlignType_Center;

    v3 hdrPos = sui_panel_get_header_position(pSuiPanel);
    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v3 hdrTxtPos = sui_align_get_text_position(
	pSuiPanel->Label,
	hdrPos, hdrSize,
	verticalAlign, horizontalAlign,
	0.0f);
    return hdrTxtPos;
}

v2
sui_panel_get_close_button_size(v2 hdrSize)
{
    v2 minSize = sui_style_get_size(
	GetStyle(),
	SuiStyleItemType_Panel_Header_Close_Button).Min;

    v2 closeBtnSize = v2_mul(hdrSize, minSize);
    f32 value = MIN(closeBtnSize.X, closeBtnSize.Y);
    return v2_new(value, value);
}

v3
sui_panel_get_close_button_position(SuiPanel* pSuiPanel, v2 clsBtnSize)
{
    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleOffsetItem offsetItem =
	sui_style_get_offset(pSuiStyle,
			     SuiStyleItemType_Panel_Header_Close_Button);

    v2 hdrSize = sui_panel_get_header_size(pSuiPanel);
    v3 hdrPosition = sui_panel_get_header_position(pSuiPanel);
    v3 clsBtnPos = sui_align_get_position(
	hdrSize,
	hdrPosition, clsBtnSize,
	offsetItem.VerticalAlign,
	offsetItem.HorizontalAlign,
	v2_new(-5.0f, 0));

    return clsBtnPos;
}

void
sui_panel_get_close_button_x_coordinates(v2 fullSize, v3 startPos, v2 xMinSize, f32* psx, f32* psy, f32* pex, f32* pey, f32* pzv)
{
    f32 ratio = Min(xMinSize.X, xMinSize.Y);
    f32 reverseRatio = 1.0f - ratio;

    f32 sxd = fullSize.X * (reverseRatio / 2.0f); //45 * 0.1
    f32 syd = fullSize.Y * (reverseRatio / 2.0f); //38 * 0.1

    f32 sx = startPos.X + sxd;
    f32 sy = startPos.Y - syd;
    f32 ex = sx + ratio * fullSize.X;
    f32 ey = sy - ratio * fullSize.Y;
    f32 zv = startPos.Z - 0.5f;

    *psx = sx;
    *psy = sy;
    *pex = ex;
    *pey = ey;
    *pzv = zv;

}

v3
sui_panel_get_widget_offset(v3 panelPos, v2 headerSize, v2 padding)
{
    v3 pos = v3_new(panelPos.X + padding.X, panelPos.Y - headerSize.Y - padding.Y, panelPos.Z);
    return pos;
}

i32
sui_panel_is_active(SuiPanel* pSuiPanel)
{
    i32 isCurrentPanelActive =
	(pSuiPanel->Id == gSuiInstance.PrevFrameCache.ActivePanelId);
    return isCurrentPanelActive;
}

i32
sui_panel_is_active_or_focus(SuiPanel* pSuiPanel)
{
    i32 isTrue = (
	(pSuiPanel->Id == gSuiInstance.PrevFrameCache.ActivePanelId)
	||
	(pSuiPanel->Id == gSuiInstance.PrevFrameCache.FocusPanelInd)
	);
    return isTrue;
}

SuiPanel*
sui_panel_get_by_i(i32 i)
{
    i32 ind = gSuiInstance.PrevFrameCache.Panels[i];
    SuiPanel* pSuiPanel = &gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;
    return pSuiPanel;
}

SuiPanel
sui_panel_get_by_i_value(i32 i)
{
    i32 ind = gSuiInstance.PrevFrameCache.Panels[i];
    SuiPanel suiPanel = gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;
    return suiPanel;
}

void
sui_panel_item_make_active_by_state(SuiState state, SuiPanelId id)
{
    if (state == SuiState_Clicked || state == SuiState_Grabbed)
    {
	gSuiInstance.PrevFrameCache.ActivePanelId = id;
	//GINFO("Active: %d\n", id);
    }
}

SuiPanel*
sui_panel_get_next_tab()
{
    SuiPanelId activeId = gSuiInstance.PrevFrameCache.ActivePanelId;
    i32 focusInd = gSuiInstance.PrevFrameCache.FocusPanelInd;
    SuiInstance* pSuiInstance = sui_get_instance();
    i32 cnt = array_count(gSuiInstance.PrevFrameCache.Panels);

    ++focusInd;
    if (focusInd >= cnt || focusInd == 0)
    {
	if (cnt > 1)
	    focusInd = 1;
	else
	    focusInd = 0;
    }

    gSuiInstance.PrevFrameCache.FocusPanelInd = focusInd;
    //GINFO("Focus index=%d\n", gSuiInstance.PrevFrameCache.FocusPanelInd);

    SuiPanel* pSuiPanel = sui_panel_get_by_i(focusInd);
    return pSuiPanel;
}

void
sui_is_panel_blocked_by_other_panels(SuiPanel* pCurrentPanel)
{
    // DOCS(typedef): Check if panel is blocked by another panel with smaller zorder value
    i32 cnt = array_count(gSuiInstance.PrevFrameCache.Panels);
    //GINFO("cnt: %d\n", cnt);
    for (i32 i = 0; i < cnt; ++i)
    {
	SuiPanel* pOtherPanel = sui_panel_get_by_i(i);
	if (pOtherPanel == NULL)
	    continue;

	v3 pos = pOtherPanel->Position;

	/*
	  BUG(typedef): for some reason diff id for one panel, how??
	*/
	if (pOtherPanel->Id == pCurrentPanel->Id
	    || f32_equal(pos.Z, pCurrentPanel->Position.Z)
	    || pos.Z > pCurrentPanel->Position.Z)
	    continue;

	SuiState state;
	if (sui_is_item_hovered(pOtherPanel->Position, pOtherPanel->Size))
	{
	    pCurrentPanel->State = SuiState_None;
	    pCurrentPanel->HeaderState = SuiState_None;
	    pCurrentPanel->IsBlockedByOther = 1;
	    return;
	}
    }

    pCurrentPanel->IsBlockedByOther = 0;
}

i32
sui_panel_begin(WideString* label, i32* isVisible, SuiPanelFlags flags)
{
    DefaultGuard(label);

    if (*isVisible == 0)
    {
	return 0;
    }

    //TODO(typedef): Убрать все стили из виджетовб нужно сохранять состояние виджетов внутри { .SuiState State; } после чего проходится отдельным call'ом по типу sui_apply_style(); Минимальный размер виджета также должен устанавливаться sui_auto_layout(); Первый фрейм мы пропускаем и ничего не рендерим по идее.
    WideString labelValue = *label;

    // DOCS(typedef): GET SuiPanel from cache if NULL, create
    //DO_ONES(GINFO("HASH TABLE COUNT: %d \n", table_count(gSuiInstance.PrevFrameCache.PanelsTable)));

    v3 panelPos = sui_layout_get_next_position(&gSuiInstance.PanelLayoutData);

    // DOCS(typedef): Check panel registration in cache
    if (whash_geti(gSuiInstance.PrevFrameCache.PanelsTable, labelValue) == -1)
    {
	// DOCS(typedef): Register new Panel
	GINFO("Register new panel!\n");
	//vguard(table_count(gSuiInstance.PrevFrameCache.PanelsTable) < 4);
	SuiPanelId id = (SuiPanelId) label;
	vguard(id != -1);

	SuiStyle* pStyle = GetStyle();

	v2 suiPanelSize = sui_style_get_size(pStyle, SuiStyleItemType_Panel).Min;
	//sui_style_init();
	f32 textWidth = font_get_string_width(gSuiInstance.pFont, labelValue);
	suiPanelSize.Width = MAX(1.2 * textWidth, suiPanelSize.Width);

	SuiPanel newPanel = {
	    .Id = id,
	    .Label = label,
	    .Flags = flags,
	    .Position = panelPos,
	    .Size = suiPanelSize,
	    .Widgets = NULL
	};

	whash_put(gSuiInstance.PrevFrameCache.PanelsTable, labelValue, newPanel);
    }

    // DOCS(typedef): Get panel from prev cached data
    i64 ind = table_index(gSuiInstance.PrevFrameCache.PanelsTable);
    gSuiInstance.PrevFrameCache.CurrentPanelInd = ind;
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    pCurrentPanel->LayoutData = sui_get_layout_data(gSuiInstance.LayoutType, panelPos);

    v2 hdrSize = sui_panel_get_header_size(pCurrentPanel);

    SuiInputData* pSuiInputData = GetInputData();
    //GINFO("State: %s\n", sui_state_to_string(pCurrentPanel->State));

    sui_is_panel_blocked_by_other_panels(pCurrentPanel);
    if (pCurrentPanel->IsBlockedByOther)
    {
	goto panelEndStepLabel;
    }

    if ((pCurrentPanel->Flags & SuiPanelFlags_WithOutHeader) != 1)
    {
	sui_state_print(pCurrentPanel->HeaderState);

	// DOCS(typedef): Header close button
	v2 closeBtnSize = sui_panel_get_close_button_size(hdrSize);
	v3 closeBtnPos =
	    sui_panel_get_close_button_position(pCurrentPanel, closeBtnSize);
	pCurrentPanel->CloseButtonState = sui_is_item_pressed(closeBtnPos, closeBtnSize, pCurrentPanel->Id);
	//GERROR("ActiveID: %d\n", GetDrawData()->ActiveID);
	//GERROR("HotID: %d\n", GetDrawData()->HotID);
	if (pCurrentPanel->CloseButtonState == SuiState_Clicked)
	{
	    *isVisible = 0;
	    array_remove(gSuiInstance.PrevFrameCache.Panels, ind);
	    return 0;
	}
    }

panelEndStepLabel:
    // DOCS(typedef): Pushed panel
    i32 notExist = (array_index_of(gSuiInstance.PrevFrameCache.Panels, item == ind) == -1);
    if (notExist)
    {
	i32 cnt = array_count(gSuiInstance.PrevFrameCache.Panels);
	array_push(gSuiInstance.PrevFrameCache.Panels, ind);
	gSuiInstance.PrevFrameCache.FocusPanelInd = cnt;
    }

    sui_layout_reset_for_panels(&pCurrentPanel->LayoutData, pCurrentPanel);

    return 1;
}

#include <time.h>
void
sui_panel_end()
{
    SuiPanel* pCurrentPanel = sui_get_current_panel();

    if (pCurrentPanel != NULL && pCurrentPanel->IsBlockedByOther == 0)
    {
	SuiInputData* pSuiInputData = GetInputData();

	pCurrentPanel->WidgetIndex = 0;

	// DOCS(typedef): If widget is (hovered || clicked) ignore panel (hovered || clicked) mech
	for (i32 i = 0; i < array_count(pCurrentPanel->Widgets); ++i)
	{
	    SuiWidget suiWidget = pCurrentPanel->Widgets[i];
	    if (suiWidget.State == SuiState_Hovered
		|| suiWidget.State == SuiState_Clicked)
	    {
		return;
	    }
	}

	// DOCS(typedef): Header Mech Logic
	if ((pCurrentPanel->Flags & SuiPanelFlags_WithOutHeader) != 1)
	{
	    v2 hdrSize = sui_panel_get_header_size(pCurrentPanel);
	    _sui_click_and_grab_state_machine(
		pSuiInputData,
		pCurrentPanel->Id,
		&pCurrentPanel->HeaderState,
		pCurrentPanel->Position, hdrSize);

	    sui_panel_item_make_active_by_state(pCurrentPanel->HeaderState, pCurrentPanel->Id);
	}

	//DOCS() Panel Mech Logic
	if (pCurrentPanel->HeaderState == SuiState_None)
	{
	    _sui_click_and_grab_state_machine(
		pSuiInputData,
		pCurrentPanel->Id,
		&pCurrentPanel->State,
		pCurrentPanel->Position, pCurrentPanel->Size);

	    sui_panel_item_make_active_by_state(pCurrentPanel->State, pCurrentPanel->Id);
	}
	else
	{
	    //GERROR("Hdr state: %s\n", sui_state_to_string(pCurrentPanel->HeaderState));
	    pCurrentPanel->State = pCurrentPanel->HeaderState;
	}

	// DOCS(typedef): Panel drag&drop logic (Grab is Available for Header/Panel itself)
	if (pSuiInputData->MousePositionChanged && ((pCurrentPanel->Flags & SuiPanelFlags_Movable) != 0))
	{
	    i32 isHeaderGrabbed = (pCurrentPanel->HeaderState == SuiState_Grabbed);
	    i32 isHeaderDisabledAndPanelGrabbed = ((pCurrentPanel->Flags & SuiPanelFlags_WithOutHeader) && pCurrentPanel->State == SuiState_Grabbed);
	    if (isHeaderGrabbed || isHeaderDisabledAndPanelGrabbed)
	    {
		v2 mp = pSuiInputData->MousePosition;
		v2 gp = pSuiInputData->GrabPosition;
		v2 gmp = pSuiInputData->GrabMousePosition;
		v2 delta = v2_sub(mp, gmp);
		pCurrentPanel->Position = v3_v2v(v2_add(gp, delta), pCurrentPanel->Position.Z);

		SuiStyle* pStyle = GetStyle();
		v2 padding = sui_style_get_offset(pStyle, SuiStyleItemType_Panel).Padding;
	    }
	}

	if (pCurrentPanel->State == SuiState_Clicked
	    || pCurrentPanel->State == SuiState_Hovered
	    || pCurrentPanel->HeaderState == SuiState_Clicked
	    || pCurrentPanel->HeaderState == SuiState_Hovered)
	{
	    //HandleKey(key)
	    GetInputData()->IsInputAlreadyHandled = 1;
	    time_t t = time(NULL);
	    struct tm* aTm = localtime(&t);
	    //GINFO("Input is already handled [%d %d]!\n", aTm->tm_min, aTm->tm_sec);
	}
    }

    gSuiInstance.PrevFrameCache.CurrentPanelInd = -1;
}

static SuiWidget*
_sui_panel_add_widget(SuiPanel* pSuiPanel, SuiWidget suiWidget)
{
    i32 ind = array_count(pSuiPanel->Widgets);
    array_push(pSuiPanel->Widgets, suiWidget);
    SuiWidget* pWidget = &pSuiPanel->Widgets[ind];
    return pWidget;
}

static SuiWidgetId
_sui_widget_get_id(WideString* label, SuiPanel* pSuiPanel)
{
    SuiWidgetId suiWidgetId = pSuiPanel->WidgetIndex;
    ++pSuiPanel->WidgetIndex;
    return suiWidgetId;
}

#define sui_widget_ignore_input(pSuiWidget)				\
    {									\
	if (GetInputData()->IsInputAlreadyHandled || pCurrentPanel->IsBlockedByOther) \
	{								\
	    pSuiWidget->State = SuiState_None;				\
	    return SuiState_None;					\
	}								\
    }

SuiState
sui_button(WideString* label, v2 definedSize)
{
    WidgetGuard(label);

    SuiPanel* pCurrentPanel = sui_get_current_panel();
    WideString labelValue = *label;
    SuiWidgetId id = _sui_widget_get_id(label, pCurrentPanel);

    // DOCS(): CREATION stage
    v2 size;
    if (definedSize.Width > 0.0f && definedSize.Height > 0.0f)
    {
	size = definedSize;
    }
    else
    {
	f32 w, h;
	font_get_string_metrics(gSuiInstance.pFont, labelValue, &w, &h);
	size = v2_new(1.f * w, 1.5f * h);
    }

    SuiWidget newBtn = {
	.Id = id,
	.Type = SuiWidgetType_Button,
	.Label = label,
	.Size = size,
	.State = SuiState_None,
	.Position =
	sui_layout_process_next_position(&pCurrentPanel->LayoutData, size),
	.StillExist = 1
    };

    SuiWidget* pBtnWidget =
	_sui_panel_add_widget(pCurrentPanel, newBtn);

    /*
      NOTE(typedef): USAGE
    */
    sui_widget_ignore_input(pBtnWidget);

    pBtnWidget->State = sui_is_item_pressed(pBtnWidget->Position, pBtnWidget->Size, pBtnWidget->Id);

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
	pBtnWidget->State = SuiState_None;
    }

    return pBtnWidget->State;
} // sui_button

SuiState
sui_slider_f32(WideString* label, f32* valuePtr, v2 minMax)
{
    WidgetGuard(label);

    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleItem sliderStyle = pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32];
    SuiStyleItem slideStyle = pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider];

    SuiPanel* pCurrentPanel = sui_get_current_panel();
    WideString labelValue = *label;
    SuiWidgetId id = _sui_widget_get_id(label, pCurrentPanel);
    SuiDrawData* pDrawData = GetDrawData();

    v2 size = sliderStyle.Size.Min;
    f32 textWidth, textHeight;
    sui_get_text_metrics(labelValue, &textWidth, &textHeight);
    f32 value = MinMaxV2(*valuePtr, minMax);
    f32 offsetX = sui_slider_get_slide_offset_x(value, minMax);

    v3 pos = sui_layout_process_next_position(&pCurrentPanel->LayoutData, size);

    wchar buf[256];
    size_t bufLength = swprintf(buf, 256, L"%0.2f", value);

    SuiWidget newSlider = {
	.Id = id,
	.Type = SuiWidgetType_Slider_F32,
	.Label = label,
	.Size = size,
	.State = SuiState_None,
	.Position = pos,
	.StillExist = 1,
	.TempBuffer = wide_string_new(buf, bufLength),
	.FloatValues = v4_new(value, minMax.X, minMax.Y, 0)
    };
    // END OF NEW

    SuiWidget* pSliderWidget = _sui_panel_add_widget(pCurrentPanel, newSlider);

    sui_widget_ignore_input(pSliderWidget);

    // DOCS(typedef): Slide logic
    SuiState state = SuiState_None;
    v2 slideSize = sui_slider_get_slide_size(size);
    if (sui_is_item_hovered(pSliderWidget->Position, pSliderWidget->Size))
    {
	pSliderWidget->SubState = SuiState_Hovered;

	v3 slidePosition = sui_slider_get_slide_position(pos, offsetX);
	state = sui_is_item_pressed(slidePosition, slideSize, pSliderWidget->Id);
	pSliderWidget->State = state;
	pDrawData->HotID = pSliderWidget->Id;
    }

    if (pDrawData->ActiveID == id)
    {
	if (state == SuiState_None)
	{
	    pSliderWidget->State = SuiState_Grabbed;
	    //GINFO("SubState: %s\n", sui_state_to_string(pSliderWidget->SubState));
	    //GINFO("State: %s\n", sui_state_to_string(pSliderWidget->State));
	}

	SuiInputData* pInputData = GetInputData();
	f32 slideRatio = slideSize.X / size.X;
	f32 ratioX = (pInputData->MousePosition.X - pos.X) / size.X;
	f32 newValue = MinMaxV2(ratioX * minMax.Max, minMax);
	if (newValue != value)
	{
	    *valuePtr = newValue;
	}
    }

    if (IsKeyReleased(SuiKeys_Mouse_Left_Button))
    {
	pSliderWidget->State = SuiState_None;
    }

    return SuiState_None;
} // sui_slider_f32

SuiState
sui_checkbox(WideString* label, i32* valuePtr)
{
    WidgetGuard(label);

    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleItem styleItem = pSuiStyle->Items[SuiStyleItemType_Widget_Checkbox];

    SuiPanel* pCurrentPanel = sui_get_current_panel();
    WideString labelValue = *label;
    SuiWidgetId id = _sui_widget_get_id(label, pCurrentPanel);

    i32 value = *valuePtr;
    v2 size = styleItem.Size.Min;
    v3 position = sui_layout_process_next_position(
	&pCurrentPanel->LayoutData, size);

    SuiWidget newCheckbox = {
	.Id = id,
	.Type = SuiWidgetType_Checkbox,
	.Label = label,
	.Size = size,
	.State = SuiState_None,
	.Position = position,
	.StillExist = 1,
	.Flag0 = value
    };
    // END OF NEW

    SuiWidget* pCheckboxWidget = _sui_panel_add_widget(pCurrentPanel, newCheckbox);
    sui_widget_ignore_input(pCheckboxWidget);

    if (sui_is_item_hovered(position, size))
    {
	pCheckboxWidget->State = sui_is_item_pressed(position, size, pCheckboxWidget->Id);
	if (pCheckboxWidget->State == SuiState_Clicked)
	{
	    *valuePtr = !value;
	}
    }

    return pCheckboxWidget->State;
} // sui_ckeckbox

v2
sui_text_get_size(WideString str)
{
    f32 w, h;
    sui_get_text_metrics(str, &w, &h);
    v2 size = v2_new(w, h);
    return size;
}

v3
sui_text_get_position(v3 pos, v2 size, WideString str)
{
    f32 y0, y1;
    font_get_first_char_y(gSuiInstance.pFont, str, pos, &y0, &y1);
    f32 fheight = y1 - y0;

    f32 freeYSpace = size.Y - fheight;
    freeYSpace /= 2;
    //GINFO("FreeYSpace: %f [%f]\n", freeYSpace, fheight);
    v3 resPos = v3_new(pos.X, pos.Y-freeYSpace, pos.Z);
    return resPos;
}

SuiState
sui_text(WideString* format, ...)
{
    WidgetGuard(format);

    SuiStyle* pSuiStyle = GetStyle();
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    WideString formatValue = *format;
    SuiWidgetId id = _sui_widget_get_id(format, pCurrentPanel);

    va_list vardicList;
    va_start(vardicList, format);
    wchar buf[1024];
    i32 len = vswprintf(buf, 1024, format->Buffer, vardicList);
    va_end(vardicList);
    vguard(len != -1);
    WideString newString = wide_string_new(buf, len);

    v2 size = sui_text_get_size(newString);
    v3 position = sui_layout_process_next_position(
	&pCurrentPanel->LayoutData, size);
    position = sui_text_get_position(position, size, newString);
    SuiWidget newText = {
	.Id = id,
	.Type = SuiWidgetType_Text,
	//.Label = ,
	.Size = size,
	.State = SuiState_None,
	.Position = position,
	.StillExist = 1,
	.TempBuffer = newString
    };
    // END OF NEW

    SuiWidget* pTextWidget = _sui_panel_add_widget(pCurrentPanel, newText);
    sui_widget_ignore_input(pTextWidget);
    SuiState state = sui_is_item_pressed(pTextWidget->Position, pTextWidget->Size, pTextWidget->Id);

    return state;
} // sui_text

SuiState
sui_input_string(WideString* label, wchar* buf, i32 len, i32 maxLength)
{
    WidgetGuard(label);
    WidgetGuard(buf);

    SuiStyle* pSuiStyle = GetStyle();
    SuiDrawData* pDrawData = GetDrawData();
    SuiInputData* pInputData = GetInputData();
    SuiStyleSizeItem inputStyleItem = sui_style_get_size(GetStyle(), SuiStyleItemType_Widget_Input_String);
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    SuiWidgetId id = _sui_widget_get_id(label, pCurrentPanel);

    f32 w, h;
    sui_get_text_metrics_ext(buf, len, &w, &h);
    v2 size = inputStyleItem.Min;
    v3 position = sui_layout_process_next_position(
	&pCurrentPanel->LayoutData, size);
    SuiWidget newInput = {
	.Id = id,
	.Type = SuiWidgetType_InputString,
	.Size = size,
	.State = SuiState_None,
	.Position = position,
	.StillExist = 1
    };
    // END OF NEW

    SuiWidget* pInputWidget = _sui_panel_add_widget(pCurrentPanel, newInput);
    SuiState state = SuiState_None;
    if (pInputData->IsInputAlreadyHandled || pCurrentPanel->IsBlockedByOther)
    {
	pInputWidget->State = SuiState_None;
	goto inputStringEndLabel;
    }

    state = sui_is_item_pressed(pInputWidget->Position, pInputWidget->Size, pInputWidget->Id);

    i32 textInd = pInputData->TextIndexPosition;

    if (pInputWidget->Id == pDrawData->ActiveID)
    {
	pInputData->FocusID = pInputWidget->Id;
    }

    //GINFO("ActiveID: %d HotID: %d\n", pDrawData->ActiveID, pDrawData->HotID);
    if (pInputWidget->Id == pInputData->FocusID)
    {
	if (IsKeyPressed(SuiKeys_Backspace))
	{
	    if (len > 0 && pInputData->TextIndexPosition > 0)
	    {
		i32 copyLength = len - pInputData->TextIndexPosition;
		for (i32 i = 0; i < copyLength; ++i)
		{
		    i32 curPos = i + pInputData->TextIndexPosition;
		    buf[curPos - 1] = buf[curPos];
		}

		buf[len - 1] = 0;
		--pInputData->TextIndexPosition;
	    }
	}
	else if (IsKeyPressed(SuiKeys_LeftArrow))
	{
	    pInputData->TextIndexPosition = MinMax(pInputData->TextIndexPosition - 1, 0, len);
	}
	else if (IsKeyPressed(SuiKeys_RightArrow))
	{
	    pInputData->TextIndexPosition = MinMax(pInputData->TextIndexPosition + 1, 0, len);
	}
	else if (IsKeyPressed(SuiKeys_Home))
	{
	    pInputData->TextIndexPosition = 0;
	}
	else if (IsKeyPressed(SuiKeys_End))
	{
	    pInputData->TextIndexPosition = len;
	}
	else if (pInputData->CharTyped != 0 && (pInputData->TextIndexPosition < (maxLength - 1)) && (len < maxLength))
	{
	    if (pInputData->Mode == SuiModeType_Insert)
	    {
		i32 temp = buf[textInd + 1];
		for (i32 i = len; i > textInd; --i)
		{
		    buf[i] = buf[i - 1];
		}

		buf[textInd] = pInputData->CharTyped;
		++pInputData->TextIndexPosition;
	    }
	    else if (pInputData->Mode == SuiModeType_Overwrite)
	    {
		buf[pInputData->TextIndexPosition] = pInputData->CharTyped;
		++pInputData->TextIndexPosition;
	    }
	}

    }

inputStringEndLabel:
    pInputWidget->TempBuffer = wide_string(buf);
    return state;
} // sui_input

SuiState
sui_dropdown(WideString* label, wchar** texts, i32 textsCount, i32* selected)
{
    WidgetGuard(label);
    vguard_not_null(texts);

    SuiStyle* pSuiStyle = GetStyle();
    SuiDrawData* pDrawData = GetDrawData();
    SuiInputData* pInputData = GetInputData();
    SuiStyleSizeItem dropdownStyleItem = sui_style_get_size(GetStyle(), SuiStyleItemType_Widget_Dropdown);
    SuiPanel* pCurrentPanel = sui_get_current_panel();
    SuiWidgetId id = _sui_widget_get_id(label, pCurrentPanel);

    v2 size = dropdownStyleItem.Min;
    v3 position = sui_layout_process_next_position(
	&pCurrentPanel->LayoutData, size);
    SuiWidget newDropDown = {
	.Id = id,
	.Type = SuiWidgetType_Dropdown,
	.Size = size,
	.Position = position,
	.StillExist = 1
    };

    SuiWidget* pDropdownWidget = _sui_panel_add_widget(pCurrentPanel, newDropDown);

    sui_widget_ignore_input(pDropdownWidget);

    SuiState state = sui_is_item_pressed(pDropdownWidget->Position, pDropdownWidget->Size, pDropdownWidget->Id);
    if (state == SuiState_Clicked)
    {
	pDropdownWidget->State = state;
    }
    else if (state == SuiState_None)
    {

    }

    /*
      NOTE(typedef): Popup window need for this things
    */

    //GINFO("%s\n", sui_state_to_string(pDropdownWidget->State));

    return pDropdownWidget->State;
}

f32
sui_slider_get_slide_offset_x(f32 value, v2 minMax)
{
    SuiStyle* pSuiStyle = GetStyle();
    SuiStyleItem slideStyle = pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider];
    SuiStyleItem sliderStyle = pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32];

    v2 size = slideStyle.Size.Min;
    v2 sliderSizeMin = sliderStyle.Size.Min;
    v2 sliderSize = v2_new(sliderSizeMin.X * slideStyle.Size.Min.X,
			   sliderSizeMin.Y * sliderSizeMin.Y);
    f32 maxSliderX = sliderStyle.Size.Min.X - sliderSize.X;
    // 50 / (100 - 0) * size.X
    f32 percentage = value / (minMax.Max - minMax.Min);
    f32 offsetX = MinMax(percentage * maxSliderX, minMax.Min, maxSliderX);

    return offsetX;
}

v3
sui_slider_get_slide_position(v3 position, f32 offsetX)
{
    v3 slidePosition = v3_new(position.X + offsetX,
			      position.Y,
			      position.Z - 0.3f);
    return slidePosition;
}

v2
sui_slider_get_slide_size(v2 sliderSize)
{
    SuiStyleItem slideStyle =
	GetStyle()->Items[SuiStyleItemType_Widget_Slider_F32_Slider];
    v2 slideSize = v2_new(sliderSize.X * slideStyle.Size.Min.X, sliderSize.Y);
    return slideSize;
}

void
sui_checkbox_get_metrics(v3 widgetPos, v2 widgetSize, v3* checkPos, v2* checkSize)
{
    v2 size = v2_mulv(widgetSize, 0.55f);
    v3 pos  = v3_new(widgetPos.X + (widgetSize.X - size.X)/2, widgetPos.Y - (widgetSize.Y - size.Y)/2, widgetPos.Z-0.1f);

    *checkPos = pos;
    *checkSize = size;
}

v3
sui_input_get_text_position(WideString* text, v3 backgroundPos, v2 widgetSize)
{
    v3 pos = sui_align_get_text_position(
	text,
	v3_v2v(backgroundPos.XY, backgroundPos.Z - 0.1f), widgetSize,
	SuiVerticalAlignType_Center, SuiHorizontalAlignType_None,
	0.0f);
    return pos;
}
v2
sui_input_get_text_size(v2 backgroundSize)
{
    return v2_new(0.75f * backgroundSize.X, 0.75f * backgroundSize.Y);
}

/*
  #############################
  DOCS(typedef): Public Sui Functions For Back-end
  #############################
*/
SuiInstance*
sui_get_instance()
{
    return &gSuiInstance;
}

SuiDrawData*
sui_get_draw_data()
{
    return GetDrawData();
}

SuiInputData*
sui_get_input_data()
{
    return &gSuiInstance.InputData;
}

SuiStyle*
sui_get_style()
{
    return &gSuiInstance.Style;
}

/* SuiLayoutData* */
/* sui_get_layout_data() */
/* { */
/*     return GetLayoutData(); */
/* } */

SuiPanel*
sui_get_current_panel()
{
    i32 ind = gSuiInstance.PrevFrameCache.CurrentPanelInd;
    if (ind == -1)
    {
	return NULL;
    }

    SuiPanel* pSuiPanel = &gSuiInstance.PrevFrameCache.PanelsTable[ind].Value;
    vguard_not_null(pSuiPanel);

    return pSuiPanel;
}

void
sui_set_current_panel(SuiPanel suiPanel)
{
    i32 ind = gSuiInstance.PrevFrameCache.CurrentPanelInd;
    vguard(ind != -1 && "No active panel found!");
    gSuiInstance.PrevFrameCache.PanelsTable[ind].Value = suiPanel;
}
