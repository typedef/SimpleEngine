#include "UserInterface.h"

#include <Application/Application.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Graphics/Vulkan/VulkanSimpleRenderer.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include "Suif.h"
#include "SuifBackend.h"

static Renderer2d gRenderer2dContext;
static VsaFont gVsaFont;
static CameraComponent gCameraComponent;

static void
ui_on_event(Event* pEvent)
{
    if (pEvent->Type == EventType_WindowResized)
    {
	WindowResizedEvent* wre = (WindowResizedEvent*) pEvent;
	gCameraComponent.Orthographic.Right = wre->Width;
	gCameraComponent.Orthographic.Top   = wre->Height;
	camera_component_set_orthograhic(&gCameraComponent);
    }

    sui_backend_event(pEvent);
}

static void
ui_on_destroy()
{
    vsr_2d_destroy(&gRenderer2dContext);
}

void
ui_create()
{
    SimpleWindow* pWindow = application_get()->Window;

    CameraComponentCreateInfo createInfo = {
	.IsCameraMoved = 1,
	.Base = 0,

	.Settings = {
	    .Type = CameraType_OrthographicSpectator,
	    .Position = v3_new(0, 0, 1.0f),
	    .Front = v3_new(0, 0, -1),
	    .Up    = v3_new(0, 1,  0),
	    .Right = v3_new(1, 0,  0),
	    .Yaw = 0,
	    .Pitch = 0,
	    .PrevX = 0,
	    .PrevY = 0,
	    .SensitivityHorizontal = 0.35f,
	    .SensitivityVertical = 0.45f
	},

	.Orthographic = {
	    .Near = 0.1f,
	    .Far = 1000.0f,
	    .Left = 0,
	    .Right = (f32) pWindow->Width,
	    .Bot = 0,
	    .Top = (f32) pWindow->Height
	},

	.Spectator = {
	    .Speed = 5.0f,
	    .Zoom = 0
	}
    };

    gCameraComponent =
	camera_component_new_ext(createInfo);
    camera_component_update(&gCameraComponent);

    gVsaFont = vsa_font_info_create(asset_font("NotoSans.ttf"), 32, 1);
    Renderer2dSettings renderer2dSettings = (Renderer2dSettings) {
	.MaxCount = 30000,
	.Font = gVsaFont,
	.pCamera = &gCameraComponent.Base
    };
    vsr_2d_create(&gRenderer2dContext, &renderer2dSettings);

    SuiInstanceCreateInfo suiInstanceCreateInfo = {
	.DisplaySize = window_get_size_v2(pWindow),
	.pFont = &gVsaFont.Font
    };
    sui_init(suiInstanceCreateInfo);

    Layer layer = {
	.Name = "Simple User Interface",
	.OnEvent = ui_on_event,
	.OnDestoy = ui_on_destroy
    };
    application_push_layer(layer);
}

void
ui_begin()
{
    SuiInstance* instance = sui_get_instance();
    sui_backend_new_frame();

    sui_begin_frame((SuiFrameInfo) {.Timestep = application_get()->Stats.Timestep });
}

void
ui_end(i32 imageIndex)
{
    sui_auto_layout();
    sui_backend_draw(&gRenderer2dContext);
    vsr_2d_flush(&gRenderer2dContext, imageIndex);
    sui_end_frame();
}
