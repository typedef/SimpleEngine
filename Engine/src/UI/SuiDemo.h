#ifndef SUI_DEMO_H
#define SUI_DEMO_H

void
sui_demo_of_frontend_usage()
{
    static i32 i = 0;
    static f32 value = 350.0f;
    static v2 minMax = { 0, 500 };

    static v2 size = (v2) {0,0};
    //static v2 size = {50,50};
    //GINFO("Size: "); v2_print(size);

    static i32 isAnotherPanelVisible = 1;

    static SuiPanelFlags woHdrFlag = SuiPanelFlags_WithOutHeader | SuiPanelFlags_Movable;
    static WideString russianLanguage = WideString(L"Русский язык", 12);
    if (sui_panel_begin(&russianLanguage, &isAnotherPanelVisible, woHdrFlag))
    {
	SuiPanel* pCurrentPanel = sui_get_current_panel();
	SuiPanelFlags flags = pCurrentPanel->Flags;
	//GINFO("Flag: %d %d\n", (flags & SuiPanelFlags_WithOutHeader), (flags & SuiPanelFlags_Movable));

	static WideString buttonLabel = WideString(L"Ядерный взрыв *бабах*", 21);
	if (sui_button(&buttonLabel, v2_new(0,0)) == SuiState_Clicked)
	{
	    static i32 clickCounter = 0;
	    GINFO("It works %d!\n", clickCounter);
	    ++clickCounter;
	}

	static f32 sliderF32Value = 3.14f;
	static WideString f32SliderLabel = WideString(L"Лалала", 6);
	SuiState sliderState = sui_slider_f32(&f32SliderLabel, &sliderF32Value, v2_new(0, 100));

	static i32 someBoolValue = 0;
	static WideString checkboxWstr = WideString(L"Чекбокс", 7);
	SuiState checkBoxState =
	    sui_checkbox(&checkboxWstr, &someBoolValue);

	static WideString wText = WideString(L"Формат строка f:%0.2f d:%d t:%ls %s", 35);
	SuiState textState =
	    sui_text(&wText, 3.14f, 1020, L"Новый текст", "English text");

	static WideString clickedText = WideString(L"Число ПИ равно %f", 17);
	if (sui_text(&clickedText, 3.14f) == SuiState_Clicked)
	{
	    GINFO("SuiText as Button!\n");
	}

	SuiInputData* pSuiInputData = sui_get_input_data();
	v2 mp = pSuiInputData->MousePosition;
	static WideString mousePosText = WideString(L"Мышь %0.0f %0.0f", 10);
	sui_text(&mousePosText, mp.X, mp.Y);
    }
    sui_panel_end();

    static SuiPanelFlags otherFlag = SuiPanelFlags_WithOutHeader | SuiPanelFlags_Movable;
    static WideString otherPanel = WideString(L"Другая панель", 13);
    if (sui_panel_begin(&otherPanel, &isAnotherPanelVisible, otherFlag))
    {
	static WideString notMovableButton = WideString(L"Раз два три", 11);
	sui_button(&notMovableButton, v2_new(0,0));
	sui_button(&notMovableButton, v2_new(0,0));
	sui_button(&notMovableButton, v2_new(0,0));
    }
    sui_panel_end();

    static i32 isOtherPanelVisible = 1;
    static WideString notMovableLabel = WideString(L"Статичная панель", 16);
    if (sui_panel_begin(&notMovableLabel, &isOtherPanelVisible, SuiPanelFlags_Movable))
    {

	static WideString notMovableButton = WideString(L"Не сдвинуть даже, если очень постараться", 40);
	if (sui_button(&notMovableButton, v2_new(0,0)))
	{
	}


#if 0
	WideString dropdownLabel = {
	    .Buffer = L"Выберите что-то",
	    .Length = 15
	};
	wchar* texts[5] = {
	    L"Вариант 0",
	    L"Вариант 1",
	    L"Вариант 2",
	    L"Вариант 3",
	    L"Вариант 4"
	};
	static i32 selected = 0;
	SuiState dropdownState = sui_dropdown(&dropdownLabel, texts, 5, &selected);
#endif

    }
    sui_panel_end();

    static i32 isPanelVisible = 1;
    static WideString infoAboutProgram = WideString(L"Сведения о программе", 20);
    if (sui_panel_begin(&infoAboutProgram, &isPanelVisible, SuiPanelFlags_Movable))
    {
	static WideString inputLabel = WideString(L"Поле для ввода", 14);
	static wchar buf[128] = L"Введите значение";
	//SuiState inputState = sui_input_string(&inputLabel, buf, wcslen(buf), 128);

	static i32 counter = 1;
	static i32 isFlag = 0;
	if (!isFlag)
	{
	    static WideString buttonPlus = WideString(L"Добавить и", 10);
	    if (sui_button(&buttonPlus, v2_new(190, 50)) == SuiState_Clicked)
	    {
		GINFO("Button Plus: %d\n", counter);
		++counter;
		isFlag = !isFlag;
	    }
	}

	static WideString buttonMinus = WideString(L"Уменьшить", 9);
	if (sui_button(&buttonMinus, v2_new(190, 50)) == SuiState_Clicked)
	{
	    isFlag = 0;
	}

	static WideString someBtnText = WideString(L"Текст для кнопки", 16);
	for (i32 i = 0; i < counter; ++i)
	    if (sui_button(&someBtnText, v2_new(0, 0)) == SuiState_Clicked)
	    {
	    }

    }
    sui_panel_end();

}


#endif // SUI_DEMO_H
