#include "SuifBackend.h"

#include <Utils/stb_sprintf.h>

#include <Application/Application.h>
#include <InputSystem/SimpleInput.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Graphics/Vulkan/VulkanSimpleRenderer.h>
#include <UI/Suif.h>
#include <Event/Event.h>
#include <Utils/SimpleStandardLibrary.h>

void
sui_backend_new_frame()
{
    SuiInstance* suiInstance = sui_get_instance();
    suiInstance->Monitor.DisplaySize = window_get_size_v2(application_get()->Window);

    // DOCS(typedef): We need convert coordinates to OpenGL friendly
    suiInstance->InputData.MousePositionChanged = 0;
    v2 mousePosition = input_get_cursor_position_v2();
    v2 newMousePosition =
#if 0
	mousePosition;
#else
    v2_new(mousePosition.X, suiInstance->Monitor.DisplaySize.Height - mousePosition.Y);
#endif
    if (v2_not_equal_epsilon(newMousePosition, mousePosition, 0.00001f))
    {
	suiInstance->InputData.PrevMousePosition =
	    suiInstance->InputData.MousePosition;
	suiInstance->InputData.MousePosition = newMousePosition;
	suiInstance->InputData.MousePositionChanged = 1;
    }
    else
    {
	//v2_print(mousePosition);
	//v2_print(suiInstance->InputData.PrevMousePosition);
    }
}

void
sui_backend_draw_panel_header(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel)
{

    // DOCS(typedef): Draw header background
    v4 suiPanelHdrColor =
	sui_style_get_color_by_state(pSuiStyle,
				     SuiStyleItemType_Panel_Header,
				     suiPanel.HeaderState);
    v4 headerColor = v4_new(0.5, 0.5, 0.1, 1.0f);
    v2 hdrMin = sui_style_get_size(pSuiStyle,
				   SuiStyleItemType_Panel_Header).Min;
    v2 headerSize = sui_panel_get_header_size(&suiPanel);
    v3 headerPosition = sui_panel_get_header_position(&suiPanel);
    vsr_2d_submit_rect(renderer, headerPosition, headerSize,
		       suiPanelHdrColor);

    v4 suiPanelHdrLabelColor = v4_new(1, 1, 1, 1); //sui_style_get_color_by_state(pSuiStyle, SuiStyleItemType_Panel_Header, suiPanel.HeaderState);
    SuiStyleOffsetItem offsetItem =
	sui_style_get_offset(pSuiStyle, SuiStyleItemType_Panel_Header);

    v3 textPos = sui_panel_get_header_text_position(&suiPanel);

    // DOCS(typedef): Draw header text
    vsr_2d_submit_text_ext(
	renderer, suiPanel.Label->Buffer, suiPanel.Label->Length,
	1,
	textPos, headerSize,
	suiPanelHdrLabelColor);

    // DOCS(typedef): Draw close button
    //v2_print(headerSize);
    v2 clsBtnSize = sui_panel_get_close_button_size(headerSize);
    v3 clsBtnPos = sui_panel_get_close_button_position(&suiPanel, clsBtnSize);
    //v2_print(clsBtnSize);
    //v3_print(clsBtnPos);
    v4 clsBtnColor = sui_style_get_color_by_state(
	pSuiStyle,
	SuiStyleItemType_Panel_Header_Close_Button,
	suiPanel.CloseButtonState);
    vsr_2d_submit_circle_ext(
	renderer,
	clsBtnPos, clsBtnSize,
	clsBtnColor,
	1.5f, 0.2f, 16);

    //v2_print(clsBtnMinSize);
    v4 clsBtnXColor = sui_style_get_color_by_state(
	pSuiStyle,
	SuiStyleItemType_Panel_Header_Close_Button_X,
	suiPanel.CloseButtonState);
    v2 clsBtnXSize =
	pSuiStyle->Items[SuiStyleItemType_Panel_Header_Close_Button_X]
	.Size.Min;
    f32 sx, sy, ex, ey, zv;
    sui_panel_get_close_button_x_coordinates(
	clsBtnSize, clsBtnPos, clsBtnXSize,
	&sx, &sy, &ex, &ey, &zv);

    vsr_2d_submit_line(renderer,
		       v3_new(sx, sy, zv),
		       v3_new(ex, ey, zv),
		       2.0f,
		       clsBtnXColor);
    vsr_2d_submit_line(renderer,
		       v3_new(sx, ey, zv),
		       v3_new(ex, sy, zv),
		       2.0f,
		       clsBtnXColor);
}

void
sui_backend_draw_widget_button(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget btn)
{
    v4 color = sui_style_get_color_by_state(
	pSuiStyle, SuiStyleItemType_Widget_Button, btn.State);

    vsr_2d_submit_rect(renderer, btn.Position, btn.Size, color);

    v3 textPos = sui_align_get_text_position(
	btn.Label,
	btn.Position, btn.Size,
	SuiVerticalAlignType_Center,
	SuiHorizontalAlignType_Center, 0.0f);

    vsr_2d_submit_text_ext(
	renderer,
	btn.Label->Buffer, btn.Label->Length,
	1.0f,
	textPos,
	v2_new(1000, 1000),
	v4_new(1, 1, 1, 1));
}

void
sui_backend_draw_widget_slider_f32(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget sliderF32)
{
    v4 color = sui_style_get_color_by_state(
	pSuiStyle, SuiStyleItemType_Widget_Slider_F32, sliderF32.SubState);
    vsr_2d_submit_rect(renderer, sliderF32.Position, sliderF32.Size, color);

    v2 minMax = v2_new(sliderF32.FloatValues.Y, sliderF32.FloatValues.Z);
    SuiStyleItem sliderStyle = pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32];
    SuiStyleItem slideStyle = pSuiStyle->Items[SuiStyleItemType_Widget_Slider_F32_Slider];
    f32 value = MinMaxV2(sliderF32.FloatValues.X, minMax);
    v2 size = sliderStyle.Size.Min;
    f32 offsetX = sui_slider_get_slide_offset_x(value, minMax);
    v3 slidePosition = sui_slider_get_slide_position(sliderF32.Position, offsetX);
    v2 slideSize = sui_slider_get_slide_size(size);
    v4 slideColor = sui_style_get_color_by_state(
	pSuiStyle, SuiStyleItemType_Widget_Slider_F32_Slider, sliderF32.State);
    vsr_2d_submit_rect(renderer,
		       slidePosition, slideSize,
		       slideColor);

    v3 pos = v3_new(sliderF32.Position.X + sliderF32.Size.X, sliderF32.Position.Y, sliderF32.Position.Z);
    v3 textPos =
	sui_align_get_text_position(
	    &sliderF32.TempBuffer,
	    pos, sliderF32.Size,
	    SuiVerticalAlignType_Center, SuiHorizontalAlignType_None,
	    0.0f);
    vsr_2d_submit_text_ext(
	renderer,
	sliderF32.TempBuffer.Buffer, sliderF32.TempBuffer.Length,
	1.0f,
	textPos, v2_new(1000, 1000),
	v4_new(1, 1, 1, 1));
}

void
sui_backend_draw_widget_checkbox(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget checkbox)
{
    SuiStyleItem styleItem =
	pSuiStyle->Items[SuiStyleItemType_Widget_Checkbox];

    v4 color = sui_style_get_color_by_state(
	pSuiStyle, SuiStyleItemType_Widget_Checkbox, checkbox.State);
    vsr_2d_submit_rect(renderer, checkbox.Position, checkbox.Size, styleItem.Color.Background);

    if (checkbox.Flag0 == 1)
    {
	v3 checkPos;
	v2 checkSize;
	sui_checkbox_get_metrics(checkbox.Position,
				 checkbox.Size,
				 &checkPos, &checkSize);
	vsr_2d_submit_rect(renderer, checkPos, checkSize, styleItem.Color.Clicked);
    }

    v3 pos = v3_new(checkbox.Position.X + checkbox.Size.X, checkbox.Position.Y, checkbox.Position.Z);
    v3 textPos =
	sui_align_get_text_position(
	    checkbox.Label,
	    pos, checkbox.Size,
	    SuiVerticalAlignType_Center, SuiHorizontalAlignType_None,
	    0.0f);
    vsr_2d_submit_text_ext(
	renderer,
	checkbox.Label->Buffer, checkbox.Label->Length,
	1.0f,
	textPos, v2_new(1000, 1000),
	v4_new(1, 1, 1, 1));
}

void
sui_backend_draw_widget_text(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget text)
{
    /* SuiStyleItem styleItem = */
    /*	pSuiStyle->Items[SuiStyleItemType_Widget_Input_String]; */

    /* v4 color = sui_style_get_color_by_state( */
    /*	pSuiStyle, SuiStyleItemType_Widget_Input_String, text.State); */
    /* vsr_2d_submit_rect(renderer, text.Position, text.Size, styleItem.Color.Background); */

#if 0
    vsr_2d_submit_rect(renderer,
		       v3_v2v(text.Position.XY, text.Position.Z - 0.1f), text.Size,
		       v4_new(0, 1, 1, 1));
#endif
    vsr_2d_submit_text_ext(
	renderer,
	text.TempBuffer.Buffer, text.TempBuffer.Length,
	1.0f,

	v3_v2v(text.Position.XY, text.Position.Z - 0.3f), v2_new(1000, 1000),
	v4_new(1, 1, 1, 1));
}

void
sui_backend_draw_widget_input_string(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget input)
{
    SuiStyleItem styleItem =
	pSuiStyle->Items[SuiStyleItemType_Widget_Input_String];

    v4 color = sui_style_get_color_by_state(
	pSuiStyle, SuiStyleItemType_Widget_Input_String, input.State);
    vsr_2d_submit_rect(renderer,
		       input.Position, input.Size,
		       styleItem.Color.Background);

    SuiInputData* pSuiInputData = (&sui_get_instance()->InputData);
    const i32 MAGIC = 19;
    float marginX = 5.0f;
    i32 diffInd = 0;
    if (pSuiInputData->TextIndexPosition > MAGIC)
    {
	diffInd = pSuiInputData->TextIndexPosition - MAGIC;
    }

    if (pSuiInputData->FocusID == input.Id)
    {
	vguard(pSuiInputData->TextIndexPosition >= 0);

	f32 w, h;
	sui_get_text_metrics_ext(input.TempBuffer.Buffer, Min(pSuiInputData->TextIndexPosition, MAGIC), &w, &h);

	wchar indCh =
	    input.TempBuffer.Buffer[pSuiInputData->TextIndexPosition];
	f32 xoffset = 0.0f;
	if (pSuiInputData->TextIndexPosition > 0)
	    xoffset = sui_get_char_xoffset(indCh) - 1.0f;
	//GINFO("Char: %lc %f\n", indCh, xoffset);
	v3 inputPos = v3_new(input.Position.X + w + xoffset + marginX, input.Position.Y, input.Position.Z - 0.4f);
	vsr_2d_submit_rect(renderer,
			   inputPos,
			   v2_new(2, input.Size.Y),
			   v4_new(1, 0, 0, 1)
			   /* styleItem.Color.Background */);

    }

    //GINFO("Length: %d\n", input.TempBuffer.Length);


    vsr_2d_submit_text_ext(
	renderer,
	input.TempBuffer.Buffer + diffInd, input.TempBuffer.Length,
	1.0f,
	sui_input_get_text_position(&input.TempBuffer, input.Position, input.Size),
	input.Size,
	v4_new(1, 1, 1, 1));
}

void
sui_backend_draw_widget_dropdown(Renderer2d* renderer, SuiStyle* pSuiStyle, SuiPanel suiPanel, SuiWidget dropdown)
{
    SuiStyleItem styleItem =
	pSuiStyle->Items[SuiStyleItemType_Widget_Dropdown];

    v4 color = sui_style_get_color_by_state(
	pSuiStyle, SuiStyleItemType_Widget_Dropdown, dropdown.State);

    vsr_2d_submit_rect(renderer,
		       dropdown.Position, dropdown.Size,
		       color);

}

void
sui_backend_draw_panel(Renderer2d* renderer, SuiPanel suiPanel, i32 isActive)
{
    SuiStyle* pSuiStyle = sui_get_style();

    // DOCS(typedef): Draw panel window
    v4 suiPanelColor = sui_style_get_color_by_state(pSuiStyle, SuiStyleItemType_Panel, suiPanel.State);
    vsr_2d_submit_rect(renderer, suiPanel.Position, suiPanel.Size, suiPanelColor);

    // DOCS(typedef): Draw panel border
    v4 borderColor;
    SuiStyleItem borderStyle = pSuiStyle->Items[SuiStyleItemType_Panel_Border];
    if (isActive)
    {
	borderColor = borderStyle.Color.Clicked;
    }
    else
    {
	borderColor = borderStyle.Color.Background;
    }
    vsr_2d_submit_empty_rect(renderer,
			     v3_v2v(suiPanel.Position.XY, suiPanel.Position.Z - 0.3f),
			     suiPanel.Size,
			     borderStyle.Size.Min.X,
			     borderColor);

    // DOCS(typedef): Draw header
    if ((suiPanel.Flags & SuiPanelFlags_WithOutHeader) == 0)
	sui_backend_draw_panel_header(renderer, pSuiStyle, suiPanel);

    // DOCS(typedef): Draw widgets
    DO_ONES(GINFO("Widgets Cnt: %d\n", array_count(suiPanel.Widgets)););
    v4 btnColor = v4_new(0.1, 0.253, 0.134, 1);

    for (i32 w = 0; w < array_count(suiPanel.Widgets); ++w)
    {
	SuiWidget suiWidget = suiPanel.Widgets[w];

	switch (suiWidget.Type)
	{
	case SuiWidgetType_Button:
	    sui_backend_draw_widget_button(renderer, pSuiStyle, suiPanel, suiWidget);
	    break;

	case SuiWidgetType_Slider_F32:
	    sui_backend_draw_widget_slider_f32(renderer, pSuiStyle, suiPanel, suiWidget);
	    break;

	case SuiWidgetType_Checkbox:
	    sui_backend_draw_widget_checkbox(renderer, pSuiStyle, suiPanel, suiWidget);
	    break;

	case SuiWidgetType_Text:
	    sui_backend_draw_widget_text(renderer, pSuiStyle, suiPanel, suiWidget);
	    break;

	case SuiWidgetType_InputString:
	    sui_backend_draw_widget_input_string(renderer, pSuiStyle, suiPanel, suiWidget);
	    break;

	case SuiWidgetType_Dropdown:
	    sui_backend_draw_widget_dropdown(renderer, pSuiStyle, suiPanel, suiWidget);
	    break;

	default:
	    GERROR("No backend for widget of type: %d\n", suiWidget.Type);
	    vguard(0);
	    break;
	}

    }
}

void
sui_backend_draw(Renderer2d* renderer)
{
    SuiDrawData* drawData = sui_get_draw_data();
    SuiInstance* suiInstance = sui_get_instance();
    //SuiLayoutData* suiLayoutData = sui_get_layout_data();

    SuiPanelKeyValue* panelsTable = suiInstance->PrevFrameCache.PanelsTable;
    i64 panelsCount = array_count(suiInstance->PrevFrameCache.Panels);

    if (panelsCount <= 0)
    {
	//DOCS(typedef): Clear all submited data with that
	vsr_2d_submit_rect(renderer, v3_new(0, 0, 10000), v2_new(100,100), v4_new(0,0,0,0));
    }
    else
    {
	for (i32 p = 0; p < panelsCount; ++p)
	{
	    SuiPanel suiPanel =
		sui_panel_get_by_i_value(p);

	    //GERROR("Panel Label: %ls Z: %f\n", suiPanel.Label->Buffer, suiPanel.Position.X, suiPanel.Position.Y, suiPanel.Position.Z);
	    sui_backend_draw_panel(renderer, suiPanel, (p == 0));
	}
    }

}

void
sui_backend_event(Event* event)
{
    SuiInputData* suiInputData = sui_get_input_data();

    SuiKeyState suiKeyState;
    switch (event->Type)
    {
    case EventType_KeyRealeased:
    case EventType_MouseButtonReleased:
	suiKeyState = SuiKeyState_Released;
	break;

    case EventType_KeyPressed:
    case EventType_MouseButtonPressed:
	suiKeyState = SuiKeyState_Pressed;
	break;

    default:
	suiKeyState = SuiKeyState_None;
	break;
    }

    switch (event->Category)
    {

    case EventCategory_Key:
    {
	KeyReleasedEvent* kre = (KeyReleasedEvent*) event;
	KeyPressedEvent* kpe = (KeyPressedEvent*) event;

	if (event->Type == EventType_CharTyped)
	{
	    CharEvent* ce = (CharEvent*) event;
	    suiInputData->CharTyped = ce->Char;
	    break;
	}

	switch (kre->KeyCode)
	{
	case KeyType_Tab:
	    suiInputData->KeyData[SuiKeys_Tab].State = suiKeyState;

	    //TODO(typedef): Itegrate it inside Suif.c
	    SuiInstance* pSuiInstance = sui_get_instance();
	    i32* panels = pSuiInstance->PrevFrameCache.Panels;
	    i32 cnt = array_count(panels);
	    for (i32 i = 0; i < cnt; ++i)
	    {
		SuiPanel* pSuiPanel = sui_panel_get_by_i(i);
		if (pSuiPanel == NULL)
		    continue;
		if (pSuiPanel->Id == pSuiInstance->PrevFrameCache.ActivePanelId)
		{
		    if (i != (cnt - 1))
		    {
			pSuiInstance->PrevFrameCache.ActivePanelId = i + 1;
		    }
		    else
		    {
			pSuiInstance->PrevFrameCache.ActivePanelId = 0;
		    }

		    break;
		}
	    }

	    event->IsHandled = 1;

	    break;

	case KeyType_Backspace:
	    suiInputData->KeyData[SuiKeys_Backspace].State = suiKeyState;
	    break;
	case KeyType_Delete:
	    suiInputData->KeyData[SuiKeys_Delete].State = suiKeyState;
	    break;

	case KeyType_Left:
	    suiInputData->KeyData[SuiKeys_LeftArrow].State = suiKeyState;
	    break;
	case KeyType_Right:
	    suiInputData->KeyData[SuiKeys_RightArrow].State = suiKeyState;
	    break;
	case KeyType_Up:
	    suiInputData->KeyData[SuiKeys_UpArrow].State = suiKeyState;
	    break;
	case KeyType_Down:
	    suiInputData->KeyData[SuiKeys_DownArrow].State = suiKeyState;
	    break;
	case KeyType_Home:
	    suiInputData->KeyData[SuiKeys_Home].State = suiKeyState;
	    break;
	case KeyType_End:
	    suiInputData->KeyData[SuiKeys_End].State = suiKeyState;
	    break;

	}

	break;
    }

    case EventCategory_Mouse:
    {
	if (event->Type != EventType_MouseButtonPressed
	    && event->Type != EventType_MouseButtonReleased)
	{
	    break;
	}
	if (event->Type == EventType_MouseMoved)
	{
	    //suiInputData->MousePositionChanged = 1;
	    break;
	}

	MouseButtonEvent* mbe = (MouseButtonEvent*) event;
	switch (mbe->MouseCode)
	{
	case MouseButtonType_Left:
	    suiInputData->KeyData[SuiKeys_Mouse_Left_Button].State = suiKeyState;
	    break;

	case MouseButtonType_Right:
	    suiInputData->KeyData[SuiKeys_Mouse_Right_Button].State = suiKeyState;
	    break;

	case MouseButtonType_Middle:
	    suiInputData->KeyData[SuiKeys_Mouse_Middle_Button].State = suiKeyState;
	    break;
	}
	break;

    }

    case EventCategory_Window:
    {
	if (event->Type == EventType_WindowResized)
	{
	    GINFO("[sui] Resized!\n");
	    sui_backend_window_resized();
	}

	break;
    }

    break;
    }

}

void
sui_backend_window_resized()
{
    SuiInstance* suiInstance = sui_get_instance();
    suiInstance->Monitor.PrevDisplaySize = suiInstance->Monitor.DisplaySize;
    suiInstance->Monitor.DisplaySize = window_get_size_v2(application_get()->Window);

    v2 prevDispSize = suiInstance->Monitor.PrevDisplaySize;
    v2 dispSize = suiInstance->Monitor.DisplaySize;

    f32 ratioX = (prevDispSize.X / dispSize.X);
    f32 multiplierX;
    if (prevDispSize.X == dispSize.X)
    {
	multiplierX = 1.0f;
    }
    else if (prevDispSize.X > dispSize.X)
    {
	multiplierX = 1.0f + ratioX;
    }
    else
    {
	multiplierX = 1.0f - ratioX;
    }

    //GWARNING("multiplierX: %f\n", multiplierX);

    //NOTE(typedef): resize all panels & widgets
    /* SuiPanelKeyValue* panelsTable = suiInstance->PrevFrameCache.PanelsTable; */
    /* i64 panelsCount = array_count(suiInstance->PrevFrameCache.Panels); */
    /* for (i32 p = 0; p < panelsCount; ++p) */
    /* { */
    /*	i32 ind = suiInstance->PrevFrameCache.Panels[p]; */
    /*	SuiPanel* pSuiPanel = &panelsTable[ind].Value; */

    /*	pSuiPanel->Position.X = pSuiPanel->Position.X * multiplierX; */
    /*	pSuiPanel->Size.X = pSuiPanel->Size.X * multiplierX; */

    /*	for (i32 w = 0; w < array_count(pSuiPanel->Widgets); ++w) */
    /*	{ */
    /*	    SuiWidget* pSuiWidget = &pSuiPanel->Widgets[w]; */

    /*	    pSuiWidget->Position.X = pSuiWidget->Position.X * multiplierX; */
    /*	    pSuiWidget->Size.X = pSuiWidget->Size.X * multiplierX; */
    /*	} */

    /* } */

}

/*

  float myPow(float x, int n) {
  float mul = x;

  for (int i = 1; i < n; ++i) {
  mul *= x;
  }

  return mul;
  }

  vec4 circle(vec2 uv, int multiplier, float thikness, vec4 color) {
  float xn = myPow(uv.x, multiplier);
  float yn = myPow(uv.y, multiplier);
  float d = 1.0 - (xn+yn);
  float fade = 0.009;

  vec4 col = vec4(smoothstep(0.0, fade, d));
  float thicknessRatio = smoothstep(thikness, thikness - fade, d);
  col *= vec4(thicknessRatio) * color;

  return col;
  }

  void mainImage( out vec4 fragColor, in vec2 fragCoord )
  {
  vec2 uv = fragCoord/iResolution.xy * 2.0 - 1.0; // <-1,1>
  uv.x *= (iResolution.x / iResolution.y); // <-1.69, 1.69>

  vec4 col = circle(uv, 14, 1.1999922, vec4(0.0, 1.0, 1.0, 1.0));

  fragColor = col;
  }*/
