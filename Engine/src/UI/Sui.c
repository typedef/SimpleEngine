/* #include "Sui.h" */

/* #include <Utils/SimpleStandardLibrary.h> */
/* #include <Graphics/UIRenderer.h> */
/* #include <Graphics/KeyCodes.h> */
/* #include <Math/SimpleMathIO.h> */

/* /\* */
/*   widgets to implement: */
/*   * sui_button("Some string", optional: {50, 50}) */
/*   * sui_button_image("Some string", optional: {50, 50}) */
/*   * sui_toggle_button("Some string || null", optional: {50, 50}, optional: statesCount) */
/*   * sui_text("Some label text with integer inside %d", 100); */
/*   * sui_slider("some label || null", &floatVal); */
/*   * sui_drop_down_list("some label || null", &listValues, listCount: 5) */
/*   * sui_select_list("some label || null", &listValues, listCount: 5) */
/*   * sui_color_picker("some label || null", &colorV4) */


/*   steps: */
/*   1. set widgets + data */
/*   [all] */
/*   * WideString*    [+] */
/*   * v2             [+] */
/*   * i32            [-] */
/*   * WideString*    [-] */
/*   * v4             [-] */
/*   * Texture2D */

/*   Diff data can be solved by using hierarchy of Widget */
/*   OR use BIG struct SuiWidget with everything inside. */

/*   typedef enum SuiWidgetType */
/*   { */
/*   SuiWidgetType_Button = 0, */
/*   SuiWidgetType_ButtonImage, */
/*   SuiWidgetType_ToggleButton, */
/*   SuiWidgetType_Text, */
/*   SuiWidgetType_Slider, */
/*   SuiWidgetType_DropDownList, */
/*   SuiWidgetType_SelectList, */
/*   SuiWidgetType_ColorPicker, */
/*   SuiWidgetType_Count */
/*   } SuiWidgetType; */

/*   typedef struct SuiWidget */
/*   { */
/*   SuiWidgetType Type; */
/*   WideString* Label; */
/*   v2 Size; */
/*   i32 Integer; */
/*   f32 Float; */
/*   v4 Color; */
/*   u32 TextureId; */
/*   } SuiWidget; */

/*   * sui_button("Some string", optional: {50, 50}) */
/*   * sui_button_image("Some string", optional: {50, 50}, image) */
/*   * sui_toggle_button("Some string || null", optional: {50, 50}, optional: statesCount) */
/*   * sui_text("Some label text with integer inside %d", 100); */
/*   * sui_slider("some label || null", &floatVal); */
/*   * sui_drop_down_list("some label || null", &listValues, listCount: 5) */
/*   * sui_select_list("some label || null", &listValues, listCount: 5) */
/*   * sui_color_picker("some label || null", &colorV4) */

/* *\/ */


/* static SuiInstance Instance; */

/* #define DefaultGuard(name)						\ */
/*     ({									\ */
/*	vassert(Instance.IsInitialized && "Call sui_init() first!");	\ */
/*	vassert_not_null(name);						\ */
/*     }) */

/* #define GetDrawData() (&Instance.DrawData) */
/* #define GetStyle() (&Instance.Style) */
/* #define GetLayoutData() (&Instance.LayoutData) */
/* #define GetWidgetData() (&Instance.WidgetData) */

/* #define IsKeyPressed(key) (GetDrawData()->KeyData[key].State == SuiKeyState_Pressed) */
/* #define IsKeyReleased(key) (GetDrawData()->KeyData[key].State == SuiKeyState_Released) */

/* #define SUI_CLAMP(v, v2v) MINMAX(v, v2v.Min, v2v.Max) */

/* static void */
/* simple_ui_draw_data_init(SuiDrawData* data, v2 displaySize) */
/* { */
/*     data->Rects = NULL; */
/*     data->Texts = NULL; */
/*     data->StartPosition = v2_mulv(displaySize, 0.5); */
/*     data->NextSize = v2_mulv(displaySize, 0.25); */
/*     data->DisplaySize = displaySize; */

/*     data->ActiveID = 0; */
/*     data->HotID = 0; */

/*     data->LastFocusedID = 0; */
/*     data->LastFocusedPosition = (v2){ 0, 0 }; */
/*     data->LastFocusedSize = (v2){ 0, 0 }; */

/*     data->FocusID = 0; */
/*     data->TextIndexPosition = -1; */
/*     data->Mode = SuiModeType_Insert; */

/*     data->FontMaxHeight = 25; */

/*     data->MousePosition = (v2){0}; */
/* } */

/* static void */
/* simple_ui_layout_data_init(SuiLayoutData* data) */
/* { */
/*     data->Type = SuiLayoutType_VerticalStack; */
/*     data->CurrentPosition = v2_new(0, 1200); */
/*     data->RowSize = v2_new(1000, 50); */
/* } */

/* void */
/* sui_init_base_ui_style(SuiStyle* style) */
/* { */
/*     v4 darkColor = v4_new(0.11, 0.11, 0.11, 1); */
/*     v4 veryDarkColor = v4_new(0.09, 0.09, 0.09, 1); */

/*     style->Colors[SuiColor_Button_Foreground] = v4_new(1, 1, 1, 1); */
/*     style->Colors[SuiColor_Button_Background] = v4_new(0.24, 0.34, 0.34, 1); */
/*     style->Colors[SuiColor_Button_Hovered   ] = v4_new(1, 0, 0, 1); */
/*     style->Colors[SuiColor_Button_Clicked   ] = v4_new(0, 0.7f, 0.7f, 1); */
/*     style->Colors[SuiColor_Button_Focused   ] = v4_new(0.18f, 0.25f, 0.25f, 1); */

/*     style->Colors[SuiColor_Slider_Foreground] = v4_new(1, 1, 1, 1)         ; */
/*     style->Colors[SuiColor_Slider_Background] = v4_new(0.11, 0.11, 0.11, 1); */
/*     style->Colors[SuiColor_Slider_Hovered	 ] = veryDarkColor; */
/*     style->Colors[SuiColor_Slider_Clicked	 ] = v4_new(0, 0.7f, 0.7f, 1)   ; */
/*     style->Colors[SuiColor_Slider_Control_Foreground] = v4_new(1, 1, 1, 1)         ; */
/*     style->Colors[SuiColor_Slider_Control_Background] = v4_new(0.24, 0.34, 0.34, 1); */
/*     style->Colors[SuiColor_Slider_Control_Hovered	 ] = v4_new(1, 0, 0, 1)         ; */
/*     style->Colors[SuiColor_Slider_Control_Clicked	 ] = v4_new(0, 0.7f, 0.7f, 1)   ; */

/*     style->Colors[SuiColor_Input_Foreground] = v4_new(1, 1, 1, 1); */
/*     style->Colors[SuiColor_Input_Background] = darkColor; */
/*     style->Colors[SuiColor_Input_Hovered   ] = veryDarkColor; */
/*     style->Colors[SuiColor_Input_Clicked   ] = v4_new(0.01f, 0.01f, 0.01f, 1); */

/*     style->Colors[SuiColor_Checkbox_Background ] = (v4){0, 0, 0, 1}; */
/*     style->Colors[SuiColor_Checkbox_Hovered    ] = veryDarkColor; */
/*     style->Colors[SuiColor_Checkbox_Clicked    ] = v4_new(0.01f, 0.01f, 0.01f, 1); */
/*     style->Colors[SuiColor_Checkbox_InsideBox  ] = v4_new(0.34, 0.44, 0.44, 1); */

/*     // NOTE(typedef): work here we are here */
/*     //style->Colors[SuiColor_Focused_Background] = v4_new(0.34, 0.44, 0.44, 1); */
/*     //v4_new(0.34, 0.44, 0.44, 1); // this looks good */
/*     //v4_new(0.24, 0.64, 0.34, 1); // too bright */
/*     style->Colors[SuiColor_Border_Focused  ] = */
/*	//v4_new(0.34, 0.44, 0.44, 1); // 0.7726 */
/*	//v4_new(0.5949, 0.77, 0.77, 1); */
/*	v4_new(0.4172, 0.54, 0.54, 1); // NOTE(typedef): the best g/r, b/r = 0.7726 */

/*     style->Borders[SuiBorder_Focused] = v2_new(2, 2); */


/*     style->Sizes[SuiSize_Checkbox_Min] = v2_new(25, 25); */
/*     style->Sizes[SuiSize_Checkbox_Offset] = v2_new(5.0f, 4.5f); */

/* } */

/* void */
/* sui_init_widget_data(SuiWidgetData* suiWidgetData) */
/* { */
/*     SuiPanel suiPanel = { -1, { 0, 0 }, { 0, 0 }}; */
/*     suiWidgetData->Panel = suiPanel; */

/*     suiWidgetData->Widgets = NULL; */
/*     suiWidgetData->Panels = NULL; */
/* } */

/* void */
/* sui_init(SuiSettings settings) */
/* { */
/*     Instance.IsInitialized = 1; */

/*     Instance.PlatformCallbacks.GetDisplaySize = settings.GetDisplaySize; */
/*     //Instance.PlatformCallbacks.GetMousePosition = settings.GetMousePosition; */

/*     i32 width, height; */
/*     Instance.PlatformCallbacks.GetDisplaySize(&width, &height); */

/*     simple_ui_draw_data_init(&Instance.DrawData, v2_new(width, height)); */
/*     simple_ui_layout_data_init(&Instance.LayoutData); */

/*     sui_init_base_ui_style(GetStyle()); */
/*     sui_init_widget_data(GetWidgetData()); */

/* } */

/* static void */
/* sui_item_pressed(SuiDrawData* drawData, SuiRect* rect, i32 id, v4 hovered, v4 clicked) */
/* { */
/*     if (is_item_hovered(rect->Position, rect->Size)) */
/*     { */
/*	drawData->HotID = id; */
/*	rect->Color = hovered; */

/*	if ((drawData->ActiveID == 0 || drawData->ActiveID == id) */
/*	    && IsKeyPressed(SuiKeys_Mouse_Left_Button)) */
/*	{ */
/*	    drawData->ActiveID = id; */
/*	    drawData->LeftKeyWasPressed = 1; */
/*	    rect->Color = clicked; */
/*	} */
/*     } */
/* } */

/* static void */
/* sui_tab_pressed(SuiDrawData* drawData, i32 id, SuiRect* rect) */
/* { */
/*     if (drawData->FocusID == id) */
/*     { */
/*	v4 focused = GetStyle()->Colors[SuiColor_Border_Focused]; */

/*	if (IsKeyReleased(SuiKeys_Tab)) */
/*	{ */
/*	    drawData->FocusID = 0; */
/*	} */
/*	else */
/*	{ */
/*	    v2 focusedBorder = GetStyle()->Borders[SuiBorder_Focused]; */
/*	    v2 focusedSize = v2_new(rect->Size.X + focusedBorder.X, rect->Size.Y + focusedBorder.Y); */

/*	    SuiRect focusedRect = { */
/*		.Position = v2_new( */
/*		    rect->Position.X - (focusedSize.X - rect->Size.X) / 2, */
/*		    rect->Position.Y - (focusedSize.Y - rect->Size.Y) / 2), */
/*		.Size = focusedSize, */
/*		.Color = focused */
/*	    }; */
/*	    array_push(drawData->Rects, focusedRect); */
/*	} */
/*     } */
/*     else if (drawData->FocusID == 0) */
/*     { */
/*	drawData->FocusID = id; */
/*     } */
/* } */

/* void */
/* sui_begin_frame() */
/* { */
/*     i32 width, height; */
/*     Instance.PlatformCallbacks.GetDisplaySize(&width, &height); */
/*     Instance.DrawData.DisplaySize = v2_new(width, height); */

/*     SuiDrawData* drawData = GetDrawData(); */
/*     drawData->HotID = 0; */
/*     //memset(drawData->KeyData, 0, SuiKeys_Count * sizeof(SuiKeyData)); */
/* } */

/* void */
/* sui_end_frame() */
/* { */
/*     SuiDrawData* drawData = GetDrawData(); */
/*     if (IsKeyReleased(SuiKeys_Mouse_Left_Button)) */
/*     { */
/*	drawData->ActiveID = 0; */
/*	//NOTE(typedef): We can ignore this assign but i want this to be 0 */
/*	drawData->HotID = 0; */
/*     } */

/*     drawData->LeftKeyWasPressed = 0; */

/*     memset(drawData->KeyData, SuiKeyState_None, SuiKeys_Count * sizeof(SuiKeyData)); */

/*     // NOTE(typedef): for debug only */
/*     for (i32 i = SuiKeys_Start; i < SuiKeys_Count; ++i) */
/*     { */
/*	vassert(drawData->KeyData[i].State == SuiKeyState_None); */
/*     } */
/*     drawData->CharTyped = 0; */

/*     simple_ui_layout_data_init(GetLayoutData()); */
/* } */

/* i32 */
/* sui_panel_begin(WideString* label) */
/* { */
/*     SuiWidgetData* suiWidgetData = GetWidgetData(); */

/*     vassert(suiWidgetData->Panel.Id == -1 && "Fogot call sui_panel_end() !!!"); */

/*     SuiPanelId id = GetSuiPanelId(label); */
/*     SuiPanel suiPanel = { */
/*	.Id = id, */
/*	.Size = v2_new(0, 0), */
/*	.Position = GetLayoutData()->CurrentPosition */
/*     }; */
/*     suiWidgetData->Panel = suiPanel; */
/*     array_push(suiWidgetData->Panels, suiPanel); */

/*     return 1; */
/* } */

/* void */
/* sui_panel_end() */
/* { */
/*     SuiWidgetData* suiWidgetData = GetWidgetData(); */
/*     suiWidgetData->Panel.Id = -1; */
/*     suiWidgetData->Panel.Size = v2_new(0, 0); */
/* } */


/* u8 */
/* sui_button(WideString* name, v2 definedSize) */
/* { */
/*     DefaultGuard(name); */

/*     SuiWidgetId id = GetSuiWidgetId(name); DO_ONES(GINFO("HashID: %d\n", id)); */

/*     SuiStyle* style = GetStyle(); */
/*     v4 background = style->Colors[SuiColor_Button_Background]; */
/*     v4 hovered = style->Colors[SuiColor_Button_Hovered]; */
/*     v4 clicked = style->Colors[SuiColor_Button_Clicked]; */
/*     v4 focused = style->Colors[SuiColor_Button_Focused]; */

/*     //v2 textOffset = v2_new(15.0f, 0.35f * size.Y); */
/*     /\* SuiRect rect = { *\/ */
/*     /\*	.Position = pos, *\/ */
/*     /\*	.Size = size, *\/ */
/*     /\*	.Color = background *\/ */
/*     /\* }; *\/ */
/*     /\* SuiText text = { *\/ */
/*     /\*	.Buffer = name->Buffer, *\/ */
/*     /\*	.Length = name->Length, *\/ */
/*     /\*	.IsTemp = 0, *\/ */
/*     /\*	.Position = v2_add(pos, textOffset), *\/ */
/*     /\*	.Drawable = v2_new(size.X - textOffset.X, UNUSED) *\/ */
/*     /\* }; *\/ */

/*     v2 size = definedSize; */
/*     if (definedSize.X == 0.0f && definedSize.Y == 0.0f) */
/*     { */
/*	size = v2_new(190, 50); */
/*     } */

/*     SuiWidget btnWidget = { */
/*	.Type = SuiWidgetType_Button, */
/*	.Label = name, */
/*	.Size = size, */
/*	.Integer = 0, */
/*	.Float = 0.0f, */
/*	.Color = background, */
/*	.TextureId = -1 */
/*     }; */

/*     SuiDrawData* drawData = GetDrawData(); */
/*     /\* sui_item_pressed(drawData, &rect, id, hovered, clicked); *\/ */
/*     /\* sui_tab_pressed(drawData, id, &rect); *\/ */

/*     i32 result = 0; */

/*     if (IsKeyReleased(SuiKeys_Mouse_Left_Button) */
/*	&& drawData->HotID == id */
/*	&& drawData->ActiveID == id) */
/*     { */
/*	drawData->HotID = 0; */
/*	drawData->ActiveID = 0; */

/*	result = 1; */
/*	goto returnLabel; */
/*     } */

/* returnLabel: */
/*     /\* array_push(drawData->Rects, rect); *\/ */
/*     /\* array_push(drawData->Texts, text); *\/ */
/*     SuiWidgetData* suiWidgetData = GetWidgetData(); */
/*     array_push(suiWidgetData->Widgets, btnWidget); */

/*     return result; */
/* } */

/* u8 */
/* sui_slider_f32(WideString* name, v2 pos, f32* valuePtr, v2 minMax) */
/* { */
/*     DefaultGuard(name); */

/*     i32 id = i32(u64(name)); DO_ONES(GINFO("HashID: %d\n", id)); */

/*     SuiStyle* style = GetStyle(); */
/*     v4 background    = style->Colors[SuiColor_Slider_Background]; */
/*     v4 hovered       = style->Colors[SuiColor_Slider_Hovered   ]; */
/*     v4 clicked       = style->Colors[SuiColor_Slider_Clicked   ]; */
/*     v4 btnBackground = style->Colors[SuiColor_Slider_Control_Background]; */
/*     v4 btnHovered    = style->Colors[SuiColor_Slider_Control_Hovered   ]; */
/*     v4 btnClicked    = style->Colors[SuiColor_Slider_Control_Clicked   ]; */

/*     //NOTE(typedef): hardcoded for now */
/*     v2 H_size = v2_new(150, 35); */
/*     const f32 H_sliderSizeInPercentage = 0.1f; */

/*     v2 sliderSize = v2_new(H_size.X * H_sliderSizeInPercentage, H_size.Y); */
/*     f32 maxSliderX = H_size.X - sliderSize.X; */

/*     f32 textWidth = sui_get_text_width(name);//45.0f; */
/*     //GWARNING("Text Size: %f\n", textWidth); */

/*     f32 value = *valuePtr; */
/*     // 50 / (100 - 0) * size.X */
/*     f32 percentage = value / (minMax.Max - minMax.Min); */
/*     f32 offsetX = MINMAX(percentage * maxSliderX, minMax.Min, maxSliderX); */
/*     v2 textOffset = v2_new(pos.X + (H_size.X / 2.0f) - (textWidth / 2) , pos.Y + 0.3f * H_size.Y); */

/*     value = SUI_CLAMP(value, minMax); */

/*     wchar buf[256]; */
/*     size_t bufLength = swprintf(buf, 256, L"%0.2f", value); */
/*     DO_ONES(GINFO("LENGTH: %d\n", bufLength)); */

/*     SuiRect rect0 = { */
/*	.Position = pos, */
/*	.Size = H_size, */
/*	.Color = background */
/*     }; */
/*     SuiRect rect1 = { */
/*	.Position = v2_new(pos.X + offsetX, pos.Y), */
/*	.Size = v2_new(H_size.X * H_sliderSizeInPercentage, H_size.Y), */
/*	.Color = btnBackground */
/*     }; */
/*     SuiText text = { */
/*	.Buffer = wide_string_raw(buf, bufLength), */
/*	.Length = bufLength, */
/*	.IsTemp = 1, */
/*	.Position = textOffset, */
/*	.Drawable = v2_new(H_size.X, UNUSED) */
/*     }; */

/*     SuiDrawData* drawData = GetDrawData(); */

/*     if (is_item_hovered(rect0.Position, rect0.Size)) //set active and hot state */
/*     { */
/*	rect0.Color = hovered; */

/*	sui_item_pressed(drawData, &rect1, id, btnHovered, btnClicked); */
/*     } */

/*     sui_tab_pressed(drawData, id, &rect0); */

/*     i32 result = 0; */

/*     if (drawData->ActiveID == id) */
/*     { */
/*	rect0.Color = hovered; */
/*	rect1.Color = btnClicked; */

/*	f32 ratioX = (drawData->MousePosition.X - pos.X) / H_size.X; */
/*	f32 newValue = SUI_CLAMP(ratioX * minMax.Max, minMax); */
/*	if (newValue != value) */
/*	{ */
/*	    *valuePtr = newValue; */

/*	    result = 1; */
/*	    goto returnLabel; */
/*	} */
/*     } */

/* returnLabel: */
/*     array_push(drawData->Rects, rect0); */
/*     array_push(drawData->Rects, rect1); */
/*     array_push(drawData->Texts, text); */

/*     return result; */
/* } */

/* u8 */
/* sui_input(i32* str, size_t maxLength, v2 pos) */
/* { */
/*     DefaultGuard(str); */

/*     i32 id = i32(u64(str)); */
/*     SuiDrawData* drawData = GetDrawData(); */
/*     SuiStyle* style = GetStyle(); */
/*     v4 background    = style->Colors[SuiColor_Input_Background]; */
/*     v4 hovered       = style->Colors[SuiColor_Input_Hovered   ]; */
/*     v4 clicked       = style->Colors[SuiColor_Input_Clicked   ]; */

/*     f32 H_offsetX = 10.0f; */

/*     i32 len = wcslen(str); */
/*     //here neeed Instance.DrawData.FontMaxHeight */
/*     f32 maxHeight = MINMAX(get_text_max_height(str, len), drawData->FontMaxHeight, 1000); */
/*     f32 textWidth = sui_get_text_width_ext(str, len); */
/*     textWidth = (textWidth < 250) ? 250 : textWidth; */
/*     v2 size = v2_new(textWidth + 3*H_offsetX, maxHeight * 2); */
/*     //GERROR("TextWidth: %f, %d\n", textWidth, len); */
/*     v2 textOffset = v2_new(pos.X + H_offsetX, pos.Y + maxHeight/2 + maxHeight/5); */

/*     SuiRect rect0 = { */
/*	.Position = pos, */
/*	.Size = size, */
/*	.Color = background */
/*     }; */
/*     SuiRect rect1 = { */
/*	.Position = v2_new(textOffset.X, textOffset.Y - 5), */
/*	.Size = v2_new(1, maxHeight), */
/*	.Color = v4_new(1, 1, 1, 1) */
/*     }; */
/*     SuiText text = { */
/*	.Buffer = str, */
/*	.Length = maxLength, */
/*	.IsTemp = 0, */
/*	.Position = textOffset, */
/*	.Drawable = {size.X - 2 * H_offsetX, size.Y} */
/*     }; */

/*     sui_item_pressed(drawData, &rect0, id, hovered, clicked); */
/*     sui_tab_pressed(drawData, id, &rect0); */

/*     i32 returnValue = 0; */

/*     if ((IsKeyReleased(SuiKeys_Mouse_Left_Button) */
/*	 && drawData->HotID == id */
/*	 && drawData->ActiveID == id) */
/*	|| drawData->FocusID == id) */
/*     { */
/*	drawData->FocusID = id; */
/*	drawData->LastFocusedPosition = pos; */
/*	drawData->LastFocusedSize = size; */

/*	rect0.Color = clicked; */
/*	rect1.Position.X = textOffset.X + sui_get_text_offset(str, len, drawData->TextIndexPosition) + 0; */

/*	if (drawData->TextIndexPosition == -1) */
/*	    drawData->TextIndexPosition = len; */

/*	if (IsKeyPressed(SuiKeys_Backspace)) */
/*	{ */
/*	    if (len > 0 && drawData->TextIndexPosition > 0) */
/*	    { */
/*		i32 copyLength = len - drawData->TextIndexPosition; */
/*		for (i32 i = 0; i < copyLength; ++i) */
/*		{ */
/*		    i32 curPos = i + drawData->TextIndexPosition; */
/*		    str[curPos - 1] = str[curPos]; */
/*		} */

/*		str[len - 1] = 0; */
/*		--drawData->TextIndexPosition; */

/*		returnValue = 1; */
/*	    } */
/*	} */
/*	else if (IsKeyPressed(SuiKeys_Delete)) */
/*	{ */
/*	    if (len > 0 && drawData->TextIndexPosition >= 0) */
/*	    { */
/*		i32 copyLength = len - drawData->TextIndexPosition; */
/*		for (i32 i = 0; i < copyLength; ++i) */
/*		{ */
/*		    i32 curPos = i + drawData->TextIndexPosition; */
/*		    str[curPos] = str[curPos + 1]; */
/*		} */

/*		str[len] = 0; */

/*		returnValue = 1; */
/*	    } */
/*	} */
/*	else if (IsKeyPressed(SuiKeys_LeftArrow)) */
/*	{ */
/*	    drawData->TextIndexPosition = MINMAX(drawData->TextIndexPosition - 1, 0, len); */
/*	    GWARNING("Text index position: %d\n", drawData->TextIndexPosition); */
/*	} */
/*	else if (IsKeyPressed(SuiKeys_RightArrow)) */
/*	{ */
/*	    drawData->TextIndexPosition = MINMAX(drawData->TextIndexPosition + 1, 0, len); */
/*	    GWARNING("Text index position: %d\n", drawData->TextIndexPosition); */
/*	} */
/*	else if (IsKeyPressed(SuiKeys_Home)) */
/*	{ */
/*	    drawData->TextIndexPosition = 0; */
/*	} */
/*	else if (IsKeyPressed(SuiKeys_End)) */
/*	{ */
/*	    drawData->TextIndexPosition = len; */
/*	} */
/*	else if (drawData->CharTyped != 0) */
/*	{ */
/*	    if (drawData->Mode == SuiModeType_Insert) */
/*	    { */
/*		vassert((len + 1) < maxLength && "Out of bounds!"); */
/*		i32 temp = str[drawData->TextIndexPosition + 1]; */
/*		for (i32 i = len; i > drawData->TextIndexPosition; --i) */
/*		{ */
/*		    str[i] = str[i - 1]; */
/*		} */

/*		str[drawData->TextIndexPosition] = drawData->CharTyped; */
/*		++drawData->TextIndexPosition; */
/*	    } */
/*	    else if (drawData->Mode == SuiModeType_Overwrite) */
/*	    { */
/*		str[drawData->TextIndexPosition] = drawData->CharTyped; */
/*		++drawData->TextIndexPosition; */
/*	    } */
/*	} */

/*	goto returnLabel; */
/*     } */

/* returnLabel: */
/*     array_push(drawData->Rects, rect0); */
/*     array_push(drawData->Rects, rect1); */
/*     array_push(drawData->Texts, text); */

/*     return returnValue; */
/* } */

/* u8 */
/* sui_checkbox_label(WideString* str, i32* check, v2 pos) */
/* { */
/*     DefaultGuard(str); */

/*     i32 id = i32(u64(str)); */
/*     SuiDrawData* drawData = GetDrawData(); */
/*     SuiStyle* style = GetStyle(); */
/*     v4 background     = style->Colors[SuiColor_Checkbox_Background]; */
/*     v4 hovered        = style->Colors[SuiColor_Checkbox_Hovered   ]; */
/*     v4 clicked        = style->Colors[SuiColor_Checkbox_Clicked   ]; */
/*     v4 insideBoxColor = style->Colors[SuiColor_Checkbox_InsideBox ]; */
/*     v2 size   = v2_mulv(style->Sizes[SuiSize_Checkbox_Min], 2.0f); */
/*     v2 offset = v2_mulv(style->Sizes[SuiSize_Checkbox_Offset], 2.0f); */

/*     v2 rect1Size = v2_new(size.X - offset.X * 2, size.Y - offset.Y * 2); */

/*     f32 textWidth = sui_get_text_width(str); */

/*     SuiRect rect0 = { */
/*	.Position = pos, */
/*	.Size = size, */
/*	.Color = background */
/*     }; */
/*     //v4_print(background); */
/*     SuiRect rect1 = { */
/*	.Position = v2_add(pos, offset), */
/*	.Size = rect1Size, */
/*	.Color = insideBoxColor */
/*     }; */
/*     //DO_ONES(GERROR("X: %f\n", textWidth, str->Buffer, str->Length);); */
/*     SuiText text = { */
/*	.Buffer = str->Buffer, */
/*	.Length = str->Length, */
/*	.IsTemp = 0, */
/*	.Position = v2_new(pos.X + size.X + offset.X, pos.Y + size.Y/3), */
/*	.Drawable = v2_new(textWidth, 0) */
/*     }; */

/*     sui_item_pressed(drawData, &rect0, id, hovered, clicked); */
/*     sui_tab_pressed(drawData, id, &rect0); */

/*     i32 result = 0; */

/*     if (IsKeyReleased(SuiKeys_Mouse_Left_Button) */
/*	&& drawData->HotID == id */
/*	&& drawData->ActiveID == id) */
/*     { */
/*	drawData->HotID = 0; */
/*	drawData->ActiveID = 0; */

/*	*check = !(*check); */
/*	result = 1; */
/*	goto returnLabel; */
/*     } */

/* returnLabel: */
/*     array_push(drawData->Rects, rect0); */
/*     array_push(drawData->Texts, text); */
/*     if (*check == 1) */
/*	array_push(drawData->Rects, rect1); */

/*     return result; */
/* } */

/* static void */
/* rgb_to_hsv_standard(v3 rgb, v3* hsv) */
/* { */
/*     f32 h, s, v, */
/*	k = 0.0f, */
/*	r = rgb.R, */
/*	g = rgb.G, */
/*	b = rgb.B; */
/*     const f32 constantValue = 1e-20f; */
/*     const f32 ratioValue = -0.333333333f; */

/*     if (g < b) */
/*     { */
/*	Swap(g, b); */
/*	k = -1.0f; */
/*     } */

/*     if (r < g) */
/*     { */
/*	Swap(r, g); */
/*	k = ratioValue - k; */
/*     } */

/*     f32 chroma = r - Min(g, b); */
/*     h = fabs(k + (g - b) / (6.0f * chroma + constantValue)); */
/*     s = chroma / (r + constantValue); */
/*     v = r; */

/*     hsv->Hue = h; */
/*     hsv->Saturation = s; */
/*     hsv->Value = v; */
/* } */

/* /\* */
/*   NOTE(typedef): this is back-end, it should be in different file */
/* *\/ */
/* void */
/* sui_draw() */
/* { */
/*     SuiDrawData* drawData = GetDrawData(); */
/*     SuiWidgetData* suiWidgetData = GetWidgetData(); */

/*     //suiWidgetData->Panel.Id = -1; // Костыль */
/*     vassert(suiWidgetData->Panel.Id == -1 && "You probably fogot call sui_panel_end() in the end part of the code"); */

/*     i32 i, */
/*	widgetsCount = array_count(suiWidgetData->Widgets);; */

/*     SuiLayoutData* suiLayoutData = GetLayoutData(); */
/*     ui_renderer_draw_rect(suiLayoutData->CurrentPosition, suiLayoutData->RowSize, v4_new(0.3, 0.2,0.1, 1.0)); */

/*     for (i32 w = 0; w < widgetsCount; ++w) */
/*     { */
/*	SuiWidget suiWidget = suiWidgetData->Widgets[w]; */

/*	/\* SuiLayoutType Type; *\/ */
/*	/\* v2 CurrentPosition; *\/ */
/*	/\* v2 RowSize; *\/ */

/*	//ui_renderer_draw_rect(suiLayoutData->CurrentPosition, suiLayoutData->RowSize, suiWidget.Color); */
/*     } */

/*     array_clear(suiWidgetData->Widgets); */
/*     array_clear(suiWidgetData->Panels); */

/*     /\*BIG FONT: w bigger e smaller */
/*       SMALL FONT: w smaller e bigger*\/ */
/*     f32 w, e; */
/*     ui_renderer_flush(w = 0.4, e = 0.25); */
/* } */

/* /\* */
/*   NOTE(typedef): this is back-end, it should be in different file */
/* *\/ */
/* void */
/* sui_add_key_event(Event* event) */
/* { */
/*     SuiDrawData* drawData = GetDrawData(); */

/*     switch (event->Category) */
/*     { */

/*     case KeyCategory: */
/*     { */

/*	switch (event->Type) */
/*	{ */
/*	case KeyPressed: */
/*	case KeyRepeatPressed: */
/*	{ */
/*	    KeyPressedEvent* kpe = (KeyPressedEvent*) event; */
/*	    const SuiKeyState state = SuiKeyState_Pressed; */

/*	    switch (kpe->KeyCode) */
/*	    { */
/*	    case KEY_TAB: */
/*		drawData->KeyData[SuiKeys_Tab].State = state; */
/*		break; */

/*	    case KEY_BACKSPACE: */
/*		drawData->KeyData[SuiKeys_Backspace].State = state; */
/*		break; */
/*	    case KEY_DELETE: */
/*		drawData->KeyData[SuiKeys_Delete].State = state; */
/*		break; */

/*	    case KEY_LEFT: */
/*		drawData->KeyData[SuiKeys_LeftArrow].State = state; */
/*		break; */
/*	    case KEY_RIGHT: */
/*		drawData->KeyData[SuiKeys_RightArrow].State = state; */
/*		break; */
/*	    case KEY_UP: */
/*		drawData->KeyData[SuiKeys_UpArrow].State = state; */
/*		break; */
/*	    case KEY_DOWN: */
/*		drawData->KeyData[SuiKeys_DownArrow].State = state; */
/*		break; */
/*	    case KEY_HOME: */
/*		drawData->KeyData[SuiKeys_Home].State = state; */
/*		break; */
/*	    case KEY_END: */
/*		drawData->KeyData[SuiKeys_End].State = state; */
/*		break; */

/*	    } */

/*	    break; */
/*	} */

/*	case KeyRealeased: */
/*	{ */
/*	    KeyReleasedEvent* kre = (KeyReleasedEvent*) event; */
/*	    const SuiKeyState state = SuiKeyState_Released; */

/*	    switch (kre->KeyCode) */
/*	    { */
/*	    case KEY_TAB: */
/*		drawData->KeyData[SuiKeys_Tab].State = state; */
/*		break; */

/*	    case KEY_BACKSPACE: */
/*		drawData->KeyData[SuiKeys_Backspace].State = state; */
/*		break; */
/*	    case KEY_DELETE: */
/*		drawData->KeyData[SuiKeys_Delete].State = state; */
/*		break; */

/*	    case KEY_LEFT: */
/*		drawData->KeyData[SuiKeys_LeftArrow].State = state; */
/*		break; */
/*	    case KEY_RIGHT: */
/*		drawData->KeyData[SuiKeys_RightArrow].State = state; */
/*		break; */
/*	    case KEY_UP: */
/*		drawData->KeyData[SuiKeys_UpArrow].State = state; */
/*		break; */
/*	    case KEY_DOWN: */
/*		drawData->KeyData[SuiKeys_DownArrow].State = state; */
/*		break; */
/*	    case KEY_HOME: */
/*		drawData->KeyData[SuiKeys_Home].State = state; */
/*		break; */
/*	    case KEY_END: */
/*		drawData->KeyData[SuiKeys_End].State = state; */
/*		break; */

/*	    } */
/*	    break; */
/*	} */

/*	case CharTyped: */
/*	{ */
/*	    CharEvent* ce = (CharEvent*) event; */
/*	    drawData->CharTyped = ce->Char; */
/*	    GINFO("%d\n", ce->Char); */
/*	    break; */
/*	} */
/*	} */

/*	break; */
/*     } */

/*     case EventCategory_MouseCategory: */
/*     { */

/*	switch (event->Type) */
/*	{ */

/*	case MouseButtonPressed: */
/*	{ */
/*	    MouseButtonEvent* mbe = (MouseButtonEvent*) event; */
/*	    switch (mbe->MouseCode) */
/*	    { */
/*	    case MOUSE_BUTTON_LEFT: */
/*		drawData->KeyData[SuiKeys_Mouse_Left_Button].State = SuiKeyState_Pressed; */
/*		break; */

/*	    case MOUSE_BUTTON_RIGHT: */
/*		drawData->KeyData[SuiKeys_Mouse_Right_Button].State = SuiKeyState_Pressed; */
/*		break; */

/*	    case MOUSE_BUTTON_MIDDLE: */
/*		drawData->KeyData[SuiKeys_Mouse_Middle_Button].State = SuiKeyState_Pressed; */
/*		break; */
/*	    } */

/*	    break; */
/*	} */

/*	case MouseButtonReleased: */
/*	{ */
/*	    MouseButtonEvent* mbe = (MouseButtonEvent*) event; */
/*	    switch (mbe->MouseCode) */
/*	    { */
/*	    case MOUSE_BUTTON_LEFT: */
/*		drawData->KeyData[SuiKeys_Mouse_Left_Button].State = SuiKeyState_Released; */
/*		break; */

/*	    case MOUSE_BUTTON_RIGHT: */
/*		drawData->KeyData[SuiKeys_Mouse_Right_Button].State = SuiKeyState_Released; */
/*		break; */

/*	    case MOUSE_BUTTON_MIDDLE: */
/*		drawData->KeyData[SuiKeys_Mouse_Middle_Button].State = SuiKeyState_Released; */
/*		break; */
/*	    } */

/*	    break; */
/*	} */

/*	case MouseMoved: */
/*	{ */
/*	    i32 w, h; */
/*	    Instance.PlatformCallbacks.GetDisplaySize(&w, &h); */

/*	    MouseMovedEvent* mme = (MouseMovedEvent*) event; */
/*	    //NOTE(typedef): hardcoded for now */
/*	    f32 xpos = mme->X /\*viewport_x()*\/, ypos = h - mme->Y; */

/*	    drawData->MousePosition = v2_new(xpos, ypos); */
/*	    //v2_print(drawData->MousePosition); */

/*	    break; */
/*	} */

/*	case MouseScrolled: */
/*	{ */
/*	    MouseScrolledEvent* mse = (MouseScrolledEvent*) event; */
/*	    break; */
/*	} */

/*	} */

/*	break; */
/*     } */

/*     } */

/* } */

/* /\* Additional Helpers Functions *\/ */

/* /\* NOTE(typedef): in px *\/ */
/* f32 */
/* sui_get_text_width_ext(wchar* buffer, size_t length) */
/* { */
/*     f32 width = 0, offsetForCurrentFont = ui_renderer_get_offset_for_current_font(); */
/*     i32 i, ind; */
/*     wchar code; */

/*     for (i = 0; i < length; ++i) */
/*     { */
/*	code = buffer[i]; */
/*	if (code == 0) */
/*	    break; */

/*	CharChar cc = GetCharChar(code, &ind); */
/*	if (ind != -1) */
/*	{ */
/*	    //GINFO("FullWidth: %f\n", cc.FullWidth); */
/*	    width += cc.FullWidth * offsetForCurrentFont; */
/*	} */
/*     } */

/*     return width; */
/* } */

/* f32 */
/* sui_get_text_width(WideString* str) */
/* { */
/*     f32 width = sui_get_text_width_ext((i32*)str->Buffer, str->Length); */
/*     return width; */
/* } */

/* f32 */
/* sui_get_text_offset(i32* str, size_t len, i32 textIndexPosition) */
/* { */
/*     f32 offsetX = 0.0f, offsetForCurrentFont = ui_renderer_get_offset_for_current_font(); */
/*     i32 i; */
/*     for (i = 0; i < len; ++i) */
/*     { */
/*	if (i >= textIndexPosition) */
/*	{ */
/*	    break; */
/*	} */

/*	i32 ind; */
/*	CharChar cc = GetCharChar(str[i], &ind); */
/*	offsetX += (cc.FullWidth * offsetForCurrentFont); */
/*     } */

/*     return offsetX; */
/* } */

/* f32 */
/* get_text_max_height(i32* text, i32 maxLength) */
/* { */
/*     i32 i, count = maxLength; */
/*     f32 maxHeight = 0; */
/*     for (i = 0; i < count; ++i) */
/*     { */
/*	i32 code = text[i]; */
/*	if (code == 0) */
/*	{ */
/*	    return maxHeight; */
/*	} */

/*	i32 ind; */
/*	f32 currHeight = GetCharChar(code, &ind).FullHeight; */
/*	if (currHeight > maxHeight) */
/*	{ */
/*	    maxHeight = currHeight; */
/*	} */
/*     } */

/*     return maxHeight; */
/* } */


/* /\* Core Utils *\/ */

/* u8 */
/* is_item_hovered(v2 pos, v2 size) */
/* { */
/*     v2 mousePos = GetDrawData()->MousePosition; */
/*     //v2_print(pos); */
/*     //v2_print(size); */
/*     if (mousePos.X >= pos.X */
/*	&& mousePos.X <= (pos.X + size.Width) */
/*	&& mousePos.Y >= pos.Y */
/*	&& mousePos.Y <= (pos.Y + size.Height)) */
/*     { */
/*	return 1; */
/*     } */

/*     return 0; */
/* } */


/* /\* */
/*   DOCS(typedef): Public Sui Functions For Back-end */
/* *\/ */
/* SuiDrawData* */
/* sui_get_draw_data() */
/* { */
/*     return GetDrawData(); */
/* } */

/* SuiWidgetData* */
/* sui_get_widget_data() */
/* { */
/*     return GetWidgetData(); */
/* } */

/* SuiLayoutData* */
/* sui_get_layout_data() */
/* { */
/*     return GetLayoutData(); */
/* } */
