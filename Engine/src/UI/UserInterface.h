#ifndef USER_INTERFACE_H
#define USER_INTERFACE_H

#include <Utils/Types.h>

void ui_create();
void ui_begin();
void ui_end(i32 imageIndex);

#endif // USER_INTERFACE_H
