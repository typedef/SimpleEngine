#include "AssetManager.h"

#include <stdio.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Utils/SimpleStandardLibrary.h>

static AssetManager gAssetManager;

void
asset_manager_create()
{
    /*
      NOTE(typedef): init defaults
    */

    gAssetManager = (AssetManager) {};

    // DOCS(typedef): Loading default assets
    asset_manager_load_asset_id(AssetType_Texture, asset_texture("default/white.png"));
    asset_manager_load_asset_id(AssetType_Texture, asset_texture("default/white_texture.png"));

}

void*
asset_manager_load_asset(AssetType type, const char* diskPath)
{
    switch (type)
    {
    case AssetType_Texture:
    {
	i64 id = asset_manager_load_asset_id(type, diskPath);
	void* textureData = (void*) &gAssetManager.pTextures[id];
	return textureData;
    }

    default:
	vguard(0);
	break;
    }

    return NULL;
}

i64
asset_manager_load_asset_id(AssetType type, const char* diskPath)
{
    switch (type)
    {
    case AssetType_Texture:
    {
	printf(RED("[Loading texture ...]")" %s\n", diskPath);

	//VsaTexture* Textures;
	//TexturesMapTable* pTexturesTable;

	i64 ind = shash_geti(gAssetManager.pTexturesTable, diskPath);
	if (ind == -1)
	{
	    VsaTexture vsaTexture = vsa_texture_new(diskPath);
	    i64 id = array_count(gAssetManager.pTextures);
	    array_push(gAssetManager.pTextures, vsaTexture);
	    shash_put(gAssetManager.pTexturesTable, diskPath, id);
	    return id;
	}

	ind = table_index(gAssetManager.pTexturesTable);
	i64 id = gAssetManager.pTexturesTable[ind].Value;
	return id;
    }

    default:
	vguard(0);
	break;
    }

    return -1;
}

void*
asset_manager_get(AssetType type, i64 id)
{
    switch (type)
    {
    case AssetType_Texture:
    {
	if (id != -1)
	{
	    return &gAssetManager.pTextures[id];
	}

	//GINFO("GetDefault Texture!\n");
	return &gAssetManager.pTextures[0];
    }

    default:
	vguard(0);
	break;
    }

    return NULL;
}

void
asset_manager_destroy()
{
    i32 texturesCnt = array_count(gAssetManager.pTextures);
    for (i32 i = 0; i < texturesCnt; ++i)
    {
	VsaTexture vsaTexture = gAssetManager.pTextures[i];
	vsa_texture_destroy(vsaTexture);
    }

    array_free(gAssetManager.pTextures);
    table_free(gAssetManager.pTexturesTable);
}
