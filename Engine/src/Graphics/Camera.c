#include "Camera.h"

#include <string.h>
#include <Math/SimpleMath.h>

#define df -5
static f32 DefaultValuesForM4[16] = {
    df, df, df,
    df, df, df,
    df, df, df,
    df, df, df,
    df, df, df,
    df
};

Camera
camera_new_default()
{
    Camera base = {
	.View = m4_copy_array(DefaultValuesForM4),
	.Projection = m4_copy_array(DefaultValuesForM4),
	.ViewProjection = m4_copy_array(DefaultValuesForM4)
    };

    return base;
}

void
camera_update_projection(Camera* camera, m4 projection)
{
    camera->Projection = projection;
    camera->ViewProjection = m4_mul(camera->Projection, camera->View);
}

void
camera_update_view(Camera* camera, m4 view)
{
    camera->View = view;
    camera->ViewProjection = m4_mul(camera->Projection, camera->View);
}

i32
camera_is_valid(Camera* camera)
{
    i32 viewDefault = memcmp(&camera->View.M[0][0], &DefaultValuesForM4[0], sizeof(DefaultValuesForM4));
    i32 projectionDefault = memcmp(&camera->Projection.M[0][0], DefaultValuesForM4, sizeof(DefaultValuesForM4));
    i32 viewProjectionDefault = memcmp(&camera->ViewProjection.M[0][0], DefaultValuesForM4, sizeof(DefaultValuesForM4));

    i32 result = ((viewDefault != 0) && (projectionDefault != 0) && (viewProjectionDefault != 0));
    return result;
}
