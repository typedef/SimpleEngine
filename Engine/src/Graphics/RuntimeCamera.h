#ifndef RUNTIME_CAMERA_H
#define RUNTIME_CAMERA_H

#include <Utils/Types.h>
//#define SIMPLE_MATH_IMPLEMENTATION
//#include <Math/SimpleMath.h>

#include "CameraType.h"

typedef enum DirectionType
{
    DirectionType_None = 0,
    DirectionType_Forward,
    DirectionType_Backward,
    DirectionType_Left,
    DirectionType_Right
} DirectionType;

typedef struct RuntimeCameraSettings
{
    CameraType Type;

    //NOTE(bies): Projection
    f32 Near;
    f32 Far;
    f32 AspectRatio;
    f32 FovAngle; // NOTE(bies): in degree

    //Arcball
    quat Rotation;
    v3 LookAt;

    // Spectator
    v3  Position;
    f32 Yaw;   // x
    f32 Pitch; // y
    f32 Speed;
    f32 SensitivityHorizontal;
    f32 SensitivityVertical;
    f32 Zoom;

    u32 Left;
    u32 ORight;
    u32 Bot;
    u32 Top;

    // Mouse Move
    f32 PrevX;
    f32 PrevY;
} RuntimeCameraSettings;

typedef struct RuntimeCamera
{
    CameraType Type;

    i32 IsCameraMoved;

    //NOTE(bies): Projection
    f32 Near;
    f32 Far;
    f32 AspectRatio;
    f32 Fov;

    //NOTE(typedef): Orthographic
    u32 Left;
    u32 ORight;
    u32 Bot;
    u32 Top;

    //InternalCamera
    m4 View;
    m4 Projection;
    m4 ViewProjection;

    //
    v3 Position;
    v3 Front;
    v3 Right;
    v3 Up;
    v3 LookAtPoint;

    quat Orientation;

    f32 Distance;
    f32 SensitivityHorizontal;
    f32 SensitivityVertical;
    f32 Zoom;
    f32 Speed;
    f32 Sensitivity;

    f32 Yaw;   // x
    f32 Pitch; // y
    // Mouse Move
    f32 PrevX;
    f32 PrevY;

} RuntimeCamera;

void runtime_camera_update(RuntimeCamera* camera);
void get_all_direction(RuntimeCamera* camera, v3* up, v3* right, v3* front);
void _runtime_camera_update_vectors(RuntimeCamera* camera);
i32 runtime_camera_mouse_move(RuntimeCamera* camera, f64 x, f64 y, f32 timestep);
void runtime_camera_update_view(RuntimeCamera* camera);
void runtime_camera_set_orthograhic(RuntimeCamera* camera);
void runtime_camera_set_perspective(RuntimeCamera* camera);
void runtime_camera_update_projection(RuntimeCamera* camera);
RuntimeCamera runtime_camera_create_from_settings(RuntimeCameraSettings settings);
RuntimeCamera runtime_camera_create(RuntimeCameraSettings settings);
void runtime_camera_move(RuntimeCamera* camera, DirectionType direction, f32 timestep);
void runtime_camera_resize(RuntimeCamera* runtimeCamera, f32 width, f32 height);

/*
//camera->View = m4_look_at(camera->Position, v3_new(0, 0, 0), v3_new(0,1,0));

/* v3 forwardDirection = quat_rotate_v3(camera->Orientation, v3_new(0, 0, -1)); */
/* v3 forwardDistance = v3_scale(forwardDirection, camera->Distance); */
/* camera->Position = v3_sub(camera->LookAtPoint, forwardDistance); */

/* m4 translate = m4_translate_identity(camera->Position); */
/* m4 view = m4_mul(translate, quat_to_m4(camera->Orientation)); */
/* camera->View = m4_inverse(view); */

#endif
