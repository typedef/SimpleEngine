#ifndef SIMPLE_WINDOW_H
#define SIMPLE_WINDOW_H

#include <InputSystem/KeyCodes.h>
#include <Utils/Types.h>
//#include <vulkan.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

typedef struct Event Event;

typedef struct SimpleWindowSettings
{
    i16 Width;
    i16 Height;
    i8 IsInitOpenGl;
    const char* Name;
} SimpleWindowSettings;

typedef struct SimpleWindow
{
    u32 Width;
    u32 Height;
    u32 VsyncLevel;
    f32 AspectRatio;
    const char* Title;
    void (*OnEvent)(Event* event);
    GLFWwindow* GlfwWindow;
} SimpleWindow;

i32 window_create(SimpleWindow* window, SimpleWindowSettings winSet, void (*onEvent)(Event* event));
VkResult window_create_surface(VkInstance instance, VkAllocationCallbacks* allocator, VkSurfaceKHR* surface);
SimpleWindow* window_get();

void window_set_fullscreen(SimpleWindow* window);
i32 window_should_close(SimpleWindow* window);
void window_set_should_close(SimpleWindow* window, i8 shouldClose);
void window_terminate();
void window_set_title(SimpleWindow* window, const char* title);
void window_set_vsync(i32 isVsync);
void window_on_update(SimpleWindow* window);
void window_get_size(SimpleWindow* window, i32* width, i32* height);
v2 window_get_size_v2(SimpleWindow* window);
void window_get_frame_size(SimpleWindow* window, i32* left, i32* top, i32* right, i32* bottom);
void window_minimize(SimpleWindow* window);
void window_restore(SimpleWindow* window);
void window_maximize(SimpleWindow* window);
void window_set_icon(SimpleWindow* window, const char* path);
f64 window_get_short_time();
void window_get_framebuffer_size(SimpleWindow* window, i32* w, i32* h);
void window_get_framebuffer_size_shared(i32* w, i32* h);
char** window_get_required_exts(i32* extCount);

#endif // SIMPLE_WINDOW_H
