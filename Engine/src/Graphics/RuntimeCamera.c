#include "RuntimeCamera.h"

#include <Math/SimpleMath.h>

void
runtime_camera_update(RuntimeCamera* camera)
{
    camera->ViewProjection = m4_mul(camera->Projection, camera->View);
    camera->IsCameraMoved = 0;
}

i32
runtime_camera_mouse_move(RuntimeCamera* camera, f64 mx, f64 my, f32 timestep)
{
    /*
      NOTE(typedef): spectator behaviour
    */
    if (camera->PrevX == 0.0f && camera->PrevY == 0.0f)
    {
	camera->PrevX = mx;
	camera->PrevY = my;

	return 0;
    }

    f64 dx = mx - camera->PrevX;
    f64 dy = my - camera->PrevY;
    //TODO(typedef): find more accurate value
    const f32 scv = 0.1f;
    camera->Yaw   = dx * camera->SensitivityHorizontal * timestep * scv;
    camera->Pitch = dy * camera->SensitivityVertical * timestep * scv;

    camera->PrevX = mx;
    camera->PrevY = my;

    // GINFO("[dx, dy] [%f %f]\n", dx, dy);
    // GINFO("[mx, my] [%f %f]\n", mx, my);
    // GINFO("[px, py] [%f %f]\n", camera->PrevX, camera->PrevY);
    if (!f32_equal(dx, 0.0f) || !f32_equal(dy, 0.0f))
    {
	camera->Up = v3_new(0, 1, 0);
	camera->Right = v3_cross(camera->Front, camera->Up);
	quat qpitch = quat_new(-camera->Pitch, camera->Right);
	quat qyaw = quat_new(-camera->Yaw, camera->Up);
	quat q = quat_normalize(quat_mul(qpitch, qyaw));
	camera->Front = quat_rotate_v3(q, camera->Front);

	return 1;
    }

    return 0;
}

void
runtime_camera_update_view(RuntimeCamera* camera)
{
    if (camera->Type == CameraType_PerspectiveSpectator)
    {
	camera->View = m4_look_at(camera->Position, v3_add(camera->Position, camera->Front), camera->Up);
    }
    else if (camera->Type == CameraType_OrthographicSpectator)
    {
	camera->View = m4_translate_identity(camera->Position);
    }
    else if (camera->Type & CameraType_Arcball)
    {
	camera->View = view_get2(camera->Position, camera->Orientation);
    }
    else
    {
	vassert_break();
    }

    runtime_camera_update(camera);
}

void
runtime_camera_set_orthograhic(RuntimeCamera* camera)
{
    camera->Type |=  CameraType_Orthographic;
    camera->Type &= ~CameraType_Perspective ;
    camera->Projection = orthographic(camera->Left, camera->ORight, camera->Bot, camera->Top, camera->Near, camera->Far);
}

void
runtime_camera_set_perspective(RuntimeCamera* camera)
{
    camera->Type |=  CameraType_Perspective ;
    camera->Type &= ~CameraType_Orthographic;
    camera->Projection = perspective(camera->Near, camera->Far, camera->AspectRatio, camera->Fov);
}

void
runtime_camera_update_projection(RuntimeCamera* camera)
{
    if (camera->Type & CameraType_Orthographic)
    {
	runtime_camera_set_orthograhic(camera);
    }
    else
    {
	runtime_camera_set_perspective(camera);
    }

    runtime_camera_update(camera);
}

RuntimeCamera
runtime_camera_create_from_settings(RuntimeCameraSettings settings)
{
    vassert(camera_type_is_valid(settings.Type)
	    && "Value of camera type passed into create function is not value!");

    RuntimeCamera this;
    this.Type        = settings.Type         ;
    //NOTE(bies): Projection
    this.Near        = settings.Near         ;
    this.Far	     = settings.Far          ;
    this.AspectRatio = settings.AspectRatio  ;
    this.Fov         = rad(settings.FovAngle);

    this.Front = v3_new(0, 0, -1);
    this.Right = v3_new(1, 0, 0);
    this.Up    = v3_new(0, 1, 0);

    this.Position = settings.Position;
    this.LookAtPoint = v3_new(0, 0, 0);

    this.Yaw = 0;
    this.Pitch = 0;

    this.Distance = 15;
    this.Zoom = 5;
    this.Speed = 5.0f;
    this.Sensitivity = 0.4f;
    this.SensitivityHorizontal = settings.SensitivityHorizontal;
    this.SensitivityVertical   = settings.SensitivityVertical;

    this.Left = settings.Left;
    this.ORight = settings.ORight;
    this.Bot = settings.Bot;
    this.Top = settings.Top;
    this.Near = settings.Near;
    this.Far = settings.Far;

    this.PrevX = settings.PrevX;
    this.PrevY = settings.PrevY;

    runtime_camera_update_view(&this);
    runtime_camera_update_projection(&this);

    return this;
}

RuntimeCamera
runtime_camera_create(RuntimeCameraSettings settings)
{
    RuntimeCamera runtimeCamera = runtime_camera_create_from_settings(settings);
    return runtimeCamera;
}

void
runtime_camera_move(RuntimeCamera* camera, DirectionType direction, f32 timestep)
{
    if (camera->Type & CameraType_Spectator)
    {
	f32 velocity = camera->Speed * timestep;
	camera->IsCameraMoved = 1;

	switch (direction)
	{
	case DirectionType_Forward:
	    camera->Position = v3_add(camera->Position, v3_scale(camera->Front, velocity));
	    break;
	case DirectionType_Backward:
	    camera->Position = v3_sub(camera->Position, v3_scale(camera->Front, velocity));
	    break;
	case DirectionType_Left:
	    camera->Position = v3_sub(camera->Position, v3_scale(camera->Right, velocity));
	    break;
	case DirectionType_Right:
	    camera->Position = v3_add(camera->Position, v3_scale(camera->Right, velocity));
	    break;

	default:
	    camera->IsCameraMoved = 0;
	    break;
	}
    }
    else if (camera->Type & CameraType_Arcball)
    {
	/* v3 forward = get_front_direction(camera); */
	/* v3 forwardDistance = v3_mulv(forward, 1/\* camera->Distance *\/); */
	/* camera->Position = v3_sub(camera->LookAtPoint, forwardDistance); */
    }
}

void
runtime_camera_resize(RuntimeCamera* runtimeCamera, f32 width, f32 height)
{
    runtimeCamera->AspectRatio = width / height;
    runtime_camera_update_projection(runtimeCamera);
}
