#ifndef CAMERA_H
#define CAMERA_H

#include <Utils/Types.h>

/*
  NOTE(typedef): just base camera, only matrices
*/
typedef struct Camera
{
    m4 View;
    m4 Projection;
    m4 ViewProjection;
} Camera;

Camera camera_new_default();
void camera_update_projection(Camera* camera, m4 projection);
void camera_update_view(Camera* camera, m4 view);
i32 camera_is_valid(Camera* camera);

#endif // CAMERA_H
