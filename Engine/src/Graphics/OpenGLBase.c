#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

#include "OpenGLBase.h"

#include <stdlib.h>
#include <assert.h>
#include <stdio.h>
#include <glad/glad.h>

#include <Math/SimpleMath.h>
#include <Utils/stb_image.h>
#include <Utils/stb_image_write.h>
#include <Utils/SimpleStandardLibrary.h>

void
opengl_error_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:
	DO_ONES(GERROR("TYPE_ERROR (mb some set_uniform function is incorrect or smth like this)\n"));
	break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
	GERROR("DEPRECATED_BEHAVIOR\n");
	break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
	GERROR("UNDEFINED_BEHAVIOR\n");
	break;
    case GL_DEBUG_TYPE_PORTABILITY:
	GERROR("PORTABILITY\n");
	break;
    case GL_DEBUG_TYPE_PERFORMANCE:
	GERROR("PERFORMANCE\n");
	break;
    }

    switch (id)
    {
    case GL_NO_ERROR:
    {
	DO_ONES(GSUCCESS("No opengl errors!\n"));
	break;
    }
    case GL_INVALID_ENUM:
    {
	GERROR("%s\n", message);
	GERROR("GL_INVALID_ENUM: An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag.\n");
	break;
    }
    case GL_INVALID_VALUE:
    {
	GERROR("%s\n", message);
	GERROR("GL_INVALID_VALUE: A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag.\n");
	break;
    }
    case GL_INVALID_OPERATION:
    {
	GERROR("%s\n", message);
	GERROR("GL_INVALID_OPERATION: The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag.\n");
	break;
    }
    case GL_INVALID_FRAMEBUFFER_OPERATION:
    {
	GERROR("%s\n", message);
	GERROR("GL_INVALID_FRAMEBUFFER_OPERATION: The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag.\n");
	break;
    }
    case GL_OUT_OF_MEMORY:
    {
	GERROR("%s\n", message);
	GERROR("GL_OUT_OF_MEMORY: There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded.\n");
	break;
    }
    case GL_STACK_UNDERFLOW:
    {
	GERROR("%s\n", message);
	GERROR("GL_STACK_UNDERFLOW: An attempt has been made to perform an operation that would cause an internal stack to underflow.\n");
	break;
    }
    case GL_STACK_OVERFLOW:
    {
	GWARNING("%s\n", message);
	GERROR("GL_STACK_OVERFLOW: An attempt has been made to perform an operation that would cause an internal stack to overflow.\n");
	break;
    }
    default:
	DO_ONES(GWARNING("Unknown error!\n"));
	break;
    }
}

i32
opengl_context_init(GLADloadproc gladLoadProc)
{
    i32 openGLLoadStatus = gladLoadGLLoader(gladLoadProc);
    if (openGLLoadStatus == 0)
    {
	GERROR("Failed to init GLAD\n");
	vassert(openGLLoadStatus == 0 && "OpenGL initialization failed!");
    }

    GINFO("OpenGL version %s\n", glGetString(GL_VERSION));
    GINFO("GLSL version %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    glEnable(GL_DEBUG_OUTPUT);
    glDebugMessageCallback(opengl_error_callback, 0);
    return 1;
}

// change this to (DataType types[SOME_COUNT], i32 count)
BufferLayout*
buffer_layout_create(DataType types[], i32 count)
{
    BufferLayout* result = NULL;
    for (i32 i = 0; i < count; ++i)
    {
	DataType type = types[i];
	BufferLayout layout = {
	    .IsNormalized = 0, //NOTE(bies): always !normalized
	    .Type = type,
	    .Size = data_type_get_size(type),
	    .Count = data_type_get_count(type)
	};

	array_push(result, layout);
    }

    return result;
}
size_t
buffer_layout_get_size(BufferLayout* layouts)
{
    size_t finalSize = 0;
    i32 i, count = array_count(layouts);
    for (i = 0; i < count; ++i)
    {
	BufferLayout layout = layouts[i];
	finalSize += layout.Size;
    }
    return finalSize;
}

i32
vertex_buffer_get_current()
{
    u32 id;
    glGetIntegerv(GL_ARRAY_BUFFER_BINDING, &id);
    return id;
}
void
vertex_buffer_set_data(VertexBuffer* buffer, f32* data, u32 size)
{
    glBindBuffer(GL_ARRAY_BUFFER, buffer->RendererID);
    glBufferSubData(GL_ARRAY_BUFFER, 0, size, data);
}
VertexBuffer
vertex_buffer_create(f32* vertices, u32 size)
{
    VertexBuffer buffer = {
	.Stride = 0,
	.Layout = NULL
    };
    glGenBuffers(1, &buffer.RendererID);
    glBindBuffer(GL_ARRAY_BUFFER, buffer.RendererID);
    glBufferData(GL_ARRAY_BUFFER, size, vertices, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return buffer;
}

VertexBuffer
vertex_buffer_create_allocated(u32 size)
{
    VertexBuffer allocatedBuffer = vertex_buffer_create(NULL, size);
    return allocatedBuffer;
}
force_inline void
_vertex_buffer_stride_update(VertexBuffer* buffer)
{
    buffer->Stride = 0;

    i32 i, offset = 0, layoutsCount = array_count(buffer->Layout);
    for (i = 0; i < layoutsCount; ++i)
    {
	BufferLayout* layout = &buffer->Layout[i];
	layout->Offset  = buffer->Stride;
	buffer->Stride += layout->Size;
    }
}
void
vertex_buffer_add_layout(VertexBuffer* buffer, BufferLayout* layout)
{
    buffer->Layout = layout;
    _vertex_buffer_stride_update(buffer);
}
void
vertex_buffer_bind(VertexBuffer* vbo)
{
    glBindBuffer(GL_ARRAY_BUFFER, vbo->RendererID);
}
void
vertex_buffer_unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

i32
index_buffer_get_current()
{
    u32 id;
    glGetIntegerv(GL_ELEMENT_ARRAY_BUFFER_BINDING, &id);
    return id;
}
IndexBuffer
index_buffer_create(u32* indices, u32 count)
{
    IndexBuffer buffer;
    glGenBuffers(1, &buffer.ID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.ID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(u32), indices, GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    return buffer;
}
IndexBuffer
index_buffer_allocated(size_t size)
{
    IndexBuffer buffer;
    glGenBuffers(1, &buffer.ID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.ID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, NULL, GL_DYNAMIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    return buffer;
}
void
index_buffer_set_data(IndexBuffer* buffer, u32* indices, size_t indicesSize)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->ID);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, indicesSize, indices);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void
index_buffer_bind(IndexBuffer* buffer)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer->ID);
}
void
index_buffer_unbind()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

force_inline void
_vertex_array_add_vbo(VertexArray* vertexArray, VertexBuffer vbo)
{
    vertexArray->Vertex = vbo;

    glBindBuffer(GL_ARRAY_BUFFER, vertexArray->Vertex.RendererID);

    vertex_array_enable_layout(vertexArray);
}
force_inline void
_vertex_array_add_ibo(VertexArray* vertexArray, IndexBuffer ibo)
{
    vertexArray->Index = ibo;
    glBindVertexArray(vertexArray->RendererID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo.ID);
}
i32
vertex_array_get_current()
{
    i32 id = 0;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &id);
    return id;
}
VertexArray
vertex_array_create(VertexBuffer vertexBuffer, IndexBuffer indexBuffer)
{
    VertexArray vertexArray;
    glCreateVertexArrays(1, &vertexArray.RendererID);
    glBindVertexArray(vertexArray.RendererID);

    _vertex_array_add_vbo(&vertexArray, vertexBuffer);
    _vertex_array_add_ibo(&vertexArray, indexBuffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

    return vertexArray;
}
VertexArray
vertex_array_create_wo_index(VertexBuffer vertexBuffer)
{
    VertexArray vertexArray;
    glCreateVertexArrays(1, &vertexArray.RendererID);
    glBindVertexArray(vertexArray.RendererID);

    _vertex_array_add_vbo(&vertexArray, vertexBuffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vertexArray;
}
void
vertex_array_enable_layout(VertexArray* vertexArray)
{
    vguard_not_null(vertexArray);

    BufferLayout* bufferLayouts = vertexArray->Vertex.Layout;
    i32 i,
	stride = vertexArray->Vertex.Stride,
	layoutsCount = array_count(bufferLayouts);
    for (i = 0; i < layoutsCount; ++i)
    {
	BufferLayout layout = bufferLayouts[i];
	glEnableVertexAttribArray(i);
	glVertexAttribPointer(i, layout.Count, GL_FLOAT, layout.IsNormalized, stride, (const void*)layout.Offset);
    }
}
void
vertex_array_disable_layout(VertexArray* vertexArray)
{
    vguard_not_null(vertexArray);

    i32 i, layoutsCount = array_count(vertexArray->Vertex.Layout);
    for (i = 0; i < layoutsCount; ++i)
    {
	glDisableVertexAttribArray(i);
    }
}
void
vertex_array_bind(VertexArray* vertexArray)
{
    vguard_not_null(vertexArray);

    glBindVertexArray(vertexArray->RendererID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexArray->Vertex.RendererID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexArray->Index.ID);
    vertex_array_enable_layout(vertexArray);
}
void
vertex_array_unbind(VertexArray* vertexArray)
{
    vguard_not_null(vertexArray);

    vertex_array_disable_layout(vertexArray);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    //For some reason this auto unbind
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void
vertex_array_bind_wo_index(VertexArray* vertexArray)
{
    glBindVertexArray(vertexArray->RendererID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexArray->Vertex.RendererID);
    vertex_array_enable_layout(vertexArray);
}
void
vertex_array_unbind_wo_index(VertexArray* vertexArray)
{
    vertex_array_disable_layout(vertexArray);
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void
vertex_array_destroy(VertexArray* va)
{
    glDeleteVertexArrays(1, &va->RendererID);
}



force_inline Framebuffer
framebuffer_attachments_validate(FramebufferType* attachmentTypes, i32 attachmentCount)
{
    Framebuffer framebuffer = {
	.IsSwapChainTarget = 0,
	.SamplesCount = 0,
	.Width = 0,
	.Height = 0,
	.Samples = 0,
	.RendererId = -1,
	.Count = 0,
	.ColorAttachments = { -1, -1, -1, -1 },
	.DepthAttachment = 0,
	.AttachmentTypes = { FramebufferType_None, FramebufferType_None, FramebufferType_None, FramebufferType_None }
    };

    i32 depthAttachmentsCount = 0;
    i32 colorAttachmentsCount = 0;

    vassert(attachmentCount >= 1 && "At least 1 attachment type!");
    vassert(attachmentCount <= 4 && "4 - max attachments types!");

    for (i32 a = 0; a < attachmentCount; ++a)
    {
	FramebufferType attachmentType = attachmentTypes[a];
	switch (attachmentType)
	{
	case FramebufferType_Depth:
	    framebuffer.AttachmentTypes[colorAttachmentsCount] = attachmentType;
	    ++colorAttachmentsCount;
	    ++depthAttachmentsCount;
	    break;
	default:
	    framebuffer.AttachmentTypes[colorAttachmentsCount] = attachmentType;
	    ++colorAttachmentsCount;
	    break;
	}
    }

    vassert(depthAttachmentsCount <= 1 && "Only 1 depth buffer attachment needed for Framebuffer!");
    vassert(colorAttachmentsCount >= 1 && "At least 1 color attachment needed for Framebuffer");

    return framebuffer;
}

Framebuffer
framebuffer_create(FramebufferSettings settings)
{
    Framebuffer framebuffer =
	framebuffer_attachments_validate(settings.Attachments, settings.Count);

    framebuffer_invalidate(&framebuffer, settings.Width, settings.Height, settings.Attachments, settings.Count, settings.SamplesCount);

    return framebuffer;
}

void
framebuffer_invalidate(Framebuffer* framebuffer, u32 width, u32 height,
		       FramebufferType* attachments, i32 count, i32 samplesCount)
{
    vassert(framebuffer->AttachmentTypes && "You need framebuffer attachments if you want framebuffer to work!");

    if (framebuffer->RendererId)
    {
	framebuffer_destroy(framebuffer);
    }

    i32 isMultisampled = samplesCount > 0;
    i32 textureType = isMultisampled ? GL_TEXTURE_2D_MULTISAMPLE : GL_TEXTURE_2D;

    framebuffer->SamplesCount = samplesCount;
    framebuffer->Count = count;

    glCreateFramebuffers(1, &framebuffer->RendererId);
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->RendererId);

#define TextureParams(textureType)					\
    {									\
	glTexParameteri(textureType, GL_TEXTURE_MIN_FILTER, GL_LINEAR); \
	glTexParameteri(textureType, GL_TEXTURE_MAG_FILTER, GL_LINEAR); \
	glTexParameteri(textureType, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); \
	glTexParameteri(textureType, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); \
    }

    for (i32 i = 0; i < count; ++i)
    {
	FramebufferType type = attachments[i];

	switch (type)
	{

	case FramebufferType_Rgba:
	{
	    glCreateTextures(textureType, 1, &framebuffer->ColorAttachments[i]);
	    glBindTexture(textureType, framebuffer->ColorAttachments[i]);
	    if (isMultisampled)
	    {
		glTexImage2DMultisample(textureType, framebuffer->SamplesCount, GL_RGBA8, width, height, GL_TRUE);
	    }
	    else
	    {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
		TextureParams(textureType);
	    }
	    glBindTexture(textureType, 0);

	    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, textureType, framebuffer->ColorAttachments[i], 0);
	    break;
	}

	case FramebufferType_RedInteger:
	{
	    glCreateTextures(textureType, 1, &framebuffer->ColorAttachments[i]);
	    glBindTexture(textureType, framebuffer->ColorAttachments[i]);
	    if (isMultisampled)
	    {
		glTexImage2DMultisample(textureType, framebuffer->SamplesCount, GL_R32I, width, height, GL_TRUE);
	    }
	    else
	    {
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, width, height, 0, GL_RED_INTEGER, GL_UNSIGNED_BYTE, NULL);
		TextureParams(textureType);
	    }
	    glBindTexture(textureType, 0);

	    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, textureType, framebuffer->ColorAttachments[i], 0);
	    break;
	}

	case FramebufferType_Depth:
	{
	    glCreateTextures(textureType, 1, &framebuffer->DepthAttachment);
	    glBindTexture(textureType, framebuffer->DepthAttachment);
	    if (isMultisampled)
	    {
		glTexImage2DMultisample(textureType, framebuffer->SamplesCount, GL_DEPTH24_STENCIL8, width, height, GL_TRUE);
	    }
	    else
	    {
		glTexImage2D(textureType, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);
		TextureParams(textureType);
	    }
	    glBindTexture(textureType, 0);
	    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, textureType, framebuffer->DepthAttachment, 0);

	    break;
	}

	}

    }

    //GLenum error = glGetError();
    //GERROR("");
    //vassert(error ==  GL_NO_ERROR && "Error!");

    vassert(framebuffer->DepthAttachment > 0 && "DepthAttachment texture problem!");
    GLenum framebufferStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if (framebufferStatus != GL_FRAMEBUFFER_COMPLETE)
    {
	GERROR("FramebufferStatus: %d\n", framebufferStatus);
	vassert(framebufferStatus == GL_FRAMEBUFFER_COMPLETE && "Failed in framebuffer creation !!! :(");
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    framebuffer->Width = width;
    framebuffer->Height = height;
}

i32
framebuffer_read_pixel(Framebuffer* framebuffer, i32 attachment, i32 x, i32 y)
{
    i32 pixelData;
    glReadBuffer(GL_COLOR_ATTACHMENT0 + attachment);
    glReadPixels(x, y, 1, 1, GL_RED_INTEGER, GL_INT, &pixelData);
    return pixelData;
}

void
framebuffer_read_pixel_color(Framebuffer* framebuffer, i32 attachment, i32 x, i32 y, v4 result)
{
    u8 data[4];
    glReadBuffer(GL_COLOR_ATTACHMENT0 + attachment);
    // argb
    glReadPixels(x, y, 1, 1, GL_RGBA, GL_UNSIGNED_BYTE, data);

    result.X = data[0] / 255.0f;
    result.Y = data[1] / 255.0f;
    result.Z = data[2] / 255.0f;
    result.W = data[3] / 255.0f;
}

void
framebuffer_bind(Framebuffer* framebuffer)
{
    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->RendererId);
    glViewport(0, 0, framebuffer->Width, framebuffer->Height);
}

void
framebuffer_multisample_blit(Framebuffer* framebuffer, Framebuffer* toDraw)
{
    glBlitNamedFramebuffer(framebuffer->RendererId, toDraw->RendererId, 0, 0, framebuffer->Width, framebuffer->Height, 0, 0, framebuffer->Width, framebuffer->Height, GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void
framebuffer_unbind()
{
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void
framebuffer_transfer(u32 read, u32 draw, u32 width, u32 height)
{
    glBlitNamedFramebuffer(read, draw,
			   0, 0, width, height,
			   0, 0, width, height,
			   GL_COLOR_BUFFER_BIT, GL_NEAREST);
}

void
framebuffer_destroy(Framebuffer* framebuffer)
{
    //GWARNING("Destroy framebuffer!\n");

    glDeleteFramebuffers(1, &framebuffer->RendererId);
    glDeleteTextures(1, framebuffer->ColorAttachments);
    glDeleteTextures(1, &framebuffer->DepthAttachment);
    framebuffer->DepthAttachment = 0;
}

Texture2D
texture2d_create_from_buffer(void* data, u32 width, u32 height, u8 channels, TextureFilterType filterType)
{
    GLenum dataFormat, internalFormat;
    Texture2D texture;

    if (channels == 1)
    {
	internalFormat = GL_RED;
	dataFormat = GL_RED;
    }
    else if (channels == 3)
    {
	internalFormat = GL_RGB;
	dataFormat = GL_RGB;
    }
    else if (channels == 4)
    {
	internalFormat = GL_RGBA;
	dataFormat = GL_RGBA;
    }
    else
    {
	vassert(0 && "Channels error!");
    }

    texture.Width = width;
    texture.Height = height;
    texture.Channels = channels;
    texture.DataFormat = dataFormat;
    texture.Slot = -1;

    glCreateTextures(GL_TEXTURE_2D, 1, &texture.ID);
    glBindTexture(GL_TEXTURE_2D, texture.ID);

    if (filterType == TextureFilterType_Linear)
    {
	glTextureParameteri(texture.ID, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTextureParameteri(texture.ID, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    }
    else
    {
	glTextureParameteri(texture.ID, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTextureParameteri(texture.ID, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    }
    // x clamp to edge
    glTextureParameteri(texture.ID, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE/*GL_REPEAT*/);
    // y clamp to edge
    glTextureParameteri(texture.ID, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE/*GL_REPEAT*/);

    glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0, dataFormat, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    return texture;
}

Texture2D
texture2d_create(const char* path, Texture2DSettings settings)
{
    if (settings.IsFlipOnLoad)
    {
	stbi_set_flip_vertically_on_load(1);
    }
    else
    {
	stbi_set_flip_vertically_on_load(0);
    }

    // passing 4 instead of 0 for forcing 4 channels
    i32 width, height, channels;
    stbi_uc* data = stbi_load(path, &width, &height, &channels, 0);
    if (data == NULL)
    {
	GERROR("Texture could not be loaded %s\n", path);
	assert(0 && "Failed to load a texture!");
    }
    Texture2D texture = texture2d_create_from_buffer(data, width, height, channels, settings.FilterType);
    texture.Path = path;

    stbi_image_free(data);
    glBindTexture(GL_TEXTURE_2D, 0);

    return texture;
}

void
texture2d_set_data(Texture2D* texture, void* data)
{
    glBindTextureUnit(texture->Slot, texture->ID);
    glTextureSubImage2D(texture->ID, 0, 0, 0, texture->Width, texture->Height, texture->DataFormat, GL_UNSIGNED_BYTE, data);
}

void
texture2d_bind(Texture2D* texture, u32 slot)
{
    texture->Slot = slot;
    glBindTextureUnit(slot, texture->ID);
}

void
texture2d_unbind(Texture2D* texture)
{
    glBindTextureUnit(texture->Slot, 0);
}

#define NEW 0
void
texture2d_bind_index(u32 id, u32 slot)
{
    //4.5
#if NEW == 1
    glBindTextureUnit(slot, id);
#else
    //old
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, id);
#endif
}
void
texture2d_unbind_index(u32 id, u32 slot)
{
#if NEW == 1
    glBindTextureUnit(slot, 0);
#else
    glActiveTexture(GL_TEXTURE0 + slot);
    glBindTexture(GL_TEXTURE_2D, 0);
#endif
}

void
texture2d_delete(Texture2D* texture)
{
    glDeleteTextures(1, &texture->ID);
}

void
texture2d_destroy(Texture2D texture)
{
    texture2d_delete(&texture);
}

//WIP
void
texture2d_jpg_to_png(const char* path)
{
    const char* directory = path_get_directory(path);
    char** splitted = string_split((char*)path_get_name(path), '.');
    char* resultString = path_combine(directory, splitted[0]);
    char* writePath = string_concat(resultString, ".png");

    GINFO("WritePath: %s\n", writePath);

    stbi_set_flip_vertically_on_load(0);

    i32 width, height, channels;
    stbi_uc* data = stbi_load(path, &width, &height, &channels, 0);
    if (data == NULL)
    {
	assert(0 && "Failed to load a texture!");
    }

    i32 result = stbi_write_png(writePath, width, height, channels, data, 0);
    if (result != 1)
    {
	GERROR("Write PNG image failed!\n");
	vassert_break();
    }

    stbi_image_free(data);
    memory_free((void*)directory);
    array_free(splitted);
    memory_free(resultString);
}

void
save_image_edge(void* data, const char* edgeFilePath, v2i startPos, v2i imageSize, i32 bigImageWidth, i32 channels)
{
    size_t size = imageSize.Width * imageSize.Height * channels;
    void* edgeData = memory_allocate(size);
    u32* writeEdgeData = (u32*) edgeData;
    i32 y, x, imageHeight = startPos.Y + imageSize.Height, imageWidth = startPos.X + imageSize.Width;

    for (y = startPos.Y; y < imageHeight; ++y)
    {
	for (x = startPos.X; x < imageWidth; ++x)
	{
	    u32* edgeDataRead = (u32*) (((void*)data) + (y * bigImageWidth * channels) + (x * channels));

	    *writeEdgeData = *edgeDataRead;

	    ++writeEdgeData;
	}
    }

    i32 result = stbi_write_png(edgeFilePath, imageSize.Width, imageSize.Height, channels, edgeData, 0);
    if (result != 1)
    {
	GERROR("TopImage write failed!\n");
	vassert_break();
    }

    memory_free(edgeData);
}
/*


		       w
		a*___________*b
		  |         |
		h |         |
		  |         |
	________c*===========*d____________________
	|         |         ||         ||         |
	|         |         ||         ||         |
	|         |         ||         ||         |
	----------===========----------------------
		  |         |
		  |         |
		  |         |
		  -----------


*/
void
cubemap_texture_split(const char* loadPath)
{
    stbi_set_flip_vertically_on_load(0);

    i32 width, height, channels;
    stbi_uc* data = stbi_load(loadPath, &width, &height, &channels, 0);
    if (data == NULL)
    {
	assert(0 && "Failed to load a texture!");
    }

    vassert(channels == 4 && "Channels should be 32 bits only!");

    void* wdata = memory_allocate(width * height * channels);
    u32* readData = (u32*) data;
    u32* writeData = (u32*) wdata;

    i32 imageWidth = 0;
    i32 imageHeight = 0;

    i32 topAIsNotSet = 1;
    i32 topBIsNotSet = 1;
    i32 topCIsNotSet = 1;
    v2i topA = v2i_new(-1, -1);
    v2i topB = v2i_new(-1, -1);
    v2i topC = v2i_new(-1, -1);

    v2i frontA = { 0, 0 };
    v2i leftA  = { 0, 0 };
    v2i bottomA = { 0, 0 };
    v2i rightA = { 0, 0 };
    v2i backA = { 0, 0 };

    for (i32 y = 0; y < height; ++y)
    {
	for (i32 x = 0; x < width; ++x)
	{
	    u32 readPixel = *readData;
	    u32 nextPixel = *((u32*) (((void*)readData) + channels));
	    u32 prevPixel = *((u32*) (((void*)readData) - channels));
	    //u32 rowPrevPixel = *((u32*) (((void*)readData) - (width * channels)));
	    u32 botLeft = *((u32*) (((void*)readData) + (width * (channels - 1))));

	    if (topAIsNotSet && readPixel > 0 && prevPixel == 0)
	    {
		topAIsNotSet = 0;

		topA = v2i_new(x, y);
		GINFO("A: %d %d\n", topA.X, topA.Y);

		*writeData = RGBA(255, 0, 0, 255);
	    }
	    else if (topBIsNotSet && prevPixel > 0 && nextPixel == 0)
	    {
		topBIsNotSet = 0;

		topB = v2i_new(x, y);
		GINFO("B: %d %d\n", x, y);

		imageWidth = topB.X - topA.X;

		*writeData = RGBA(255, 0, 0, 255);
	    }
	    else if (topCIsNotSet && x > 0 && y > 0 && readPixel > 0 && prevPixel == 0 && botLeft > 0)
	    {
		topCIsNotSet = 0;

		topC = v2i_new(x, y);
		GINFO("C: %d %d\n", x, y);

		imageHeight = topC.Y - topA.Y;
		*writeData = RGBA(255, 0, 0, 255);

		GINFO("Width: %d Height: %d\n", imageWidth, imageHeight);

		//setting other positions
		frontA = v2i_new(topA.X, topA.Y + imageHeight);
		leftA = v2i_new(frontA.X - imageWidth, frontA.Y);
		bottomA = v2i_new(frontA.X, frontA.Y + imageHeight);
		rightA = v2i_new(frontA.X + imageWidth, frontA.Y);
		backA = v2i_new(frontA.X + 2*imageWidth, frontA.Y);
	    }
	    else
	    {
		*writeData = *readData;
	    }

	    ++writeData;
	    ++readData;
	}
    }

#define v2i_is_default(v)			\
    ({						\
	vguard(v.X != 0 && v.Y != 0 && "Wrong v type");	\
    })

    // BUG GUARD TIME
    v2i_is_default(frontA);
    v2i_is_default(leftA);
    v2i_is_default(bottomA);
    v2i_is_default(rightA);
    v2i_is_default(backA);

    size_t size = imageWidth * imageHeight * channels;
    GERROR("Final Image Size: %d\n", size);

    const char* edgeDirectory = path_get_directory(loadPath);
    char* topPath = path_combine(edgeDirectory, "top.png");
    char* frontPath = path_combine(edgeDirectory, "front.png");
    char* bottomPath = path_combine(edgeDirectory, "bottom.png");
    char* leftPath = path_combine(edgeDirectory, "left.png");
    char* rightPath = path_combine(edgeDirectory, "right.png");
    char* backPath = path_combine(edgeDirectory, "back.png");

    save_image_edge(data, topPath, topA, v2i_new(imageWidth, imageHeight), width, channels);
    save_image_edge(data, frontPath, frontA, v2i_new(imageWidth, imageHeight), width, channels);
    save_image_edge(data, bottomPath, bottomA, v2i_new(imageWidth, imageHeight), width, channels);
    save_image_edge(data, leftPath, leftA, v2i_new(imageWidth, imageHeight), width, channels);
    save_image_edge(data, rightPath, rightA, v2i_new(imageWidth, imageHeight), width, channels);
    save_image_edge(data, backPath, backA, v2i_new(imageWidth, imageHeight), width, channels);

    memory_free((void*)edgeDirectory);
    memory_free(topPath);
    memory_free(frontPath);
    memory_free(bottomPath);
    memory_free(leftPath);
    memory_free(rightPath);
    memory_free(backPath);

    stbi_image_free(data);
    memory_free(wdata);
}

CubeMap
cube_map_create(const char* directoryPath)
{
    CubeMap cubemap;
    glGenTextures(1, &cubemap.ID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemap.ID);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    char* top = path_combine(directoryPath, "top.png");
    char* front = path_combine(directoryPath, "front.png");
    char* bottom = path_combine(directoryPath, "bottom.png");
    char* left = path_combine(directoryPath, "left.png");
    char* right = path_combine(directoryPath, "right.png");
    char* back = path_combine(directoryPath, "back.png");

    stbi_set_flip_vertically_on_load(0);
#define TextureImage(path, attribute)					\
    ({									\
	i32 width, height, channels;					\
	stbi_uc* data = stbi_load(path, &width, &height, &channels, 0); \
	if (!data)							\
	{								\
	    vassert_break();						\
	}								\
									\
	if (channels == 3)						\
	{								\
	    glTexImage2D(attribute, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data); \
	}								\
	else								\
	{								\
	    glTexImage2D(attribute, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data); \
	}								\
									\
	stbi_image_free(data);						\
    })

    TextureImage(right, GL_TEXTURE_CUBE_MAP_POSITIVE_X);
    TextureImage(left, GL_TEXTURE_CUBE_MAP_NEGATIVE_X);
    TextureImage(top, GL_TEXTURE_CUBE_MAP_POSITIVE_Y);
    TextureImage(bottom, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y);
    TextureImage(front, GL_TEXTURE_CUBE_MAP_POSITIVE_Z);
    TextureImage(back, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z);

    memory_free(top);
    memory_free(front);
    memory_free(bottom);
    memory_free(left);
    memory_free(right);
    memory_free(back);

    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

    return cubemap;
}

CubeMap
cube_map_create_from_single_file(const char* path)
{
    cubemap_texture_split(path);

    const char* directory = path_get_directory(path);

    CubeMap cubemap = cube_map_create(directory);

    memory_free((void*)directory);

    return cubemap;
}

void
texture_atlas_create(TextureAtlas* atlas, const char* path, v2 atlasSize, v2 textureSize)
{
    atlas->AtlasWidth    = atlasSize.X;
    atlas->AtlasHeight   = atlasSize.Y;
    atlas->TextureWidth  = textureSize.X;
    atlas->TextureHeight = textureSize.Y;

    Texture2DSettings settings = {
	.IsFlipOnLoad = 1,
	.FilterType = TextureFilterType_Linear
    };
    atlas->Texture = texture2d_create(path, settings);
}

static Shader* g_Shaders = NULL;
static i32 IsShaderDebugInfoVisible = 0;

void
shader_debug(i32 visible)
{
    IsShaderDebugInfoVisible = visible;
}

ShaderSource
shader_load(const char* shaderPath)
{
    i32 vertexIndex;
    i32 fragmentIndex;
    i32 vertexKeywordLength;
    i32 fragmentKeywordLength;
    char* shaderSource;
    const char* vertexShaderSource;
    const char* fragmentShaderSource;

    shaderSource = file_read_string(shaderPath);
    if (shaderSource == NULL)
    {
	GERROR("shader file %s open error!\n", shaderPath);
	vassert(0 && "shader file");
	return (ShaderSource) { };
    }

    vertexIndex = string_index_of_string(shaderSource, "#vertex shader");
    fragmentIndex = string_index_of_string(shaderSource, "#fragment shader");
    vertexKeywordLength = string_length("#vertex shader");
    fragmentKeywordLength = string_length("#fragment shader");

    vertexShaderSource = string_substring_range(shaderSource, vertexIndex + vertexKeywordLength + 1, (fragmentIndex - 1));
    fragmentShaderSource = string_substring(shaderSource, fragmentIndex + fragmentKeywordLength + 1);

    if (IsShaderDebugInfoVisible)
    {
	GDEBUG("vertex shader:\n"YELLOW("%s\n"), vertexShaderSource);
	GDEBUG("fragment shader:\n"GREEN("%s\n"), fragmentShaderSource);
    }

    if (shaderSource)
    {
	memory_free(shaderSource);
    }

    ShaderSource source = (ShaderSource) {
	.vertex_shader   = (char*) vertexShaderSource,
	.fragment_shader = (char*) fragmentShaderSource
    };

    return source;
}

#define ShaderErrorCheck(shaderID, shaderTypeName) ({ shader_error_check(shaderID, #shaderTypeName); })

static void
shader_error_check(u32 shaderID, const char* shaderTypeName)
{
    i32 isCompiled = GL_FALSE;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &isCompiled);
    if (isCompiled != GL_FALSE)
    {
	return;
    }

    GLint maxLength = 0;
    glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &maxLength);
    if (maxLength <= 0)
    {
	return;
    }
    char* message = memory_allocate(maxLength * sizeof(char));

    char errorMessage[1024];
    glGetShaderInfoLog(shaderID, maxLength, &maxLength, message);

    GERROR("shader_error_check[is_compiled: %d, log_length:%d]:\n        %s\n", isCompiled, maxLength, message);
    memory_free(message);
    GERROR("(%s) Shader compilation ERROR!!!\n\n", shaderTypeName);
    vassert(0 && "Shader compilation error!!!");
    glDeleteShader(shaderID);
}

Shader
shader_compile(ShaderSource source)
{
    Shader shader = {};

    u32 vertexShaderId;
    u32 fragmentShaderId;
    u32 shaderProgramId;

    if (source.vertex_shader == NULL || source.fragment_shader == NULL || string_length(source.vertex_shader) < 0 || string_length(source.fragment_shader) < 0)
    {
	vassert(0 && "Shader source is not loaded correctly!");
	return (Shader) { .ShaderID = -1, .UniformTable = NULL };
    }

    // Vertex shader
    vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShaderId, 1, &source.vertex_shader, 0);
    glCompileShader(vertexShaderId);
    ShaderErrorCheck(vertexShaderId, vertexShaderId);

    // Fragment shader
    fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShaderId, 1, &source.fragment_shader, 0);
    glCompileShader(fragmentShaderId);
    ShaderErrorCheck(fragmentShaderId, fragmentShaderId);

    // GDEBUG(GREEN("Linking")" program\n");
    shaderProgramId = glCreateProgram();
    glAttachShader(shaderProgramId, vertexShaderId);
    glAttachShader(shaderProgramId, fragmentShaderId);
    glLinkProgram(shaderProgramId);
    i32 isLinkingSuccess = 0;
    glGetProgramiv(shaderProgramId, GL_LINK_STATUS, &isLinkingSuccess);
    if (!isLinkingSuccess)
    {
	char infoLog[512];
	glGetProgramInfoLog(shaderProgramId, 512, NULL, infoLog);
	GERROR("Shader Linking Error, details: %s\n", infoLog);
	vassert_break();
    }

    glDetachShader(shaderProgramId, vertexShaderId);
    glDetachShader(shaderProgramId, fragmentShaderId);

    glDeleteShader(vertexShaderId);
    glDeleteShader(fragmentShaderId);

    array_push(g_Shaders, shader);

    return (Shader) { .ShaderID = shaderProgramId, .UniformTable = NULL };
}

Shader
shader_create(const char* shaderPath)
{
    ShaderSource sources = shader_load(shaderPath);
    Shader shader = shader_compile(sources);
    return shader;
}
Shader
shader_compile_safe(const char* shaderPath)
{
    ShaderSource shaderSource = shader_load(shaderPath);
    Shader shader = shader_compile(shaderSource);
    shader_unbind();
    return shader;
}

void
shader_delete(Shader* shader)
{
    glDeleteProgram(shader->ShaderID);
    shash_free(shader->UniformTable);
}

void
shader_bind(Shader* shader)
{
    glUseProgram(shader->ShaderID);
}

void
shader_unbind()
{
    glUseProgram(0);
}

void
shader_delete_collection()
{
    // GDEBUG(GREEN("Begin: delete collection of shaders\n"));

    if (g_Shaders == NULL)
    {
	return;
    }

    Shader shader;
    i32 i, count = array_count(g_Shaders);
    for (i = 0; i < count; i++)
    {
	shader = g_Shaders[i];
	shader_delete(&shader);
    }

    // GDEBUG(GREEN("End: delete collection of shaders\n"));
}

#define CheckShaderUniform(shader, uniformName, functionWithParams)	\
    {									\
	i32 location = shader_get_location(shader, uniformName);	\
	if (location >= 0)						\
	{								\
	    functionWithParams;						\
	}								\
	else								\
	{								\
	    GERROR("NAME: %s\n", uniformName);				\
	    vassert(0 && "Unknown uniform!");				\
	}								\
    }


force_inline i32
shader_get_location(Shader* shader, const char* uniformName)
{
    vassert(uniformName != NULL);
    return glGetUniformLocation(shader->ShaderID, uniformName);
}

void
shader_set_1float(Shader* shader, const char* uniformName, f32 v0)
{
    CheckShaderUniform(shader, uniformName, glUniform1f(location, v0));
}

void
shader_set_2float(Shader* shader, const char* uniformName, f32 v0, f32 v1)
{
    CheckShaderUniform(shader, uniformName, glUniform2f(location, v0, v1););
}

void
shader_set_3float(Shader* shader, const char* uniformName, f32 v0, f32 v1, f32 v2)
{
    CheckShaderUniform(shader, uniformName, glUniform3f(location, v0, v1, v2););
}

void
shader_set_4float(Shader* shader, const char* uniformName, f32 v0, f32 v1, f32 v2, f32 v3)
{
    CheckShaderUniform(shader, uniformName, glUniform4f(location, v0, v1, v2, v3););
}
void
shader_set_color(Shader* shader, const char* uniformName, v4 color)
{
    CheckShaderUniform(shader, uniformName, glUniform4f(location, color.R, color.G, color.B, color.A););
}


void
shader_set_1int(Shader* shader, const char* uniformName, i32 v0)
{
    CheckShaderUniform(shader, uniformName, glUniform1i(location, v0));
}

void
shader_set_2int(Shader* shader, const char* uniformName, i32 v0, i32 v1)
{
    CheckShaderUniform(shader, uniformName, glUniform2i(location, v0, v1));
}

void
shader_set_3int(Shader* shader, const char* uniformName, i32 v0, i32 v1, i32 v2)
{
    CheckShaderUniform(shader, uniformName, glUniform3i(location, v0, v1, v2));
}

void
shader_set_4int(Shader* shader, const char* uniformName, i32 v0, i32 v1, i32 v2, i32 v3)
{
    CheckShaderUniform(shader, uniformName, glUniform4i(location, v0, v1, v2, v3));
}

void
shader_set_1uint(Shader* shader, const char* uniformName, u32 v0)
{
    CheckShaderUniform(shader, uniformName, glUniform1ui(location, v0););
}

void
shader_set_2uint(Shader* shader, const char* uniformName, u32 v0, u32 v1)
{
    CheckShaderUniform(shader, uniformName, glUniform2ui(location, v0, v1));
}

void
shader_set_3uint(Shader* shader, const char* uniformName, u32 v0, u32 v1, u32 v2)
{
    CheckShaderUniform(shader, uniformName, glUniform3ui(location, v0, v1, v2));
}

void
shader_set_4uint(Shader* shader, const char* uniformName, u32 v0, u32 v1, u32 v2, u32 v3)
{
    CheckShaderUniform(shader, uniformName, glUniform4ui(location, v0, v1, v2, v3));
}


void
shader_set_float1(Shader* shader, const char* uniformName, i32 count, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform1fv(location, count, values));
}
void
shader_set_float(Shader* shader, const char* uniformName, f32 value)
{
    CheckShaderUniform(shader, uniformName, glUniform1f(location, value));
}
void
shader_set_f32(Shader* shader, const char* uniformName, f32 value)
{
    CheckShaderUniform(shader, uniformName, glUniform1fv(location, 1, &value));
}

void
shader_set_float2(Shader* shader, const char* uniformName, i32 count, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform2fv(location, count, values));
}

void
shader_set_float3(Shader* shader, const char* uniformName, i32 count, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform3fv(location, count,  values));
}
void
shader_set_v3(Shader* shader, const char* uniformName, v3 vector)
{
    CheckShaderUniform(shader, uniformName, glUniform3fv(location, 1,  vector.V));
}

void
shader_set_float4(Shader* shader, const char* uniformName, i32 count, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform4fv(location, count, values));
}
void
shader_set_v4(Shader* shader, const char* uniformName, v4 vector)
{
    CheckShaderUniform(shader, uniformName, glUniform4fv(location, 1, vector.V));
}

void
shader_set_i32(Shader* shader, const char* uniformName, i32 value)
{
    CheckShaderUniform(shader, uniformName, glUniform1i(location, value));
}
void
shader_set_texture(Shader* shader, const char* uniformName, u32 textureID)
{
    i32 location = shader_get_location(shader, uniformName);
    if (location >= 0)
    {
	glUniform1i(location, textureID);
    }
    else
    {
	GERROR("NAME: %sn", uniformName);
	vassert(0 && "Unknown uniform!");
    }
}

void
shader_set_int1(Shader* shader, const char* uniformName, i32 count, i32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform1iv(location, count, values));
}

void
shader_set_int2(Shader* shader, const char* uniformName, i32 count, i32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform2iv(location, count, values));
}

void
shader_set_int3(Shader* shader, const char* uniformName, i32 count, i32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform3iv(location, count, values));
}

void
shader_set_int4(Shader* shader, const char* uniformName, i32 count, i32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform4iv(location, count, values));
}

void
shader_set_uint1(Shader* shader, const char* uniformName, i32 count, u32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform4uiv(location, count, values));
}

void
shader_set_uint2(Shader* shader, const char* uniformName, i32 count, u32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform2uiv(location, count, values));
}

void
shader_set_uint3(Shader* shader, const char* uniformName, i32 count, u32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform3uiv(location, count, values));
}

void
shader_set_uint4(Shader* shader, const char* uniformName, i32 count, u32* values)
{
    CheckShaderUniform(shader, uniformName, glUniform4uiv(location, count, values));
}

void
shader_set_mat2(Shader* shader, const char* uniformName, i32 count, i8 transpose, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniformMatrix2fv(location, count, transpose, values));
}

void
shader_set_m3(Shader* shader, const char* uniformName, i32 count, i8 transpose, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniformMatrix3fv(location, count, transpose, values));
}

void
shader_set_m4(Shader* shader, const char* uniformName, i32 count, i8 transpose, f32* values)
{
    CheckShaderUniform(shader, uniformName, glUniformMatrix4fv(location, count, transpose, values));
}
