#include "VulkanSimpleRenderer.h"

#include <AssetManager/AssetManager.h>
#include <Graphics/Camera.h>
#include <Math/SimpleMath.h>
#include <Utils/SimpleStandardLibrary.h>

typedef struct VsrFrameData
{
    i32 ImageIndex;
    VsaPipeline** Pipelines;
} VsrFrameData;

static VsrFrameData gVsrFrameData;

void
vsr_add_pipeline(VsaPipeline* pVsaPipeline)
{
    array_push(gVsrFrameData.Pipelines, pVsaPipeline);
}

void
vsr_frame_begin()
{
    vsa_local_frame_start(&gVsrFrameData.ImageIndex);
}

void
vsr_draw_all()
{
    vsa_render_pass_proccess(gVsrFrameData.Pipelines, array_count(gVsrFrameData.Pipelines), gVsrFrameData.ImageIndex);
}

void
vsr_frame_end()
{
    vsa_local_frame_end(&gVsrFrameData.ImageIndex);
}

i32
vsr_get_image_index()
{
    return gVsrFrameData.ImageIndex;
}

/*
  DOCS(typedef):

*/

VsaFont
vsa_font_info_create(const char* path, i32 size, i32 isSaveBitmap)
{
    const char* texturePrefixPath = asset_texture("Generated_");
    char* nameWoExt = path_get_name_wo_extension(path);
    char* finalTexturePath = string_concat3(texturePrefixPath, nameWoExt, ".png");
    memory_free(nameWoExt);

    FontInfoSettings fontInfoSet = {
	.Path = path,
	.FontSize = size,
	.IsSaveBitmap = isSaveBitmap,
	.BitmapSavePath = finalTexturePath
    };
    Font font = font_cirilic_create(fontInfoSet);
    VsaTexture atlas = vsa_texture_new(finalTexturePath);

    VsaFont vsaFontInfo = {
	.Font = font,
	.Atlas = atlas
    };

    return vsaFontInfo;
}

void
vsa_font_info_destroy(VsaFont vsaFontInfo)
{
    font_destroy(vsaFontInfo.Font);
    vsa_texture_destroy(vsaFontInfo.Atlas);
}

// Generic and for all
typedef struct CameraUbo
{
    m4 ViewProjection;
} CameraUbo;

typedef struct FontUbo
{
    f32 Width;
    f32 EdgeTransition;
} FontUbo;

void
renderer2d_draw_data_copy(Renderer2dDrawData* this, Renderer2dDrawData* toCopy)
{
    Arena* arena = memory_get_arena();
    if (arena != NULL)
	GINFO("Arena not NULL while data copy!\n");

    memory_unbind_current_arena();
    if (this->Vertices != NULL)
    {
	array_free(this->Vertices);
    }

    this->Vertices = toCopy->Vertices;
    this->IndicesToDraw = toCopy->IndicesToDraw;
    toCopy->Vertices = NULL;
    toCopy->IndicesToDraw = 0;

    memory_bind_current_arena();
}

void
vsr_2d_create(Renderer2d* renderer, const Renderer2dSettings* settings)
{
    renderer->pCamera = settings->pCamera;

    // ===============================
    //       DOCS() Demo PART
    // ===============================
    VsaShaderStage stages[] = {
	[0] = {
	    .Type = VsaShaderType_Vertex,
	    .ShaderSourcePath = asset_shader("Vulkan/Renderer2d.vert"),
	    .Descriptors = {
		[0] = {
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(CameraUbo)
		}
	    },
	    .DescriptorsCount = 1
	},

	[1] = {
	    .Type = VsaShaderType_Fragment,
	    .ShaderSourcePath = asset_shader("Vulkan/Renderer2d.frag"),
	    .Descriptors = {
		[0] = {
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(FontUbo)
		},
		[1] = {
		    .Type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
		    .Count = 32,
		    .Size = 0
		},
		[2] = {
		    .Type = VK_DESCRIPTOR_TYPE_SAMPLER,
		    .Count = 32,
		    .Size = 0
		},

	    },
	    .DescriptorsCount = 3
	}
    };

    VsaShaderAttribute attributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(Vertex2D, Position)
	},
	[1] = {
	    .Format = VK_FORMAT_R32G32B32A32_SFLOAT,
	    .Offset = OffsetOf(Vertex2D, Color)
	},
	[2] = {
	    .Format = VK_FORMAT_R32G32_SFLOAT,
	    .Offset = OffsetOf(Vertex2D, Uv)
	},
	[3] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(Vertex2D, TextureInd)
	},
	[4] = {
	    .Format = VK_FORMAT_R32_SINT,
	    .Offset = OffsetOf(Vertex2D, IsFont)
	}
    };

    VsaPipelineDescription pipeDescr = {
	.Stride = sizeof(Vertex2D),
	.Stages = stages,
	.StagesCount = ARRAY_COUNT(stages),
	.Attributes = attributes,
	.AttributesCount = ARRAY_COUNT(attributes)
    };
    renderer->Pipeline = vsa_pipeline_create(pipeDescr);

    renderer->CurrentFontInfo = settings->Font;

    renderer->IsDataSubmited = 0;
    memset(renderer->Textures, 0, MaxTextureCount * sizeof(VsaTexture));
    renderer->TexturesCount = 0;
    renderer->TexturesBoundCount = 0;
    renderer->MaxCount = Max(settings->MaxCount, 1);
    renderer->Current.IndicesToDraw = 0;
    renderer->Current.Vertices = NULL;

    u32* indices = NULL;
    array_reserve(indices, 6*renderer->MaxCount);
    i32 i, temp, count = renderer->MaxCount;
    for (i = 0, temp = 0; i < count; i += 6, temp += 4)
    {
	indices[i]     = 0 + temp;
	indices[i + 1] = 1 + temp;
	indices[i + 2] = 2 + temp;
	indices[i + 3] = 2 + temp;
	indices[i + 4] = 3 + temp;
	indices[i + 5] = 0 + temp;
    }

    VsaBuffer vertex =
	vsa_buffer_new(VsaBufferType_Dynamic, NULL,
		       renderer->MaxCount * sizeof(Vertex2D),
		       VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
    VsaBuffer index =
	vsa_buffer_new(VsaBufferType_Static, indices,
		       renderer->MaxCount * sizeof(u32),
		       VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
    array_free(indices);

    vsa_pipeline_set_buffers(&renderer->Pipeline, vertex, index);

    vsr_add_pipeline(&renderer->Pipeline);

}

static void
vsr_2d_default_guard(Renderer2d* renderer)
{
    vassert_not_null(renderer);
    if (array_count(renderer->Current.Vertices) > (renderer->MaxCount - 4))
    {
	vguard(0 && "Out of memory!");
    }
}

i32
_vsr_2d_texture_submit(Renderer2d* renderer, VsaTexture vsaTexture)
{
    i32 textureInd = -1;

    if (vsaTexture.Name != NULL)
    {
	if (renderer->TexturesCount > MaxTextureCount)
	{
	    vguard(0);
	}

	vguard(vsa_texture_is_valid(vsaTexture));
	for (i32 i = 0; i < renderer->TexturesCount; ++i)
	{
	    VsaTexture texture = renderer->Textures[i];
	    if (string_compare(texture.Name, vsaTexture.Name))
	    {
		textureInd = i;
		break;
	    }
	}

	if (textureInd == -1)
	{
	    renderer->Textures[renderer->TexturesCount] = vsaTexture;
	    textureInd = renderer->TexturesCount;
	    ++renderer->TexturesCount;
	    ++renderer->TexturesBoundCount;
	}
	vguard(textureInd != -1 && "Wrong texture index!");
    }

    return textureInd;
}

void
vsr_2d_submit(Renderer2d* renderer, v3 position, v2 size, v4 color, VsaTexture vsaTexture)
{
    vsr_2d_default_guard(renderer);

    i32 textureInd = _vsr_2d_texture_submit(renderer, vsaTexture);

    Vertex2D v0 = {
	.Position = position,
	.Color = color,
	.Uv = v2_new(0, 0),
	.TextureInd = textureInd
    };
    Vertex2D v1 = {
	.Position = v3_sub_y(position, size.Height),
	.Color = color,
	.Uv = v2_new(0, 1),
	.TextureInd = textureInd
    };
    Vertex2D v2 = {
	.Position = v3_new(position.X + size.Width, position.Y - size.Height, position.Z),
	.Color = color,
	.Uv = v2_new(1, 1),
	.TextureInd = textureInd
    };
    Vertex2D v3 = {
	.Position = v3_new(position.X + size.Width, position.Y, position.Z),
	.Color = color,
	.Uv = v2_new(1, 0),
	.TextureInd = textureInd
    };

    array_push(renderer->Current.Vertices, v0);
    array_push(renderer->Current.Vertices, v1);
    array_push(renderer->Current.Vertices, v2);
    array_push(renderer->Current.Vertices, v3);

    renderer->IsDataSubmited = 1;
    renderer->Current.IndicesToDraw += 6;
}

void
vsr_2d_submit_rect(Renderer2d* renderer, v3 position, v2 size, v4 color)
{
    VsaTexture empty = {};
    vsr_2d_submit(renderer, position, size, color, empty);
}

void
vsr_2d_submit_line(Renderer2d* renderer, v3 start, v3 end, f32 thickness, v4 color)
{
    vsr_2d_default_guard(renderer);

    vguard(start.X <= end.X && "start.X ");

    f32 dx = f32_abs(start.X - end.X);
    f32 dyn = start.Y - end.Y;
    f32 dy = f32_abs(dyn);

    //if (dx > dy)
    {
	f32 dt = (dy > 0) ? (-thickness) : (+thickness);
	Vertex2D v0 = {
	    .Position = start,
	    .Color = color,
	    .Uv = {0,0},
	    .TextureInd = -1,
	    .IsFont = 0
	};
	Vertex2D v1 = {
	    .Position = v3_new(start.X, start.Y + dt, start.Z),
	    .Color = color,
	    .Uv = {0,0},
	    .TextureInd = -1,
	    .IsFont = 0
	};
	Vertex2D v2 = {
	    .Position = v3_new(end.X, end.Y + dt, end.Z),
	    .Color = color,
	    .Uv = {0,0},
	    .TextureInd = -1,
	    .IsFont = 0
	};
	Vertex2D v3 = {
	    .Position = v3_new(end.X, end.Y, end.Z),
	    .Color = color,
	    .Uv = {0,0},
	    .TextureInd = -1,
	    .IsFont = 0
	};

	array_push(renderer->Current.Vertices, v0);
	array_push(renderer->Current.Vertices, v1);
	array_push(renderer->Current.Vertices, v2);
	array_push(renderer->Current.Vertices, v3);

    }

    renderer->IsDataSubmited = 1;
    renderer->Current.IndicesToDraw += 6;
}

void
vsr_2d_submit_empty_rect(Renderer2d* renderer, v3 position, v2 size, f32 thickness, v4 color)
{
    // top line
    vsr_2d_submit_rect(renderer, position, v2_new(size.X, thickness), color);

    // bot line
    vsr_2d_submit_rect(renderer,
		       v3_new(position.X, position.Y - size.Y + thickness, position.Z),
		       v2_new(size.X, thickness),
		       color);

    // left
    vsr_2d_submit_rect(renderer,
		       v3_new(position.X - thickness, position.Y, position.Z),
		       v2_new(thickness, size.Y),
		       color);

    // right
    vsr_2d_submit_rect(renderer,
		       v3_new(position.X + size.X - thickness, position.Y, position.Z),
		       v2_new(thickness, size.Y),
		       color);

}

void
vsr_2d_submit_text_ext(Renderer2d* renderer, void* buffer, size_t length, i32 scale, v3 position, v2 drawable, v4 glyphColor)
{
    vsr_2d_default_guard(renderer);

    i32 textureInd = _vsr_2d_texture_submit(renderer, renderer->CurrentFontInfo.Atlas);

    wchar* text = (wchar*) buffer;

    vguard(' ' == L' ' && "Fatal error");

    f32 offsetX = 0.0f;
    f32 firstCharHeight;
    for (i32 i = 0; i < length; ++i)
    {
	i32 charCode = (i32) text[i];

	v3 positions[4];
	f32 startX, startY, endX, endY, xhOffset = 0.0f;
	CharRecord* charTable = renderer->CurrentFontInfo.Font.CharTable;

	f32 x0 = 0.0f, x1 = 0.0f, y0 = 0.0f, y1 = 0.0f;

	if (charCode == ' ')
	{
	    xhOffset = renderer->CurrentFontInfo.Font.MaxHeight / 4 * scale;
	    offsetX += xhOffset;
	    continue;
	}

	if (hash_geti(charTable, charCode) != -1)
	{
	    CharChar charChar = hash_get(charTable, charCode);

	    if (i == 0)
	    {
		firstCharHeight = charChar.FullHeight;
	    }

	    f32 yCharOffset = scale * charChar.YOffset;
	    f32 vOffset = scale * charChar.FullHeight - yCharOffset;
	    xhOffset = scale * charChar.FullWidth;

	    x0 = position.X + offsetX;
	    x1 = x0 + xhOffset;
#if 0
	    y0 = position.Y
		- scale * renderer->CurrentFontInfo.Font.MaxHeight
		- yCharOffset
		+ renderer->CurrentFontInfo.Font.MaxYOffset;
	    y1 = y0 + scale * charChar.FullHeight;
#else

	    y0 = position.Y
		- scale * (firstCharHeight + yCharOffset);
	    y1 = y0 + scale * charChar.FullHeight;

#endif

	    startX = charChar.UV.X;
	    startY = charChar.UV.Y + charChar.Size.Height;
	    endX   = charChar.UV.X + charChar.Size.Width;
	    endY   = charChar.UV.Y;
	}
	else
	{
	    DO_ONES(GERROR("Unknown font character %lc %d!\n", charCode, charCode));
	}

	positions[0] = /* 0 1 */ v3_new(x0, y1, position.Z);
	positions[1] = /* 0 0 */ v3_new(x0, y0, position.Z);
	positions[2] = /* 1 0 */ v3_new(x1, y0, position.Z);
	positions[3] = /* 1 1 */ v3_new(x1, y1, position.Z);

	offsetX += xhOffset;
	//GWARNING("OFFSETX: %f, %f\n", offsetX, drawable.X);
	if (offsetX >= drawable.X)
	{
	    DO_ONES(GINFO("Hide!\n"););
	    return;
	}

	Vertex2D v0 = {
	    .Position = positions[0],
	    .Color = glyphColor,
	    .Uv = v2_new(startX, endY),
	    .TextureInd = textureInd,
	    .IsFont = 1
	};
	Vertex2D v1 = {
	    .Position = positions[1],
	    .Color = glyphColor,
	    .Uv = v2_new(startX, startY),
	    .TextureInd = textureInd,
	    .IsFont = 1
	};
	Vertex2D v2 = {
	    .Position = positions[2],
	    .Color = glyphColor,
	    .Uv = v2_new(endX, startY),
	    .TextureInd = textureInd,
	    .IsFont = 1
	};
	Vertex2D v3 = {
	    .Position = positions[3],
	    .Color = glyphColor,
	    .Uv = v2_new(endX, endY),
	    .TextureInd = textureInd,
	    .IsFont = 1
	};

	array_push(renderer->Current.Vertices, v0);
	array_push(renderer->Current.Vertices, v1);
	array_push(renderer->Current.Vertices, v2);
	array_push(renderer->Current.Vertices, v3);

	renderer->IsDataSubmited = 1;
	renderer->Current.IndicesToDraw += 6;
    }

}

void
vsr_2d_submit_circle_ext(Renderer2d* renderer, v3 position, v2 size, v4 color, f32 thikness, f32 fade, i32 multiplier)
{
    vsr_2d_default_guard(renderer);

    if (fade == 0.0f)
    {
	fade = 0.049f;
    }
    if (multiplier == 0)
    {
	multiplier = 2.0f;
    }

    Vertex2D v0 = {
	.Position = position,
	.Color = color,
	.Uv = v2_new(thikness, fade),
	.TextureInd = -2,
	.IsFont = multiplier
    };
    Vertex2D v1 = {
	.Position = v3_sub_y(position, size.Height),
	.Color = color,
	.Uv = v2_new(thikness, fade),
	.TextureInd = -2,
	.IsFont = multiplier
    };
    Vertex2D v2 = {
	.Position = v3_new(position.X + size.Width, position.Y - size.Height, position.Z),
	.Color = color,
	.Uv = v2_new(thikness, fade),
	.TextureInd = -2,
	.IsFont = multiplier
    };
    Vertex2D v3 = {
	.Position = v3_new(position.X + size.Width, position.Y, position.Z),
	.Color = color,
	.Uv = v2_new(thikness, fade),
	.TextureInd = -2,
	.IsFont = multiplier
    };

    array_push(renderer->Current.Vertices, v0);
    array_push(renderer->Current.Vertices, v1);
    array_push(renderer->Current.Vertices, v2);
    array_push(renderer->Current.Vertices, v3);

    renderer->IsDataSubmited = 1;
    renderer->Current.IndicesToDraw += 6;
}

void
vsr_2d_flush(Renderer2d* pRenderer, i32 imageIndex)
{
#if 0
    if (array_count(pRenderer->Current.Vertices) <= 0)
	return;

    DO_ONES(GINFO("Textures Bound: %d\n", pRenderer->TexturesBoundCount));
    DO_ONES(
	GINFO("[Current] cnt=%d size=%d\n ",
	      array_count(pRenderer->Current.Vertices),
	      array_count(pRenderer->Current.Vertices) * sizeof(Vertex2D));
	GINFO("[Prev] cnt=%d size=%d\n",
	      array_count(pRenderer->Prev.Vertices),
	      array_count(pRenderer->Prev.Vertices) * sizeof(Vertex2D))
	);
#endif

    if (pRenderer->IsDataSubmited)
    {
	//GINFO("IsSubmitted image_index=%d!\n", imageIndex);
	vsa_buffer_set_data(
	    pRenderer->Pipeline.Vertex,
	    pRenderer->Current.Vertices,
	    array_count(pRenderer->Current.Vertices) * sizeof(Vertex2D));
    }

    vsa_pipeline_bind_textures(&pRenderer->Pipeline, pRenderer->Textures, pRenderer->TexturesBoundCount, 32);


    { // NOTE(typedef): Uniform updates

	CameraUbo ubo = {
	    .ViewProjection = pRenderer->pCamera->ViewProjection
	};
	vsa_uniform_set_data(pRenderer->Pipeline.Uniforms[0], &ubo, sizeof(CameraUbo));

	// TODO(typedef): this should be configured w dependency on font size
	FontUbo fontUbo = {
	    .Width = 0.6f,
	    .EdgeTransition = 0.325f
	};
	vsa_uniform_set_data(pRenderer->Pipeline.Uniforms[1], &fontUbo, sizeof(FontUbo));

	vsa_pipeline_update_uniforms(pRenderer->Pipeline);
    }

    if (pRenderer->IsDataSubmited)
    {
	vsa_pipeline_update(&pRenderer->Pipeline, pRenderer->Current.IndicesToDraw);

	// NOTE() pRenderer->Prev is not cleared
	renderer2d_draw_data_copy(&pRenderer->Prev, &pRenderer->Current);
	Renderer2dDrawData emptyData = {};
	pRenderer->Current = emptyData;
    }
    else
    {
	vsa_pipeline_update(&pRenderer->Pipeline, pRenderer->Prev.IndicesToDraw);
    }

    pRenderer->IsDataSubmited = 0;
    array_clear(pRenderer->Current.Vertices);
}

void
vsr_2d_destroy(Renderer2d* pRenderer)
{
    array_free(pRenderer->Current.Vertices);
    array_free(pRenderer->Prev.Vertices);
    vsa_pipeline_destroy(pRenderer->Pipeline);
    vsa_font_info_destroy(pRenderer->CurrentFontInfo);
}

/*
  DOCS(typedef): 3d renderer
*/

typedef struct Camera3dUbo
{
    m4 ViewProjection;
    m4 ModelTransform;
} Camera3dUbo;

void
vsr_3d_static_create(Renderer3dStatic* pRenderer, const Renderer3dStaticSettings* settings)
{
    vguard(settings->MaxCount > 0);
    vguard_not_null(settings->pCamera);

    pRenderer->pCamera = settings->pCamera;

    // ===============================
    //       DOCS() Demo PART
    // ===============================
    VsaShaderStage stages[] = {
	[0] = {
	    .Type = VsaShaderType_Vertex,
	    .ShaderSourcePath = asset_shader("Vulkan/ForwardStatic.vert"),
	    .Descriptors = {
		[0] = {
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(Camera3dUbo)
		}
	    },
	    .DescriptorsCount = 1
	},

	[1] = {
	    .Type = VsaShaderType_Fragment,
	    .ShaderSourcePath = asset_shader("Vulkan/ForwardStatic.frag"),
	    .Descriptors = {
		[0] = {
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(LightDataUbo)
		},
		[1] = {
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(MeshMaterialUbo)
		},
		[2] = {
		    .Type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
		    .Count = 1,
		    .Size = sizeof(ViewPositionUbo)
		},
		[3] = {
		    .Type = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
		    .Count = 1,
		    .Size = 0
		},
		[4] = {
		    .Type = VK_DESCRIPTOR_TYPE_SAMPLER,
		    .Count = 1,
		    .Size = 0
		},

	    },

	    .DescriptorsCount = 5
	}
    };

    VsaShaderAttribute attributes[] = {
	[0] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(Vertex3D, Position)
	},
	[1] = {
	    .Format = VK_FORMAT_R32G32B32_SFLOAT,
	    .Offset = OffsetOf(Vertex3D, Normal)
	},
	[2] = {
	    .Format = VK_FORMAT_R32G32_SFLOAT,
	    .Offset = OffsetOf(Vertex3D, Uv)
	}
    };

    VsaPipelineDescription pipeDescr = {
	.Stride = sizeof(Vertex3D),
	.Stages = stages,
	.StagesCount = ARRAY_COUNT(stages),
	.Attributes = attributes,
	.AttributesCount = ARRAY_COUNT(attributes)
    };
    pRenderer->Pipeline = vsa_pipeline_create(pipeDescr);
    pRenderer->MaxCount = Max(settings->MaxCount, 1);

    u32* indices = NULL;
    array_reserve(indices, 6*pRenderer->MaxCount);
    i32 i, temp, count = pRenderer->MaxCount;
    for (i = 0, temp = 0; i < count; i += 6, temp += 4)
    {
	indices[i]     = 0 + temp;
	indices[i + 1] = 1 + temp;
	indices[i + 2] = 2 + temp;
	indices[i + 3] = 2 + temp;
	indices[i + 4] = 3 + temp;
	indices[i + 5] = 0 + temp;
    }

    VsaBuffer vertex =
	vsa_buffer_new(VsaBufferType_Dynamic, NULL,
		       pRenderer->MaxCount * sizeof(Vertex3D),
		       VK_BUFFER_USAGE_VERTEX_BUFFER_BIT);
    VsaBuffer index =
	vsa_buffer_new(VsaBufferType_Static, indices,
		       pRenderer->MaxCount * sizeof(u32),
		       VK_BUFFER_USAGE_INDEX_BUFFER_BIT);
    array_free(indices);

    vsa_pipeline_set_buffers(&pRenderer->Pipeline, vertex, index);

    vsr_add_pipeline(&pRenderer->Pipeline);
}

void
vsr_3d_static_destroy(Renderer3dStatic* pRenderer)
{
    //array_free(pRenderer->Current.Vertices);
    vsa_pipeline_destroy(pRenderer->Pipeline);
}

static void
vsr_3d_static_draw_mesh(Renderer3dStatic* pRenderer, i32 imageIndex, StaticMesh mesh, LightDataUbo lightData, v3 viewPosition)
{
    i64 meshVerticesCount = array_count(mesh.Vertices);
    if (meshVerticesCount > pRenderer->MaxCount)
    {
	GERROR("Can't render StaticModel max count exceeded!\n");
	return;
    }

    i64 vertCnt = array_count(mesh.Vertices);
    vsa_buffer_set_data(
	pRenderer->Pipeline.Vertex,
	mesh.Vertices,
	vertCnt * sizeof(MeshVertex));

    // const char* Name;
    // i32 IsMetallicExist;
    // PbrMetallic Metallic;
    //VsaTextureId BaseColorTexture;
    //VsaTextureId MetallicRoughnessTexture;

    VsaTexture* pVsaTexture =
	asset_manager_get(AssetType_Texture,
			  mesh.Material.Metallic.BaseColorTexture);
    vsa_pipeline_bind_textures(&pRenderer->Pipeline, pVsaTexture, 1, 1);

    { // NOTE(typedef): Uniform updates
	Camera3dUbo camera3dUbo = {
	    .ViewProjection = pRenderer->pCamera->ViewProjection,
		.ModelTransform = m4_identity()
	};
	vsa_uniform_set_data(pRenderer->Pipeline.Uniforms[0], &camera3dUbo, sizeof(Camera3dUbo));

	vsa_uniform_set_data(pRenderer->Pipeline.Uniforms[1], &lightData, sizeof(LightDataUbo));

	// v4 BaseColor;
	// f32 MetallicFactor;
	// f32 RoughnessFactor;

	MeshMaterialUbo meshMaterialUbo = {
	    .BaseColor = mesh.Material.Metallic.BaseColor,
	    .Shininess = 0.5f
	};
	vsa_uniform_set_data(pRenderer->Pipeline.Uniforms[2], &meshMaterialUbo, sizeof(MeshMaterialUbo));

	ViewPositionUbo viewPositionUbo = {
	    .Position = viewPosition
	};
	vsa_uniform_set_data(pRenderer->Pipeline.Uniforms[3], &viewPositionUbo, sizeof(ViewPositionUbo));

	vsa_pipeline_update_uniforms(pRenderer->Pipeline);
    }

    //GINFO("Mesh indices count: %d\n", mesh.IndicesCount);
    vsa_pipeline_update(&pRenderer->Pipeline, mesh.IndicesCount);
}

void
vsr_3d_static_draw_model(Renderer3dStatic* pRenderer, i32 imageIndex, StaticModel model, LightDataUbo lightData, v3 viewPosition)
{
    for (i32 i = 0; i < array_count(model.Meshes); ++i)
    {
	vsr_3d_static_draw_mesh(pRenderer, imageIndex, model.Meshes[i], lightData, viewPosition);
    }
}

/*
  DOCS(typedef): Tests
*/

void
vsr_test_submit_circle_and_line(Renderer2d* renderer, VsaTexture someTexture)
{
    // NOTE(typedef): Just check circle rendering
    f32 thikness, fade;
    vsr_2d_submit_circle_ext(renderer,
			     v3_new(100.0f, 500.0f, 1.1f),
			     v2_new(200.0f, 200.0f),
			     v4_new(0, 0.23f, 0.71f, 1.0f),
			     thikness = 1.35f,
			     fade = 0.049f,
			     32);

    vsr_2d_submit_circle_ext(
	renderer,
	v3_new(400.0f, 500.0f, 1.1f),
	v2_new(200.0f, 200.0f),
	v4_new(0, 0.23f, 0.71f, 1.0f),
	thikness = 1.35f,
	fade = 0.049f,
	4);

    vsr_2d_submit_circle_ext(
	renderer,
	v3_new(10, 400, 4),
	v2_new(20, 20),
	v4_new(0.4, 0.4, 0.4, 1.0),
	1.1f, 0, 2);

    //NOTE(): Line
    vsr_2d_submit_line(
	renderer,
	v3_new(150, 240, 4),
	v3_new(450, 260, 4),
	2.0f,
	v4_new(1,1,1,1));

    vsr_2d_submit_line(
	renderer,
	v3_new(250, 560, 4),
	v3_new(350, 160, 4),
	2.0f,
	v4_new(1,1,1,1));

    vsr_2d_submit_line(
	renderer,
	v3_new(250, 160, 4),
	v3_new(350, 66, 4),
	2.0f,
	v4_new(1,1,1,1));

    vsr_2d_submit_line(
	renderer,
	v3_new(150, 66, 4),
	v3_new(350, 160, 4),
	20.0f,
	v4_new(1,1,1,1));

    vsr_2d_submit(renderer, v3_new(600.0f, 1000.0f, 100.0f), v2_new(200,300), v4_new(1,1,1,1), someTexture);
    vsr_2d_submit_rect(renderer, v3_new(500, 950, 50), v2_new(100,100), v4_new(0.9541, 0.1, 0.541, 0.8));
    vsr_2d_submit_rect(renderer, v3_new(550, 1000, 49), v2_new(100,100), v4_new(0.2141, 0.1,0.7541, 0.78));
    vsr_2d_submit_rect(renderer, v3_new(250.0f, 250.0f, 0.5f), v2_new(100,100), v4_new(1.41,.6541,0.541,1));
    vsr_2d_submit_rect(renderer, v3_new(700.0f, 1100.0f, 1.0f), v2_new(100,40), v4_new(1.41,.6541,0.541,1));
    vsr_2d_submit_empty_rect(renderer, v3_new(100.0, 100.5, 0.5f), v2_new(50, 50), 2.0f, v4_new(0.43, 0.843, 0.234, 0.6));

    static WideString justSomeMessage = {
	.Buffer = L"Просто сообщение",
	.Length = 16
    };
    vsr_2d_submit_text_ext(
	renderer, justSomeMessage.Buffer, justSomeMessage.Length,
	4,
	v3_new(1220, 400, 1),
	v2_new(1000, 1000),
	v4_new(1, 1, 1, 1));
}
