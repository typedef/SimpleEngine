#include "VulkanSimpleApi.h"

#include <Utils/stb_image.h>

#include <Utils/Types.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <Application/Application.h> //delete this BUG()
#include <EntitySystem/Components/Base/CameraComponent.h>

#include "VulkanSimpleApiHelper.h"

#include <shaderc/shaderc.h>
#include <shaderc/status.h>

/*
  NOTE(typedef): for now vsa (Vulkan Simple Api) have global state object
  maybe it is subject to change in the future, for now i don't have all
  necessary information and knowladge about Vulkan API for taking such serious
  decisions.

  Maybe this vars should be global:
  VkInstance, VkDebugUtilsMessengerExt, VkSurfaceKHR,
  VkPhysicalDevice, VkExtensionProperties, VkDevice

  [CONV]
  struct:
  typedef struct Vsa* {} Vsa*;

  func:
  vsa_{object}_{action}(...);

*/


/*
  DOCS(typedef): Vsa Core Api Objects
*/
i32 VsaCoreInitialized = 0;
static VkAllocationCallbacks* AllocatorCallbacks = NULL;
VsaSettings VsaSetting;
VkInstance VulkanInstance;
// NOTE(typedef): Optional, only for debugging
VkDebugUtilsMessengerEXT VulkanDebugMessanger;
VkSurfaceKHR VulkanSurface;
VkPhysicalDevice VulkanPhysicalDevice;
VkExtensionProperties* PhysicalDeviceExts;
VkDevice VulkanDevice;
VkQueue VulkanGraphicsQueue;
VkQueue VulkanPresentationQueue;
// NOTE(typedef): Configuring SwapChain part (Infrastructure for presentation)
VkSwapchainKHR VulkanSwapChain = VK_NULL_HANDLE;
VkSwapChainSupportDetails VulkanSupportDetails;
VsaQueueFamily VulkanQueueFamily;
VkImage* VulkanSwapChainImages;
VkSurfaceFormatKHR VulkanSurfaceFormat;
VkExtent2D VulkanExtent;
// DOCS(typedef): Depth Buffer Related
VkImage VulkanDepthImage;
VkDeviceMemory VulkanDepthImageMemory;
VkImageView VulkanDepthImageView;
//NOTE(typedef): VkImageView - is render target at the end, used in framebuffer
VkImageView* VulkanImageViews = NULL;
VkRenderPass VulkanRenderPass;
// NOTE(typedef): Framebuffers part
VkFramebuffer* VulkanFramebuffers = NULL;
/*DOCS(typedef): Drawing commands*/
VkCommandPool VulkanCommandPool;
VkCommandBuffer VulkanCommandBuffer;

/*
  DOCS(typedef): Sync primitives for render loop
*/
VkSemaphore VulkanImageAvailableSemaphores;
VkSemaphore VulkanRenderFinishedSemaphores;
VkFence VulkanFrameFences;

// DOCS(typedef): Multisampling Related
VkSampleCountFlagBits VulkanSamplesCount = VK_SAMPLE_COUNT_1_BIT;
VkImage VulkanMultisampleImage;
VkDeviceMemory VulkanMultisampleImageMemory;
VkImageView VulkanMultisampleImageView;

typedef struct VsaSamplersTable
{
    i32 Key;
    VkSampler Value;
} VsaSamplersTable;
VsaSamplersTable* VsaSamplers = NULL;

static void
vsa_samplers_init()
{
    VsaSamplers = _table_new(VsaSamplers, sizeof(VsaSamplersTable), -1);
    for (i32 i = 0; i < table_capacity(VsaSamplers); ++i)
    {
	VsaSamplers[i].Value = VK_NULL_HANDLE;
    }
}

static void
vsa_samplers_deinit()
{
    for (i32 i = 0; i < table_capacity(VsaSamplers); ++i)
    {
	VsaSamplersTable item = VsaSamplers[i];
	if (item.Value != VK_NULL_HANDLE)
	{
	    vkDestroySampler(VulkanDevice, item.Value, AllocatorCallbacks);
	}
    }

    table_free(VsaSamplers);
}

/*
  DOCS(typedef): [END] Vsa Core Api Objects
*/


/*

  DOCS(typedef): Demo related variables

*/
i32 IsWindowBeenResized = 0;

VkShaderStageFlagBits
vsa_shader_type_to_stage_flag(VsaShaderType type)
{
    switch (type)
    {

    case VsaShaderType_Vertex: return VK_SHADER_STAGE_VERTEX_BIT;
    case VsaShaderType_Fragment: return VK_SHADER_STAGE_FRAGMENT_BIT;
    case VsaShaderType_Geometry: return VK_SHADER_STAGE_GEOMETRY_BIT;
    case VsaShaderType_Compute: return VK_SHADER_STAGE_COMPUTE_BIT;
    case VsaShaderType_TesselationControl:
	return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
    case VsaShaderType_TesselationEvaluation:
	return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    }

    GERROR("Wrong VsaShaderType passed to vsa_shader_type_to_stage\n");
    vguard(0);
    return -1;
}

const char*
vsa_shader_type_to_string(VsaShaderType type)
{
    switch (type)
    {

    case VsaShaderType_Vertex: return "Vertex";
    case VsaShaderType_Fragment: return "Fragment";
    case VsaShaderType_Compute: return "Compute";
    case VsaShaderType_Geometry: return "Geometry";
    case VsaShaderType_TesselationControl: return "TesselationControl";
    case VsaShaderType_TesselationEvaluation: return "TesselationEvaluation";

    }

    vguard(0);
    return NULL;
}

void
vsa_shader_bindings_destroy(VsaShaderBindingsCore vsaShaderBindingsCore)
{
    vkDestroyDescriptorSetLayout(
	VulkanDevice,
	vsaShaderBindingsCore.DescriptorSetLayout,
	AllocatorCallbacks);
    vkDestroyDescriptorPool(
	VulkanDevice,
	vsaShaderBindingsCore.DescriptorPool,
	AllocatorCallbacks);
}

void
vsa_shader_output_free(VsaShaderOutput vsaOut)
{
    if (vsaOut.Vertex.Bytecode)
	memory_free(vsaOut.Vertex.Bytecode);

    if (vsaOut.Fragment.Bytecode)
	memory_free(vsaOut.Fragment.Bytecode);

    if (vsaOut.Geometry.Bytecode)
	memory_free(vsaOut.Geometry.Bytecode);

    if (vsaOut.Compute.Bytecode)
	memory_free(vsaOut.Compute.Bytecode);

    if (vsaOut.TesselationControl.Bytecode)
	memory_free(vsaOut.TesselationControl.Bytecode);

    if (vsaOut.TesselationEvaluation.Bytecode)
	memory_free(vsaOut.TesselationEvaluation.Bytecode);
}

VkInstance
vsa_instance_create()
{
    /*
      DOCS(typedef): Create VkInstance
    */
    u32 version;
    VkResult getVersionResult = vkEnumerateInstanceVersion(&version);
    if (getVersionResult != VK_SUCCESS)
    {
	GERROR("Error geting version!\n");
    }

    vkPrintVersionLine(version);

    VkApplicationInfo applicationInfo = {
	.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
	.pNext = NULL,
	.pApplicationName = "Simple Engine",
	.applicationVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
	.pEngineName = "Simple Engine",
	.engineVersion = VK_MAKE_API_VERSION(0, 1, 0, 0),
	.apiVersion = VK_API_VERSION_1_3
    };

    // NOTE(typedef): Get device exts
    // get rid off validation layers here
    const char* const* requiredExts = vkGetExtensions();
    vkCheckExtSupport(requiredExts);

    // NOTE(typedef): create validation layers
    VkLayerProperties* validationLayers = NULL;
    char** validationLayerNames = NULL;
    array_push(validationLayerNames, "VK_LAYER_KHRONOS_validation");

    VkDebugUtilsMessengerCreateInfoEXT debugUtilsCreateInfo =
	vkCreateDebugUtilsMessengerCreateInfo();

    VkInstanceCreateInfo instanceCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
	.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugUtilsCreateInfo,
	.flags = 0,
	.pApplicationInfo = &applicationInfo,
	.enabledLayerCount = array_count(validationLayerNames),
	.ppEnabledLayerNames = (const char* const*)validationLayerNames,
	.enabledExtensionCount = array_count(requiredExts),
	.ppEnabledExtensionNames = requiredExts
    };
    VkResult createInstanceResult = vkCreateInstance(&instanceCreateInfo, AllocatorCallbacks, &VulkanInstance);
    if (createInstanceResult != VK_SUCCESS)
    {
	GERROR("Can't create instance!\n%s\n",
	       vkResultToString(createInstanceResult));
    }

    vkLoadExtsFunctions(VulkanInstance);

    VulkanDebugMessanger =
	vkCreateDebugUtilsMessenger(VulkanInstance, debugUtilsCreateInfo, AllocatorCallbacks);

    return VulkanInstance;
}

/*
  DOCS(typedef): this thing is needed for on-screen rendering,
  it's just a connection between window and GPU Renderer.
*/
VkSurfaceKHR
vsa_surface_create()
{
    vkValidHandle(VulkanInstance);

    VkResult windowCreateSurfaceResult =
	window_create_surface(VulkanInstance, AllocatorCallbacks, &VulkanSurface);
    vkValidResult(windowCreateSurfaceResult, "Can't create vk surface!");

    return VulkanSurface;
}

VkPhysicalDevice
vsa_physical_device_create()
{
    vkValidHandle(VulkanInstance);

    /*
      DOCS(typedef):Pick physical device
    */
    u32 physicalDevicesCount;
    VkResult enumeratePhysicalDeviceResult =
	vkEnumeratePhysicalDevices(VulkanInstance, &physicalDevicesCount, NULL);
    vassert(physicalDevicesCount != 0 && "Can't find any physical device!");

    if (enumeratePhysicalDeviceResult == VK_ERROR_INITIALIZATION_FAILED)
    {
	GERROR("Can't enemerate physical devices, maybe installable client driver (ICD) is not installed on your machine!\n");
	vguard(0);
    }

    VkPhysicalDevice* physicalDevices = NULL;
    array_reserve(physicalDevices, physicalDevicesCount);
    array_header(physicalDevices)->Count = physicalDevicesCount;
    enumeratePhysicalDeviceResult =
	vkEnumeratePhysicalDevices(VulkanInstance, &physicalDevicesCount, physicalDevices);
    GINFO("Found %d devices\n", physicalDevicesCount);
    if (enumeratePhysicalDeviceResult != VK_SUCCESS)
    {
	vkPrintError(enumeratePhysicalDeviceResult, "Can't enumerate physical devices!\n");
	vassert_break();
    }

    i32 isPicked = 0;
    for (i32 i = 0; i < physicalDevicesCount; ++i)
    {
	VkPhysicalDeviceProperties physicalDeviceProperties;
	VkPhysicalDevice vkPhysicalDevice = physicalDevices[i];
	vkGetPhysicalDeviceProperties(vkPhysicalDevice, &physicalDeviceProperties);

	vkPrintPhysicalDeviceProperties(physicalDeviceProperties);

	VkPhysicalDeviceFeatures deviceFeatures;
	vkGetPhysicalDeviceFeatures(vkPhysicalDevice, &deviceFeatures);

	if ((physicalDeviceProperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
	    && deviceFeatures.samplerAnisotropy)
	{
	    VulkanPhysicalDevice = vkPhysicalDevice;
	    isPicked = 1;
	    break;
	}
    }

    vassert(isPicked && "Can't pick physical device, you should have discrete gpu!");

    i32 physicalDeviceExtCount;
    VkResult getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(VulkanPhysicalDevice, NULL, &physicalDeviceExtCount, NULL);
    PhysicalDeviceExts = NULL;
    array_reserve(PhysicalDeviceExts, physicalDeviceExtCount);
    array_header(PhysicalDeviceExts)->Count = physicalDeviceExtCount;
    getPhysicalDeviceExtResult = vkEnumerateDeviceExtensionProperties(VulkanPhysicalDevice, NULL, &physicalDeviceExtCount, PhysicalDeviceExts);
    if (getPhysicalDeviceExtResult != VK_SUCCESS)
    {
	vkPrintError(getPhysicalDeviceExtResult, "Can't get physical device exts!");
	vassert_break();
    }

    return VulkanPhysicalDevice;
}

VsaQueueFamily
vsa_queue_family_create(VkSwapChainSupportDetails* supportDetails)
{
    vkValidHandle(VulkanSurface);
    vkValidHandle(VulkanPhysicalDevice);

    /*
      DOCS(typedef): Get Queue's
    */
    u32 familyPropertyCount;
    vkGetPhysicalDeviceQueueFamilyProperties(VulkanPhysicalDevice, &familyPropertyCount, NULL);
    VkQueueFamilyProperties* familyProperties = NULL;
    array_reserve(familyProperties, familyPropertyCount);
    vkGetPhysicalDeviceQueueFamilyProperties(VulkanPhysicalDevice, &familyPropertyCount, familyProperties);
    GINFO("Families count: %d\n", familyPropertyCount);
    i32 graphicsIndex = -1,
	presentationIndex = -1;
    VkSwapChainSupportDetails scDetails;
    for (i32 i = 0; i < familyPropertyCount; ++i)
    {
	char* flagAsStr = NULL;
	VkQueueFamilyProperties prop = familyProperties[i];

	VkBool32 isSurfaceKhrSupported;
	VkResult getSupportForKhrResult =
	    vkGetPhysicalDeviceSurfaceSupportKHR(VulkanPhysicalDevice, i, VulkanSurface, &isSurfaceKhrSupported);
	if (getSupportForKhrResult != VK_SUCCESS)
	{
	    vkPrintError(getSupportForKhrResult, "Can't get physical device surface khr!");
	    vassert_break();
	}

	scDetails =
	    vkGetSwapChainSupportDetails(VulkanPhysicalDevice, VulkanSurface);
	i8 isSwapChainSupported =
	    array_any(scDetails.SurfaceFormats)
	    && array_any(scDetails.PresentModes);
	if (!isSwapChainSupported)
	    continue;

	if (prop.queueFlags & VK_QUEUE_GRAPHICS_BIT && prop.queueFlags & VK_QUEUE_COMPUTE_BIT)
	{
	    graphicsIndex = i;
	}

	if (isSurfaceKhrSupported == VK_TRUE)
	{
	    presentationIndex = i;
	}

	if (graphicsIndex != -1 && presentationIndex != -1)
	{
	    break;
	}
    }

    *supportDetails = scDetails;

    /*
      DOCS(typedef): yes, on most gpu's this queue indices is the same
    */
    vassert(graphicsIndex > -1 && "Queue for graphics and compute not found!");
    vassert(presentationIndex > -1 && "Queue for presentation not found!");

    VsaQueueFamily vsaQueueFamily = {
	.GraphicsIndex = graphicsIndex,
	.PresentationIndex = presentationIndex
    };

    VulkanQueueFamily = vsaQueueFamily;

    return vsaQueueFamily;
}

VkDevice
vsa_device_create(VsaQueueFamily vsaQueueFamily)
{
    vkValidHandle(VulkanInstance);
    vkValidHandle(VulkanPhysicalDevice);

    /*
      DOCS(typedef): Create (Logical) Device
    */
    const char* deviceExts[] = {
	VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
    for (i32 d = 0; d < ARRAY_COUNT(deviceExts); ++d)
    {
	const char* ext = deviceExts[d];
	i32 index = array_index_of(
	    PhysicalDeviceExts,
	    string_compare(item.extensionName, ext));
	if (index == -1)
	{
	    GERROR("Can't get ext for device: %s\n", ext);
	    vassert_break();
	}
    }

    VkPhysicalDeviceFeatures physcialDeviceFeatures = {
	/*
	  NOTE(typedef): we need special features
	*/
	.samplerAnisotropy = VK_TRUE
    };

    /*
      DOCS(typedef): Create info for creating Queue's.
      As Queue's created within VkDevice we need make CreateInfo
      structs for them as well, before creating VkDevice.
    */
    f32 queuePriority = 1.0f;
    VkDeviceQueueCreateInfo* queuesCreateInfos = NULL;
    VkDeviceQueueCreateInfo graphicsQueueCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.queueFamilyIndex = vsaQueueFamily.GraphicsIndex,
	.queueCount = 1,
	.pQueuePriorities = &queuePriority
    };
    array_push(queuesCreateInfos, graphicsQueueCreateInfo);
    /*
      DOCS(typedef): If .GraphicsIndex == .PresentationIndex then
      we need to pass it only once, if in app we have differentiation
    */
    if (vsaQueueFamily.GraphicsIndex != vsaQueueFamily.PresentationIndex)
    {
	VkDeviceQueueCreateInfo presentationQueueCreateInfo = {
	    .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
	    .pNext = NULL,
	    .flags = 0,
	    .queueFamilyIndex = vsaQueueFamily.PresentationIndex,
	    .queueCount = 1,
	    .pQueuePriorities = &queuePriority
	};
	array_push(queuesCreateInfos, presentationQueueCreateInfo);
    }

    VkDeviceCreateInfo deviceCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.queueCreateInfoCount = array_count(queuesCreateInfos),
	.pQueueCreateInfos = queuesCreateInfos,
	.enabledExtensionCount = ARRAY_COUNT(deviceExts),
	.ppEnabledExtensionNames = deviceExts,
	.pEnabledFeatures = &physcialDeviceFeatures
    };

    VkResult deviceCreateResult = vkCreateDevice(VulkanPhysicalDevice, &deviceCreateInfo, AllocatorCallbacks, &VulkanDevice);
    if (deviceCreateResult != VK_SUCCESS)
    {
	vkPrintError(deviceCreateResult, "Can't create logical device");
	vassert_break();
    }

    return VulkanDevice;
}

VkSampleCountFlagBits
vsa_samples_get_max_count()
{
    vkValidHandle(VulkanPhysicalDevice);

    vguard(VK_SAMPLE_COUNT_1_BIT   == 1 && "W1");
    vguard(VK_SAMPLE_COUNT_2_BIT   == 2 && "W2");
    vguard(VK_SAMPLE_COUNT_4_BIT   == 4 && "W4");
    vguard(VK_SAMPLE_COUNT_8_BIT   == 8 && "W8");
    vguard(VK_SAMPLE_COUNT_16_BIT  == 16 && "W16");
    vguard(VK_SAMPLE_COUNT_32_BIT  == 32 && "W32");
    vguard(VK_SAMPLE_COUNT_64_BIT  == 64 && "W64");

    VkPhysicalDeviceProperties pdp;
    vkGetPhysicalDeviceProperties(VulkanPhysicalDevice, &pdp);

    VkSampleCountFlags samples =
	pdp.limits.framebufferColorSampleCounts;
    & pdp.limits.framebufferDepthSampleCounts;
    if (samples & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
    if (samples & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
    if (samples & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
    if (samples & VK_SAMPLE_COUNT_8_BIT ) { return VK_SAMPLE_COUNT_8_BIT ; }
    if (samples & VK_SAMPLE_COUNT_4_BIT ) { return VK_SAMPLE_COUNT_4_BIT ; }
    if (samples & VK_SAMPLE_COUNT_2_BIT ) { return VK_SAMPLE_COUNT_2_BIT ; }

    return VK_SAMPLE_COUNT_1_BIT;
}

VkQueue
vsa_graphics_queue_create(i32 queueFamilyIndex)
{
    vkValidHandle(VulkanDevice);
    vkGetDeviceQueue(VulkanDevice, queueFamilyIndex, 0, &VulkanGraphicsQueue);
    vkValidHandle(VulkanGraphicsQueue);
    return VulkanGraphicsQueue;
}

VkQueue
vsa_presentation_queue_create(i32 queueFamilyIndex)
{
    vkValidHandle(VulkanDevice);
    vkGetDeviceQueue(VulkanDevice, queueFamilyIndex, 0, &VulkanPresentationQueue);
    vkValidHandle(VulkanPresentationQueue);
    return VulkanPresentationQueue;
}

VkSwapchainKHR
vsa_swapchain_create(VsaSwapChainSettings set, VkSwapChainSupportDetails supportDetails, VsaQueueFamily queueFamily)
{
    VkSurfaceFormatKHR pickedSurfaceFormat = supportDetails.SurfaceFormats[0];
    VkPresentModeKHR pickedPresentationMode = supportDetails.PresentModes[0];
    for (i32 f = 0; f < array_count(supportDetails.SurfaceFormats); ++f)
    {
	VkSurfaceFormatKHR surfaceFormat = supportDetails.SurfaceFormats[f];
	if (surfaceFormat.format == set.SurfaceFormat.format && surfaceFormat.colorSpace == set.SurfaceFormat.colorSpace)
	{
	    pickedSurfaceFormat = surfaceFormat;
	    //GINFO("Format is picked!\n");
	    break;
	}
    }

    for (i32 m = 0; m < array_count(supportDetails.PresentModes); ++m)
    {
	VkPresentModeKHR presentMode = supportDetails.PresentModes[m];
	if (presentMode == set.PresentationMode)
	{
	    pickedPresentationMode = presentMode;
	    //GINFO("Presentation is picked!\n");
	    break;
	}
    }

    VkSurfaceCapabilitiesKHR capabilities = supportDetails.SurfaceCapabilities;
    i32 imagesInSwapChain = capabilities.maxImageCount;
    if (capabilities.maxImageCount == 0)
    {
	imagesInSwapChain = capabilities.minImageCount + capabilities.minImageCount * 0.7f;
    }

    VulkanSurfaceFormat = pickedSurfaceFormat;
    i32 w, h;
    window_get_framebuffer_size_shared(&w, &h);
    VulkanExtent = (VkExtent2D) {
	.width = w,
	.height = h
    };
    VkSwapchainCreateInfoKHR swapChainCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
	.pNext = NULL,
	.flags = 0,
	.surface = VulkanSurface,
	.minImageCount = imagesInSwapChain,
	.imageFormat = pickedSurfaceFormat.format,
	.imageColorSpace = pickedSurfaceFormat.colorSpace,
	.imageExtent = VulkanExtent,
	.imageArrayLayers = 1,
	.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
	.preTransform = capabilities.currentTransform,
	.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
	.presentMode = pickedPresentationMode,
	.clipped = VK_TRUE,
	.oldSwapchain = VK_NULL_HANDLE //VulkanSwapChain
    };

    u32 queueIndices[2];
    queueIndices[0] = queueFamily.GraphicsIndex;
    queueIndices[1] = queueFamily.PresentationIndex;
    if (queueFamily.GraphicsIndex != queueFamily.PresentationIndex)
    {
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
	swapChainCreateInfo.queueFamilyIndexCount = 2;
	swapChainCreateInfo.pQueueFamilyIndices = queueIndices;
    }
    else
    {
	swapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }

    VkResult swapChainCreateResult =
	vkCreateSwapchainKHR(VulkanDevice, &swapChainCreateInfo, AllocatorCallbacks, &VulkanSwapChain);
    vkValidResult(swapChainCreateResult, "Failed Swap Chain creation!");
    vkValidHandle(VulkanSwapChain);

    VulkanSwapChainImages = vkGetSwapChainImages(VulkanDevice, VulkanSwapChain);
    //GINFO("Get %d SwapChain images\n", array_count(VulkanSwapChainImages));

    return VulkanSwapChain;
}

VkImageView
vsa_image_view_create(VkImage vkImage, VkFormat vkFormat, VkImageAspectFlags vkImageAspectFlags, i32 mipLevels)
{
    VkImageView vkImageView;

    VkImageViewCreateInfo imageViewCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.image = vkImage,
	.viewType = VK_IMAGE_VIEW_TYPE_2D,
	.format = vkFormat,
	.components = {
	    .r = VK_COMPONENT_SWIZZLE_IDENTITY,
	    .g = VK_COMPONENT_SWIZZLE_IDENTITY,
	    .b = VK_COMPONENT_SWIZZLE_IDENTITY,
	    .a = VK_COMPONENT_SWIZZLE_IDENTITY
	},
	.subresourceRange = {
	    .aspectMask = vkImageAspectFlags,//VK_IMAGE_ASPECT_COLOR_BIT,
	    .baseMipLevel = 0,
	    .levelCount = mipLevels,
	    .baseArrayLayer = 0,
	    .layerCount = 1
	}
    };

    VkResult createImageViewResult =
	vkCreateImageView(
	    VulkanDevice, &imageViewCreateInfo,
	    AllocatorCallbacks, &vkImageView);
    vkValidResult(createImageViewResult, "Can't create vk image view!");

    return vkImageView;
}

static VkShaderModule
vsa_shader_module_create_ext(VkDevice device, u32* byteCode, size_t byteCodeSize)
{
    VkShaderModuleCreateInfo shaderModuleCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.codeSize = byteCodeSize,
	.pCode = byteCode
    };

    VkShaderModule shaderModule;
    VkResult createShaderModuleResult =
	vkCreateShaderModule(VulkanDevice, &shaderModuleCreateInfo,  AllocatorCallbacks, &shaderModule);
    vkValidResult(createShaderModuleResult, "Can't create shader!");

    return shaderModule;
}

static void
vsa_shader_compiler_error_callback(void* userData, const char* error)
{
    GERROR("[ShaderC] %s\n", error);
}

static const char*
vsa_shader_type_to_spv_file_ending(VsaShaderType vsaShaderType)
{
    switch (vsaShaderType)
    {
    case VsaShaderType_Vertex  : return "Vert.spv";
    case VsaShaderType_Fragment: return "Frag.spv";
    case VsaShaderType_Geometry: return "Geom.spv";
    case VsaShaderType_Compute: return "Comp.spv";
    case VsaShaderType_TesselationControl: return "Tesc.spv";
    case VsaShaderType_TesselationEvaluation: return "Tese.spv";
    }

    GERROR("Shader type: %d\n", vsaShaderType);
    vguard(0 && "Wrong VsaShaderType!!!");
    return NULL;
}

static VsaShaderType
vsa_shader_type_from_spv_short_ending(const char* shortEnding)
{
    if (string_compare_length(shortEnding, "Vert", 4))
    {
	return VsaShaderType_Vertex;
    }
    else if (string_compare_length(shortEnding, "Frag", 4))
    {
	return VsaShaderType_Fragment;
    }
    else if (string_compare_length(shortEnding, "Geom", 4))
    {
	return VsaShaderType_Geometry;
    }
    else if (string_compare_length(shortEnding, "Comp", 4))
    {
	return VsaShaderType_Compute;
    }
    else if (string_compare_length(shortEnding, "Tesc", 4))
    {
	return VsaShaderType_TesselationControl;
    }
    else if (string_compare_length(shortEnding, "Tese", 4))
    {
	return VsaShaderType_TesselationEvaluation;
    }

    GERROR("Short ending: %s\n", shortEnding);
    vguard(0 && "Short ending!!!");
    return -1;
}

static const char*
vsa_shader_type_to_spv_short_file_ending(VsaShaderType vsaShaderType)
{
    switch (vsaShaderType)
    {
    case VsaShaderType_Vertex  : return "Vert.spv";
    case VsaShaderType_Fragment: return "Frag.spv";
    case VsaShaderType_Geometry: return "Geom.spv";
    }

    GERROR("Shader type: %d\n", vsaShaderType);
    vguard(0 && "Wrong VsaShaderType!!!");
    return NULL;
}

static i32
_vsa_shader_is_compilation_needed(const char* path, const char* spvPath)
{
    size_t lastModificationTimeRaw =
	path_get_last_modification_time_raw(path);

    if (path_is_file_exist(spvPath) == 0)
    {
	return 1;
    }

    size_t lastCreationTimeRaw =
	path_get_last_modification_time_raw(spvPath);

    if (lastModificationTimeRaw > lastCreationTimeRaw)
    {
	return 1;
    }

    return 0;
}

const char*
vsa_shader_compilation_result_to_string(VsaShaderCompilationResult result)
{

    switch (result)
    {

    case VsaShaderCompilationResult_FileNotExist:
	return "File no exist";
    case VsaShaderCompilationResult_AlreadyCompiled:
	return "Already compiled";
    case VsaShaderCompilationResult_CompilationError:
	return "Compilation error";
    case VsaShaderCompilationResult_InvalidStage:
	return "Invalid stage";
    case VsaShaderCompilationResult_InternalError:
	return "Internal error";
    case VsaShaderCompilationResult_NullResultObject:
	return "Null result object";
    case VsaShaderCompilationResult_ValidationError:
	return "Validation error";
    case VsaShaderCompilationResult_TransformationError:
	return "Transformation error";
    case VsaShaderCompilationResult_ConfigurationError:
	return "Configuration error";
    case VsaShaderCompilationResult_Successed:
	return "Successed";

    }

    GERROR("Wrong Shader Result %d\n", result);
    vguard(0 && "Wrong Shader Result!");
    return "Null";
}

VsaShaderCompilationResult
vsa_shaderc_compilation_status_to_vsa(shaderc_compilation_status scStatus)
{
    switch (scStatus)
    {
    case shaderc_compilation_status_success:
	return VsaShaderCompilationResult_Successed;
    case shaderc_compilation_status_invalid_stage:
	return VsaShaderCompilationResult_InvalidStage;
    case shaderc_compilation_status_compilation_error:
	return VsaShaderCompilationResult_CompilationError;
    case shaderc_compilation_status_internal_error:
	return VsaShaderCompilationResult_InternalError;
    case shaderc_compilation_status_null_result_object:
	return VsaShaderCompilationResult_NullResultObject;
    case shaderc_compilation_status_invalid_assembly:
	return VsaShaderCompilationResult_InvalidStage;
    case shaderc_compilation_status_validation_error:
	return VsaShaderCompilationResult_ValidationError;
    case shaderc_compilation_status_transformation_error:
	return VsaShaderCompilationResult_TransformationError;
    case shaderc_compilation_status_configuration_error:
	return VsaShaderCompilationResult_ConfigurationError;
    }

    GERROR("Wrong Shader Status %d\n", scStatus);
    vguard(0 && "Wrong Shader Status!");
    return -1;
}

// NOTE(): Internal struct
typedef struct VsaShaderCompileUnitSettings
{
    i32 IsInitialized;
    VsaShaderType Type;
    const char* Path;
    shaderc_compiler_t ScCompiler;
    shaderc_compile_options_t ScOptions;
} VsaShaderCompileUnitSettings;

shaderc_shader_kind
vsa_shader_type_to_shaderc(VsaShaderType vsaShaderType)
{
    switch (vsaShaderType)
    {
    case VsaShaderType_Vertex: return shaderc_glsl_vertex_shader;
    case VsaShaderType_Fragment: return shaderc_glsl_fragment_shader;
    case VsaShaderType_Compute: return shaderc_glsl_compute_shader;
    case VsaShaderType_Geometry: return shaderc_glsl_geometry_shader;
    case VsaShaderType_TesselationControl:
	return shaderc_glsl_tess_control_shader;
    case VsaShaderType_TesselationEvaluation:
	return shaderc_glsl_tess_evaluation_shader;
    }

    GERROR("Wrong Shader Type %d\n", vsaShaderType);
    vguard(0 && "Wrong Shader Type!");
    return -1;
}

char*
vsa_shader_get_spv_path_from_source(const char* path, VsaShaderType type)
{
    char* unitName = path_get_name_wo_extension(path);
    const char* spvEnding =
	vsa_shader_type_to_spv_short_file_ending(type);
    char* unitNameWithExt = string_concat(unitName, spvEnding);
    char* dir = path_get_directory(path);
    char* finalSpvPath = path_combine(dir, unitNameWithExt);

    memory_free(unitName);
    memory_free(unitNameWithExt);
    memory_free(dir);

    return finalSpvPath;
}

char*
vsa_shader_get_source_path_from_spv(const char* path)
{
    char* unitName = path_get_name_wo_extension(path);

    i32 unitNameLength = string_length(unitName);
    unitName;
    i32 endingInd = string_last_index_of_upper(unitName, unitNameLength);
    char* shaderEnding = string_substring_range(unitName, endingInd, (unitNameLength - 1));
    VsaShaderType vsaShaderType =
	vsa_shader_type_from_spv_short_ending(shaderEnding);

    const char* ext = NULL;
    switch (vsaShaderType)
    {

    case VsaShaderType_Vertex:
	ext = ".vert";
	break;

    case VsaShaderType_Fragment:
	ext = ".frag";
	break;

    case VsaShaderType_Geometry:
	ext = ".geom";
	break;

    case VsaShaderType_Compute:
	ext = ".comp";
	break;

    case VsaShaderType_TesselationControl:
	ext = ".tesc";
	break;

    case VsaShaderType_TesselationEvaluation:
	ext = ".tesc";
	break;

    default:
	vguard(0 && "can't translate shader type, not exist!");
	break;
    }

    char* nameWithExt = string_concat(unitName, ext);
    char* dir = path_get_directory(path);
    char* result = path_combine(dir, nameWithExt);

    memory_free(dir);
    memory_free(nameWithExt);
    memory_free(shaderEnding);

    return result;
}

VsaShaderCompilationResult
_vsa_shader_compile_single_unit(VsaShaderCompileUnitSettings* compileUnitSettings, VsaShaderCompiledBytecode* output)
{
    // DOCS(typedef): Check source file for existance
    const char* path = compileUnitSettings->Path;
    if (path_is_file_exist(path) == 0)
    {
	return VsaShaderCompilationResult_FileNotExist;
    }

    // DOCS(typedef): Compile initialization part
    shaderc_compiler_t scCompiler;
    shaderc_compile_options_t scOptions;
    if (compileUnitSettings->IsInitialized)
    {
	scCompiler = compileUnitSettings->ScCompiler;
	scOptions  = compileUnitSettings->ScOptions;
    }
    else
    {
	scCompiler = shaderc_compiler_initialize();
	scOptions  = shaderc_compile_options_initialize();
	shaderc_compile_options_set_optimization_level(scOptions, shaderc_optimization_level_performance);
	shaderc_compile_options_set_target_env(scOptions, shaderc_target_env_vulkan, shaderc_env_version_vulkan_1_3);
	compileUnitSettings->ScCompiler = scCompiler;
	compileUnitSettings->ScOptions = scOptions;
	compileUnitSettings->IsInitialized = 1;
    }

    char* finalSpvPath = vsa_shader_get_spv_path_from_source(path, compileUnitSettings->Type);
    i32 isRecompilationNeeded =
	_vsa_shader_is_compilation_needed(path, finalSpvPath);
    if (!isRecompilationNeeded)
    {
	output->Bytecode = (u32*) file_read_bytes_ext(finalSpvPath, &output->Size);
	memory_free(finalSpvPath);
	return VsaShaderCompilationResult_AlreadyCompiled;
    }

    // DOCS(typedef): Compile shader code
    size_t length;
    char* shaderUnitSource = file_read_string_ext(path, &length);
    char* unitName = path_get_name_wo_extension(path);
    shaderc_shader_kind shaderKind =
	vsa_shader_type_to_shaderc(compileUnitSettings->Type);
    char* temp = string_concat(unitName, ".glsl");
    shaderc_compilation_result_t result = shaderc_compile_into_spv(
	scCompiler,
	shaderUnitSource, length,
	shaderKind,
	temp,
	"main",
	scOptions);
    memory_free(temp);

    VsaShaderCompilationResult funcResult;
    shaderc_compilation_status status =
	shaderc_result_get_compilation_status(result);
    if (status == shaderc_compilation_status_success)
    {
	GSUCCESS("Shader compiled successufully\n");
	funcResult = VsaShaderCompilationResult_Successed;
    }
    else
    {
	GERROR("Error\n");
	funcResult = vsa_shaderc_compilation_status_to_vsa(status);
	GWARNING("Error Message: %s\n",
		 shaderc_result_get_error_message(result));
	goto endOfFunctionLabel;
    }


    u32* bytecode = (u32*) shaderc_result_get_bytes(result);
    i64 bytecodeLength = shaderc_result_get_length(result);
    if (bytecode == NULL || bytecodeLength <= 0)
    {
	GERROR("[Shader Compilation error] %s %p\n", path, bytecode);
	vguard(bytecodeLength > 0 && "Compiled shader output should be > 0!!!");
    }
    output->Size = bytecodeLength;
    output->Bytecode = memory_allocate(output->Size);
    memcpy(output->Bytecode, bytecode, output->Size);

    // TODO(typedef): make reflection stuff spirv_cross/spirv_cross_c.h

    // NOTE(typedef): we can write to file in another thread mb
    file_write_bytes(finalSpvPath, (u8*)output->Bytecode, output->Size);

endOfFunctionLabel:
    shaderc_result_release(result);
    memory_free(shaderUnitSource);
    memory_free(unitName);
    memory_free(finalSpvPath);

    return funcResult;
}

force_inline void
_vsa_shader_compile_single_unit_safe(
    VsaShaderCompileUnitSettings* set,
    VsaShaderType type,
    const char* path,
    VsaShaderCompiledBytecode* bytecode)
{
    if (path == NULL)
    {
	return;
    }

    set->Type = type;
    set->Path = path;

    VsaShaderCompilationResult unitResult =
	_vsa_shader_compile_single_unit(set,
					bytecode);
    if (unitResult != VsaShaderCompilationResult_Successed)
    {
	const char* message =
	    vsa_shader_compilation_result_to_string(unitResult);
	if (unitResult
	    ==
	    VsaShaderCompilationResult_AlreadyCompiled)
	{
	    GWARNING("[%s Shader Warning]: %s %s\n", vsa_shader_type_to_string(type), message, path);
	}
	else
	{
	    GERROR("[%s Shader Error]: %s %s\n", vsa_shader_type_to_string(type), message, path);
	}

	if (unitResult == VsaShaderCompilationResult_FileNotExist)
	{
	    GERROR("File not exist: %s\n", path);
	    vguard(unitResult != VsaShaderCompilationResult_FileNotExist);
	}
    }
}

void
vsa_shader_compile(VsaShaderPaths vsaShaderPaths, VsaShaderOutput* output)
{
    if (vsaShaderPaths.VertexPath == NULL
	&& vsaShaderPaths.FragmentPath == NULL
	&& vsaShaderPaths.GeometryPath == NULL
	&& vsaShaderPaths.ComputePath == NULL
	&& vsaShaderPaths.TesselationControlPath == NULL
	&& vsaShaderPaths.TesselationEvaluationPath == NULL)
    {
	GERROR("Compile Settings all paths for shader units is NULL!\n");
	vguard(0 && "Wrong parameters!");
    }

    VsaShaderCompileUnitSettings compileUnitSet = {
	.IsInitialized = 0
    };

    _vsa_shader_compile_single_unit_safe(&compileUnitSet, VsaShaderType_Vertex, vsaShaderPaths.VertexPath, &output->Vertex);

    _vsa_shader_compile_single_unit_safe(&compileUnitSet, VsaShaderType_Fragment, vsaShaderPaths.FragmentPath, &output->Fragment);

    _vsa_shader_compile_single_unit_safe(&compileUnitSet, VsaShaderType_Geometry, vsaShaderPaths.GeometryPath, &output->Geometry);

    _vsa_shader_compile_single_unit_safe(&compileUnitSet, VsaShaderType_Compute, vsaShaderPaths.ComputePath, &output->Compute);

    _vsa_shader_compile_single_unit_safe(&compileUnitSet, VsaShaderType_TesselationControl, vsaShaderPaths.TesselationControlPath, &output->TesselationControl);

    _vsa_shader_compile_single_unit_safe(&compileUnitSet, VsaShaderType_TesselationEvaluation,vsaShaderPaths.TesselationEvaluationPath, &output->TesselationEvaluation);

    if (compileUnitSet.IsInitialized)
    {
	shaderc_compile_options_release(compileUnitSet.ScOptions);
	shaderc_compiler_release(compileUnitSet.ScCompiler);
    }
}

VkShaderModule
vsa_shader_module_create(VkDevice vkDevice, const char* path)
{
    size_t byteCodeSize;
    u32* byteCode = (u32*) file_read_bytes_ext(path, &byteCodeSize);
    vassert_not_null(byteCode);
    GINFO("Creating shader for: %s\n", path);

    VkShaderModule shaderModule =
	vsa_shader_module_create_ext(vkDevice, byteCode, byteCodeSize);

    return shaderModule;
}

i32
vsa_find_memory_type_index(u32 typeBit, VkMemoryPropertyFlags vkMemoryPropertyFlags)
{
    VkPhysicalDeviceMemoryProperties dMemoryProperties;
    vkGetPhysicalDeviceMemoryProperties(VulkanPhysicalDevice, &dMemoryProperties);

    i32 index = -1;
    for (i32 i = 0; i < dMemoryProperties.memoryTypeCount; ++i)
    {
	VkMemoryType memoryType =
	    dMemoryProperties.memoryTypes[i];
	if ((typeBit & (1 << i)) && (memoryType.propertyFlags & vkMemoryPropertyFlags))
	{
	    return i;
	}
    }

    vguard(0 && "Can't find memory type index!");
    return -1;
}

VkBuffer
vsa_buffer_create(VkBufferUsageFlags usage, VkDeviceSize size, VkMemoryPropertyFlags memoryPropertyFlags, VkDeviceMemory* deviceMemory)
{
    VkBuffer vkBuffer;

    VkBufferCreateInfo vkCreateBufferCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.size = size,
	.usage = usage,
	.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	.queueFamilyIndexCount = 0,
	.pQueueFamilyIndices = NULL
    };

    VkResult createBufferResult = vkCreateBuffer(VulkanDevice, &vkCreateBufferCreateInfo, AllocatorCallbacks, &vkBuffer);
    vkValidResult(createBufferResult, "Failed Vertex Buffer creation!");

    //NOTE(typedef): Allocating memory for VkBuffer
    VkMemoryRequirements vertexBufferMemoryRequirements;
    vkGetBufferMemoryRequirements(VulkanDevice, vkBuffer, &vertexBufferMemoryRequirements);

    // NOTE(typedef): Find suitable memory type
    i32 index = vsa_find_memory_type_index(
	vertexBufferMemoryRequirements.memoryTypeBits, memoryPropertyFlags);

    VkMemoryAllocateInfo memoryAllocateInfo = {
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.pNext = NULL,
	.allocationSize = vertexBufferMemoryRequirements.size,
	.memoryTypeIndex = index
    };

    VkResult allocateMemoryResult = vkAllocateMemory(VulkanDevice, &memoryAllocateInfo, AllocatorCallbacks, deviceMemory);
    vkValidResult(allocateMemoryResult, "Failed triangle deivce memory allocation!");

    VkResult bindBufferMemoryResult = vkBindBufferMemory(VulkanDevice, vkBuffer, *deviceMemory, 0);
    vkValidResult(bindBufferMemoryResult, "Failed binding buffer memory with buffer!");

    return vkBuffer;
}

void
vsa_staging_memory_set_data(VkDeviceMemory deviceMemory, void* dataToCopy, size_t size)
{
    vguard_not_null(deviceMemory);
    vguard_not_null(dataToCopy);
    vguard(size > 0);

    void* data;
    VkResult vkMapResult = vkMapMemory(VulkanDevice, deviceMemory, 0, size, 0, &data);
    vkValidResult(vkMapResult, "Failed mapping memory!");
    memcpy(data, dataToCopy, size);
    vkUnmapMemory(VulkanDevice, deviceMemory);
}

VkCommandBuffer
vsa_command_buffer_begin(VkCommandPool vkCommandPool)
{
    vkValidHandle(vkCommandPool);

    VkCommandBufferAllocateInfo vkCmdBufferAllocatedInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
	.pNext = NULL,
	.commandPool = vkCommandPool,
	.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	.commandBufferCount = 1
    };

    VkCommandBuffer vkCmdBuffer;
    VkResult vkAllocateCmdBuffersResult =
	vkAllocateCommandBuffers(VulkanDevice, &vkCmdBufferAllocatedInfo, &vkCmdBuffer);
    vkValidResult(vkAllocateCmdBuffersResult, "Failed cmd buffer allocation!\n");

    VkCommandBufferBeginInfo beginInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	.pNext = NULL,
	.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
	.pInheritanceInfo = NULL
    };
    vkBeginCommandBuffer(vkCmdBuffer, &beginInfo);

    return vkCmdBuffer;
}

void
vsa_command_buffer_end(VkCommandPool vkCommandPool, VkCommandBuffer vkCommandBuffer)
{
    vkValidHandle(vkCommandPool);
    vkValidHandle(vkCommandBuffer);
    vkValidHandle(VulkanGraphicsQueue);

    VkResult vkEndCmdBufferResult = vkEndCommandBuffer(vkCommandBuffer);
    vkValidResult(vkEndCmdBufferResult, "Failed end cmd buffer!");

    // Execute command
    VkSubmitInfo submitInfo = {
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.pNext = NULL,
	.waitSemaphoreCount = 0,
	.pWaitSemaphores = NULL,
	.pWaitDstStageMask = NULL,
	.commandBufferCount = 1,
	.pCommandBuffers = &vkCommandBuffer,
	.signalSemaphoreCount = 0,
	.pSignalSemaphores = NULL
    };

    vkQueueSubmit(VulkanGraphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(VulkanGraphicsQueue);

    // Free command for buffer
    vkFreeCommandBuffers(VulkanDevice, vkCommandPool, 1, &vkCommandBuffer);
}

void
vsa_buffer_copy(VkBuffer deviceBuffer, VkBuffer cpuStagingBuffer, VkDeviceSize size)
{
    VkCommandBuffer cmdBuffer =
	vsa_command_buffer_begin(VulkanCommandPool);

    VkBufferCopy copyRegion = {
	.srcOffset = 0,
	.dstOffset = 0,
	.size = size,
    };

    vkCmdCopyBuffer(cmdBuffer, cpuStagingBuffer, deviceBuffer, 1, &copyRegion);

    vsa_command_buffer_end(VulkanCommandPool, cmdBuffer);
}

VkBuffer
vsa_full_buffer_create_ext(VkBufferUsageFlags usage, size_t size, VkDeviceMemory* deviceMemory, VkBuffer* stagingBuffer, VkDeviceMemory* stagingMemory)
{
    *stagingBuffer =
	vsa_buffer_create(VK_BUFFER_USAGE_TRANSFER_SRC_BIT, size, (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT), stagingMemory);

    VkBuffer gpuBuffer =
	vsa_buffer_create(
	    (VK_BUFFER_USAGE_TRANSFER_DST_BIT | usage), size, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, deviceMemory);

    return gpuBuffer;
}

VkBuffer
vsa_static_buffer_create(VkBufferUsageFlags usage, void* data, size_t size, VkDeviceMemory* deviceMemory)
{
    VkBuffer stagingBuffer;
    VkDeviceMemory stagingMemory;

    VkBuffer gpuBuffer =
	vsa_full_buffer_create_ext(
	    usage,
	    size,
	    deviceMemory,
	    &stagingBuffer,
	    &stagingMemory);

    vsa_staging_memory_set_data(stagingMemory, data, size);
    vsa_buffer_copy(gpuBuffer, stagingBuffer, size);

    // NOTE(typedef): clean up
    vkDestroyBuffer(VulkanDevice, stagingBuffer, AllocatorCallbacks);
    vkFreeMemory(VulkanDevice, stagingMemory, AllocatorCallbacks);

    return gpuBuffer;
}

VsaBuffer
vsa_buffer_new(VsaBufferType type, void* data, size_t size, VkBufferUsageFlagBits usage)
{

    VsaBuffer vsaBuffer = {
	.Type = type
    };

    if (type == VsaBufferType_Static)
    {

	vsaBuffer.Gpu =
	    vsa_full_buffer_create_ext(
		usage,//VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		size,
		&vsaBuffer.GpuMemory,
		&vsaBuffer.Staging,
		&vsaBuffer.StagingMemory);
	vsa_buffer_set_data(vsaBuffer, data, size);
	vkDestroyBuffer(VulkanDevice, vsaBuffer.Staging, AllocatorCallbacks);
	vkFreeMemory(VulkanDevice, vsaBuffer.StagingMemory, AllocatorCallbacks);
    }
    else
    {
	vguard_null(data);
	vguard(size > 0);
	vsaBuffer.Gpu =
	    vsa_full_buffer_create_ext(
		usage,//VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
		size,
		&vsaBuffer.GpuMemory,
		&vsaBuffer.Staging,
		&vsaBuffer.StagingMemory);

    }

    return vsaBuffer;
}

void
vsa_buffer_set_data(VsaBuffer vsaBuffer, void* data, size_t size)
{
    vsa_staging_memory_set_data(vsaBuffer.StagingMemory, data, size);
    vsa_buffer_copy(vsaBuffer.Gpu, vsaBuffer.Staging, size);
}

void
vsa_buffer_destroy(VsaBuffer vsaBuffer)
{
    if (vsaBuffer.Type == VsaBufferType_Static)
    {
	vkDestroyBuffer(VulkanDevice, vsaBuffer.Gpu, AllocatorCallbacks);
	vkFreeMemory(VulkanDevice, vsaBuffer.GpuMemory, AllocatorCallbacks);
    }
    else
    {
	vkDestroyBuffer(VulkanDevice, vsaBuffer.Staging, AllocatorCallbacks);
	vkFreeMemory(VulkanDevice, vsaBuffer.StagingMemory, AllocatorCallbacks);
	vkDestroyBuffer(VulkanDevice, vsaBuffer.Gpu, AllocatorCallbacks);
	vkFreeMemory(VulkanDevice, vsaBuffer.GpuMemory, AllocatorCallbacks);
    }

}

void
vsa_image_create(VsaImageCreateExtSettings settings, VkImage* vkImage, VkDeviceMemory* vkDeviceMemory)
{
    vkValidHandle(VulkanDevice);

    VkImageCreateInfo vkImageCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.imageType = VK_IMAGE_TYPE_2D,
	.format = settings.Format,
	.extent = {
	    .width = settings.Width,
	    .height = settings.Height,
	    .depth = settings.Depth
	},
	.mipLevels = settings.MipLevels,
	.arrayLayers = 1,
	.samples = settings.SamplesCount,
	.tiling = settings.ImageTiling,
	.usage = settings.ImageUsageFlags,
	.sharingMode = VK_SHARING_MODE_EXCLUSIVE,
	.queueFamilyIndexCount = 0, // NOTE(): should we set this value
	.pQueueFamilyIndices = NULL,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };

    VkResult vkCreateImageResult = vkCreateImage(
	VulkanDevice, &vkImageCreateInfo, AllocatorCallbacks, vkImage);
    vkValidResult(vkCreateImageResult, "Failed image/texture creation!");

    VkImage tempVkImage = *vkImage;
    VkMemoryRequirements memRequirements;
    vkGetImageMemoryRequirements(VulkanDevice, tempVkImage, &memRequirements);

    i32 index = vsa_find_memory_type_index(
	memRequirements.memoryTypeBits, settings.MemoryPropertyFlags);
    VkMemoryAllocateInfo allocateInfo = {
	.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
	.pNext = NULL,
	.allocationSize = memRequirements.size,
	.memoryTypeIndex = index
    };

    VkResult vkAllocateMemoryResult =
	vkAllocateMemory(VulkanDevice, &allocateInfo, AllocatorCallbacks, vkDeviceMemory);
    vkValidResult(vkAllocateMemoryResult, "Failed texture memory allocation!");

    VkResult vkBindImageMemoryResult =
	vkBindImageMemory(VulkanDevice, tempVkImage, *vkDeviceMemory, 0);
    vkValidResult(vkBindImageMemoryResult, "Failed bind texture memory!");
}

void
vsa_image_change_layout(VkImage vkImage, VkImageLayout vkOldLayout, VkImageLayout vkNewLayout, VkFormat vkFormat, i32 mipLevels)
{
    VkCommandBuffer vkCommandBuffer =
	vsa_command_buffer_begin(VulkanCommandPool);

    VkImageMemoryBarrier vkImageMemoryBarrier = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
	.pNext = NULL,
	.srcAccessMask = 0, // TODO(typedef): empty for now
	.dstAccessMask = 0, // TODO(typedef): empty for now
	.oldLayout = vkOldLayout,
	.newLayout = vkNewLayout,
	.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.image = vkImage,
	.subresourceRange = {
	    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	    .baseMipLevel = 0,
	    .levelCount = mipLevels,
	    .baseArrayLayer = 0,
	    .layerCount = 1,
	}
    };

    VkPipelineStageFlags vkSrcPipelineStageFlags;
    VkPipelineStageFlags vkDstPipelineStageFlags;
    if (vkOldLayout == VK_IMAGE_LAYOUT_UNDEFINED
	&& vkNewLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
	vkImageMemoryBarrier.srcAccessMask = 0;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	vkSrcPipelineStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
	vkDstPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
    }
    else if (vkOldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
	     && vkNewLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
	vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
	vkSrcPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
	vkDstPipelineStageFlags = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
    }
    else
    {
	vguard(0 && "Wrong new||old layout picked !!!");
    }

    vkCmdPipelineBarrier(vkCommandBuffer,
			 vkSrcPipelineStageFlags,
			 vkDstPipelineStageFlags,
			 VK_DEPENDENCY_BY_REGION_BIT,
			 0,NULL, 0,NULL,
			 1,
			 &vkImageMemoryBarrier
	);

    vsa_command_buffer_end(VulkanCommandPool, vkCommandBuffer);
}

void
vsa_image_create_mipmaps(VkImage vkImage, i32 width, i32 height, i32 mipLevels)
{
    VkFormatProperties formatProperties;
    vkGetPhysicalDeviceFormatProperties(VulkanPhysicalDevice, VK_FORMAT_R8G8B8A8_SRGB, &formatProperties);
    vguard(
	formatProperties.optimalTilingFeatures
	& VK_FORMAT_FEATURE_SAMPLED_IMAGE_FILTER_LINEAR_BIT
	&& "Linear filtering not supported!");

    VkCommandBuffer vkCommandBuffer =
	vsa_command_buffer_begin(VulkanCommandPool);

    VkImageMemoryBarrier vkImageMemoryBarrier = {
	.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
	.pNext = NULL,
	.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
	.image = vkImage,
	.subresourceRange = {
	    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	    .levelCount = 1,
	    .baseArrayLayer = 0,
	    .layerCount = 1,
	}
    };

    i32 mipWidth = width;
    i32 mipHeight = height;
    for (i32 i = 1; i < mipLevels; ++i)
    {
	i32 dstWidth, dstHeight;
	if (mipWidth > 1)
	{
	    dstWidth = mipWidth / 2;
	}
	else
	{
	    dstWidth = 1;
	}

	if (mipHeight > 1)
	{
	    dstHeight = mipHeight / 2;
	}
	else
	{
	    dstHeight = 1;
	}

	vkImageMemoryBarrier.subresourceRange.baseMipLevel = i - 1;
	vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
	vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;

	vkCmdPipelineBarrier(
	    vkCommandBuffer,
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
	    0,
	    0, NULL,
	    0, NULL,
	    1, &vkImageMemoryBarrier);

	VkImageBlit vkImageBlit = {
	    .srcSubresource = {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.mipLevel = i - 1,
		.baseArrayLayer = 0,
		.layerCount = 1
	    },
	    .srcOffsets = {
		[0] = { 0, 0, 0 },
		[1] = { mipWidth, mipHeight, 1 },
	    },
	    .dstSubresource = {
		.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
		.mipLevel = i,
		.baseArrayLayer = 0,
		.layerCount = 1
	    },
	    .dstOffsets = {
		[0] = { 0, 0, 0 },
		[1] = { dstWidth, dstHeight, 1 }
	    }
	};

	vkCmdBlitImage(
	    vkCommandBuffer,
	    vkImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
	    vkImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
	    1, &vkImageBlit,
	    VK_FILTER_LINEAR);

	vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
	vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
	vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
	vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;

	vkCmdPipelineBarrier(
	    vkCommandBuffer,
	    VK_PIPELINE_STAGE_TRANSFER_BIT,
	    VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
	    0,
	    0, NULL,
	    0, NULL,
	    1, &vkImageMemoryBarrier);

	if (dstWidth > 1)
	    mipWidth = dstWidth;
	if (dstHeight > 1)
	    mipHeight = dstHeight;
    }

    vkImageMemoryBarrier.subresourceRange.baseMipLevel = mipLevels - 1;
    vkImageMemoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    vkImageMemoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    vkImageMemoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    vkImageMemoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    vkCmdPipelineBarrier(
	vkCommandBuffer,
	VK_PIPELINE_STAGE_TRANSFER_BIT,
	VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
	0,
	0, NULL,
	0, NULL,
	1, &vkImageMemoryBarrier);

    vsa_command_buffer_end(VulkanCommandPool, vkCommandBuffer);
}

void
vsa_buffer_to_image(VkBuffer vkBuffer, VkImage vkImage, i32 width, i32 height)
{
    VkCommandBuffer vkCommandBuffer =
	vsa_command_buffer_begin(VulkanCommandPool);

    VkBufferImageCopy vkBufferImageCopy = {
	.bufferOffset = 0,
	.bufferRowLength = 0,
	.bufferImageHeight = 0,
	.imageSubresource = {
	    .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
	    .mipLevel = 0,
	    .baseArrayLayer = 0,
	    .layerCount = 1
	},
	.imageOffset = {
	    .x = 0,
	    .y = 0,
	    .z = 0
	},
	.imageExtent = {
	    .width = width,
	    .height = height,
	    .depth = 1
	}
    };

    vkCmdCopyBufferToImage(
	vkCommandBuffer, vkBuffer, vkImage,
	VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &vkBufferImageCopy);

    vsa_command_buffer_end(VulkanCommandPool, vkCommandBuffer);
}

// DOCS(): Creates image views for all swap chain image views, not sure it would become different for any other app
VkImageView*
vsa_image_views_create()
{
    i32 imagesCount = array_count(VulkanSwapChainImages);
    array_reserve(VulkanImageViews, imagesCount);
    array_header(VulkanImageViews)->Count = imagesCount;

    for (i32 i = 0; i < imagesCount; ++i)
    {
	VulkanImageViews[i] = vsa_image_view_create(
	    VulkanSwapChainImages[i],
	    VulkanSurfaceFormat.format,
	    VK_IMAGE_ASPECT_COLOR_BIT,
	    1);
    }

    return VulkanImageViews;
}

void
vsa_depth_create()
{
    VkFormat depthAttachmentFormat = VK_FORMAT_D32_SFLOAT;

    VsaImageCreateExtSettings settings = {
	.Width = VulkanExtent.width,
	.Height = VulkanExtent.height,
	.Depth = 1.0f,
	.MipLevels = 1,
	.SamplesCount = VulkanSamplesCount,
	.Format = depthAttachmentFormat,
	.ImageTiling = VK_IMAGE_TILING_OPTIMAL,
	.ImageUsageFlags = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
	.MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
    };

    vsa_image_create(settings, &VulkanDepthImage, &VulkanDepthImageMemory);
    VulkanDepthImageView = vsa_image_view_create(VulkanDepthImage, depthAttachmentFormat, VK_IMAGE_ASPECT_DEPTH_BIT, 1);
}

void
vsa_multisample_create()
{
    // TODO(typedef): Configurable multisampling
    //GINFO("%x\n", vsa_samples_get_max_count());

    VsaImageCreateExtSettings settings = {
	.Width = VulkanExtent.width,
	.Height = VulkanExtent.height,
	.Depth = 1.0f,
	.MipLevels = 1,
	.SamplesCount = VulkanSamplesCount,
	.Format = VulkanSurfaceFormat.format,
	.ImageTiling = VK_IMAGE_TILING_OPTIMAL,
	.ImageUsageFlags = VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT | VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
	.MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
    };
    vsa_image_create(settings, &VulkanMultisampleImage, &VulkanMultisampleImageMemory);

    VulkanMultisampleImageView =
	vsa_image_view_create(VulkanMultisampleImage,
			      VulkanSurfaceFormat.format,
			      VK_IMAGE_ASPECT_COLOR_BIT,
			      1);
}

void
vsa_texture_image_create_ext(VsaTextureImageSettings settings, VkImage* vkImage, VkDeviceMemory* vkDeviceMemory, VkFormat vkFormat)
{
    size_t imageSize = settings.Width * settings.Height * settings.Channels;
    VkDeviceMemory stagingBufferMemory;
    VkBuffer stagingBuffer =
	vsa_buffer_create(
	    VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
	    imageSize,
	    (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
	    &stagingBufferMemory);

    void* bufferData;
    vkMapMemory(VulkanDevice, stagingBufferMemory, 0, imageSize, 0, &bufferData);
    memcpy(bufferData, settings.Data, imageSize);
    vkUnmapMemory(VulkanDevice, stagingBufferMemory);

    VsaImageCreateExtSettings vsaImageSettings = {
	.Width = settings.Width,
	.Height = settings.Height,
	.Depth = 1,
	.MipLevels = settings.MipLevels,
	.SamplesCount = VK_SAMPLE_COUNT_1_BIT,
	.Format = vkFormat,
	.ImageTiling = VK_IMAGE_TILING_OPTIMAL,
	.ImageUsageFlags = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
	.MemoryPropertyFlags = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT
    };
    vsa_image_create(vsaImageSettings, vkImage, vkDeviceMemory);

    /*
      NOTE(typedef): Make this thing faster with *_lite() function wo cmd buffers
    */
    VkImage tempImage = *vkImage;
    vsa_image_change_layout(tempImage, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, vkFormat, settings.MipLevels);
    vsa_buffer_to_image(stagingBuffer, tempImage, settings.Width, settings.Height);

#if DO_NOT_USE_MIP_LEVELS
    //DOCS(typedef): imageBlit cmd will transfer image to SHADER format
    // NOTE(typedef): This is for using image in shader
    vsa_image_change_layout(tempImage,
			    VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
			    VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
			    vkFormat,
			    1);
#else
    vsa_image_create_mipmaps(tempImage, settings.Width, settings.Height, settings.MipLevels);
#endif

    vkDestroyBuffer(VulkanDevice, stagingBuffer, AllocatorCallbacks);
    vkFreeMemory(VulkanDevice, stagingBufferMemory, AllocatorCallbacks);
}

/*
  NOTE/TODO(typedef): We need instanced AssetManager->LoadTexture
  for loading texture
*/
void
vsa_texture_image_create(const char* path, VkImage* vkImage, VkDeviceMemory* vkDeviceMemory, VkFormat* vkFormat, i32* mipLevels)
{
    vkValidHandle(VulkanDevice);
    i32 width, height, channels;
    stbi_uc* data = stbi_load(path, &width, &height, &channels, 0);
    //GINFO("[Texture] W=%d H=%d C=%d\n", width, height, channels);
    if (data == NULL)
    {
	GERROR("Texture could not be loaded %s\n", path);
	vguard(data != NULL && "Texture could not be loaded");
    }
    vguard(width    > 0);
    vguard(height   > 0);
    vguard(channels > 0);
    i32 minDim = Min(width, height);
    f32 logValue = log2f(minDim);
    i32 mipLevelsValue = Min(i32(logValue) + 1, 10);

#if 0
    {
	printf("[MipMap] wh: {%d, %d} maxDim: %d logValue: %f Final: %d\n",
	       width, height,
	       minDim,
	       logValue,
	       mipLevelsValue);
    }
#endif

    SimpleImage* simpleImage =
	simple_image_create(data, width, height, channels);

    *vkFormat = VK_FORMAT_R8G8B8A8_SRGB;
    VsaTextureImageSettings settings = {
	.Path = path,
	.MipLevels = mipLevelsValue,
	.Width = width,
	.Height = height,
	.Channels = 4,
	.Data = simpleImage->Data
    };
    vsa_texture_image_create_ext(settings, vkImage, vkDeviceMemory, *vkFormat);

    stbi_image_free(data);
    simple_image_destroy(simpleImage);

    *mipLevels = mipLevelsValue;
}

VkSampler
vsa_texture_sampler_create(i32 mipLevels)
{
    VkFilter magFilter = VK_FILTER_LINEAR; //VK_FILTER_NEAREST
    VkFilter minFilter = VK_FILTER_LINEAR;
    VkBool32 isAnisotropyFilterEnabled = VK_TRUE;
    f32 maxAnisotropy = vkGetMaxAnisotropy(VulkanPhysicalDevice);
    //GINFO("Max anisatropy: %f\n", vkGetMaxAnisotropy(VulkanPhysicalDevice));
    VkBool32 compareEnabled = VK_FALSE; // NOTE(typedef): used for PCF in shadow map technique
    VkCompareOp compareOp = VK_COMPARE_OP_ALWAYS; // NOTE(typedef): used for PCF in shadow map technique

    VkSamplerCreateInfo vkSamplerCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.magFilter = magFilter,
	.minFilter = minFilter,
	.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR,
	.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT,
	.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT,
	.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT,
	.mipLodBias = 0.0f,
	.anisotropyEnable = isAnisotropyFilterEnabled,
	.maxAnisotropy = maxAnisotropy,
	.compareEnable = compareEnabled,
	.compareOp = compareOp,
	.minLod = 0.0f, //NOTE(typedef): Multisample trick (f32) (mipLevels)/8,
	.maxLod = (f32) mipLevels,
	.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK,
	.unnormalizedCoordinates = VK_FALSE
    };

    VkSampler vkSampler;
    VkResult vkCreateSamplerResult =
	vkCreateSampler(VulkanDevice, &vkSamplerCreateInfo, AllocatorCallbacks, &vkSampler);

    return vkSampler;
}

VkSampler
vsa_texture_get_sampler(i32 mipLevels)
{
    i64 index = hash_geti(VsaSamplers, mipLevels);
    VkSampler vkSampler;
    if (index == -1)
    {
	vkSampler = vsa_texture_sampler_create(mipLevels);
	hash_put(VsaSamplers, mipLevels, vkSampler);
	index = table_index(VsaSamplers);
	vguard(index > -1 && "VsaSamplers error!");
    }

    vkSampler = VsaSamplers[index].Value;

    return vkSampler;
}

void
vsa_texture_image_destroy(VkImage vkImage, VkImageView vkImageView, VkDeviceMemory vkDeviceMemory)
{
    vkDestroyImage(VulkanDevice, vkImage, AllocatorCallbacks);
    vkFreeMemory(VulkanDevice, vkDeviceMemory, AllocatorCallbacks);
    vkDestroyImageView(VulkanDevice, vkImageView, AllocatorCallbacks);
}

void
vsa_texture_image_destroy_w_sampler(VkImage vkImage, VkImageView vkImageView, VkDeviceMemory vkDeviceMemory, VkSampler vkSampler)
{
    vsa_texture_image_destroy(vkImage, vkImageView, vkDeviceMemory);
    vkDestroySampler(VulkanDevice, vkSampler, AllocatorCallbacks);
}

VsaTexture
vsa_texture_new(const char* path)
{
    const char* pathName = path_get_name(path);

    VsaTexture vsaTexture = {
	.Name = string(pathName)
    };

    vsa_texture_image_create(
	path,
	&vsaTexture.Image,
	&vsaTexture.ImageMemory,
	&vsaTexture.Format,
	&vsaTexture.MipLevels);
    vsaTexture.ImageView = vsa_image_view_create(
	vsaTexture.Image,
	vsaTexture.Format,
	VK_IMAGE_ASPECT_COLOR_BIT,
	vsaTexture.MipLevels);
    vsaTexture.Sampler = vsa_texture_get_sampler(vsaTexture.MipLevels);

    return vsaTexture;
}

i32
vsa_texture_is_valid(VsaTexture vsaTexture)
{
    if (vsaTexture.Image != NULL
	&& vsaTexture.ImageMemory != NULL
	&& vsaTexture.ImageView != NULL
	&& vsaTexture.Sampler != NULL)
    {
	return 1;
    }

    return 0;
}

void
vsa_texture_destroy(VsaTexture vsaTexture)
{
    if (!vsa_texture_is_valid(vsaTexture))
	return;

    vsa_texture_image_destroy(vsaTexture.Image, vsaTexture.ImageView, vsaTexture.ImageMemory);

    memory_free(vsaTexture.Name);
}

VsaShaderBindingsCore
vsa_shader_bindings_create(VsaShaderStage* stages, i32 stagesCount)
{
    vkValidHandle(VulkanDevice);

    VkDescriptorSetLayoutBinding* bindings = NULL;
    VkDescriptorPoolSize* poolSizes = NULL;

    i32 bindingIndex = 0;
    for (i32 i = 0; i < stagesCount; ++i)
    {
	VsaShaderStage vsaShaderStage = stages[i];
	VkShaderStageFlagBits stageFlag =
	    vsa_shader_type_to_stage_flag(vsaShaderStage.Type);

	for (i32 d = 0; d < vsaShaderStage.DescriptorsCount; ++d)
	{
	    VsaShaderDescriptor descriptor = vsaShaderStage.Descriptors[d];
	    VkDescriptorSetLayoutBinding binding = {
		.binding = bindingIndex,
		.descriptorType = descriptor.Type,
		.descriptorCount = descriptor.Count,
		.stageFlags = stageFlag,
		.pImmutableSamplers = NULL
	    };

	    VkDescriptorPoolSize poolSize = {
		.type = descriptor.Type,
		.descriptorCount = descriptor.Count
	    };

	    array_push(bindings, binding);
	    array_push(poolSizes, poolSize);

	    ++bindingIndex;
	}
    }

    VkDescriptorSetLayoutCreateInfo descriptorCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.bindingCount = array_count(bindings),
	.pBindings = bindings
    };

    VkDescriptorSetLayout vkDescriptorSetLayout;
    VkResult creteDescriptorSetLayout =
	vkCreateDescriptorSetLayout(VulkanDevice, &descriptorCreateInfo, AllocatorCallbacks, &vkDescriptorSetLayout);
    vkValidResult(creteDescriptorSetLayout, "Failed descriptor set layout creation!");

    VkDescriptorPoolCreateInfo vkPoolCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.maxSets = 1,
	.poolSizeCount = array_count(poolSizes),
	.pPoolSizes = poolSizes,
    };

    VkDescriptorPool vkDescriptorPool;
    VkResult descriptorPoolCreateResult =
	vkCreateDescriptorPool(VulkanDevice, &vkPoolCreateInfo, AllocatorCallbacks, &vkDescriptorPool);

    vkValidResult(descriptorPoolCreateResult, "Failed Descriptor Pool Creation!!!");

    VkDescriptorSet vkDescriptorSet;
    VkDescriptorSetAllocateInfo vkDescriptorSetAllocateInfo = {
	.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
	.pNext = NULL,
	.descriptorPool = vkDescriptorPool,
	.descriptorSetCount = 1,
	.pSetLayouts = &vkDescriptorSetLayout,
    };

    VkResult allocateDescritorSetsResult =
	vkAllocateDescriptorSets(VulkanDevice, &vkDescriptorSetAllocateInfo, &vkDescriptorSet);
    vkValidResult(allocateDescritorSetsResult, "Failed allocate descriptor sets!");

    VsaShaderBindingsCore vsaShaderBindings = {
	.DescriptorSet = vkDescriptorSet,
	.DescriptorSetLayout = vkDescriptorSetLayout,
	.DescriptorPool = vkDescriptorPool
    };

    array_free(bindings);
    array_free(poolSizes);

    return vsaShaderBindings;
}

void
vsa_render_pass_create()
{
    /*NOTE(typedef): this is layout(location=0) out vec4 Color; in .frag*/

    VkAttachmentDescription colorAttachmentDescription = {
	.flags = 0,
	.format = VulkanSurfaceFormat.format,
	.samples = VulkanSamplesCount,
	.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
	.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
	.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	.finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };
    VkAttachmentReference colorAttachmentReference = {
	.attachment = 0,
	.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkAttachmentDescription depthAttachmentDescription = {
	.flags = 0,
	.format = VK_FORMAT_D32_SFLOAT,
	.samples = VulkanSamplesCount,
	.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
	.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };
    VkAttachmentReference depthAttachmentReference = {
	.attachment = 1,
	.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    };

    VkAttachmentDescription resolvedMultisampleAttachmentDescription = {
	.flags = 0,
	.format = VulkanSurfaceFormat.format,
	.samples = VK_SAMPLE_COUNT_1_BIT,
	.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	.storeOp = VK_ATTACHMENT_STORE_OP_STORE,
	.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
	.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
	.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
	.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    };
    VkAttachmentReference resolvedMultisampleAttachmentReference = {
	.attachment = 2,
	.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    };

    VkSubpassDescription subpassDescription = {
	.flags = 0,
	.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS,
	.inputAttachmentCount = 0,
	.pInputAttachments = NULL,
	.colorAttachmentCount = 1,
	.pColorAttachments = &colorAttachmentReference,
	.pResolveAttachments = &resolvedMultisampleAttachmentReference,
	.pDepthStencilAttachment = &depthAttachmentReference,
	.preserveAttachmentCount = 0,
	.pPreserveAttachments = NULL
    };

    VkSubpassDependency subpassDependency = {
	.srcSubpass = VK_SUBPASS_EXTERNAL,
	.dstSubpass = 0,
	.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
	.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT,
	.srcAccessMask = 0,
	.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
	.dependencyFlags = 0,
    };

    VkAttachmentDescription attachments[] = {
	[0] = colorAttachmentDescription,
	[1] = depthAttachmentDescription,
	[2] = resolvedMultisampleAttachmentDescription
    };
    VkRenderPassCreateInfo renderPassCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.attachmentCount = ARRAY_COUNT(attachments),
	.pAttachments = attachments,
	.subpassCount = 1,
	.pSubpasses = &subpassDescription,
	.dependencyCount = 1,
	.pDependencies = &subpassDependency
    };

    VkResult createRenderPassResult =
	vkCreateRenderPass(VulkanDevice, &renderPassCreateInfo, AllocatorCallbacks, &VulkanRenderPass);
    vkValidResult(createRenderPassResult, "Can't create RenderPass!");
}

void
vsa_render_pass_proccess(VsaPipeline** pipelines, i32 pipelinesCount, i32 imageIndex)
{

    /*
      DOCS(typedef): Recording Commands BEGIN
    */

    VkCommandBufferBeginInfo commandBufferBeginInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
	    .pNext = NULL,
	    .flags = 0,
	    .pInheritanceInfo = NULL
	    };
    VkResult beginCommandBufferResult =
	vkBeginCommandBuffer(VulkanCommandBuffer, &commandBufferBeginInfo);
    vkValidResult(beginCommandBufferResult, "Begin command buffer!");

    //TODO(typedef): set in pipeline
    VkClearValue clearValues[2] = {
	[0] = (VkClearValue) {
	    .color = { 0.0034f, 0.0037f, 0.0039f, 1 }
	},
	[1] = (VkClearValue) {
	    .depthStencil = { 1.0f, 0.0f }
	}
    };

    // TODO/BUG remove from pipeline_update()
    VkRenderPassBeginInfo renderPassBeginInfo = {
	.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
	.pNext = NULL,
	.renderPass = VulkanRenderPass,
	.framebuffer = VulkanFramebuffers[imageIndex],
	.renderArea = {
	    .offset = {	0, 0 },
	    .extent = VulkanExtent
	},
	.clearValueCount = 2,
	.pClearValues = clearValues
    };
    vkCmdBeginRenderPass(VulkanCommandBuffer, &renderPassBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

    //TODO(typedef): set in pipeline
    VkViewport viewport = {
	.x = 0,
	.y = 0,
	.width = VulkanExtent.width,
	.height = VulkanExtent.height,
	.minDepth = 0,
	.maxDepth = 1
    };
    vkCmdSetViewport(VulkanCommandBuffer, 0, 1, &viewport);

    //TODO(typedef): set in pipeline
    VkRect2D scissor = {
	.offset = { 0, 0 },
	.extent = VulkanExtent
    };
    vkCmdSetScissor(VulkanCommandBuffer, 0, 1, &scissor);

    //TODO(): for (var pipeline in pipelines)
    for (i32 i = 0; i < pipelinesCount; ++i)
    {
	VsaPipeline* pVsaPipeline = pipelines[i];

	// VsaTempPipeline -> vsaPipeline
	vkCmdBindPipeline(VulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->Pipeline);
	VkDeviceSize offsets = 0;
	vkCmdBindVertexBuffers(VulkanCommandBuffer, 0, 1, &pVsaPipeline->Vertex.Gpu, &offsets);
	vkCmdBindIndexBuffer(VulkanCommandBuffer, pVsaPipeline->Index.Gpu, 0, VK_INDEX_TYPE_UINT32);
	vkCmdBindDescriptorSets(VulkanCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pVsaPipeline->PipelineLayout, 0, 1, &pVsaPipeline->BindingCore.DescriptorSet, 0, NULL);

	vkCmdDrawIndexed(VulkanCommandBuffer, pVsaPipeline->IndicesCount, 1, 0, 0, 0);
    }

    /*
      DOCS(typedef): Recording Commands END
    */

    vkCmdEndRenderPass(VulkanCommandBuffer);
    VkResult endCommandBufferResult = vkEndCommandBuffer(VulkanCommandBuffer);
    vkValidResult(endCommandBufferResult, "Can't end command buffer!");
}

VkPipelineShaderStageCreateInfo
vsa_shader_type_to_stage_create_info(VsaShaderType type, VkShaderModule vkShaderModule)
{
    VkShaderStageFlagBits flag = vsa_shader_type_to_stage_flag(type);

    VkPipelineShaderStageCreateInfo stageCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.stage = flag,
	.module = vkShaderModule,
	.pName = "main",
	/* NOTE(typedef): use this thing if we need constants in shader */
	.pSpecializationInfo = NULL
    };

    return stageCreateInfo;
}

VsaPipeline
vsa_pipeline_create(VsaPipelineDescription descriptions)
{
    VkPipeline vkPipeline;
    VkPipelineLayout vkPipelineLayout;
    VsaShaderPaths paths = {};
    VsaPipeline vsaPipeline = {};

    // NOTE(typedef): mb place bindings && poolSizes inside bot loop??
    vsaPipeline.BindingCore = vsa_shader_bindings_create(descriptions.Stages, descriptions.StagesCount);

    for (i32 i = 0; i < descriptions.StagesCount; ++i)
    {
	VsaShaderStage stage = descriptions.Stages[i];
	const char* path = stage.ShaderSourcePath;
	switch (stage.Type)
	{
	case VsaShaderType_Vertex:
	    paths.VertexPath = path;
	    break;

	case VsaShaderType_Fragment:
	    paths.FragmentPath = path;
	    break;

	case VsaShaderType_Geometry:
	    paths.GeometryPath = path;
	    break;

	case VsaShaderType_Compute:
	    paths.ComputePath = path;
	    break;

	case VsaShaderType_TesselationControl:
	    paths.TesselationControlPath = path;
	    break;

	case VsaShaderType_TesselationEvaluation:
	    paths.TesselationEvaluationPath = path;
	    break;
	}

	for (i32 i = 0; i < stage.DescriptorsCount; ++i)
	{
	    VsaShaderDescriptor vsaShaderDescriptor = stage.Descriptors[i];
	    switch (vsaShaderDescriptor.Type)
	    {
	    case VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
		VsaUniform uniform =
		    vsa_uniform_create(vsaShaderDescriptor.Size);
		array_push(vsaPipeline.Uniforms, uniform);
		break;
	    }
	}
    }

    // DOCS(typedef): to reduce vulkan error
    vsa_pipeline_update_uniforms(vsaPipeline);

    VsaShaderOutput vsaShaderOutput = {};
    vsa_shader_compile(paths, &vsaShaderOutput);

    VkPipelineShaderStageCreateInfo* stages = NULL;
    for (i32 i = 0; i < descriptions.StagesCount; ++i)
    {
	VsaShaderStage vsaStage = descriptions.Stages[i];

	VsaShaderCompiledBytecode compiledBytecode;
	switch (vsaStage.Type)
	{

	case VsaShaderType_Vertex:
	    compiledBytecode = vsaShaderOutput.Vertex;
	    break;
	case VsaShaderType_Fragment:
	    compiledBytecode = vsaShaderOutput.Fragment;
	    break;
	case VsaShaderType_Geometry:
	    compiledBytecode = vsaShaderOutput.Geometry;
	    break;
	case VsaShaderType_Compute:
	    compiledBytecode = vsaShaderOutput.Compute;
	    break;
	case VsaShaderType_TesselationControl:
	    compiledBytecode = vsaShaderOutput.TesselationControl;
	    break;
	case VsaShaderType_TesselationEvaluation:
	    compiledBytecode = vsaShaderOutput.TesselationEvaluation;
	    break;

	default:
	    vguard(0);
	}

	vguard_not_null(compiledBytecode.Bytecode);
	vguard(compiledBytecode.Size > 0);

	VkShaderModule vkShaderModule =
	    vsa_shader_module_create_ext(VulkanDevice, compiledBytecode.Bytecode, compiledBytecode.Size);

	VkPipelineShaderStageCreateInfo stage =
	    vsa_shader_type_to_stage_create_info(
		vsaStage.Type,
		vkShaderModule);
	array_push(stages, stage);
    }

    // NOTE(typedef): Vertex Buffer description
    VkVertexInputBindingDescription vkVertexInputBindingDescription = {
	.binding = 0,
	.stride = descriptions.Stride,
	.inputRate = VK_VERTEX_INPUT_RATE_VERTEX
    };

    VkVertexInputAttributeDescription* attributeDescrs = NULL;
    for (i32 i = 0; i < descriptions.AttributesCount; ++i)
    {
	VsaShaderAttribute vsaAttribute = descriptions.Attributes[i];

	VkVertexInputAttributeDescription descr = {
	    .location = i,
	    .binding = 0,
	    .format = vsaAttribute.Format,
	    .offset = vsaAttribute.Offset
	};

	array_push(attributeDescrs, descr);
    }

    VkPipelineVertexInputStateCreateInfo pipelineVertexInputCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
	.vertexBindingDescriptionCount = 1,
	.pVertexBindingDescriptions = &vkVertexInputBindingDescription,
	.vertexAttributeDescriptionCount = array_count(attributeDescrs),
	.pVertexAttributeDescriptions = attributeDescrs
    };

    VkDynamicState dynamicStates[2] = {
	[0] = VK_DYNAMIC_STATE_VIEWPORT,
	[1] = VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo pipelineDynamicStateCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.dynamicStateCount = ARRAY_COUNT(dynamicStates),
	.pDynamicStates = dynamicStates,
    };

    VkPipelineInputAssemblyStateCreateInfo pipelineInputAssemblyCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
	.primitiveRestartEnable = VK_FALSE
    };

    // DOCS(typedef): passing viewport, scissor here make them immutable
    VkPipelineViewportStateCreateInfo pipelineViewportCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.viewportCount = 1,
	.scissorCount = 1
    };

    VkPipelineRasterizationStateCreateInfo pipelineRasterizationCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	// NOTE(typedef): enabling that requires gpu feature
	.depthClampEnable = VK_FALSE,
	.rasterizerDiscardEnable = VK_FALSE,
	// NOTE(typedef): requires enabling gpu feature
	.polygonMode = VK_POLYGON_MODE_FILL,
	.cullMode = VK_CULL_MODE_BACK_BIT,
	.frontFace =
#if 1 // NOTE(): best option
	VK_FRONT_FACE_COUNTER_CLOCKWISE,
#else
	VK_FRONT_FACE_CLOCKWISE,
#endif
	.depthBiasEnable = VK_FALSE,
	.depthBiasConstantFactor = 0.0f,
	.depthBiasClamp = 0.0f,
	.depthBiasSlopeFactor = 0.0f,
	// NOTE(typedef): anything other then 1.0f requires enabling GPU feature
	.lineWidth = 1.0f
    };

    VkPipelineMultisampleStateCreateInfo pipelineMultisampleCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.rasterizationSamples = VulkanSamplesCount,
	.sampleShadingEnable = VK_FALSE,
	.minSampleShading = 1.0f,
	.pSampleMask = NULL,
	.alphaToCoverageEnable = VK_FALSE,
	.alphaToOneEnable = VK_FALSE
    };

    // NOTE(typedef): Depth Stencil code creation here
    VkPipelineDepthStencilStateCreateInfo depthStencilCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.depthTestEnable = VK_TRUE,
	.depthWriteEnable = VK_TRUE,
	.depthCompareOp = VK_COMPARE_OP_LESS,
	.depthBoundsTestEnable = VK_FALSE,
	.stencilTestEnable = VK_FALSE,
	.front = 0.0f,
	.back = 0.0f,
	.minDepthBounds = -1.0f,
	.maxDepthBounds =  1.0f
    };

#if 0 || BLENDING_DISABLED == 1
    // NOTE(typedef): Blending not configured now
    VkPipelineColorBlendAttachmentState pipelineColorBlendAttachment = {
	.blendEnable = VK_FALSE,
	.srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
	.dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
	.colorBlendOp = VK_BLEND_OP_ADD,
	.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
	.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
	.alphaBlendOp = VK_BLEND_OP_ADD,
	.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };

#else
    // NOTE(typedef): Blending not configured now
    VkPipelineColorBlendAttachmentState pipelineColorBlendAttachment = {
	.blendEnable = VK_TRUE,
	.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA,
	.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
	.colorBlendOp = VK_BLEND_OP_ADD,
	.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
	.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
	.alphaBlendOp = VK_BLEND_OP_ADD,
	.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT,
    };
#endif


    VkPipelineColorBlendStateCreateInfo pipelineColorBlendCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.logicOpEnable = VK_FALSE,
	.logicOp = VK_LOGIC_OP_COPY,
	.attachmentCount = 1,
	.pAttachments = &pipelineColorBlendAttachment,
	.blendConstants = { 0, 0, 0, 0 }
    };

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.setLayoutCount = 1,
	.pSetLayouts = &vsaPipeline.BindingCore.DescriptorSetLayout, //&VulkanTempDescriptorSetLayout,
	.pushConstantRangeCount = 0,
	.pPushConstantRanges = 0
    };

    VkResult createPipelineLayoutResult =
	vkCreatePipelineLayout(VulkanDevice, &pipelineLayoutCreateInfo, AllocatorCallbacks, &vkPipelineLayout);
    vkValidResult(createPipelineLayoutResult, "Can't create pipeline layout!");

    VkGraphicsPipelineCreateInfo graphicsPipelineCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0,
	.stageCount = array_count(stages),
	.pStages = stages,
	.pVertexInputState = &pipelineVertexInputCreateInfo,
	.pInputAssemblyState = &pipelineInputAssemblyCreateInfo,
	.pTessellationState = NULL,
	.pViewportState = &pipelineViewportCreateInfo,
	.pRasterizationState = &pipelineRasterizationCreateInfo,
	.pMultisampleState = &pipelineMultisampleCreateInfo,
	.pDepthStencilState = &depthStencilCreateInfo,
	.pColorBlendState = &pipelineColorBlendCreateInfo,
	.pDynamicState = &pipelineDynamicStateCreateInfo,
	.layout = vkPipelineLayout,
	.renderPass = VulkanRenderPass,
	.subpass = 0,
	.basePipelineHandle = VK_NULL_HANDLE,
	.basePipelineIndex = -1
    };

    VkResult createGraphicsPipelinesResult =
	vkCreateGraphicsPipelines(VulkanDevice, VK_NULL_HANDLE, 1, &graphicsPipelineCreateInfo, AllocatorCallbacks, &vkPipeline);
    vkValidResult(createGraphicsPipelinesResult, "Can't crete Graphics Pipeline!");
    vsaPipeline.Pipeline = vkPipeline;
    vsaPipeline.PipelineLayout = vkPipelineLayout;

    // DOCS(typedef): Clean up
    array_free(attributeDescrs);
    vsa_shader_output_free(vsaShaderOutput);
    for (i32 i = 0; i < array_count(stages); ++i)
    {
	VkPipelineShaderStageCreateInfo stageCreateInfo = stages[i];
	vkDestroyShaderModule(VulkanDevice, stageCreateInfo.module, AllocatorCallbacks);
    }
    array_free(stages);

    return vsaPipeline;
}

void
vsa_pipeline_set_buffers(VsaPipeline* vsaPipeline, VsaBuffer vertex, VsaBuffer index)
{
    vsaPipeline->Vertex = vertex;
    vsaPipeline->Index = index;
    // NOTE(typedef): Not sure about command buffer
    vsaPipeline->CmdBuffer = VulkanCommandBuffer;
}

i32
vsa_pipeline_find_descriptor(VsaPipeline* vsaPipeline, VkDescriptorType vkDescriptorType)
{
    i32 ind = -1;

    /* for (i32 i = 0; i < vsaPipeline->DescriptorsCount; ++i) */
    /* { */
    /*	VsaShaderDescriptor vsaShaderDescriptor = vsaPipeline->Descriptors[i]; */
    /*	if (vsaShaderDescriptor.Type == vkDescriptorType) */
    /*	{ */
    /*	    ind = i; */
    /*	    break; */
    /*	} */
    /* } */

    return ind;
}

void
vsa_pipeline_bind_textures(VsaPipeline* vsaPipeline, VsaTexture* textures, i32 count, i32 maxCount)
{
    i32 ind = vsa_pipeline_find_descriptor(vsaPipeline, VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE);

    /*DOCS(): glslangValidator -c > gpu.conf*/
#define MaxTextureCount 32
    VkDescriptorImageInfo imageInfos[MaxTextureCount];
    VkDescriptorImageInfo samplerInfos[MaxTextureCount];

    //TODO(typedef): optimize
    for (i32 i = 0; i < MaxTextureCount/* count */; ++i)
    {
	VsaTexture texture = (i < count) ? textures[i] : textures[0];
	vguard(vsa_texture_is_valid(texture));

	imageInfos[i]   = (VkDescriptorImageInfo) {
	    .sampler = VK_NULL_HANDLE,
	    .imageView = texture.ImageView,
	    .imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
	};
	samplerInfos[i] = (VkDescriptorImageInfo) {
	    .sampler = texture.Sampler
	};
    }

    i32 binding = array_count(vsaPipeline->Uniforms);
    VkWriteDescriptorSet textureArray = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.pNext = NULL,
	.dstSet = vsaPipeline->BindingCore.DescriptorSet,
	.dstBinding = binding,
	.dstArrayElement = 0,
	.descriptorCount = maxCount,
	.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
	.pImageInfo = imageInfos,
	.pBufferInfo = NULL,
	.pTexelBufferView = NULL
    };

    VkWriteDescriptorSet samplers = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.pNext = NULL,
	.dstSet = vsaPipeline->BindingCore.DescriptorSet,
	.dstBinding = binding + 1,
	.dstArrayElement = 0,
	.descriptorCount = maxCount,
	.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER,
	.pImageInfo = samplerInfos,
	.pBufferInfo = NULL,
	.pTexelBufferView = NULL
    };

    VkWriteDescriptorSet sets[] = {
	textureArray,
	samplers
    };

    vkUpdateDescriptorSets(VulkanDevice, 2, sets, 0, NULL);
}

void
vsa_pipeline_update_uniforms(VsaPipeline vsaPipeline)
{
    for (i32 u = 0; u < array_count(vsaPipeline.Uniforms); ++u)
    {
	VsaUniform vsaUniform = vsaPipeline.Uniforms[u];
	VkWriteDescriptorSet vkWriteDescriptorSets[] = {
	    [0] = vsa_uniform_get_write_descriptor(
		vsaPipeline.BindingCore.DescriptorSet,
		vsaUniform,
		u)
	};

	vkUpdateDescriptorSets(VulkanDevice, 1, vkWriteDescriptorSets, 0, NULL);
    }
}

void
vsa_pipeline_update(VsaPipeline* pVsaPipeline, i32 indicesCount)
{
    pVsaPipeline->IndicesCount = indicesCount;
}

void
vsa_pipeline_destroy(VsaPipeline vsaPipeline)
{
    for (i32 i = 0; i < array_count(vsaPipeline.Uniforms); ++i)
    {
	VsaUniform vsaUniform = vsaPipeline.Uniforms[i];
	vsa_uniform_destroy(vsaUniform);
    }

    array_free(vsaPipeline.Uniforms);

    vsa_shader_bindings_destroy(vsaPipeline.BindingCore);

    vsa_buffer_destroy(vsaPipeline.Vertex);
    vsa_buffer_destroy(vsaPipeline.Index);

    vkDestroyPipeline(VulkanDevice, vsaPipeline.Pipeline, AllocatorCallbacks);
    vkDestroyPipelineLayout(VulkanDevice, vsaPipeline.PipelineLayout, AllocatorCallbacks);
}

VkFramebuffer*
vsa_framebuffer_create()
{
    vkValidHandle(VulkanDevice);
    vkValidHandle(VulkanRenderPass);

    i32 imageViewsCount = array_count(VulkanImageViews);
    array_reserve(VulkanFramebuffers, imageViewsCount);
    array_header(VulkanFramebuffers)->Count = imageViewsCount;

    for (i32 i = 0; i < imageViewsCount; ++i)
    {

	/*
	  DEPENDS(typedef):
	  render_pass_creation"
	  VkAttachmentDescription attachments[] = {
	  [0] = colorAttachmentDescription,
	  [1] = depthAttachmentDescription,
	  [2] = resolvedMultisampleAttachmentDescription
	  };
	*/

	VkImageView attachmentViews[] = {
	    [0] = VulkanMultisampleImageView,
	    [1] = VulkanDepthImageView,
	    [2] = VulkanImageViews[i]
	};

	VkFramebufferCreateInfo frabufferCreateInfo = {
	    .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
	    .pNext = NULL,
	    .flags = 0,
	    .renderPass = VulkanRenderPass,
	    .attachmentCount = ARRAY_COUNT(attachmentViews),
	    .pAttachments = attachmentViews,
	    .width = VulkanExtent.width,
	    .height = VulkanExtent.height,
	    .layers = 1
	};

	VkResult createFramebufferResult =
	    vkCreateFramebuffer(VulkanDevice, &frabufferCreateInfo, AllocatorCallbacks, &VulkanFramebuffers[i]);
	vkValidResult(createFramebufferResult, "Can't create framebuffer!");
    }

    return VulkanFramebuffers;
}

void
vsa_sync_create()
{
    vkValidHandle(VulkanDevice);

    VkSemaphoreCreateInfo semaphoreCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
	.pNext = NULL,
	.flags = 0
    };

    VkFenceCreateInfo frameFenceCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
	.pNext = NULL,
	//NOTE(typedef): this will become unsignaled at the 2 line (vkResetFences) of the render loop
	.flags = VK_FENCE_CREATE_SIGNALED_BIT
    };

    VkResult createImageSemaphoreResult =
	vkCreateSemaphore(VulkanDevice, &semaphoreCreateInfo, AllocatorCallbacks, &VulkanImageAvailableSemaphores);
    vkValidResult(createImageSemaphoreResult, "Failed image available semaphore creation!");

    VkResult createRenderSemaphoreResult =
	vkCreateSemaphore(VulkanDevice, &semaphoreCreateInfo, AllocatorCallbacks, &VulkanRenderFinishedSemaphores);
    vkValidResult(createRenderSemaphoreResult, "Failed image available semaphore creation!");

    VkResult createFrameFenceResult =
	vkCreateFence(VulkanDevice, &frameFenceCreateInfo, AllocatorCallbacks, &VulkanFrameFences);
    vkValidResult(createFrameFenceResult, "Failed frame fence creation!");

}

VkCommandPool
vsa_command_pool_create(VsaQueueFamily vsaQueueFamily)
{
    vkValidHandle(VulkanDevice);

    VkCommandPool vkCommandPool;
    VkCommandPoolCreateInfo commandPoolCreateInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
	.pNext = NULL,
	.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
	.queueFamilyIndex = vsaQueueFamily.GraphicsIndex
    };
    VkResult createCommandPoolResult =
	vkCreateCommandPool(VulkanDevice, &commandPoolCreateInfo, AllocatorCallbacks, &vkCommandPool);
    vkValidResult(createCommandPoolResult, "Can't create Command Pool!");

    return vkCommandPool;
}

VkCommandBuffer
vsa_command_buffer_create(VkCommandPool vkCommandPool)
{
    VkCommandBuffer vkCommandBuffer;

    VkCommandBufferAllocateInfo commandBufferAllocatedInfo = {
	.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
	.pNext = NULL,
	.commandPool = vkCommandPool,
	.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
	.commandBufferCount = 1
    };
    VkResult allocatingCommandBufferResult =
	vkAllocateCommandBuffers(VulkanDevice, &commandBufferAllocatedInfo, &vkCommandBuffer);
    vkValidResult(allocatingCommandBufferResult, "Can't allocate Command Buffer!");

    return vkCommandBuffer;
}

VsaUniform
vsa_uniform_create(size_t size)
{
    vkValidHandle(VulkanDevice);

    VsaUniform vsaUniform = {
	.Size = size
    };

    vsaUniform.Buffer =
	vsa_buffer_create(
	    VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
	    size,
	    (VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT),
	    &vsaUniform.DeviceMemory);
    // DOCS(typedef): persistence mapping
    VkResult mapMemoryResult =
	vkMapMemory(
	    VulkanDevice, vsaUniform.DeviceMemory,
	    0, size, 0, &vsaUniform.MappedMemory);
    vkValidResult(mapMemoryResult,
		  "Failed memory mapping for uniform buffer!");

    return vsaUniform;
}

void
vsa_uniform_set_data(VsaUniform vsaUniform, void* data, size_t size)
{
    memcpy(vsaUniform.MappedMemory, data, size);
}

VkWriteDescriptorSet
vsa_uniform_get_write_descriptor(VkDescriptorSet vkDescriptorSet, VsaUniform vsaUniform, i32 binding)
{
    static VkDescriptorBufferInfo bufferInfo = {};
    bufferInfo.buffer = vsaUniform.Buffer;
    bufferInfo.offset = 0;
    bufferInfo.range = vsaUniform.Size;

    VkWriteDescriptorSet vkWriteDescriptorSet = {
	.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
	.pNext = NULL,
	.dstSet = vkDescriptorSet,
	.dstBinding = binding,
	.dstArrayElement = 0,
	.descriptorCount = 1,
	.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
	.pImageInfo = NULL,
	.pBufferInfo = &bufferInfo,
	.pTexelBufferView = NULL
    };

    return vkWriteDescriptorSet;
}

void
vsa_uniform_destroy(VsaUniform vsaUniform)
{
    vkDestroyBuffer(VulkanDevice, vsaUniform.Buffer, AllocatorCallbacks);
    vkFreeMemory(VulkanDevice, vsaUniform.DeviceMemory, AllocatorCallbacks);
}

void
vsa_frame_start(VkCommandBuffer vkCommandBuffer, i32* imageIndex)
{
    /*
      NOTE(typedef): Render Loop
      * Wait for the previous frame to finish
      * Acquire an image from the swap chain
      * Reset cmd buffer
      */

    vkWaitForFences(VulkanDevice, 1, &VulkanFrameFences, VK_TRUE, VsaNoTimeout);

    VkResult accuireNextImageResult =
	vkAcquireNextImageKHR(VulkanDevice, VulkanSwapChain, VsaNoTimeout, VulkanImageAvailableSemaphores, VK_NULL_HANDLE, imageIndex);
    if (accuireNextImageResult == VK_ERROR_OUT_OF_DATE_KHR)
    {
	GINFO("Recreate!\n");
	vsa_swapchain_objects_recreate();
	return;
    }
    else if (accuireNextImageResult == VK_SUBOPTIMAL_KHR)
    {
	// DOCS(typedef): this thing is not an error or smth, just continue there
    }
    else
    {
	vkValidResult(accuireNextImageResult, "Can't acquire next image khr!");
    }

    vkResetFences(VulkanDevice, 1, &VulkanFrameFences);

    vkResetCommandBuffer(vkCommandBuffer, VsaNoFlags);
}

void
vsa_local_frame_start(i32* imageIndex)
{
    vsa_frame_start(VulkanCommandBuffer, imageIndex);
}

void
vsa_frame_end(VkCommandBuffer vkCommandBuffer, i32* pImageIndex)
{
    /*
      NOTE(typedef):
      * Submit the recorded command buffer
      * Present the swap chain image
      */

    VkPipelineStageFlags pipelineStageFlag = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo = {
	.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
	.pNext = NULL,
	.waitSemaphoreCount = 1,
	.pWaitSemaphores = &VulkanImageAvailableSemaphores,
	.pWaitDstStageMask = &pipelineStageFlag,
	.commandBufferCount = 1,
	.pCommandBuffers = &vkCommandBuffer,
	.signalSemaphoreCount = 1,
	.pSignalSemaphores = &VulkanRenderFinishedSemaphores
    };

    // NOTE(typedef): Submiting Recorded Commands
    VkResult queueSubmitResult =
	vkQueueSubmit(VulkanGraphicsQueue, 1, &submitInfo, VulkanFrameFences);
    vkValidResult(queueSubmitResult, "Can't Submit Queue!");

    VkPresentInfoKHR presentInfoKhr = {
	.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
	.pNext = NULL,
	.waitSemaphoreCount = 1,
	.pWaitSemaphores = &VulkanRenderFinishedSemaphores,
	.swapchainCount = 1,
	.pSwapchains = &VulkanSwapChain,
	.pImageIndices = pImageIndex,
	.pResults = NULL
    };

    VkResult queuePresentKhr =
	vkQueuePresentKHR(VulkanPresentationQueue, &presentInfoKhr);
    if (queuePresentKhr == VK_ERROR_OUT_OF_DATE_KHR || queuePresentKhr == VK_SUBOPTIMAL_KHR || IsWindowBeenResized)
    {
	GINFO("Recreate!\n");
	IsWindowBeenResized = 0;
	vsa_swapchain_objects_recreate();
	return;
    }
    else
    {
	vkValidResult(queuePresentKhr, "Queue Present KHR!\n");
    }

}

void
vsa_local_frame_end(i32* pImageIndex)
{
    vsa_frame_end(VulkanCommandBuffer, pImageIndex);
}

void
vsa_swapchain_objects_cleanup()
{
    if (VulkanSwapChain == VK_NULL_HANDLE)
    {
	GWARNING("SwapChain is NULL HANDLE!\n");
	return;
    }

    for (i32 i = 0; i < array_count(VulkanImageViews); ++i)
	vkDestroyImageView(VulkanDevice, VulkanImageViews[i], AllocatorCallbacks);
    for (i32 i = 0; i < array_count(VulkanFramebuffers); ++i)
	vkDestroyFramebuffer(VulkanDevice, VulkanFramebuffers[i], AllocatorCallbacks);

    vkDestroySwapchainKHR(VulkanDevice, VulkanSwapChain, AllocatorCallbacks);

    vsa_texture_image_destroy(VulkanDepthImage, VulkanDepthImageView, VulkanDepthImageMemory);
    vsa_texture_image_destroy(VulkanMultisampleImage, VulkanMultisampleImageView, VulkanMultisampleImageMemory);
}

VsaSwapChainSettings
vsa_get_swapchain_settings(VsaSettings settings)
{
    VsaSwapChainSettings swapChainSettings = {
	.SurfaceFormat = {
	    .format = VK_FORMAT_B8G8R8A8_SRGB,
	    .colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
	}
    };
    if (settings.IsVSyncEnabled)
    {
	swapChainSettings.PresentationMode = VK_PRESENT_MODE_FIFO_KHR;
    }
    else
    {
	swapChainSettings.PresentationMode = VK_PRESENT_MODE_IMMEDIATE_KHR;
    }

    return swapChainSettings;
}

void
vsa_swapchain_objects_recreate()
{
    vkDeviceWaitIdle(VulkanDevice);
    vsa_swapchain_objects_cleanup();

    array_free(VulkanSwapChainImages);
    VsaSwapChainSettings swapChainSettings =
	vsa_get_swapchain_settings(VsaSetting);
    vsa_swapchain_create(swapChainSettings, VulkanSupportDetails, VulkanQueueFamily);

    array_free(VulkanImageViews);
    array_free(VulkanFramebuffers);
    vsa_image_views_create();
    vsa_depth_create();
    vsa_multisample_create();
    vsa_framebuffer_create();

    GINFO("Extent: %d %d\n", VulkanExtent.width, VulkanExtent.height);
}

void
vsa_core_init(VsaSettings vsaSettings)
{
    if (VsaCoreInitialized == 1)
    {
	return;
    }
    VsaCoreInitialized = 1;
    GINFO("Init Vulkan Core\n");

    VsaSetting = vsaSettings;

    /*
      DOCS(typedef): Core
    */

    vsa_instance_create();
    vsa_surface_create();
    vsa_physical_device_create();
    // DEPENDS(typedef): On Physical Device
    // TODO(typedef): Make this configurable
    VulkanSamplesCount = Min(VsaSetting.SamplesCount, vsa_samples_get_max_count());
    vsa_queue_family_create(&VulkanSupportDetails);
    vsa_device_create(VulkanQueueFamily);
    vsa_graphics_queue_create(VulkanQueueFamily.GraphicsIndex);
    vsa_presentation_queue_create(VulkanQueueFamily.PresentationIndex);

    vsa_sync_create();

    /*
      NOTE(typedef): Create "Infrastructure"|SwapChains for managing ALL
      Buffers (Framebuffer, VkBuffers, etc..)
      SwapChain is essentially array of images for presentation on the screen

      Creates: VulkanExtent
    */
    VsaSwapChainSettings swapChainSettings =
	vsa_get_swapchain_settings(VsaSetting);
    vsa_swapchain_create(swapChainSettings, VulkanSupportDetails, VulkanQueueFamily);

    /*
      DEPENDS(typedef): VulkanExtent
    */
    vsa_depth_create();
    /*
      DEPENDS(typedef): VulkanSurfaceFormat
    */
    vsa_multisample_create();

    vsa_image_views_create();
    vsa_render_pass_create();

    /*
      DEPENDS(typedef):
      VulkanRenderPass
      VulkanImageViews
    */
    vsa_framebuffer_create();

    // NOTE(typedef): For now it's part of Core Api
    VulkanCommandPool = vsa_command_pool_create(VulkanQueueFamily);
    VulkanCommandBuffer = vsa_command_buffer_create(VulkanCommandPool);

    vsa_samplers_init();
}

void
vsa_set_window_resized()
{
    IsWindowBeenResized = 1;
}

void
vsa_device_wait_idle()
{
    vkDeviceWaitIdle(VulkanDevice);
}

void
vsa_core_deinit()
{
    vsa_samplers_deinit();

    vkDestroySemaphore(VulkanDevice, VulkanImageAvailableSemaphores, AllocatorCallbacks);
    vkDestroySemaphore(VulkanDevice, VulkanRenderFinishedSemaphores, AllocatorCallbacks);
    vkDestroyFence(VulkanDevice, VulkanFrameFences, AllocatorCallbacks);
    vkDestroyCommandPool(VulkanDevice, VulkanCommandPool, AllocatorCallbacks);
    vkDestroyRenderPass(VulkanDevice, VulkanRenderPass, AllocatorCallbacks);
    vsa_swapchain_objects_cleanup();
    vkDestroyDevice(VulkanDevice, AllocatorCallbacks);
    vkDestroySurfaceKHR(VulkanInstance, VulkanSurface, AllocatorCallbacks);
    vkDestroyDebugUtilsMessenger(VulkanInstance, VulkanDebugMessanger, AllocatorCallbacks);
    vkDestroyInstance(VulkanInstance, AllocatorCallbacks);

}
