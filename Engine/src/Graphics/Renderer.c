#include "Renderer.h"

#include <glad/glad.h>

#include <Graphics/RuntimeCamera.h>
#include <EntitySystem/Components/Base/CameraComponent.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>

/* NOTE(typedef): OpenGL Structs - bad idea from the start */
#define MaxTextureSlotsNumber 128
typedef struct TextureList
{
    u32 NextTextureIndex; // it can be called count
    u32 StartIndex;
    u32 MaxIndex;
    u32 Textures[MaxTextureSlotsNumber];
} TextureList;

force_inline TextureList
texture_list_create()
{
    TextureList result = (TextureList) { .NextTextureIndex = 0, .StartIndex = 0 };
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &result.MaxIndex);
    //GINFO("Max texture slots: %d\n", result.MaxIndex);
    return result;
}

force_inline void
texture_list_set_immutable(TextureList* list, u32 textureId)
{
    list->Textures[list->StartIndex] = textureId;
    ++list->StartIndex;
}

force_inline TextureList
texture_list_create_default(u32 defaultTextureId)
{
    TextureList result = (TextureList) { .NextTextureIndex = 0, .StartIndex = 0 };
    texture_list_set_immutable(&result, defaultTextureId);
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &result.MaxIndex);
    //GINFO("Max texture slots: %d\n", result.MaxIndex);
    return result;
}
force_inline i32
texture_list_is_full(TextureList* list)
{
    return list->NextTextureIndex >= list->MaxIndex;
}
force_inline i32
texture_list_contains(TextureList* list, u32 textureID)
{
    i32 i;
    for (i = list->StartIndex; i < list->NextTextureIndex; ++i)
    {
	if (list->Textures[i] == textureID)
	{
	    return i;
	}
    }

    return -1;
}
force_inline void
texture_list_add(TextureList* list, u32 textureId, i32 textureIndex)
{
    list->Textures[textureIndex] = textureId;
    ++list->NextTextureIndex;
}
force_inline i32
texture_list_add_to_begining(TextureList* list, u32 textureId)
{
    i32 textureIndex = list->NextTextureIndex;
    texture_list_add(list, textureId, list->NextTextureIndex);
    return textureIndex;
}
force_inline void
texture_list_bind(TextureList* list)
{
    i32 i;
    for (i = 0; i < list->NextTextureIndex; ++i)
    {
	//GINFO("Bound texture %d on slot %d\n", list->Textures[i], i);
	texture2d_bind_index(list->Textures[i], i);
    }
}
force_inline void
texture_list_unbind(TextureList* list)
{
    i32 i;
    for (i = 0; i < list->NextTextureIndex; i++)
    {
	texture2d_unbind_index(list->Textures[i], i);
    }
}
force_inline i32
texture_list_submit_texture_or_flush(TextureList* list, u32 textureId)
{
    if (textureId == 0)
    {
	return 0;
    }

    i32 index = texture_list_contains(list, textureId);
    if (index != -1)
    {
	return index;
    }

    if (texture_list_is_full(list))
    {
	vassert_break();
	return -1;
    }

    i32 textureIndex = list->NextTextureIndex;
    texture_list_add(list, textureId, textureIndex);
    return textureIndex;
}



/* Base Renderer */
void
renderer_set_viewport(u32 width, u32 height)
{
    glViewport(0, 0, width, height);
}

void
renderer_clear(v4 color)
{
    glClearColor(color.R, color.G, color.B, color.A);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

typedef enum RendererFlagType
{
    RendererFlagType_None          = 0,
    RendererFlagType_WireframeMode = 2 << 0,
    RendererFlagType_FillMode      = 2 << 1,
    RendererFlagType_FaceCulling   = 2 << 2,
    RendererFlagType_Blending      = 2 << 3,
    RendererFlagType_DepthTesting  = 2 << 4,
    RendererFlagType_Multisample   = 2 << 5
} RendererFlagType;
RendererFlagType CurrentFlagType[10];

void
renderer_flag_type_enable(RendererType rendererType)
{
    RendererFlagType flagType = CurrentFlagType[rendererType];
    if (flagType & RendererFlagType_WireframeMode)
    {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    }
    if (flagType & RendererFlagType_FillMode)
    {
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
    if (flagType & RendererFlagType_FaceCulling)
    {
	glEnable(GL_CULL_FACE);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glFrontFace(GL_CW); //Порядок обхода граней ClockWise
    }
    if (flagType & RendererFlagType_Blending)
    {
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    }
    if (flagType & RendererFlagType_DepthTesting)
    {
	glEnable(GL_DEPTH_TEST);
    }
    if (flagType & RendererFlagType_Multisample)
    {
	glEnable(GL_MULTISAMPLE);
    }
}
void
renderer_flag_type_disable(RendererType rendererType)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    RendererFlagType flagType = CurrentFlagType[rendererType];
    if (flagType & RendererFlagType_FaceCulling)
    {
	glDisable(GL_CULL_FACE);
    }
    else if (flagType & RendererFlagType_Blending)
    {
	glDisable(GL_BLEND);
    }
    else if (flagType & RendererFlagType_DepthTesting)
    {
	glDisable(GL_DEPTH_TEST);
    }
    else if (flagType & RendererFlagType_Multisample)
    {
	glDisable(GL_MULTISAMPLE);
    }
}

void
renderer_enable_wireframe_mode(RendererType type)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    CurrentFlagType[type] |=  RendererFlagType_WireframeMode;
    CurrentFlagType[type] &= ~RendererFlagType_FillMode     ;
}

void
renderer_enable_fill_mode(RendererType type)
{
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    CurrentFlagType[type] |=  RendererFlagType_FillMode     ;
    CurrentFlagType[type] &= ~RendererFlagType_WireframeMode;
}

void
renderer_enable_face_culling(RendererType type)
{
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW); //Порядок обхода граней ClockWise

    CurrentFlagType[type] |= RendererFlagType_FaceCulling;
}

void
renderer_enable_blending(RendererType type)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    CurrentFlagType[type] |= RendererFlagType_Blending;
}

void
renderer_enable_depth_testing(RendererType type)
{
    glEnable(GL_DEPTH_TEST);

    CurrentFlagType[type] |= RendererFlagType_DepthTesting;
}

void
renderer_disable_face_culling(RendererType type)
{
    glDisable(GL_CULL_FACE);

    CurrentFlagType[type] &= ~RendererFlagType_FaceCulling;
}

void
renderer_disable_depth_testing(RendererType type)
{
    glDisable(GL_DEPTH_TEST);

    CurrentFlagType[type] &= ~RendererFlagType_DepthTesting;
}

void
renderer_disable_blending(RendererType type)
{
    glDisable(GL_BLEND);

    CurrentFlagType[type] &= ~RendererFlagType_Blending;
}

/*
	 Framebuffer Rendering
*/
typedef struct RendererData
{
    VertexArray Vao;
    Shader Shader;
} RendererData;

static RendererData CurrentRendererData;

void
renderer_init()
{
    VertexBuffer vertexBuffer = vertex_buffer_create_allocated(sizeof(v2));

    DataType types[] = { Float2, Float2 };
    BufferLayout* layout = buffer_layout_create(types, 2);
    vertex_buffer_add_layout(&vertexBuffer, layout);
    u32 Indices[] = { 0, 1, 2, 2, 3, 0 };
    IndexBuffer  indexBuffer  = index_buffer_create(Indices, sizeof(Indices));
    VertexArray  vertexArray  = vertex_array_create(vertexBuffer, indexBuffer);
    Shader shader = shader_compile_safe(asset_shader("FramebufferRectangle.glsl"));

    CurrentRendererData.Vao = vertexArray;
    CurrentRendererData.Shader = shader;
}

void
renderer_flush(u32 screenTextureID)
{
    renderer_clear(v4_new(0.1f, 0.1f, 0.1f, 1));
    renderer_flag_type_enable(RendererType_Renderer);
    shader_bind(&CurrentRendererData.Shader);
    vertex_array_bind(&CurrentRendererData.Vao);

    texture2d_bind_index(screenTextureID, 0);
    static i32 TextureIndices[32] = { 0 };
    shader_set_int1(&CurrentRendererData.Shader, "u_Textures", 32, TextureIndices);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);

    shader_unbind(&CurrentRendererData.Shader);
    vertex_array_unbind(&CurrentRendererData.Vao);
    texture2d_unbind_index(screenTextureID, 0);

    renderer_flag_type_disable(RendererType_Renderer);
}


/*
	     2D Renderer
*/

/*

  NOTE(bies): Necessary global variables

*/

typedef struct RenderableRectangle
{
    v3 Position[4];
    v4 Color;
    v2 TextureCoordinates[4];
    f32 TextureID;
} RenderableRectangle;
typedef struct RectangleRenderData
{
    Shader Shader;
    CameraComponent* Camera;
    VertexArray VAO;
    TextureList List;
    RenderableRectangle* Data;
} RectangleRenderData;
#define RectangleRenderData(shader, camera, vao, textureList, data) ({(RectangleRenderData) { .Shader = shader, .Camera = camera, .VAO = vao, .List = textureList, .Data = data };})

typedef struct RenderableLine
{
    v3 Position[2];
    v4 Color;
} RenderableLine;
typedef struct LineRenderData
{
    Shader Shader;
    CameraComponent* Camera;
    VertexArray VAO;
    RenderableLine* Data;
} LineRenderData;
#define LineRenderData(shader, camera, vao, data)			\
    ({									\
	 \
    })

typedef struct RenderableCursorVertex
{
    v3 Position;
    v2 UV;
} RenderableCursorVertex;
typedef struct CursorRendererData
{
    Shader Shader;
    VertexArray VAO;
    RenderableCursorVertex Data[4];
} CursorRendererData;
#define CursorRendererData(shader, vao) ((CursorRendererData) { .Shader = shader, .VAO = vao })

RectangleRenderData RectangleData;
LineRenderData LineData;
CursorRendererData CursorData;
Texture2D DefaultTexture = {0};

//NOTE(bies): can be optimezed to v2 {x: 1.0f, y: 1.0f}, not for atlas map
static v2 DefaultTextureCoords[4] = {
    { 0.0f, 1.0f },
    { 1.0f, 1.0f },
    { 1.0f, 0.0f },
    { 0.0f, 0.0f }
};
static i32 TextureIndices[32] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31 };
static v4 DefaultQuadPositions[4] = {
    { -0.5f,  0.5f, 0.5f, 1.0f }, /* 0 1 */
    {  0.5f,  0.5f, 0.5f, 1.0f }, /* 1 1 */
    {  0.5f, -0.5f, 0.5f, 1.0f }, /* 1 0 */
    { -0.5f, -0.5f, 0.5f, 1.0f }  /* 0 0 */
};
static v4 DefaultLinePositions = { 0.5f, 0.5f, 0.5f, 1.0f };

#define DefaultMaxRectangles 100
#define DefaultMaxLines 1000
f32* RenderDataBuffer = NULL;
i32 RectangleVerticesSize = 0;
IndexBuffer Ibo;
i32 LinesVerticesSize = 0;

force_inline u32*
_create_indices_array(u32 count)
{
    u32 temp, *indices = memory_allocate(count * sizeof(u32));
    i32 i;
    for (i = 0, temp = 0; i < count; i += 6, temp += 4)
    {
	indices[i]     = 0 + temp;
	indices[i + 1] = 1 + temp;
	indices[i + 2] = 2 + temp;
	indices[i + 3] = 2 + temp;
	indices[i + 4] = 3 + temp;
	indices[i + 5] = 0 + temp;
    }
    return indices;
}

void
renderer2d_init_rectangle_pipeline(CameraComponent* camera, TextureList textureList)
{
    // Data Layout
    DataType types[] = { Float3, Float4, Float2, Float1 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 4);
    i32 rectangleVertexSize = buffer_layout_get_size(bufferLayout);
    vassert(rectangleVertexSize > 0 && "Buffer layout size error!!!");
    i32 verticesCount = DefaultMaxRectangles * (4 * rectangleVertexSize);
    i32 indicesCount = DefaultMaxRectangles * 6;

    RectangleVerticesSize = verticesCount * sizeof(f32);

    u32* indices = _create_indices_array(indicesCount);
    Shader shader = shader_compile_safe(asset_shader("RectangleShader.glsl"));
    VertexBuffer vbo = vertex_buffer_create_allocated(RectangleVerticesSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    Ibo = index_buffer_create(indices, indicesCount);
    VertexArray vao = vertex_array_create(vbo, Ibo);
    vertex_array_unbind(&vao);

    // NOTE(bies): Placing all needed data to RenderableRect instance, so we can
    // dynamically change pipeline as we want
    RenderDataBuffer = memory_allocate(RectangleVerticesSize);
    RenderableRectangle* data = NULL;
    array_reserve(data, DefaultMaxRectangles);
    array_header(data)->Count = 0;
    RectangleData = RectangleRenderData(shader, camera, vao, textureList, data);
}

void
renderer2d_init_line_pipeline(CameraComponent* camera)
{
    renderer2d_set_line_thickness(31.0f);

    // Data Layout
    DataType types[] = { Float3, Float4 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 2);
    i32 lineVertexSize = buffer_layout_get_size(bufferLayout);
    vassert(lineVertexSize > 0 && "Buffer layout size error!!!");
    i32 verticesCount = DefaultMaxLines * (2 * lineVertexSize);

    LinesVerticesSize = verticesCount * sizeof(f32);

    Shader shader = shader_compile_safe(asset_shader("LineShader.glsl"));
    VertexBuffer vbo = vertex_buffer_create_allocated(LinesVerticesSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    VertexArray vao = vertex_array_create(vbo, (IndexBuffer) {.ID = 0});
    vertex_array_unbind(&vao);

    RenderableLine* data = NULL;
    array_reserve(data, DefaultMaxLines);
    LineData = (LineRenderData) {
	.Shader = shader,
	.Camera = camera,
	.VAO = vao,
	.Data = data
    };
}

void
renderer2d_init_cursor_pipeline()
{
    DataType types[] = { Float4 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 1);

    const f32 v = 0.0015f;
    static f32 positions[] = {
	-v,  v, 0.0f, 1.0f, /* 0 1 */
	v,  v, 0.0f, 1.0f, /* 1 1 */
	v, -v, 0.0f, 1.0f, /* 1 0 */
	-v, -v, 0.0f, 1.0f  /* 0 0 */
    };
    Shader shader = shader_compile_safe(asset_shader("CursorShader.glsl"));
    VertexBuffer vbo = vertex_buffer_create(positions, sizeof(positions));
    vertex_buffer_add_layout(&vbo, bufferLayout);
    vertex_buffer_set_data(&CursorData.VAO.Vertex, (f32*)positions, sizeof(positions));

    u32* indices = NULL;
    array_push(indices, 0);
    array_push(indices, 1);
    array_push(indices, 2);
    array_push(indices, 2);
    array_push(indices, 3);
    array_push(indices, 0);
    IndexBuffer ibo = index_buffer_create(indices, array_count(indices));
    VertexArray vao = vertex_array_create(vbo, ibo);
    vertex_array_unbind(&vao);

    CursorData = CursorRendererData(shader, vao);
}

void
renderer2d_init(CameraComponent* camera)
{
    //default thing stuff
    Texture2DSettings settings = {
	.IsFlipOnLoad = 1,
	.FilterType = TextureFilterType_Linear
    };
    DefaultTexture = texture2d_create(asset_texture("default/white_texture.png"), settings);
    TextureList textureList = texture_list_create_default(DefaultTexture.ID);

    renderer2d_init_rectangle_pipeline(camera, textureList);
    renderer2d_init_line_pipeline(camera);
    renderer2d_init_cursor_pipeline();
}

void
renderer2d_set_camera(CameraComponent* camera)
{
    RectangleData.Camera = camera;
    LineData.Camera = camera;
}

void
renderable_rectangle_to_array(RenderableRectangle* renderableRectangles, f32* destination)
{
    i32 i, ind = -1, count = array_count(renderableRectangles);
    for (i = 0; i < count; ++i)
    {
	RenderableRectangle rect = renderableRectangles[i];
	for (i32 c = 0; c < 4; ++c)
	{
	    destination[++ind] = rect.Position[c].X;
	    destination[++ind] = rect.Position[c].Y;
	    destination[++ind] = rect.Position[c].Z;
	    destination[++ind] = rect.Color.R;
	    destination[++ind] = rect.Color.G;
	    destination[++ind] = rect.Color.B;
	    destination[++ind] = rect.Color.A;
	    destination[++ind] = rect.TextureCoordinates[c].X;
	    destination[++ind] = rect.TextureCoordinates[c].Y;
	    destination[++ind] = (f32) (rect.TextureID + 0.5f);
	}
    }
}

void
renderer2d_submit(m4 transform, v4 color, u32 textureAsID)
{
    /*
      NOTE(typedef): Нужно придумать, чем заменить texture_list или как его переписать
      в текущем виде это пиздец.
     */
    i32 textureID = texture_list_submit_texture_or_flush(&RectangleData.List, textureAsID);
    if (textureID == -1)
    {
	vassert_break();
	GINFO("-1\n");
	renderer2d_flush();
	//NOTE(bies): never free(texture);
	RectangleData.List = texture_list_create_default(DefaultTexture.ID);
	texture_list_add_to_begining(&RectangleData.List, textureAsID);
    }

    v3 positions[4];
    positions[0] = v3_v4(m4_mul_v4(transform, DefaultQuadPositions[0]));
    positions[1] = v3_v4(m4_mul_v4(transform, DefaultQuadPositions[1]));
    positions[2] = v3_v4(m4_mul_v4(transform, DefaultQuadPositions[2]));
    positions[3] = v3_v4(m4_mul_v4(transform, DefaultQuadPositions[3]));

    RenderableRectangle rect;
    rect.Position[0] = positions[0];
    rect.Position[1] = positions[1];
    rect.Position[2] = positions[2];
    rect.Position[3] = positions[3];
    rect.Color = color;
    rect.TextureCoordinates[0] = v2_new(0, 1);
    rect.TextureCoordinates[1] = v2_new(1, 1);
    rect.TextureCoordinates[2] = v2_new(1, 0);
    rect.TextureCoordinates[3] = v2_new(0, 0);
    rect.TextureID = (f32) textureID + 0.5f;

    array_insert_w_func(RectangleData.Data,
		 {
		     renderer2d_flush_rectangles();
		 }, rect);
}

void
renderer2d_submit_rectangle(v3 position, v2 size, v4 color, u32 textureAsID)
{
    v3 scaleVec = v3_new(size.Width, size.Height, 1.0f);
    v3 rotation = v3_new(0.0f, 0.0f, 0.0f);
    m4 transform = m4_transform(position, rotation, scaleVec);
    renderer2d_submit(transform, color, textureAsID);
}

void
renderer2d_submit_rectangle_color(v3 position, v2 size, v4 color)
{
    renderer2d_submit_rectangle(position, size, color, 0);
}

void
renderer2d_submit_empty_rectangle(v3 position, v2 size, v4 color)
{
    const f32 lineSize = .03f;
    f32 hwidth  = size.Width / 2;
    f32 hheight = size.Height / 2;

    v3 p0 = v3_new(position.X, position.Y + hheight, position.Z);
    v2 horLine = v2_new(size.Width, lineSize);
    renderer2d_submit_rectangle(p0, horLine, color, 0);

    v3 vx = v3_new(position.X - hwidth, position.Y, position.Z);
    v2 vertLine = v2_new(lineSize / 3, size.Height);
    renderer2d_submit_rectangle(vx, vertLine, color, 0);

    v3 p3 = v3_new(p0.X, position.Y - (size.Height / 2), position.Z);
    renderer2d_submit_rectangle(p3, horLine, color, 0);

    v3 p4 = v3_new(position.X + hwidth, position.Y, position.Z);
    renderer2d_submit_rectangle(p4, vertLine, color, 0);
}

void
renderer2d_flush_rectangles()
{
    i32 rectanglesCount = array_count(RectangleData.Data);
    if (rectanglesCount <= 0)
	return;

    shader_bind(&RectangleData.Shader);
    vertex_array_bind(&RectangleData.VAO);
    texture_list_bind(&RectangleData.List);

    shader_set_m4(&RectangleData.Shader, "u_ViewProjection", 1, 0, &RectangleData.Camera->Base.ViewProjection.M[0][0]);
    shader_set_int1(&RectangleData.Shader, "u_Textures", RectangleData.List.NextTextureIndex, TextureIndices);

    renderable_rectangle_to_array(RectangleData.Data, RenderDataBuffer);
    vertex_buffer_set_data(&RectangleData.VAO.Vertex, RenderDataBuffer, RectangleVerticesSize);

    //GINFO("Count: %d %d\n", rectanglesCount, rectanglesCount*6);
    glDrawElements(GL_TRIANGLES, 6*rectanglesCount, GL_UNSIGNED_INT, NULL);

    array_clear(RectangleData.Data);
    shader_unbind();
    vertex_array_unbind(&RectangleData.VAO);
    texture_list_unbind(&RectangleData.List);
}

void
renderable_lines_to_array(RenderableLine* renderableLines, f32* destination)
{
    i32 i, ind = -1, count = array_count(renderableLines);
    for (i = 0; i < count; ++i)
    {
	RenderableLine line = renderableLines[i];
	for (i32 c = 0; c < 2; ++c)
	{
	    destination[++ind] = line.Position[c].X;
	    destination[++ind] = line.Position[c].Y;
	    destination[++ind] = line.Position[c].Z;
	    destination[++ind] = line.Color.R;
	    destination[++ind] = line.Color.G;
	    destination[++ind] = line.Color.B;
	    destination[++ind] = line.Color.A;
	}
    }
}

f32 LineThickness = 0.1f;

void
renderer2d_submit_line(v3 start, v3 end, v4 color)
{
    v3 scaleVec = v3_new(1, 1, 1);
    m4 transformStart = m4_transform_wo_rotation(start, scaleVec);
    m4 transformEnd   = m4_transform_wo_rotation(end, scaleVec);

    v3 positions[2];
    positions[0] = v3_v4(m4_mul_v4(transformStart, DefaultLinePositions));
    positions[1] = v3_v4(m4_mul_v4(transformEnd, DefaultLinePositions));

    RenderableLine line;
    line.Position[0] = positions[0];
    line.Position[1] = positions[1];
    line.Color = color;

    array_insert_w_func(LineData.Data,
		 {
		     renderer2d_flush_lines();
		 }, line);
}

void
renderer2d_set_line_thickness(f32 thickness)
{
    LineThickness = thickness;
    glLineWidth(LineThickness);
}

void
renderer2d_flush_lines()
{
    i32 linesCount = array_count(LineData.Data);
    if (linesCount <= 0)
	return;

    glEnable(GL_LINE_SMOOTH);

    shader_bind(&LineData.Shader);
    vertex_array_bind(&LineData.VAO);

    shader_set_m4(&LineData.Shader, "u_ViewProjection", 1, 0, &LineData.Camera->Base.ViewProjection.M[0][0]);

    renderable_lines_to_array(LineData.Data, RenderDataBuffer);
    vertex_buffer_set_data(&LineData.VAO.Vertex, RenderDataBuffer, LinesVerticesSize);

    glDrawArrays(GL_LINES, 0, 2*linesCount);

    array_clear(LineData.Data);
    shader_unbind();
    vertex_array_unbind(&LineData.VAO);

    glDisable(GL_LINE_SMOOTH);
}

void
renderer2d_flush()
{
    renderer_flag_type_enable(RendererType_Renderer2D);
    renderer2d_flush_lines();
    renderer2d_flush_rectangles();
    renderer_flag_type_disable(RendererType_Renderer2D);
}

void
renderer2d_draw_cursor()
{
    shader_bind(&CursorData.Shader);
    vertex_array_bind(&CursorData.VAO);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, NULL);

    shader_unbind(CursorData.Shader);
    vertex_array_unbind(&CursorData.VAO);
}

/* 3D Renderer */

#define CubeVerticeCount 24
#define DefaultCubeIndicesCount 36
#define DefaultMaxCubes 1000000

static i64 CubeVerticesSize = 0;
// 1 073 741 824 bytes = 1 GB
// Chunk 32x32x32 = 32 768 cubes
// 1 GB = 2340 Chunks of 14 per cube
// 1 GB = 1024 Chunks of 22 per cube
// 12 - positions, 8 - textCoord 4 - textureId              = 24 bytes per cube
// 12 - positions, 8 - textCoord 4 - textureId 8 - entityId = 32 bytes per cube
// Нам не нужны textCoord внутри нашего Chunk, достаточно Id на массив кубических
// координат для кубов, textureId также получим из этого массива, думаю, что
// 2^16 текстур нам хватит, так что
// 12-positions,2-textureArrayIndex = 14 bytes per cube (76 695 844 cubes in 1 GB)
// 12 - positions, 2 - textureArrayIndex, 8 - entityId = 22 bytes per cube
//

    // Front - CW    0      1     3     3     1     2
    // Back - CCW    4      7     5     5     7     6
    // LEFT - CCW    8     11     9     9    11    10
    // RIGHT - CW    12    13    15    15    13    14
    // BOTTOM - CCW  16    19    17    17    19    18
    // UP    - CW    20    21    23    23    21    22


static const f32 vp = 0.5f;
static const f32 dv = 2 * vp;
static v4 DefaultCubePositions[24] = {
    // Front
    //0              1              2                3
    {-vp,vp,vp,dv}, {vp,vp,vp,dv}, {vp,-vp,vp,dv}, {-vp,-vp,vp,dv},

    // Back
    //4               5                6                 7
    {-vp,vp,-vp,dv}, {vp, vp,-vp,dv}, {vp,-vp,-vp,dv}, {-vp,-vp,-vp,dv},

    // Left
    //8               9                10                11
    {-vp,vp,vp,dv}, {-vp,vp,-vp,dv}, {-vp,-vp,-vp,dv}, {-vp,-vp,vp,dv},

    // Right
    //12             13              14               15
    { vp,vp,vp,dv}, {vp,vp,-vp,dv}, {vp,-vp,-vp,dv}, {vp,-vp,vp,dv},

    // Bottom
    //16               17               18               19
    {-vp,-vp,-vp,dv}, {vp,-vp,-vp,dv}, {vp,-vp,vp,dv}, {-vp,-vp,vp,dv},

    // Up
    //20              21              22              23
    {-vp,vp,-vp,dv}, {vp,vp,-vp,dv}, {vp,vp,vp,dv}, {-vp,vp,vp,dv}
};
static v3 DefaultCubeNormals[24] = {
    // Front
    //0         1          2          3
    {0, 0, 1}, {0, 0, 1}, {0, 0, 1}, {0, 0, 1},

    // Back
    //4          5           6           7
    {0, 0, -1}, {0, 0, -1}, {0, 0, -1}, {0, 0, -1},

    // Left
    //8           9           10          11
    {-1, 0, 0}, {-1, 0, 0}, {-1, 0, 0}, {-1, 0, 0},

    // Right
    //12        13         14         15
    {1, 0, 0}, {1, 0, 0}, {1, 0, 0}, {1, 0, 0},

    // Bottom
    //16         17          18          19
    {0, -1, 0}, {0, -1, 0}, {0, -1, 0}, {0, -1, 0},

    // Up
    //20        21         22         23
    {0, 1, 0}, {0, 1, 0}, {0, 1, 0}, {0, 1, 0}
};

static v2 CubeUV[24] = {
    // Front
    //0         1         2         3
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Back
    //4         5         6         7
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Left
    //8         9         10        11
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Right
    //12        13        14        15
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Bottom
    //16        17        18        19
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 },
    // Up
    //20        21        22        23
    { 0, 1 }, { 1, 1 }, { 1, 0 }, { 0, 0 }
};

static f32* Render3DDataBuffer;

typedef struct RenderableCube
{
    v3 Position[CubeVerticeCount];  // 288
    v3 Normals[CubeVerticeCount];   // 288
    v2 UV[CubeVerticeCount];        // 192
    i32 TextureID[CubeVerticeCount];// 96
} RenderableCube;

typedef struct RenderableCubeLight
{
    v3 Position[CubeVerticeCount];  // 288
    v4 LightColor;
} RenderableCubeLight;

typedef struct CubeRenderer
{
    Shader Shader;
    RuntimeCamera* Camera;
    VertexArray VAO;
    i32 DataSet;
    i32 DataSubmitIndex;
    RenderableCube* Data;
    TextureList List;
    u32 NextID;
} CubeRenderer;

static CubeRenderer CubeRendererData;

#define CubeRenderer(shader, camera, vao, data, list, nextId)		\
    ({									\
	(CubeRenderer) { .Shader = shader, .Camera = camera, .VAO = vao, .Data = data, .List = list, .NextID = nextId }; \
    })

//NOTE(bies): вроде тут все верно
force_inline u32*
_create_cube_indices_array(u32 count)
{
    i32 i;
    u32 temp, *in = memory_allocate(count * sizeof(u32));
    for (i = 0, temp = 0; i < count; i += 36, temp += 24)
    {
	// Front - CW
	in[i     ] = 0 + temp;
	in[i +  1] = 1 + temp;
	in[i +  2] = 3 + temp;
	in[i +  3] = 3 + temp;
	in[i +  4] = 1 + temp;
	in[i +  5] = 2 + temp;

	// Back - CCW
	in[i +  6] = 4 + temp;
	in[i +  7] = 7 + temp;
	in[i +  8] = 5 + temp;
	in[i +  9] = 5 + temp;
	in[i + 10] = 7 + temp;
	in[i + 11] = 6 + temp;

	// LEFT - CCW
	in[i + 12] = 8  + temp;
	in[i + 13] = 11 + temp;
	in[i + 14] = 9  + temp;
	in[i + 15] = 9  + temp;
	in[i + 16] = 11 + temp;
	in[i + 17] = 10 + temp;

	// RIGHT - CW
	in[i + 18] = 12 + temp;
	in[i + 19] = 13 + temp;
	in[i + 20] = 15 + temp;
	in[i + 21] = 15 + temp;
	in[i + 22] = 13 + temp;
	in[i + 23] = 14 + temp;

	// BOTTOM - CCW
	in[i + 24] = 16 + temp;
	in[i + 25] = 19 + temp;
	in[i + 26] = 17 + temp;
	in[i + 27] = 17 + temp;
	in[i + 28] = 19 + temp;
	in[i + 29] = 18 + temp;

	// UP    - CW
	in[i + 30] = 20 + temp;
	in[i + 31] = 21 + temp;
	in[i + 32] = 23 + temp;
	in[i + 33] = 23 + temp;
	in[i + 34] = 21 + temp;
	in[i + 35] = 22 + temp;
    }
    return in;
}

void
renderer3d_init_cubes(RuntimeCamera* camera)
{
    // Data Layout
    DataType types[] = { Float3, Float3, Float2, Float1 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 4);
    i32 layoutSize = buffer_layout_get_size(bufferLayout);
    {
	vassert(layoutSize > 0 && "[Cube] Buffer layout size error!!!");
	GINFO("layoutSize: %d\n", layoutSize);
    }
    i64 verticesCount = DefaultMaxCubes * (CubeVerticeCount * layoutSize);
    CubeVerticesSize = verticesCount * sizeof(f32);
    GERROR("CubeVerticesSize: %lld\n", CubeVerticesSize);
    Render3DDataBuffer = (f32*) memory_allocate(CubeVerticesSize);
    memset(Render3DDataBuffer, '\0', CubeVerticesSize);

    //Cubes data
    const i32 localIndicesCount  = DefaultMaxCubes * DefaultCubeIndicesCount;
    u32* indices = _create_cube_indices_array(localIndicesCount);
    Shader shader = shader_compile_safe(asset_shader("CubeShaderLight.glsl"));
    VertexBuffer vbo = vertex_buffer_create_allocated(CubeVerticesSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    IndexBuffer ibo = index_buffer_create(indices, localIndicesCount);
    VertexArray vao = vertex_array_create(vbo, ibo);
    vertex_array_unbind(&vao);

    RenderableCube* data = NULL;
    array_reserve(data, DefaultMaxCubes);
    array_header(data)->Count = 0;

    CubeRendererData = CubeRenderer(shader, camera, vao, data, texture_list_create(), 0);
    CubeRendererData.DataSet = 0;
}

void
renderer3d_init(RuntimeCamera* camera)
{
    renderer3d_init_cubes(camera);
}

void
renderer3d_submit_cube_light(m4 transform, v4 lightColor)
{
}

i32
_renderer3d_submit_texture(Texture2D texture)
{
    i32 textureID = texture_list_submit_texture_or_flush(&CubeRendererData.List, texture.ID);
    return textureID;
}

void
renderer3d_submit_cube(m4 transform,
		       Texture2D front, Texture2D back,
		       Texture2D left, Texture2D right,
		       Texture2D bottom, Texture2D up)
{
    i32 frontID = _renderer3d_submit_texture(front);
    i32 backID = _renderer3d_submit_texture(back);
    i32 leftID = _renderer3d_submit_texture(left);
    i32 rightID = _renderer3d_submit_texture(right);
    i32 bottomID = _renderer3d_submit_texture(bottom);
    i32 upID = _renderer3d_submit_texture(up);

    RenderableCube cube;
    u32 ids[6] = { frontID, backID, leftID, rightID, bottomID, upID };
    m3 inversedAndTranposedModel;
    for (i32 i = 0; i < CubeVerticeCount; ++i)
    {
	//3
	cube.Position[i]  = v3_v4(m4_mul_v4(transform, DefaultCubePositions[i]));
	//3
	inversedAndTranposedModel = m3_m4(m4_t(m4_inverse(transform)));
	cube.Normals[i]   = m3_mul_v3(inversedAndTranposedModel, DefaultCubeNormals[i]);
	//2
	cube.UV[i]        = CubeUV[i];
	//1
	cube.TextureID[i] = ids[i / 4]; //try %
    }

    array_insert_w_func(CubeRendererData.Data,
		 {
		     renderer3d_flush_cubes();
		 }, cube);
}

void
renderable_cubes_to_array(RenderableCube* cubes, f32* destination)
{
    i32 i, ind = 0, count = array_count(cubes);
    for (i = 0; i < count; ++i)
    {
	RenderableCube cube = cubes[i];

	i32 textureIDShouldBe;
	for (i32 c = 0; c < CubeVerticeCount; ++c)
	{
	    destination[ind + 0] = cube.Position[c].X;
	    destination[ind + 1] = cube.Position[c].Y;
	    destination[ind + 2] = cube.Position[c].Z;
	    destination[ind + 3] = cube.Normals[c].X;
	    destination[ind + 4] = cube.Normals[c].Y;
	    destination[ind + 5] = cube.Normals[c].Z;
	    destination[ind + 6] = cube.UV[c].X;
	    destination[ind + 7] = cube.UV[c].Y;
	    destination[ind + 8] = (cube.TextureID[c] + 0.5f);

	    ind += 9;
	}
    }
}

void
renderer3d_submit_cube_position(v3 position,
				Texture2D front, Texture2D back,
				Texture2D left, Texture2D right,
				Texture2D bottom, Texture2D up)
{
    m4 transform = m4_transform(position, v3_new(0, 0, 0), v3_new(1, 1, 1));
    renderer3d_submit_cube(transform, front, back, left, right, bottom, up);
}

v4 LightPosition = { -0.2f, 0.0f, -0.3f, 1.0f };
v3 LightColor = {1, 1, 1};

v4*
temp_renderer3d_get_light_position()
{
    return &LightPosition;
}

v3*
temp_renderer3d_get_light_color()
{
    return &LightColor;
}

typedef struct Material
{
    v3 Ambient;
    v3 Diffuse;
    v3 Specular;
    f32 Shininess;
} Material;

#define Material_GetShininess(shininess) shininess * 128

static Material GoldMaterial = {
    .Ambient   = { 0.24725, 0.1995, 0.0745 },
    .Diffuse   = { 0.75164, 0.60648, 0.22648 },
    .Specular  = { 0.628281, 0.555802, 0.366065 },
    .Shininess = Material_GetShininess(0.4)
};
static Material DefaultMaterial = {
    .Ambient   = { 0.1, 0.1, 0.1 },
    .Diffuse   = { 0.5, 0.5, 0.5 },
    .Specular  = { 0.1, 0.1, 0.1 },
    .Shininess = Material_GetShininess(0.25f)
};

void
renderer3d_set_geometry()
{
    if (CubeRendererData.DataSet != 0)
    {
	GWARNING("Data is set!\n");
	return;
    }

    i32 count = array_count(CubeRendererData.Data);
    renderable_cubes_to_array(CubeRendererData.Data, Render3DDataBuffer);
    vertex_buffer_set_data(&CubeRendererData.VAO.Vertex, Render3DDataBuffer, count*sizeof(RenderableCube));

    CubeRendererData.DataSet = 1;
}

const f32 TimestepConst = 1.0f/120;
void
renderer3d_flush_cubes()
{
    renderer_flag_type_enable(RendererType_Renderer3D);

    shader_bind(&CubeRendererData.Shader);
    vertex_array_bind(&CubeRendererData.VAO);
    texture_list_bind(&CubeRendererData.List);

    shader_set_m4(&CubeRendererData.Shader, "u_ViewProjection", 1, 0, &CubeRendererData.Camera->ViewProjection.M[0][0]);
    shader_set_v3(&CubeRendererData.Shader, "u_ViewPosition", CubeRendererData.Camera->Position);

    // light
    shader_set_v3(&CubeRendererData.Shader, "u_Light.Color", LightColor);
    //LightPosition.W = 0;
    shader_set_v3(&CubeRendererData.Shader, "u_Light.Position", v3_v4(LightPosition));
    shader_set_v3(&CubeRendererData.Shader, "u_Light.Direction", v3_new(0, 0, -1));
    shader_set_f32(&CubeRendererData.Shader, "u_Light.CutOff", cos(rad(20)));
    shader_set_f32(&CubeRendererData.Shader, "u_Light.OuterCutOff", cos(rad(25)));

    //this is gold
    shader_set_v3(&CubeRendererData.Shader, "u_Material.Ambient", DefaultMaterial.Ambient);
    shader_set_v3(&CubeRendererData.Shader, "u_Material.Diffuse", DefaultMaterial.Diffuse);
    shader_set_v3(&CubeRendererData.Shader, "u_Material.Specular", DefaultMaterial.Specular);
    shader_set_float(&CubeRendererData.Shader, "u_Material.Shininess", DefaultMaterial.Shininess);

    i32 count = array_count(CubeRendererData.Data);
    glDrawElements(GL_TRIANGLES,
		   count * DefaultCubeIndicesCount,
		   GL_UNSIGNED_INT, NULL);

    shader_unbind(CubeRendererData.Shader);
    vertex_array_unbind(CubeRendererData.VAO);
    texture_list_unbind(&CubeRendererData.List);

    renderer_flag_type_disable(RendererType_Renderer3D);
}

/* Flush functions */
void
renderer3d_flush()
{
    renderer3d_flush_cubes();
}

typedef struct CubemapRenderer
{
    Shader Shader;
    CameraComponent* Camera;
    VertexArray VAO;
} CubemapRenderer;

CubemapRenderer CubemapRendererData;

/* Cubemap */
void
renderer3d_cubemap_init()
{
    f32 skyboxVertices[3 * 36] = {
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f
    };

    DataType types[] = { Float3 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 1);
    vassert(buffer_layout_get_size(bufferLayout) > 0 && "Buffer layout size error!!!");
    VertexBuffer vbo = vertex_buffer_create(skyboxVertices, 3*36*sizeof(f32));
    vertex_buffer_add_layout(&vbo, bufferLayout);

    CubemapRendererData.Shader  = shader_compile_safe(
	asset_shader("CubemapShader.glsl"));
    CubemapRendererData.VAO = vertex_array_create_wo_index(vbo);

    vertex_array_unbind(&CubemapRendererData.VAO);

}

void
renderer3d_cubemap_set_camera(CameraComponent* camera)
{
    CubemapRendererData.Camera  = camera;
}

void
renderer3d_cubemap_draw(u32 cubemapId)
{
    glDepthMask(GL_FALSE);
    shader_bind(&CubemapRendererData.Shader);
    m4 view = m4_m3_array(CubemapRendererData.Camera->Base.View.M);
    m4 viewProjection = m4_mul(CubemapRendererData.Camera->Base.Projection, view);
    shader_set_m4(&CubemapRendererData.Shader, "u_ViewProjection", 1, 0, &viewProjection.M[0][0]);
    vertex_array_bind_wo_index(&CubemapRendererData.VAO);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapId);

    glDrawArrays(GL_TRIANGLES, 0, 36);

    glDepthMask(GL_TRUE);
    glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
    vertex_array_unbind_wo_index(&CubemapRendererData.VAO);
}
