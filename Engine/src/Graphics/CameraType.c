#include "CameraType.h"

const char*
camera_type_to_string(CameraType type)
{
    switch (type)
    {
    case CameraType_OrthographicSpectator:
	return "CameraType_OrthographicSpectator";
    case CameraType_OrthographicArcball:
	return "CameraType_OrthographicArcball";
    case CameraType_PerspectiveSpectator:
	return "CameraType_PerspectiveSpectator";
    case CameraType_PerspectiveArcball:
	return "CameraType_PerspectiveArcball";
    }

    vassert(0 && "Wrong CameraType passed into to_string function!");
    return "NULL";
}

i32
camera_type_is_valid(CameraType cameraType)
{
    return cameraType > CameraType_BaseOnly && cameraType < CameraType_End;
}
