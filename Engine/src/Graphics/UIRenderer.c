#if 0
#include "UIRenderer.h"

#include <glad/glad.h>

#include <Graphics/OpenGLBase.h>
#include <Graphics/RuntimeCamera.h>
#include <Utils/Types.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Math/SimpleMath.h>
#include <Math/SimpleMathIO.h>
#include <EntitySystem/Components/Base/CameraComponent.h>

#define Default2DFlags(code)					\
    {								\
	glDepthFunc(GL_ALWAYS);					\
	glEnable(GL_DEPTH_TEST);				\
	glEnable(GL_BLEND);					\
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	\
								\
	code							\
								\
	glDisable(GL_BLEND);					\
	glDisable(GL_DEPTH_TEST);				\
    }

typedef enum SimpleFontOffsetsType
{
    SimpleFontOffsetsType_Less45 = 0,
    SimpleFontOffsetsType_Less65,
    SimpleFontOffsetsType_Less75,
    SimpleFontOffsetsType_Count
} SimpleFontOffsetsType;

typedef struct RenderableFontVertex
{
    v3 Position;
    v4 Color;
    v2 UV;
} RenderableFontVertex;
typedef struct RenderableFont
{
    size_t MaxGlyphsCount;
    size_t ItemSize;
    CameraComponent* Camera;
    Font Font;
    f32 Offsets[SimpleFontOffsetsType_Count];
    RenderableFontVertex* VertexBuffer;
    IndexBuffer Ibo;
    VertexArray Vao;
    Shader Shader;
} RenderableFont;

static Font MainFontInfo;
static RenderableFont FontData;
static IndexBuffer Ibo;

force_inline u32*
_create_indices_array(u32 count)
{
    u32 temp, *indices = memory_allocate(count * sizeof(u32));
    i32 i;
    for (i = 0, temp = 0; i < count; i += 6, temp += 4)
    {
	indices[i]     = 0 + temp;
	indices[i + 1] = 1 + temp;
	indices[i + 2] = 2 + temp;
	indices[i + 3] = 2 + temp;
	indices[i + 4] = 3 + temp;
	indices[i + 5] = 0 + temp;
    }
    return indices;
}

static void
ui_renderer_font_init(RendererSettings settings)
{
    f32 offsets[3] = {
	[SimpleFontOffsetsType_Less45] = 0.65f,
	[SimpleFontOffsetsType_Less65] = 0.50f,
	[SimpleFontOffsetsType_Less75] = 0.45f
    };
    memcpy(FontData.Offsets, offsets, sizeof(offsets));

    FontData.MaxGlyphsCount = settings.MaxGlyphsCount;
    FontData.Camera = settings.Camera;
    FontData.Font = font_cirilic_create((FontInfoSettings)
					{
					    .Path = asset_font("NotoSans.ttf"),
					    .FontSize = 32,
					    .IsOpenGLEnabled = 1
					});

    /* Renderer Part */
    DataType types[] = { Float3, Float4, Float2 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 3);
    size_t layoutSize = buffer_layout_get_size(bufferLayout);
    vassert(layoutSize > 0 && "Buffer layout size error!!!");

    FontData.ItemSize = 4 * layoutSize;
    FontData.VertexBuffer = NULL;
    size_t totalSize = FontData.MaxGlyphsCount * FontData.ItemSize;

    //FontData.RawBuffer = memory_allocate(totalSize);

    i32 indicesCount = FontData.MaxGlyphsCount * 6;
    u32* indices = _create_indices_array(indicesCount);
    Shader shader = shader_compile_safe(asset_shader("FontShader.glsl"));
    VertexBuffer vbo = vertex_buffer_create_allocated(totalSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    IndexBuffer ibo = index_buffer_create(indices, indicesCount);
    VertexArray vao = vertex_array_create(vbo, ibo);
    vertex_array_unbind(&vao);

    FontData.Ibo    = ibo;
    FontData.Vao    = vao;
    FontData.Shader = shader;

    memory_free(indices);
}

typedef struct RenderableGeometryVertex
{
    v2 Position;
    v4 Color;
} RenderableGeometryVertex;
typedef struct RenderableGeometry
{
    size_t MaxRectsCount;
    size_t ItemSize;
    CameraComponent* Camera;
    RenderableGeometryVertex* VertexBuffer;
    IndexBuffer Ibo;
    VertexArray Vao;
    Shader Shader;
} RenderableGeometry;

static RenderableGeometry GeometryData;

void
ui_renderer_geometry_init(RendererSettings settings)
{
    GeometryData.MaxRectsCount = settings.MaxRectsCount;
    GeometryData.Camera = settings.Camera;

    /* Renderer Part */
    DataType types[] = { Float2, Float4 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 2);

    GeometryData.ItemSize = 4 * sizeof(RenderableGeometryVertex);
    GeometryData.VertexBuffer = NULL;
    size_t totalSize = GeometryData.MaxRectsCount * GeometryData.ItemSize;

    //GeometryData.RawBuffer = memory_allocate(totalSize);

    i32 indicesCount = GeometryData.MaxRectsCount * 6;
    u32* indices = _create_indices_array(indicesCount);
    Shader shader = shader_compile_safe(asset_shader("GeometryShader.glsl"));
    VertexBuffer vbo = vertex_buffer_create_allocated(totalSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    IndexBuffer ibo = index_buffer_create(indices, indicesCount);
    VertexArray vao = vertex_array_create(vbo, ibo);
    vertex_array_unbind(&vao);

    GeometryData.Ibo    = ibo;
    GeometryData.Vao    = vao;
    GeometryData.Shader = shader;

    memory_free(indices);
}

typedef struct RenderableLineVertex
{
    v2 Position;
    v4 Color;
} RenderableLineVertex;
typedef struct RenderableLine
{
    size_t MaxLinesCount;
    size_t ItemSize;
    CameraComponent* Camera;
    RenderableLineVertex* VertexBuffer;
    IndexBuffer Ibo;
    VertexArray Vao;
    Shader Shader;
} RenderableLine;

static RenderableLine LineData;

void
ui_renderer_line_init(RendererSettings settings)
{
    LineData.MaxLinesCount = settings.MaxLinesCount;
    LineData.Camera = settings.Camera;

    /* Renderer Part */
    DataType types[] = { Float2, Float4 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 2);

    LineData.ItemSize = 2 * sizeof(RenderableLineVertex);
    LineData.VertexBuffer = NULL;
    size_t totalSize = LineData.MaxLinesCount * LineData.ItemSize;

    Shader shader = shader_compile_safe(asset_shader("GeometryShader.glsl"));
    VertexBuffer vbo = vertex_buffer_create_allocated(totalSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    VertexArray vao = vertex_array_create_wo_index(vbo);
    vertex_array_unbind(&vao);

    LineData.Vao    = vao;
    LineData.Shader = shader;
}

static RuntimeCamera* LegacyCamera = NULL;
#define USE_LEGACY_CAMERA 0

void
ui_renderer_init(RendererSettings settings)
{
    LegacyCamera = settings.LegacyCamera;
    ui_renderer_font_init(settings);
    ui_renderer_geometry_init(settings);
    ui_renderer_line_init(settings);
}

void
ui_renderer_deinit()
{
    array_free(FontData.VertexBuffer);
}

CharChar
GetCharChar(i32 c, i32* ind)
{
    if (hash_geti(FontData.Font.CharTable, c) != -1)
    {
	CharChar charChar = hash_get(FontData.Font.CharTable, c);
	*ind = table_header(FontData.Font.CharTable)->Index;
	return charChar;
    }
    else if (c == 32)
    {
	CharChar cc = (CharChar) {
	    .UV = (v2) {0,0},
	    .Size = (v2) {0,0},
	    .FullWidth = FontData.Font.MaxHeight / 3,
	    .FullHeight = 0,
	    .XOffset = 0,
	    .YOffset = 0,
	    .Character = 0
	};
	return cc;
    }

    *ind = -1;
    return (CharChar) {0};
}
void
SetCharChar(CharChar charChar, i32 ind)
{
    FontData.Font.CharTable[ind].Value = charChar;
}

void ui_renderer_font_flush(f32 w, f32 e);

void
ui_renderer_draw_text_ext(void* buffer, size_t length, v3 position, v2 drawable, v4 color)
{
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    wchar* rbuffer = (wchar*) buffer;

    if ((array_count(FontData.VertexBuffer)/4 + length) >= FontData.MaxGlyphsCount)
    {
	Default2DFlags(
	    GINFO("Extra Flush!\n");
	    ui_renderer_font_flush(0.48f, 0.1f);
	    );
    }

    f32 offsetX = 0.0f, offsetForCurrentFont = 1;//ui_renderer_get_offset_for_current_font();
    i32 i, count = length;
    for (i = 0; i < count; ++i)
    {
	i32 charCode = (i32)rbuffer[i];

	i32 scale = 1;
	v3 positions[4];
	f32 startX, startY, endX, endY, w = 0.0f;

	//TODO(typedef): fix problem with getting value that doesnt exist!
	if (charCode == 32)
	{
	    w = FontData.Font.MaxHeight / 3 * scale;
	    positions[0] = /* 0 1 */ v3_add(position, v3_new(offsetX, w, 0.0f));
	    positions[1] = /* 1 1 */ v3_add(position, v3_new(offsetX + w, w, 0.0f));
	    positions[2] = /* 1 0 */ v3_add(position, v3_new(offsetX + w, 0, 0.0f));
	    positions[3] = /* 0 0 */ v3_add(position, v3_new(offsetX, 0, 0.0f));

	    startX = startY = endX = endY = 0;
	}
	else if (hash_geti(FontData.Font.CharTable, charCode) != -1)
	{
	    CharChar charChar = hash_get(FontData.Font.CharTable, charCode);

	    offsetX = offsetX /* - charChar.XOffset */;
	    w = scale * charChar.FullWidth;
	    f32 h = scale * charChar.FullHeight,
		yOffset = scale * charChar.YOffset, //h + yof,
		vOffset =
#if 1
		scale * (charChar.FullHeight - charChar.YOffset);
#else
	    h - yOffset;
#endif

	    positions[0] = /* 0 1 */ v3_add(position, v3_new(offsetX, vOffset, 0.0f));
	    positions[1] = /* 1 1 */ v3_add(position, v3_new(offsetX + w, vOffset, 0.0f));
	    positions[2] = /* 1 0 */ v3_add(position, v3_new(offsetX + w, -yOffset, 0.0f));
	    positions[3] = /* 0 0 */ v3_add(position, v3_new(offsetX, -yOffset, 0.0f));

	    startX = charChar.UV.X;
	    startY = 1.0f - charChar.Size.Height - charChar.UV.Y;
	    endX   = charChar.UV.X + charChar.Size.Width;
	    endY   = 1.0f - charChar.UV.Y;

	}

	offsetX += offsetForCurrentFont * w;
	//GWARNING("OFFSETX: %f, %f\n", offsetX, drawable.X);
	if (offsetX >= drawable.X)
	{
	    DO_ONES(GINFO("Hide!\n"););
	    return;
	}

	RenderableFontVertex vertex = {
	    .Color = color,
	};

	vertex.Position = positions[0];
	vertex.UV = v2_new(startX, endY);
	array_push(FontData.VertexBuffer, vertex);

	vertex.Position = positions[1];
	vertex.UV = v2_new(endX, endY);
	array_push(FontData.VertexBuffer, vertex);

	vertex.Position = positions[2];
	vertex.UV = v2_new(endX, startY);
	array_push(FontData.VertexBuffer, vertex);

	vertex.Position = positions[3];
	vertex.UV = v2_new(startX, startY);
	array_push(FontData.VertexBuffer, vertex);

    }
}

void
ui_renderer_draw_text(WideString* string, v3 position, v2 drawable, v4 color)
{
    ui_renderer_draw_text_ext(string->Buffer, string->Length, position, drawable, color);
}

void
ui_renderer_font_flush(f32 w, f32 e)
{
    shader_bind(&FontData.Shader);
    vertex_array_bind(&FontData.Vao);
    {
	size_t size = array_count(FontData.VertexBuffer) * sizeof(RenderableFontVertex);
	vertex_buffer_set_data(&FontData.Vao.Vertex,
			       (f32*)FontData.VertexBuffer[0].Position.V,
			       size);
    }

    texture2d_bind_index(FontData.Font.FontAtlas.ID, 0);
    static i32 TextureIndices[32] = { 0 };
    shader_set_int1(&FontData.Shader, "u_Textures", 32, TextureIndices);
    /*
      where FontSize == 16.0f
      u_Width = 0.48f;
      u_EdgeTransition = 0.1f

      where FontSize == 22.0f
     */
    // smaller for small font, bigger for big font  = 0.5
    shader_set_f32(&FontData.Shader, "u_Width", w);
    // bigger for small font, smaller for big font  = 0.1
    shader_set_f32(&FontData.Shader, "u_EdgeTransition", e);
#if USE_LEGACY_CAMERA == 1
    shader_set_m4(&FontData.Shader, "u_ViewProjection", 1, 0, &LegacyCamera->ViewProjection.M[0][0]);
#else
    shader_set_m4(&FontData.Shader, "u_ViewProjection", 1, 0, &FontData.Camera->Base.ViewProjection.M[0][0]);
#endif

    glDrawElements(GL_TRIANGLES, 6 * (array_count(FontData.VertexBuffer) / 4), GL_UNSIGNED_INT, NULL);

    shader_unbind(&FontData.Shader);
    vertex_array_unbind(&FontData.Vao);
    texture2d_unbind_index(FontData.Font.FontAtlas.ID, 0);

    array_clear(FontData.VertexBuffer);
}

void ui_renderer_geometry_flush();

v3 DefPos[4] = {
    { 0, 1, 0 },
    { 1, 1, 0 },
    { 1, 0, 0 },
    { 0, 0, 0 },
};
void
ui_renderer_draw_rect_ext(v2 position, v2 scale, v4 color0, v4 color1, v4 color2, v4 color3)
{
    if ((array_count(GeometryData.VertexBuffer) / 4) >= GeometryData.MaxRectsCount)
    {
	Default2DFlags(
	    GINFO("Extra Geometry Flush!\n");
	    ui_renderer_geometry_flush();
	    );
    }

    //GERROR("VertexBuffer.Count: %d\n", array_count(GeometryData.VertexBuffer));

    m4 transform = m4_transform(v3_v2(position), (v3){0}, v3_v2(scale));

    RenderableGeometryVertex vertex0 = {
	/* 0 1 */
	.Position = v2_new(position.X, position.Y),
	.Color = color0
    };
    RenderableGeometryVertex vertex1 = {
	/* 1 1 */
	.Position = v2_new(position.X + scale.Width, position.Y),
	.Color = color1
    };
    RenderableGeometryVertex vertex2 = {
	/* 1 0 */
	.Position = v2_new(position.X + scale.Width, position.Y - scale.Height),
	.Color = color2
    };
    RenderableGeometryVertex vertex3 = {
	/* 0 0 */
	.Position = v2_new(position.X, position.Y - scale.Height),
	.Color = color3
    };

    //DO_ONES(v2_print(vertex0.Position));
    //DO_ONES(v2_print(vertex1.Position));
    //DO_ONES(v2_print(vertex2.Position));
    //DO_ONES(v2_print(vertex3.Position));

    array_push(GeometryData.VertexBuffer, vertex0);
    array_push(GeometryData.VertexBuffer, vertex1);
    array_push(GeometryData.VertexBuffer, vertex2);
    array_push(GeometryData.VertexBuffer, vertex3);
}

void
ui_renderer_draw_rect(v2 position, v2 scale, v4 color)
{
    ui_renderer_draw_rect_ext(position, scale, color, color, color, color);
}

void
ui_renderer_geometry_flush()
{
    i32 geometryCount = array_count(GeometryData.VertexBuffer);
    if (geometryCount <= 0)
	return;

    //GINFO("Flush!\n");

    shader_bind(&GeometryData.Shader);
    vertex_array_bind(&GeometryData.Vao);

#if USE_LEGACY_CAMERA == 1
    shader_set_m4(&GeometryData.Shader, "u_ViewProjection", 1, 0, &LegacyCamera->ViewProjection.M[0][0]);
#else
    shader_set_m4(&GeometryData.Shader, "u_ViewProjection", 1, 0, &GeometryData.Camera->Base.ViewProjection.M[0][0]);
#endif


    //GINFO("GeometryData.ItemSize: %d\n", GeometryData.ItemSize);

    vertex_buffer_set_data(&GeometryData.Vao.Vertex, (f32*)GeometryData.VertexBuffer[0].Position.V, geometryCount * GeometryData.ItemSize);

    DO_ONES(GINFO("Count: %d\n", geometryCount););

    glDrawElements(GL_TRIANGLES, 6 * (geometryCount / 4), GL_UNSIGNED_INT, NULL);

    shader_unbind(GeometryData.Shader);
    vertex_array_unbind(&GeometryData.Vao);
    array_clear(GeometryData.VertexBuffer);
}

void ui_renderer_line_flush();

void
ui_renderer_draw_line_ext(v2 from, v2 to, v4 color0, v4 color1, f32 lineThickness)
{
    RenderableGeometryVertex vertex0 = {
	.Position = from,
	.Color = color0
    };
    RenderableGeometryVertex vertex1 = {
	.Position = to,
	.Color = color1
    };
    RenderableGeometryVertex vertex2 = {
	.Position = v2_new(to.X, to.Y - lineThickness),
	.Color = color1
    };
    RenderableGeometryVertex vertex3 = {
	.Position = v2_new(from.X, from.Y - lineThickness),
	.Color = color0
    };

    array_push(GeometryData.VertexBuffer, vertex0);
    array_push(GeometryData.VertexBuffer, vertex1);
    array_push(GeometryData.VertexBuffer, vertex2);
    array_push(GeometryData.VertexBuffer, vertex3);
}

void
ui_renderer_draw_line(v2 from, v2 to, v4 color, f32 lineThickness)
{
    ui_renderer_draw_line_ext(from, to, color, color, lineThickness);
}

void
ui_renderer_line_flush()
{

}

void
ui_renderer_flush(f32 w, f32 e)
{
    Default2DFlags(
	ui_renderer_geometry_flush();
	ui_renderer_line_flush();
	ui_renderer_font_flush(w, e);
	);
}


/* Additional Helper Functions */

f32
ui_renderer_get_offset_for_current_font()
{
    return 1.0f;
    if (FontData.Font.FontSize < 45)
    {
	return FontData.Offsets[SimpleFontOffsetsType_Less45];
    }
    else if (FontData.Font.FontSize >= 45 && FontData.Font.FontSize < 65)
    {
	return FontData.Offsets[SimpleFontOffsetsType_Less65];
    }
    else if (FontData.Font.FontSize >= 65 && FontData.Font.FontSize < 75)
    {
	return FontData.Offsets[SimpleFontOffsetsType_Less75];
    }
    else
    {
	return 1.0f;
    }

    vassert_break();
    return 0.0f;
}

UIRendererStatistics
ui_renderer_statistics_get()
{
    UIRendererStatistics stats = (UIRendererStatistics) {
	.MaxGlyphsCount = FontData.MaxGlyphsCount,
	.MaxRectsCount = GeometryData.MaxRectsCount,
	.MaxLinesCount = LineData.MaxLinesCount,

	.GlyphsCount = array_count(FontData.VertexBuffer),
	.RectsCount = array_count(GeometryData.VertexBuffer),
	.LinesCount = array_count(LineData.VertexBuffer),
    };

    return stats;
}
#endif
