#include "ModelRenderer.h"

#include <stdio.h>
#include <string.h>
#include <glad/glad.h>

#include <Utils/SimpleStandardLibrary.h>
#include <Graphics/RuntimeCamera.h>
#include <Graphics/Renderer.h>
#include <EntitySystem/Components/Base/CameraComponent.h>

static ModelRenderer ModelRendererData;
f32* ModelRendererMemory = NULL;

force_inline VertexArray
vao_create(size_t vboSize, size_t indicesSize)
{
    DataType types[] = { Float3, Float3, Float2 };
    BufferLayout* bufferLayout = buffer_layout_create(types, 3);
    vassert(buffer_layout_get_size(bufferLayout) > 0 && "Buffer layout size error!!!");
    // Not the same
    VertexBuffer vbo = vertex_buffer_create_allocated(vboSize);
    vertex_buffer_add_layout(&vbo, bufferLayout);
    IndexBuffer ibo = index_buffer_allocated(indicesSize);

    // Not the same
    VertexArray vao = vertex_array_create(vbo, ibo);
    return vao;
}

void
renderer3d_model_init(CameraComponent* camera)
{
    ModelRendererData.Camera = camera;
    ModelRendererData.Shader = shader_compile_safe(
	asset_shader("ForwardRenderer.glsl"));

    ModelRendererData.Size = R3D_MESH_MAX_SIZE;
    ModelRendererData.IndicesCount = R3D_MESH_INDEX_COUNT;
    ModelRendererData.VAO = vao_create(ModelRendererData.Size, R3D_MESH_INDEX_SIZE);

    vertex_array_unbind(&ModelRendererData.VAO);
}

void
renderer3d_model_set_camera(CameraComponent* camera)
{
    ModelRendererData.Camera = camera;
}

void
legacy_light_approach()
{
	/* { */
    /*	i32 i, dirLightCount = array_count(storage.DirectionalLights); */
    /*	//vassert(dirLightCount == 1 && "Wrong dirLightCount!"); */
    /*	for (i = 0; i < dirLightCount; ++i) */
    /*	{ */
    /*	    DirectionalLightComponent dirLight = storage.DirectionalLights[i]; */

    /*	    char baseAmbient[128]; */
    /*	    char baseDiffuse[128]; */
    /*	    char baseSpecular[128]; */
    /*	    char color[128]; */
    /*	    char direction[128]; */

    /*	    sprintf(baseAmbient, "u_LightData.DirectionalLights[%d].Base.Ambient", i); */
    /*	    sprintf(baseDiffuse, "u_LightData.DirectionalLights[%d].Base.Diffuse", i); */
    /*	    sprintf(baseSpecular, "u_LightData.DirectionalLights[%d].Base.Specular", i); */
    /*	    sprintf(color, "u_LightData.DirectionalLights[%d].Color", i); */
    /*	    sprintf(direction, "u_LightData.DirectionalLights[%d].Direction", i); */

    /*	    shader_set_v3(&ModelRendererData.Shader, baseAmbient, dirLight.Base.Ambient); */
    /*	    shader_set_v3(&ModelRendererData.Shader, baseDiffuse, dirLight.Base.Diffuse); */
    /*	    shader_set_v3(&ModelRendererData.Shader, baseSpecular, dirLight.Base.Specular); */
    /*	    shader_set_v3(&ModelRendererData.Shader, color, dirLight.Color); */
    /*	    shader_set_v3(&ModelRendererData.Shader, direction, dirLight.Direction); */
    /*	} */

    /*	shader_set_i32(&ModelRendererData.Shader, "u_LightData.DirectionalLightsCount", dirLightCount); */
    /* } */

    /* { */
    /*	i32 i, pointLightsCount = array_count(storage.PointLights); */
    /*	//vassert(pointLightsCount == 1 && "Wrong pointLightsCount!"); */
    /*	for (i = 0; i < pointLightsCount; ++i) */
    /*	{ */
    /*	    PointLightComponent pointLight = storage.PointLights[i]; */

    /*	    char baseAmbient[128]; memset(baseAmbient, '\0', sizeof(baseAmbient)); */
    /*	    char baseDiffuse[128]; memset(baseDiffuse, '\0', sizeof(baseDiffuse)); */
    /*	    char baseSpecular[128]; memset(baseSpecular, '\0', sizeof(baseSpecular)); */
    /*	    char color[128]; memset(color, '\0', sizeof(color)); */
    /*	    char direction[128]; memset(direction, '\0', sizeof(direction)); */
    /*	    char position[128]; memset(position, '\0', sizeof(position)); */
    /*	    char constant[128]; memset(constant, '\0', sizeof(constant)); */
    /*	    char linear[128]; memset(linear, '\0', sizeof(linear)); */
    /*	    char quadratic[128]; memset(quadratic, '\0', sizeof(quadratic)); */

    /*	    //GINFO("BaseSize: %ld\n", sizeof(baseAmbient)); */

    /*	    sprintf(baseAmbient, "u_LightData.PointLights[%d].Base.Ambient", i); */
    /*	    sprintf(baseDiffuse, "u_LightData.PointLights[%d].Base.Diffuse", i); */
    /*	    sprintf(baseSpecular, "u_LightData.PointLights[%d].Base.Specular", i); */
    /*	    sprintf(color, "u_LightData.PointLights[%d].Color", i); */
    /*	    sprintf(direction, "u_LightData.PointLights[%d].Direction", i); */
    /*	    sprintf(position, "u_LightData.PointLights[%d].Position", i); */
    /*	    sprintf(constant, "u_LightData.PointLights[%d].Constant", i); */
    /*	    sprintf(linear, "u_LightData.PointLights[%d].Linear", i); */
    /*	    sprintf(quadratic, "u_LightData.PointLights[%d].Quadratic", i); */

    /*	    shader_set_v3(&ModelRendererData.Shader, baseAmbient, pointLight.Base.Ambient); */
    /*	    shader_set_v3(&ModelRendererData.Shader, baseDiffuse, pointLight.Base.Diffuse); */
    /*	    shader_set_v3(&ModelRendererData.Shader, baseSpecular, pointLight.Base.Specular); */
    /*	    shader_set_v3(&ModelRendererData.Shader, color, pointLight.Color); */
    /*	    shader_set_v3(&ModelRendererData.Shader, position, pointLight.Position); */
    /*	    shader_set_f32(&ModelRendererData.Shader, constant, pointLight.Constant); */
    /*	    shader_set_f32(&ModelRendererData.Shader, linear, pointLight.Linear); */
    /*	    shader_set_f32(&ModelRendererData.Shader, quadratic, pointLight.Quadratic); */
    /*	} */

    /*	shader_set_i32(&ModelRendererData.Shader, "u_LightData.PointLightsCount", pointLightsCount); */
    /* } */

    /* { */
    /*	i32 i, flashLightsCount = array_count(storage.FlashLights); */
    /*	//vassert(flashLightsCount == 1 && "Wrong flashLightsCount!"); */
    /*	for (i = 0; i < flashLightsCount; ++i) */
    /*	{ */
    /*	    FlashLightComponent flashLight = storage.FlashLights[i]; */

    /*	    char baseAmbient[64]; memset(baseAmbient, '\0', sizeof(baseAmbient)); */
    /*	    char baseDiffuse[64]; memset(baseDiffuse, '\0', sizeof(baseDiffuse)); */
    /*	    char baseSpecular[64]; memset(baseSpecular, '\0', sizeof(baseSpecular)); */
    /*	    char color[64]; memset(color, '\0', sizeof(color)); */
    /*	    char position[64]; memset(position, '\0', sizeof(position)); */
    /*	    char constant[64]; memset(constant, '\0', sizeof(constant)); */
    /*	    char linear[64]; memset(linear, '\0', sizeof(linear)); */
    /*	    char quadratic[64]; memset(quadratic, '\0', sizeof(quadratic)); */
    /*	    char cutoff[64]; memset(cutoff, '\0', sizeof(cutoff)); */
    /*	    char outerCutOff[64]; memset(outerCutOff, '\0', sizeof(outerCutOff)); */

    /*	    sprintf(baseAmbient, "u_LightData.FlashLights[%d].Base.Ambient", i); */
    /*	    sprintf(baseDiffuse, "u_LightData.FlashLights[%d].Base.Diffuse", i); */
    /*	    sprintf(baseSpecular, "u_LightData.FlashLights[%d].Base.Specular", i); */
    /*	    sprintf(color, "u_LightData.FlashLights[%d].Color", i); */
    /*	    sprintf(position, "u_LightData.FlashLights[%d].Position", i); */
    /*	    sprintf(constant, "u_LightData.FlashLights[%d].Constant", i); */
    /*	    sprintf(linear, "u_LightData.FlashLights[%d].Linear", i); */
    /*	    sprintf(quadratic, "u_LightData.FlashLights[%d].Quadratic", i); */
    /*	    sprintf(cutoff, "u_LightData.FlashLights[%d].CutOff", i); */
    /*	    sprintf(outerCutOff, "u_LightData.FlashLights[%d].OuterCutOff", i); */

    /*	    shader_set_v3(&ModelRendererData.Shader, baseAmbient, flashLight.Base.Ambient); */
    /*	    shader_set_v3(&ModelRendererData.Shader, baseDiffuse, flashLight.Base.Diffuse); */
    /*	    shader_set_v3(&ModelRendererData.Shader, baseSpecular, flashLight.Base.Specular); */
    /*	    shader_set_v3(&ModelRendererData.Shader, color, flashLight.Color); */
    /*	    shader_set_v3(&ModelRendererData.Shader, position, flashLight.Position); */
    /*	    shader_set_f32(&ModelRendererData.Shader, constant, flashLight.Constant); */
    /*	    shader_set_f32(&ModelRendererData.Shader, linear, flashLight.Linear); */
    /*	    shader_set_f32(&ModelRendererData.Shader, quadratic, flashLight.Quadratic); */
    /*	    shader_set_f32(&ModelRendererData.Shader, cutoff, flashLight.CutOff); */
    /*	    shader_set_f32(&ModelRendererData.Shader, outerCutOff, flashLight.OuterCutOff); */
    /*	} */

    /*	shader_set_i32(&ModelRendererData.Shader, "u_LightData.FlashLightsCount", flashLightsCount); */
    /* } */
}

force_inline void
_renderer3d_draw_mesh(StaticMesh mesh, m4 transform)
{
    renderer_enable_face_culling(RendererType_RendererModel);
    renderer_enable_depth_testing(RendererType_RendererModel);
    renderer_enable_blending(RendererType_RendererModel);

    size_t currentMeshVerticesSize = array_count(mesh.Vertices) * sizeof(MeshVertex);
    if (currentMeshVerticesSize > R3D_MESH_MAX_SIZE)
    {
	GERROR(
	    "Mesh size %ld, vertices count: %ld\n"
	    "Allowed mesh size: %ld!\n"
	    "Mesh can't be rendered due to it size!\n",
	    currentMeshVerticesSize,
	    array_count(mesh.Vertices),
	    R3D_MESH_MAX_SIZE);
	return;
    }

    if (currentMeshVerticesSize <= 0)
    {
	GWARNING("Mesh is empty\n");
	return;
    }

    vertex_buffer_set_data(&ModelRendererData.VAO.Vertex, (f32*)&mesh.Vertices[0], array_count(mesh.Vertices) * sizeof(MeshVertex));
    index_buffer_set_data(&ModelRendererData.VAO.Index, mesh.Indices, mesh.IndicesCount * sizeof(u32));

    shader_bind(&ModelRendererData.Shader);
    /*
      shader_execute(&ModelRendererData.Shader, dirLight, pointLights, flashLights);
    */
    vertex_array_bind(&ModelRendererData.VAO);

    //shader_set_v3(&ModelRendererData.Shader, "u_ViewPosition", ModelRendererData.Camera->Settings.Position);

    shader_set_m4(&ModelRendererData.Shader, "u_ViewProjection", 1, 0, &ModelRendererData.Camera->Base.ViewProjection.M[0][0]);
    shader_set_m4(&ModelRendererData.Shader, "u_ModelTransform", 1, 0, &transform.M[0][0]);

    shader_set_color(&ModelRendererData.Shader, "u_MeshMaterial.BaseColor", mesh.Material.Metallic.BaseColor);
    shader_set_f32(&ModelRendererData.Shader, "u_MeshMaterial.Shininess", 0.5f);

    /* NOTE(bies): we have to:
       1. Bind texture to a slot
       2. pass texture slot to shader by uniform
    */
    texture2d_bind_index(mesh.Material.Metallic.BaseColorTexture, 0);
    shader_set_texture(&ModelRendererData.Shader,
		       "u_MeshMaterial.BaseTexture",
		       0);

    glDrawElements(GL_TRIANGLES,
		   mesh.IndicesCount,
		   GL_UNSIGNED_INT, NULL);

    shader_unbind();
    vertex_array_unbind(&ModelRendererData.VAO);

    glDisable(GL_CULL_FACE);

    renderer_flag_type_disable(RendererType_RendererModel);
}

void
renderer3d_model_draw(StaticModel model, m4 transform)
{
    i32 m, meshesCount = array_count(model.Meshes);
    for (m = 0; m < meshesCount; ++m)
    {
	StaticMesh mesh = model.Meshes[m];
	if (mesh.IsVisible)
	{
	    _renderer3d_draw_mesh(mesh, transform);
	}
    }
}
