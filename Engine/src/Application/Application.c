#include "Application.h"

#include <Editor/ViewportPanel.h>

#include <Graphics/SimpleWindow.h>
#include <Graphics/Renderer.h>
#include <InputSystem/KeyCodes.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <Graphics/Vulkan/VulkanSimpleRenderer.h>
#include <Event/Event.h>
#include <InputSystem/SimpleInput.h>
#include <UI/UserInterface.h>

//#include <UI/SuifBackend.h>
//#include <UI/Suif.h>

#include <Utils/SimpleStandardLibrary.h>

static Application gApplication;
static SimpleWindow CurrentWindow;

Application*
application_get()
{
    return &gApplication;
}

SimpleWindow*
application_get_window()
{
    return gApplication.Window;
}

void
application_push_layer(Layer layer)
{
    TimeState timeState;
    profiler_start(&timeState);

    array_push(gApplication.Layers, layer);
    if (layer.OnAttach != NULL)
    {
	layer.OnAttach();
    }

    profiler_end(&timeState);
    GSUCCESS("PushLayer %s took %s\n", layer.Name ? layer.Name : "DEFAULT LAYER", profiler_get_string(&timeState));
}

void
application_create(ApplicationSettings appSet)
{
    GINFO("Current Directory: %s\n", path_get_current_directory());

    SimpleWindow* window = memory_allocate_type(SimpleWindow);
    SimpleWindowSettings winSettings = *((SimpleWindowSettings*) ((ApplicationSettings*)&appSet));
    i32 isWindowCreated = window_create(window, winSettings, application_on_event);
    if (isWindowCreated == -1)
    {
	GERROR("Can't create window!\n");
	return;
    }

    RuntimeStatistics runtimeStat = {
	.CurrentTime = 0,
	.LastFrameTime = 0,
	.Timestep = 0.0f,
	.SecondElapsed = 0.0f,
	.FramesCount = 0,

	.UiFps = Min(appSet.UiFps, 50),
    };

    gApplication.Window = window;
    gApplication.IsMinimized = 0;
    gApplication.IsRunning = 1;
    gApplication.IsOpenGlInit = appSet.IsInitOpenGl;
    gApplication.IsInitImGui = appSet.IsInitImGui;
    gApplication.IsFrameRatePrinted = appSet.IsFrameRatePrinted;
    gApplication.Layers = NULL;
    gApplication.Stats = runtimeStat;

    input_init(window->GlfwWindow);

    VsaSettings vsaSettings = {
	.IsDebugEnabled = 1,
	.IsVSyncEnabled = 1,
	.SamplesCount = 8
    };
    vsa_core_init(vsaSettings);

    ui_create();
}

force_inline void
application_update_statistics()
{
    gApplication.Stats.CurrentTime = (f32) window_get_short_time();
    gApplication.Stats.Timestep = gApplication.Stats.CurrentTime - gApplication.Stats.LastFrameTime;
    gApplication.Stats.LastFrameTime = gApplication.Stats.CurrentTime;
    gApplication.Stats.SecondElapsed += gApplication.Stats.Timestep;
}

void
application_start()
{
    f32 secondTimer = 0.0f;
    i32 framesCount = 0,
	layersCount = array_count(gApplication.Layers);

    gApplication.IsRunning = 1;

    for (i32 l = 0; l < layersCount; ++l)
    {
	Layer layer = gApplication.Layers[l];
	if (layer.OnAttachFinished)
	{
	    layer.OnAttachFinished();
	}
    }

    while (gApplication.IsRunning)
    {
	application_update_statistics();

	if (!gApplication.IsMinimized)
	{
	    vsr_frame_begin();

	    for (i32 l = 0; l < array_count(gApplication.Layers); ++l)
	    {
		Layer layer = gApplication.Layers[l];

		if (layer.Name != NULL && layer.OnUpdate != NULL)
		{
		    layer.OnUpdate(gApplication.Stats.Timestep);
		}
	    }

	    //GINFO("FrameCount: %d UiFps: %d\n", gApplication.Stats.FramesCount, gApplication.Stats.UiFps);
	    //if (gApplication.Stats.FramesCount <= gApplication.Stats.UiFps)
	    //{
	    //}

#if 1
	    ui_begin();
	    for (i32 l = 0; l < layersCount; l++)
	    {
		Layer layer = gApplication.Layers[l];

		if (layer.Name != NULL && layer.OnUIRender != NULL)
		    layer.OnUIRender();
	    }
	    ui_end(vsr_get_image_index());
#endif

	    vsr_draw_all();

	    vsr_frame_end();

#define ENABLE_UI 0
#if ENABLE_UI == 1
	    if (gApplication.IsInitImGui)
	    {
		ui_begin();
		for (i32 l = 0; l < layersCount; l++)
		{
		    Layer layer = gApplication.Layers[l];

		    if (layer.Name == NULL)
			continue;
		    if (layer.OnUIRender != NULL)
			layer.OnUIRender();
		}
		ui_end();

	    }
#endif // ENABLE_UI
	}

	if (gApplication.Stats.SecondElapsed >= 1.0f)
	{
	    RuntimeStatistics stats = application_get()->Stats;
	    if (gApplication.IsFrameRatePrinted)
		GINFO("delay[ms s]: %fms %fs, %d frames\n", 1000 * stats.Timestep, stats.Timestep, stats.FramesCount);

	    gApplication.Stats.SecondElapsed = 0.0f;
	    gApplication.Stats.FramesCount = 0;
	}

	window_on_update(gApplication.Window);

	++gApplication.Stats.FramesCount;
    }
}

void
application_on_event(Event* event)
{
    Layer layer;
    if (event->Category == EventCategory_Window)
    {
	if (event->Type == EventType_WindowResized)
	{
	    WindowResizedEvent* wevent = (WindowResizedEvent*) event;

	    if (wevent->Width == 0 || wevent->Height == 0)
	    {
		gApplication.IsMinimized = 1;
	    }
	    else
	    {
		gApplication.Window->AspectRatio = (f32) gApplication.Window->Width / gApplication.Window->Height;
		//GINFO("AspectRatio: %f\n", gApplication.Window->AspectRatio);
	    }

	    // BUG/TODO(typedef): place into viewport panel
	    if (gApplication.IsOpenGlInit)
		renderer_set_viewport(wevent->Width, wevent->Height);
	}
	else if (event->Type == EventType_WindowMinimized)
	{
	    gApplication.IsMinimized = 1;
	}
	else if (event->Type == EventType_WindowRestored)
	{
	    gApplication.IsMinimized = 0;
	}
    }

    i32 layersCount = array_count(gApplication.Layers);
    for (i32 l = 0; l < layersCount; l++)
    {
	layer = gApplication.Layers[l];
	if (layer.OnEvent != NULL && event->IsHandled == 0)
	{
	    layer.OnEvent(event);
	}
    }
}

void
application_close()
{
    gApplication.IsRunning = 0;
}

void
application_end()
{
    vsa_device_wait_idle();

    Layer layer;
    i32 l, layersCount = array_count(gApplication.Layers);
    for (l = 0; l < layersCount; l++)
    {
	layer = gApplication.Layers[l];
	if (layer.OnDestoy != NULL)
	{
	    if (layer.Name != NULL)
	    {
		GDEBUG("Destoying layer: %s\n", layer.Name);
	    }

	    layer.OnDestoy();
	}
    }

    array_free(gApplication.Layers);

    vsa_core_deinit();

    window_terminate();
}
