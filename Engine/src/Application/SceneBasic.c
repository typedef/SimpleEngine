#include "SceneBasic.h"

#include <Graphics/Renderer.h>
#include <Graphics/RuntimeCamera.h>
#include <Math/SimpleMath.h>
#include <EntitySystem/Components/Base/CameraComponent.h>

static i32 IsGridInitialized = 0;

static v4 GridColor;
static f32 LineThickness;
static f32 From;
static f32 To;
static f32 CellSize;

static v4 XAxisColor;
static v4 ZAxisColor;
static v3 AxisX0;
static v3 AxisX1;
static v3 AxisZ0;
static v3 AxisZ1;

void
scene_basic_create_grid(v4 gridColor, f32 lineThickness, f32 from, f32 to, f32 cellSize)
{
    IsGridInitialized = 1;

    GridColor = gridColor;
    LineThickness = lineThickness;
    From = from;
    To = to;
    CellSize = cellSize;

    XAxisColor = v4_new(1, 0, 0, 1);
    ZAxisColor = v4_new(0, 0, 1, 1);
    AxisX0 = v3_new(From, 0, 0);
    AxisX1 = v3_new(To, 0, 0);

    AxisZ0 = v3_new(0, 0, From);
    AxisZ1 = v3_new(0, 0, To);
}

void
scene_basic_render_grid()
{
    if (!IsGridInitialized)
	return;

    renderer2d_set_line_thickness(LineThickness);

    for (f32 z = From; z < To; z += CellSize)
    {
	if (f32_equal(z, 0.0f))
	    continue;

	renderer2d_submit_line(v3_new(From, 0, z), v3_new(To, 0, z), GridColor);
    }

    //x
    for (f32 x = From; x < To; x += CellSize)
    {
	if (f32_equal(x, 0.0f))
	    continue;

	renderer2d_submit_line(v3_new(x, 0, From), v3_new(x, 0, To), GridColor);
    }

    renderer2d_submit_line(AxisX0, AxisX1, XAxisColor);
    renderer2d_submit_line(AxisZ0, AxisZ1, ZAxisColor);

    renderer2d_flush();
}


void
scene_basic_render_camera_orientation(CameraComponent* camera, v4 frontColor, v4 rightColor, v4 upColor)
{
    v3 start = { 0, 0, 0 };

    renderer2d_set_line_thickness(2.0f);
    renderer2d_submit_line(start, camera->Settings.Front, frontColor);
    renderer2d_submit_line(start, camera->Settings.Right, rightColor);
    renderer2d_submit_line(start, camera->Settings.Up   , upColor   );

    renderer2d_flush();
}
