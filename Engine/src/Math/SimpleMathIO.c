#include "SimpleMathIO.h"

#include <stdio.h>

#define LOGGER_DEFINITION
#include <Utils/SimpleStandardLibrary.h>

void
_v2_print(v2 v, const char* accuracy)
{
    char format[128];

    if (v.X >= 0.0f)
    {
	sprintf(format, "%s%s", RED("x" )":  %s", accuracy);
	printf(format, v.X);
    }
    else
    {
	sprintf(format, "%s%s", RED("x" )": %s", accuracy);
	printf(format, v.X);
    }

    if (v.Y >= 0.0f)
    {
	sprintf(format, "%s%s", GREEN(" y" )":  %s", accuracy);
	printf(format, v.Y);
    }
    else
    {
	sprintf(format, "%s%s", GREEN(" y" )": %s", accuracy);
	printf(format, v.Y);
    }

    printf("\n");
}

void
_v3_print(v3 v, const char* accuracy)
{
    char format[128];

    if (v.X >= 0.0f)
    {
	sprintf(format, "%s%s", RED("x" )":  %s", accuracy);
	printf(format, v.X);
    }
    else
    {
	sprintf(format, "%s%s", RED("x" )": %s", accuracy);
	printf(format, v.X);
    }

    if (v.Y >= 0.0f)
    {
	sprintf(format, "%s%s", GREEN(" y" )":  %s", accuracy);
	printf(format, v.Y);
    }
    else
    {
	sprintf(format, "%s%s", GREEN(" y" )": %s", accuracy);
	printf(format, v.Y);
    }

    if (v.Z >= 0.0f)
    {
	sprintf(format, "%s%s", BLUE(" z" )":  ", accuracy);
	printf(format, v.Z);
    }
    else
    {
	sprintf(format, "%s%s", BLUE(" z" )":  ", accuracy);
	printf(format, v.Z);
    }

    printf("\n");
}

void
_v4_print(v4 v, const char* accuracy)
{
    char format[128];

    if (v.X >= 0.0f)
    {
	sprintf(format, "%s%s", RED("x" )":  %s", accuracy);
	printf(format, v.X);
    }
    else
    {
	sprintf(format, "%s%s", RED("x" )": %s", accuracy);
	printf(format, v.X);
    }

    if (v.Y >= 0.0f)
    {
	sprintf(format, "%s%s", GREEN(" y" )":  %s", accuracy);
	printf(format, v.Y);
    }
    else
    {
	sprintf(format, "%s%s", GREEN(" y" )": %s", accuracy);
	printf(format, v.Y);
    }

    if (v.Z >= 0.0f)
    {
	sprintf(format, "%s%s", BLUE(" z" )":  ", accuracy);
	printf(format, v.Z);
    }
    else
    {
	sprintf(format, "%s%s", BLUE(" z" )":  ", accuracy);
	printf(format, v.Z);
    }

    if (v.W >= 0.0f)
    {
	sprintf(format, "%s%s", YELLOW(" w" )":  ", accuracy);
	printf(format, v.W);
    }
    else
    {
	sprintf(format, "%s%s", YELLOW(" w" )": ", accuracy);
	printf(format, v.W);
    }

    printf("\n");
}

#if !defined(GREEN)
#define GREEN(x) "\x1B[32m"x"\033[0m"
#endif // GREEN

void
m4_print(m4 m, const char* name)
{
    char sb[511];
    sprintf(sb, "%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n%.2f %.2f %.2f %.2f\n",
	    m.M00, m.M01, m.M02, m.M03,
	    m.M10, m.M11, m.M12, m.M13,
	    m.M20, m.M21, m.M22, m.M23,
	    m.M30, m.M31, m.M32, m.M33);

    printf("M4 ("GREEN("%s")"): \n%s", name, sb);
}
