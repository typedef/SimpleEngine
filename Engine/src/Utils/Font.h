#ifndef FONT_H
#define FONT_H

#include <Graphics/OpenGLBase.h>
#include <Utils/Types.h>

typedef struct FontInfoSettings
{
    const char* Path;
    const char* BitmapSavePath;
    i32 FontSize;
    i8 IsOpenGLEnabled;
    i8 IsSaveBitmap;
} FontInfoSettings;

typedef struct CharChar
{
    v2 UV;
    v2 Size;
    f32 FullWidth;
    f32 FullHeight;
    i32 XOffset;
    i32 YOffset;
    i32 Character;
} CharChar;

typedef struct CharRecord
{
    i32 Key;
    CharChar Value;
} CharRecord;

typedef struct Font
{
    const char* Name; // for example: NotoSansJP.otf
    CharRecord* CharTable;
    i32 FontSize;
    i32 MaxHeight;
    i32 MaxYOffset;
    i32 BitmapWidth;
    i32 BitmapHeight;
    f32 Scale;
    void* Bitmap;
} Font;

Font font_generic_create(FontInfoSettings settings, i32 range0, i32 range1, i32* extRanges);
Font font_cirilic_create(FontInfoSettings settings);
void font_destroy(Font fontInfo);

void font_get_string_metrics_ext(const Font* font, wchar* buffer, i32 length, f32* w, f32* h);
void font_get_string_metrics(const Font* font, WideString str, f32* w, f32* h);
f32 font_get_first_char_y_offset(const Font* font, WideString str);
void font_get_first_char_y(const Font* font, WideString str, v3 position, f32* py0, f32* py1);
f32 font_get_string_width(const Font* font, WideString str);
f32 font_get_string_height(const Font* font, WideString str);

#endif
