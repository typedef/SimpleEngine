#ifndef JSON_PARSER_H
#define JSON_PARSER_H

#include "Types.h"

typedef enum JsonTokenType
{
    JsonTokenType_None = 0,
    JsonTokenType_KeyValueSeparator,
    JsonTokenType_String,
    JsonTokenType_IntNumber,
    JsonTokenType_FloatNumber,
    JsonTokenType_BoolTrue,
    JsonTokenType_BoolFalse,
    JsonTokenType_Null,
    JsonTokenType_Object,
    JsonTokenType_ObjectOpenBracket,
    JsonTokenType_ObjectCloseBracket,
    JsonTokenType_ArrayOpenBracket,
    JsonTokenType_ArrayCloseBracket,
    JsonTokenType_Comma,

    JSON_TOKEN_TYPE_COUNT
} JsonTokenType;
typedef struct JsonToken
{
    JsonTokenType Type;
    void* Data;
} JsonToken;

typedef enum JsonValueType
{
    JsonValueType_Int = 0,
    JsonValueType_Float = 1,
    JsonValueType_Bool = 2,
    JsonValueType_Null = 3,
    JsonValueType_String = 4,
    JsonValueType_IntArray = 5,
    JsonValueType_FloatArray = 6,
    JsonValueType_StringArray = 7,
    JsonValueType_ObjectArray = 8,
    JsonValueType_Object = 9
} JsonValueType;

static const char*
json_value_type_to_string(JsonValueType type)
{

    switch (type)
    {
    case JsonValueType_Int: return "Int";
    case JsonValueType_Float: return "Float";
    case JsonValueType_Bool: return "Bool";
    case JsonValueType_Null: return "Null";
    case JsonValueType_String: return "String";
    case JsonValueType_IntArray: return "IntArray";
    case JsonValueType_FloatArray: return "FloatArray";
    case JsonValueType_StringArray: return "StringArray";
    case JsonValueType_ObjectArray: return "ObjectArray";
    case JsonValueType_Object: return "Object";
    }

    vassert_break();
    return "";
}

typedef struct JsonValue
{
    JsonValueType Type;
    void* Data;
} JsonValue;

typedef struct JsonObject
{
    const char** Keys;
    JsonValue* Values;
} JsonObject;

typedef struct JsonParser
{
    char* SourceContent;
    JsonToken* Tokens;
    JsonObject* Object;
} JsonParser;

/*
  CREATE

  void json_add_key_value(JsonObject* object, char* key, void* data)
  {
  array_push(object->Keys, key);
  array_push(object->Values, data);
  }

*/

#define JSON_INT(num)				\
    ({						\
	i64* alloc = memory_allocate_type(i64);	\
	*alloc = num;				\
	JsonValue value;			\
	value.Data = alloc;			\
	value.Type = JsonValueType_Int;	\
	value;					\
    })
#define JSON_FLOAT(num)				\
    ({						\
	f32* alloc = memory_allocate_type(f32);	\
	*alloc = num;				\
	JsonValue value;			\
	value.Data = alloc;			\
	value.Type = JsonValueType_Float;	\
	value;					\
    })
#define JSON_STRING(str)			\
    ({						\
	char* sstr = string(str);		\
	JsonValue value;			\
	value.Data = sstr;			\
	value.Type = JsonValueType_String;	\
	value;					\
    })
#define JSON_STRING_ALLOCATED(str)		\
    ({						\
	JsonValue value;			\
	value.Data = str;			\
	value.Type = JsonValueType_String;	\
	value;					\
    })
#define JSON_TRUE(str)				\
    ({						\
	JSON_STRING("TRUE");			\
    })
#define JSON_FALSE(str)				\
    ({						\
	JSON_STRING("FALSE");			\
    })
#define JSON_NULL()				\
    ({						\
	JsonValue value;			\
	value.Data = (void*) istring("NULL");	\
	value.Type = JsonValueType_Null;	\
	value;					\
    })
#define JSON_I32_ARRAY(arr)			\
    ({						\
	JsonValue value;			\
	value.Data = arr;			\
	value.Type = JsonValueType_IntArray;	\
	value;					\
    })
#define JSON_F32_ARRAY(arr)			\
    ({						\
	JsonValue value;			\
	value.Data = arr;			\
	value.Type = JsonValueType_FloatArray;	\
	value;					\
    })
//TODO(bies): optimize this
#define JSON_F32_ARRAY_NEW(arr, count)		\
    ({						\
	f32* oldArr = (f32*)arr;		\
	f32* newArr = NULL;			\
	for(i32 i = 0; i < count; ++i)		\
	{					\
	    array_push(newArr, oldArr[i]);	\
	}					\
	JsonValue value;			\
	value.Data = newArr;			\
	value.Type = JsonValueType_FloatArray;	\
	value;					\
    })
#define JSON_STRING_ARRAY(arr)				\
    ({							\
	JsonValue value;				\
	value.Data = arr;				\
	value.Type = JsonValueType_StringArray;		\
	value;						\
    })
#define JSON_OBJECT(obj)			\
    ({						\
	JsonValue value;			\
	value.Data = obj;			\
	value.Type = JsonValueType_Object;	\
	value;					\
    })

/*
  NOTE(typedef): PUBLIC JSON UTILS
*/
JsonObject* json_object_create();
void json_check_for_type(JsonValue value, JsonValueType type);
void json_object_destroy(JsonObject* obj);
const char* json_token_to_string(JsonToken token);
char* json_tokens_to_string(JsonToken* tokens);
void json_object_free(JsonObject* obj);

typedef enum JsonLetterType
{
    JSON_LETTER_TYPE_NONE = 0,
    JSON_LETTER_TYPE_OPEN_BRACKET,
    JSON_LETTER_TYPE_CLOSE_BRACKET,
    JSON_LETTER_TYPE_OPEN_STRING
} JsonLetterType;

i32 _json_is_array_of_type(JsonToken* tokens, JsonTokenType type, i32 index);
JsonValue _json_create_array(JsonToken* tokens, JsonTokenType type, i32* index);
JsonValue _json_create_object_array(JsonToken* tokens, i32* index);
JsonToken _json_read_string_token(char* stream, i32* length);
JsonToken _json_read_number_token(char* stream, i32* length, i32 negativePart);
i32 _json_check_key_word(char* stream, const char* keyWord);
i32 _json_next_bool(char* stream);
JsonToken* _json_read_tokens(JsonParser* parser, char* stream);
JsonObject* json_parser_create_json_object(JsonToken* tokens, i32* index);
JsonValue _json_parse_value(JsonToken* tokens, i32* index);
void json_parse_string(JsonParser* parser, char* string);
void json_parse_file(JsonParser* parser, const char* path);
void json_destroy(JsonParser* parser);
void json_write_file(JsonParser* parser, const char* path);
JsonValue json_object_get_value(JsonObject* object, char* key, JsonValueType type);
char* json_object_to_string(JsonObject* root, i32 startIndentationLevel);

/*
  NOTE(typedef): PUBLIC JSON UTILS
*/
const char* json_token_to_string(JsonToken token);
char* json_tokens_to_string(JsonToken* tokens);
void json_object_free(JsonObject* obj);
void _json_print_value(JsonValue value);

#endif // JSON_PARSER_H
