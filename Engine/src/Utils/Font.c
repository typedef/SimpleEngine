#include "Font.h"

#include <string.h>
#include <stdio.h>
#include <math.h>

#include <Utils/stb_truetype.h>
#include <Utils/stb_image_write.h>
#include <Utils/SimpleStandardLibrary.h>

//TODO(typedef): rewrite this
void
stbi__vertical_flip(void *image, int w, int h, int bytes_per_pixel)
{
    int row;
    size_t bytes_per_row = (size_t)w * bytes_per_pixel;
    u8 temp[2048];
    u8 *bytes = (u8 *)image;

    for (row = 0; row < (h>>1); row++) {
	u8 *row0 = bytes + row*bytes_per_row;
	u8 *row1 = bytes + (h - row - 1)*bytes_per_row;
	// swap row0 with row1
	size_t bytes_left = bytes_per_row;
	while (bytes_left) {
	    size_t bytes_copy = (bytes_left < sizeof(temp)) ? bytes_left : sizeof(temp);
	    memcpy(temp, row0, bytes_copy);
	    memcpy(row0, row1, bytes_copy);
	    memcpy(row1, temp, bytes_copy);
	    row0 += bytes_copy;
	    row1 += bytes_copy;
	    bytes_left -= bytes_copy;
	}
    }
}

static void
char_char_print(CharChar charchar)
{
    printf("\n\n    CharChar(%lc): UV(%f %f) Size(%f %f) Char()\n\n\n",
	   charchar.Character,
	   charchar.UV.X, charchar.UV.Y,
	   charchar.Size.Width, charchar.Size.Height);
}

Font
font_generic_create(FontInfoSettings settings, i32 range0, i32 range1, i32* extRanges)
{
    vguard_not_null(extRanges);

    i32 bitmapWidth = 512,
	bitmapHeight = 512,
	lineHeight = settings.FontSize;

    u8* bytes = file_read_bytes(settings.Path);
    stbtt_fontinfo info;
    if (!stbtt_InitFont(&info, bytes, 0))
    {
	vassert(0 && "Font init failed!");
    }

    f32 scale = stbtt_ScaleForPixelHeight(&info, lineHeight);

    //char* word = "the quick brown fox";
    u8* bitmap = memory_allocate(bitmapWidth * bitmapHeight * sizeof(char));
    memset(bitmap, '\0', bitmapWidth * bitmapHeight * sizeof(char));

    i32 padding = 2, onEdgeValue = 153 /*0..255*/, globalXOffset = 0, globalYOffset = 0,
	maxHeight = 0, uvy = 0, globalMaxHeight = 0;
    CharChar* charChars = NULL;
    f32 pixelDistScale = f32(onEdgeValue) / padding;

#define GlyphProccesing()						\
    {									\
	i32 w, h, xoff, yoff;						\
	u8* singleCharacter = stbtt_GetCodepointSDF(&info, scale, i/*(i32)L'А'*/, padding, onEdgeValue, pixelDistScale, &w, &h, &xoff, &yoff); \
	i32 offset = 0, roffset = 0;					\
									\
	if ((globalXOffset + w) >= bitmapWidth)				\
	{								\
	    globalXOffset = 0;						\
	    globalYOffset += maxHeight * bitmapWidth;			\
	    uvy += maxHeight;						\
	    maxHeight = 0;						\
	}								\
									\
	if (h > maxHeight)						\
	{								\
	    maxHeight = h;						\
	}								\
									\
	if (h > globalMaxHeight)					\
	{								\
	    globalMaxHeight = h;					\
	}								\
									\
	i32 ascent, descent, lineGap;					\
	stbtt_GetFontVMetrics(&info, &ascent, &descent, &lineGap);	\
									\
	CharChar charchar = {						\
	    .UV = {							\
		.X = f32(globalXOffset) / bitmapWidth,			\
		.Y = f32(uvy) / bitmapHeight				\
	    },								\
	    .Size = {							\
		.Width  = f32(w) / bitmapWidth,				\
		.Height = f32(h) / bitmapHeight				\
	    },								\
	    .FullWidth = w,						\
	    .FullHeight = h,						\
	    .XOffset = xoff,						\
	    .YOffset = h + yoff,					\
	    .Character = i						\
	};								\
	/* printf("%lc: %d\n", i, xoff); */				\
	array_push(charChars, charchar);				\
									\
	for (i32 j = 0; j < h; ++j)					\
	{								\
	    memcpy(bitmap + globalXOffset + globalYOffset + offset, singleCharacter + roffset, w); \
	    offset += bitmapWidth;					\
	    roffset += w;						\
	}								\
	stbtt_FreeSDF(singleCharacter, NULL);				\
									\
	globalXOffset += w;						\
    }


    for (i32 i = (i32)range0; i <= (i32)range1; ++i)
    {
	GlyphProccesing();
    }

    for (i32 j = 0; j < array_count(extRanges); ++j)
    {
	i32 i = extRanges[j];
	GlyphProccesing();
    }

    i32 maxYOffset = -1.0f;
    CharRecord* charTable = NULL;
    for (i32 i = 0; i < array_count(charChars); ++i)
    {
	CharChar charchar = charChars[i];
	if (charchar.YOffset > maxYOffset)
	{
	    maxYOffset = charchar.YOffset;
	}
	hash_put(charTable, i32(charchar.Character), charchar);
    }

    Font fontInfo = {
	.Name = path_get_name(settings.Path),
	.CharTable = charTable,
	.MaxHeight = globalMaxHeight,
	.MaxYOffset = maxYOffset,
	.BitmapWidth = bitmapWidth,
	.BitmapHeight = bitmapHeight,
	.Bitmap = bitmap,
	.FontSize = settings.FontSize,
	.Scale = 1.0f
    };

    if (settings.IsSaveBitmap)
    {
	stbi_write_png(settings.BitmapSavePath, bitmapWidth, bitmapHeight, 1, bitmap, bitmapWidth);
    }

    array_free(charChars);

    return fontInfo;
}

Font
font_cirilic_create(FontInfoSettings settings)
{
    i32 cirilicRange0 = (i32)L'А', cirilicRange1 = (i32)L'я';
    vassert(cirilicRange0 > 1000 && "i32 cirilicRange0 = (i32)L'А', where A is english A!");
    vassert(cirilicRange1 > cirilicRange0 && "wrong L'я' value!");

    i32* extRanges = NULL;
    array_push(extRanges, (i32)L'Ё'); array_push(extRanges, (i32)L'ё');
    array_push(extRanges, (i32)L'0'); array_push(extRanges, (i32)L'1');
    array_push(extRanges, (i32)L'2'); array_push(extRanges, (i32)L'3');
    array_push(extRanges, (i32)L'4'); array_push(extRanges, (i32)L'5');
    array_push(extRanges, (i32)L'6'); array_push(extRanges, (i32)L'7');
    array_push(extRanges, (i32)L'8'); array_push(extRanges, (i32)L'9');

    array_push(extRanges, (i32)L'.'); array_push(extRanges, (i32)L',');
    array_push(extRanges, (i32)L'/'); array_push(extRanges, (i32)L'?');
    array_push(extRanges, (i32)L'+'); array_push(extRanges, (i32)L'!');
    array_push(extRanges, (i32)L'-'); array_push(extRanges, (i32)L'_');
    array_push(extRanges, (i32)L'=');
    array_push(extRanges, (i32)L'('); array_push(extRanges, (i32)L')');
    array_push(extRanges, (i32)L'['); array_push(extRanges, (i32)L']');
    array_push(extRanges, (i32)L'{'); array_push(extRanges, (i32)L'}');
    array_push(extRanges, (i32)L':'); array_push(extRanges, (i32)L';');
    array_push(extRanges, (i32)L'*'); array_push(extRanges, (i32)L'%');
    array_push(extRanges, (i32)L'$'); array_push(extRanges, (i32)L'^');
    array_push(extRanges, (i32)L'\"'); array_push(extRanges, (i32)L'\'');
    array_push(extRanges, (i32)L'\\'); array_push(extRanges, (i32)L'/');

    array_push(extRanges, (i32)L'A'); array_push(extRanges, (i32)L'a');
    array_push(extRanges, (i32)L'B'); array_push(extRanges, (i32)L'b');
    array_push(extRanges, (i32)L'C'); array_push(extRanges, (i32)L'c');
    array_push(extRanges, (i32)L'D'); array_push(extRanges, (i32)L'd');
    array_push(extRanges, (i32)L'E'); array_push(extRanges, (i32)L'e');
    array_push(extRanges, (i32)L'F'); array_push(extRanges, (i32)L'f');
    array_push(extRanges, (i32)L'G'); array_push(extRanges, (i32)L'g');
    array_push(extRanges, (i32)L'H'); array_push(extRanges, (i32)L'h');
    array_push(extRanges, (i32)L'I'); array_push(extRanges, (i32)L'i');
    array_push(extRanges, (i32)L'J'); array_push(extRanges, (i32)L'j');
    array_push(extRanges, (i32)L'K'); array_push(extRanges, (i32)L'k');
    array_push(extRanges, (i32)L'L'); array_push(extRanges, (i32)L'l');
    array_push(extRanges, (i32)L'M'); array_push(extRanges, (i32)L'm');
    array_push(extRanges, (i32)L'N'); array_push(extRanges, (i32)L'n');
    array_push(extRanges, (i32)L'O'); array_push(extRanges, (i32)L'o');
    array_push(extRanges, (i32)L'P'); array_push(extRanges, (i32)L'p');
    array_push(extRanges, (i32)L'Q'); array_push(extRanges, (i32)L'q');
    array_push(extRanges, (i32)L'R'); array_push(extRanges, (i32)L'r');
    array_push(extRanges, (i32)L'S'); array_push(extRanges, (i32)L's');
    array_push(extRanges, (i32)L'T'); array_push(extRanges, (i32)L't');
    array_push(extRanges, (i32)L'U'); array_push(extRanges, (i32)L'u');
    array_push(extRanges, (i32)L'V'); array_push(extRanges, (i32)L'v');
    array_push(extRanges, (i32)L'W'); array_push(extRanges, (i32)L'w');
    array_push(extRanges, (i32)L'X'); array_push(extRanges, (i32)L'x');
    array_push(extRanges, (i32)L'Y'); array_push(extRanges, (i32)L'y');
    array_push(extRanges, (i32)L'Z'); array_push(extRanges, (i32)L'z');

    Font cirilicFontInfo = font_generic_create(settings, cirilicRange0, cirilicRange1, extRanges);
    array_free(extRanges);

    return cirilicFontInfo;
}

void
font_destroy(Font fontInfo)
{
    table_free(fontInfo.CharTable);
    memory_free(fontInfo.Bitmap);
}

void
font_get_string_metrics_ext(const Font* font, wchar* buffer, i32 length, f32* w, f32* h)
{
    f32 width  = 0.0f;
    f32 height = 0.0f;
    i32 newLine = 1;

    for (i32 i = 0; i < length; ++i)
    {
	wchar wc = buffer[i];

	if (wc == L'\n' || wc == '\n')
	{
	    newLine = 1;
	}

	if (wc != L' ' && wc != ' ')
	{
	    CharChar value = hash_get(font->CharTable, (i32)wc);
	    i32 tableInd = table_index(font->CharTable);
	    if (tableInd == -1)
	    {
		wchar charBuf[2] = {
		    [0] = wc,
		    [1] = '\0'
		};
		GERROR("i: %d ind: %d wci: %d char: %ls\n", i, tableInd, wc, charBuf);
		vguard(tableInd != -1 && "No char!");
	    }
	    width  += value.FullWidth;
	    if (newLine)
	    {
		height += value.FullHeight;
		newLine = 0;
	    }
	}
	else
	{
	    f32 val = font->MaxHeight / 3 * font->Scale;
	    width  += val;
	    if (newLine)
	    {
		height += val;
		newLine = 0;
	    }
	}
    }

    *w = width;
    *h = height;
}

void
font_get_string_metrics(const Font* font, WideString str, f32* w, f32* h)
{
    font_get_string_metrics_ext(font, str.Buffer, str.Length, w, h);
}

f32
font_get_first_char_y_offset(const Font* font, WideString str)
{
    wchar wc = str.Buffer[0];
    CharChar value = hash_get(font->CharTable, (i32)wc);

    f32 yOffset = font->Scale * (value.FullHeight - value.YOffset);
    return value.YOffset;
}

void
font_get_first_char_y(const Font* font, WideString str, v3 position, f32* py0, f32* py1)
{
    wchar wc = str.Buffer[0];
    CharChar charChar = hash_get(font->CharTable, (i32)wc);
    f32 y0 = position.Y
	- font->Scale * (charChar.FullHeight + charChar.YOffset);
    f32 y1 = y0 + font->Scale * charChar.FullHeight;

    *py0 = y0;
    *py1 = y1;
}

f32
font_get_string_width(const Font* font, WideString str)
{
    f32 width, h;
    font_get_string_metrics(font, str, &width, &h);
    return width;
}

f32
font_get_string_height(const Font* font, WideString str)
{
    f32 w, height;
    font_get_string_metrics(font, str, &w, &height);
    return height;
}
