#include "SimpleThread.h"

#define WITHOUT_DEFINITION
#define LOGGER_DEFINITION
#include <Utils/SimpleStandardLibrary.h>

SimpleThread
simple_thread_create(SimpleThreadDelegate threadFunction, size_t stackSize, void* data)
{
    SimpleThread thread;

#ifdef WINDOWS_PLATFORM
    thread.ID = CreateThread(NULL, stackSize, threadFunction, NULL, 0, NULL);
#elif defined(LINUX_PLATFORM)
    pthread_attr_t threadAttributes;
    pthread_attr_init(&threadAttributes);
    i32 result = pthread_attr_setguardsize(&threadAttributes, stackSize);
    i32 status = pthread_create(&thread.ID, &threadAttributes, threadFunction, data);
    if (status != 0)
    {
	vassert(0 && "Thread creation error!");
    }
    pthread_attr_destroy(&threadAttributes);
#endif

    return thread;
}

void
simple_thread_attach(SimpleThread* thread)
{
#if defined(WINDOWS_PLATFORM)
    DWORD result = WaitForSingleObject(thread->ID, INFINITE); // NOTE(typedef): INFINITE -> no time interval, declared in some windows files
    if (result == WAIT_FAILED)
    {
	vassert(0 && "Thread can't join!");
    }
#elif defined(LINUX_PLATFORM)
    i32 status = pthread_join(thread->ID, NULL);
    if (status != 0)
    {
	vassert(0 && "Thread can't join!");
    }
#endif
}

ResultType
simple_thread_cancel(SimpleThread thread)
{
#if defined(WINDOWS_PLATFORM)
    BOOL result = TerminateThread(thread.ID, 0);
    if (result)
    {
	return ResultType_Success;
    }

    return ResultType_Error;
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_cancel(thread.ID);
    if (result == 0)
    {
	return ResultType_Success;
    }

    return ResultType_Error;
#endif
}

SimpleMutex
simple_thread_mutex_create()
{
    SimpleMutex simpleMutex;

#if defined(WINDOWS_PLATFORM)
    simpleMutex.ID = CreateMutexA(NULL, TRUE, NULL);
    if (simpleMutex.ID == NULL)
    {
	GWARNING("Can't create mutex!\n");
	vassert_break();
    }
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_mutex_init(&simpleMutex.ID, NULL);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	vassert_break();
    }
#endif

    return simpleMutex;
}

ResultType
simple_thread_mutex_destroy(SimpleMutex* simpleMutex)
{
#if defined(WINDOWS_PLATFORM)
    BOOL result = CloseHandle(simpleMutex->ID);
    if (result == FALSE)
    {
	vassert("Can't close handle!");
	return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(LINUX_PLATFORM)

    i32 result = pthread_mutex_destroy(&simpleMutex->ID);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	return ResultType_Error;
    }
    return ResultType_Success;

#endif
}

ResultType
simple_thread_mutex_lock(SimpleMutex* simpleMutex)
{
#if defined(WINDOWS_PLATFORM)
    DWORD result = WaitForSingleObject(simpleMutex->ID, INFINITE);
    if (result == WAIT_FAILED)
    {
	vassert(0 && "Thread can't join!");
	return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_mutex_lock(&simpleMutex->ID);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	return ResultType_Error;
    }
    return ResultType_Success;
#endif
}

ResultType
simple_thread_mutex_unlock(SimpleMutex* simpleMutex)
{
#if defined(WINDOWS_PLATFORM)
    BOOL result = ReleaseMutex(simpleMutex->ID);
    if (result == FALSE)
    {
	vassert(0 && "Thread can't join!");
	return ResultType_Error;
    }
    return ResultType_Success;
#elif defined(LINUX_PLATFORM)
    i32 result = pthread_mutex_unlock(&simpleMutex->ID);
    if (result != 0)
    {
	GWARNING("Can't create mutex!\n");
	return ResultType_Error;
    }
    return ResultType_Success;
#endif
}

//(), pthread_mutex_trylock(), pthread_mutex_unlock():

//#ifdef WINDOWS_PLATFORM
//#else
//#endif
