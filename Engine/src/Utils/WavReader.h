#ifndef WAV_READER_H
#define WAV_READER_H

#include <Utils/Types.h>

typedef struct WavHeader
{
    char ChunkID[4]; // RIFF
    u32 FileSize;
    char Format[4]; // WAVE
    char FmtChunkID[4]; // fmt
    u32 FmtChunkSize; // 16
    u16 AudioFormat;
    u16 ChannelsCount;
    u32 SampleRate;
    u32 ByteRate;
    u16 BlockAlign;
    u16 BitsPerSample;
} WavHeader;
typedef struct WavChunk
{
    char ID[4]; //data
    u32 Size;
} WavChunk;

typedef struct WavFile
{
    WavHeader Header;
    WavChunk Chunk;
    u8* Data;
} WavFile;

void wav_reader_header_print(WavHeader header);
WavFile wav_reader_read(const char* path);
void wav_reader_free(WavFile file);
void wav_reader_write(const char* path, WavFile wavFile);

#endif
