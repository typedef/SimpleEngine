#ifndef SIMPLE_THREAD_H
#define SIMPLE_THREAD_H

#if !defined(LINUX_PLATFORM) && !defined(WINDOWS_PLATFORM)
#error "Unsupported platform!"
#endif

#include <Utils/Types.h>

#if defined(LINUX_PLATFORM)
#include <pthread.h>
#elif defined(WINDOWS_PLATFORM)
#include <Windows.h>
#endif


typedef struct SimpleThread
{
#if defined(WINDOWS_PLATFORM)
//#error "Platform not supported!"
    HANDLE ID;
#elif defined(LINUX_PLATFORM)
    pthread_t ID;
#endif
} SimpleThread;

typedef struct SimpleMutex
{
#if defined(WINDOWS_PLATFORM)
    HANDLE ID;
#elif defined(LINUX_PLATFORM)
    pthread_mutex_t ID;
#endif
} SimpleMutex;

typedef void* (*SimpleThreadDelegate)(void* arg);

SimpleThread simple_thread_create(SimpleThreadDelegate threadFunction, size_t stackSize, void* data);
void simple_thread_attach(SimpleThread* thread);
ResultType simple_thread_cancel(SimpleThread thread);

SimpleMutex simple_thread_mutex_create();
ResultType simple_thread_mutex_destroy(SimpleMutex* simpleMutex);
ResultType simple_thread_mutex_lock(SimpleMutex* simpleMutex);
ResultType simple_thread_mutex_unlock(SimpleMutex* simpleMutex);

#endif
