#include "SimpleInput.h"

#include <GLFW/glfw3.h>

static GLFWwindow* InputWindow = NULL;

void
input_init(void* window)
{
    InputWindow = (GLFWwindow*) window;
}

i8
input_is_key_pressed(KeyType type)
{
    vassert_not_null(InputWindow);
    ActionType state = glfwGetKey(InputWindow, type);
    return (state == ActionType_Press);
}

i8
input_is_key_release(KeyType type)
{
    vassert_not_null(InputWindow);
    ActionType state = glfwGetKey(InputWindow, type);
    return (state == ActionType_Release);
}

i8
input_is_mouse_pressed(MouseButtonType type)
{
    vassert_not_null(InputWindow);
    ActionType state = glfwGetMouseButton(InputWindow, type);
    return (state == ActionType_Press);
}

void
input_get_cursor_position(f64* xpos, f64* ypos)
{
    vassert_not_null(InputWindow);
    glfwGetCursorPos(InputWindow, xpos, ypos);
}

void
input_set_cursor_position(f64 xpos, f64 ypos)
{
    vassert_not_null(InputWindow);
    glfwSetCursorPos(InputWindow, xpos, ypos);
}

v2
input_get_cursor_position_v2()
{
    f64 x, y;
    input_get_cursor_position(&x, &y);

    v2 cursorPosition = {
	.X = x,
	.Y = y
    };
    return cursorPosition;
}

void
input_enable_cursor()
{
    input_set_cursor_mode(CursorMode_Normal);
}

void
input_disable_cursor()
{
    input_set_cursor_mode(CursorMode_Locked);
}

void
input_set_cursor_mode(CursorMode mode)
{
    vassert_not_null(InputWindow);
    glfwSetInputMode(InputWindow, GLFW_CURSOR, GLFW_CURSOR_NORMAL + mode);
}

i8
input_is_cursor_disabled()
{
    vassert_not_null(InputWindow);
    return glfwGetInputMode(InputWindow, GLFW_CURSOR) == GLFW_CURSOR_DISABLED;
}
