#include "Entity.h"

Entity
entity_create(EntityID id, EntityID parent, EntityID* childs)
{
    Entity entity = {
	.ID = id,
	.Parent = parent,
	.Childs = childs
    };

    return entity;
}
