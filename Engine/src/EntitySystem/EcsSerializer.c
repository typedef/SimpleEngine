#include "EcsSerializer.h"

#include "SimpleEcs.h"

#include <Application/Application.h>
#include <AssetManager/AssetManager.h>
#include <Graphics/SimpleWindow.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>
#include <EntitySystem/Components/AllComponents.h>
#include <EntitySystem/EcsScene.h>
#include <Utils/SimpleStandardLibrary.h>
#include <Utils/JsonParser.h>

/*
  ######################################
  REVIEW(typedef): Serialization stuff
  ######################################
*/

static JsonValue
v3_to_json_value(v3 value)
{
    JsonObject* jsonObject = json_object_create();
    array_push(jsonObject->Keys, "X");
    array_push(jsonObject->Values, JSON_FLOAT(value.X));
    array_push(jsonObject->Keys, "Y");
    array_push(jsonObject->Values, JSON_FLOAT(value.Y));
    array_push(jsonObject->Keys, "Z");
    array_push(jsonObject->Values, JSON_FLOAT(value.Z));

    JsonValue result = JSON_OBJECT(jsonObject);

    return result;
}

static JsonValue
camera_base_to_json_value(Camera camera)
{
    JsonValue viewJsonValue = JSON_F32_ARRAY_NEW(camera.View.M, 16);
    JsonValue projectionJsonValue = JSON_F32_ARRAY_NEW(camera.Projection.M, 16);
    JsonValue viewProjectionJsonValue = JSON_F32_ARRAY_NEW(camera.ViewProjection.M, 16);

    JsonObject* cameraJsonObject = json_object_create();
    array_push(cameraJsonObject->Keys, "View");
    array_push(cameraJsonObject->Values, viewJsonValue);
    array_push(cameraJsonObject->Keys, "Projection");
    array_push(cameraJsonObject->Values, projectionJsonValue);
    array_push(cameraJsonObject->Keys, "ViewProjection");
    array_push(cameraJsonObject->Values, viewProjectionJsonValue);

    JsonValue result = JSON_OBJECT(cameraJsonObject);

    return result;
}

static JsonValue
camera_component_settings_to_json_value(CameraComponentSettings settings)
{
    JsonObject* settingsObject = json_object_create();
    array_push(settingsObject->Keys, "Type");
    array_push(settingsObject->Values, JSON_INT(settings.Type));
    array_push(settingsObject->Keys, "SensitivityHorizontal");
    array_push(settingsObject->Values, JSON_FLOAT(settings.SensitivityHorizontal));
    array_push(settingsObject->Keys, "SensitivityVertical");
    array_push(settingsObject->Values, JSON_FLOAT(settings.SensitivityVertical));
    array_push(settingsObject->Keys, "Position");
    array_push(settingsObject->Values, v3_to_json_value(settings.Position));
    array_push(settingsObject->Keys, "Front");
    array_push(settingsObject->Values, v3_to_json_value(settings.Front));
    array_push(settingsObject->Keys, "Up");
    array_push(settingsObject->Values, v3_to_json_value(settings.Up));
    array_push(settingsObject->Keys, "Right");
    array_push(settingsObject->Values, v3_to_json_value(settings.Right));

    JsonValue result = JSON_OBJECT(settingsObject);

    return result;
}

static JsonValue
perspective_settings_to_json_value(PerspectiveSettings perspective)
{
    JsonObject* pJsonObject = json_object_create();
    array_push(pJsonObject->Keys, "Near");
    array_push(pJsonObject->Values, JSON_FLOAT(perspective.Near));
    array_push(pJsonObject->Keys, "Far");
    array_push(pJsonObject->Values, JSON_FLOAT(perspective.Far));
    array_push(pJsonObject->Keys, "AspectRatio");
    array_push(pJsonObject->Values, JSON_FLOAT(perspective.AspectRatio));
    array_push(pJsonObject->Keys, "Fov");
    array_push(pJsonObject->Values, JSON_FLOAT(perspective.Fov));

    JsonValue result = JSON_OBJECT(pJsonObject);

    return result;
}

static JsonValue
orthographic_settings_to_json_value(OrthographicSettings orthographic)
{
    JsonObject* oJsonObject = json_object_create();
    array_push(oJsonObject->Keys, "Near");
    array_push(oJsonObject->Values, JSON_FLOAT(orthographic.Near));
    array_push(oJsonObject->Keys, "Far");
    array_push(oJsonObject->Values, JSON_FLOAT(orthographic.Far));
    array_push(oJsonObject->Keys, "Left");
    array_push(oJsonObject->Values, JSON_INT(orthographic.Left));
    array_push(oJsonObject->Keys, "Right");
    array_push(oJsonObject->Values, JSON_INT(orthographic.Right));
    array_push(oJsonObject->Keys, "Bot");
    array_push(oJsonObject->Values, JSON_INT(orthographic.Bot));
    array_push(oJsonObject->Keys, "Top");
    array_push(oJsonObject->Values, JSON_INT(orthographic.Top));

    JsonValue result = JSON_OBJECT(oJsonObject);
    return result;
}

static JsonValue
spectator_settings_to_json_value(SpectatorSettings spectator)
{

    JsonObject* oJsonObject = json_object_create();
    array_push(oJsonObject->Keys, "Speed");
    array_push(oJsonObject->Values, JSON_FLOAT(spectator.Speed));
    array_push(oJsonObject->Keys, "Zoom");
    array_push(oJsonObject->Values, JSON_FLOAT(spectator.Zoom));

    JsonValue result = JSON_OBJECT(oJsonObject);
    return result;

}

static JsonValue
camera_component_to_json_value(CameraComponent cc)
{

    JsonObject* ccJsonObject = json_object_create();

    array_push(ccJsonObject->Keys, "Base");
    array_push(ccJsonObject->Values, camera_base_to_json_value(cc.Base));
    array_push(ccJsonObject->Keys, "IsMainCamera");
    array_push(ccJsonObject->Values, JSON_INT(cc.IsMainCamera));
    array_push(ccJsonObject->Keys, "IsCameraMoved");
    array_push(ccJsonObject->Values, JSON_INT(cc.IsCameraMoved));
    array_push(ccJsonObject->Keys, "Settings");
    array_push(ccJsonObject->Values, camera_component_settings_to_json_value(cc.Settings));
    array_push(ccJsonObject->Keys, "Perspective");
    array_push(ccJsonObject->Values, perspective_settings_to_json_value(cc.Perspective));
    // NOTE(typedef): we are here
    array_push(ccJsonObject->Keys, "Orthographic");
    array_push(ccJsonObject->Values, orthographic_settings_to_json_value(cc.Orthographic));
    array_push(ccJsonObject->Keys, "Spectator");
    array_push(ccJsonObject->Values, spectator_settings_to_json_value(cc.Spectator));

    JsonValue result = JSON_OBJECT(ccJsonObject);

    return result;
}

static JsonValue
transform_component_to_json_value(TransformComponent tc)
{
    JsonObject* transformObject = json_object_create();

    // NOTE(typdef): use only trs, we'll build TransformMatrix from this
    array_push(transformObject->Keys, "Translation");
    array_push(transformObject->Values, v3_to_json_value(tc.Translation));
    array_push(transformObject->Keys, "Rotation");
    array_push(transformObject->Values, v3_to_json_value(tc.Rotation));
    array_push(transformObject->Keys, "Scale");
    array_push(transformObject->Values, v3_to_json_value(tc.Scale));

    JsonValue result = JSON_OBJECT(transformObject);
    return result;
}

static JsonValue
static_model_to_json_value(StaticModel m)
{
    JsonObject* obj = json_object_create();

    // TODO(typedef): serialize as binary data
    array_push(obj->Keys, "Type");
    array_push(obj->Values, JSON_INT(m.Type));
    array_push(obj->Keys, "Name");
    array_push(obj->Values, JSON_STRING(m.Name));

    if (m.Type & ModelType_Loaded)
    {
	array_push(obj->Keys, "Path");
	array_push(obj->Values, JSON_STRING(m.Path));
    }

    JsonValue result = JSON_OBJECT(obj);

    return result;
}

static JsonValue
static_model_component_to_json_value(StaticModelComponent smc)
{

    JsonObject* obj = json_object_create();

    JsonValue val = static_model_to_json_value(smc.Model);

    array_push(obj->Keys, "Model");
    array_push(obj->Values, val);

    JsonValue result = JSON_OBJECT(obj);

    return result;
}

static JsonValue
skybox_component_to_json_value(SkyboxComponent sc)
{
    JsonObject* obj = json_object_create();

    array_push(obj->Keys, "Path");
    array_push(obj->Values, JSON_STRING(sc.Path));

    JsonValue result = JSON_OBJECT(obj);

    return result;
}

void
ecs_serializer_write(void* ecsWorldAsVoidPtr, const char* fileName)
{
    EcsWorld* ecsWorld = (EcsWorld*) ecsWorldAsVoidPtr;

    //Arena* arena = arena_create_and_set(MB(10));

    ComponentId transformId   = EcsComponentGetId(TransformComponent);
    ComponentId cameraId      = EcsComponentGetId(CameraComponent);
    ComponentId staticModelId = EcsComponentGetId(StaticModelComponent);
    ComponentId skyboxId = EcsComponentGetId(SkyboxComponent);

    JsonObject* root = json_object_create();

    //array_push(root->Keys, "LastId");
    //array_push(root->Values, JSON_INT(ecsWorld->EStorage.LastId));
    //array_push(root->Keys, "Deleted");
    //array_push(root->Values, JSON_I32_ARRAY(ecsWorld->EStorage.Deleted));

    // NOTE(typedef): We to compose all entities before serialization
    EntityId endId = array_count(ecsWorld->EStorage.Records) + EcsIdType_EntityFirst;
    for (EntityId id = EcsIdType_EntityFirst; id < endId; ++id)
    {
	if (ecsWorld->EStorage.Records[id].Id == -1)
	    continue;

	JsonObject* entityObj = json_object_create();
	EntityRecord eRecord = ecsWorld->EStorage.Records[GetEntityAsArrayIndex(id)];

	array_push(entityObj->Keys, "Id");
	array_push(entityObj->Values, JSON_INT(eRecord.Id));
	array_push(entityObj->Keys, "Parent");
	array_push(entityObj->Values, JSON_INT(eRecord.Parent));
	array_push(entityObj->Keys, "Childs");
	array_push(entityObj->Values, JSON_I32_ARRAY(eRecord.Childs));

	if (HasComponentByIdExt(ecsWorld, id, transformId))
	{
	    TransformComponent* tc = (TransformComponent*) GetComponentByIdExt(ecsWorld, id, transformId);
	    array_push(entityObj->Keys, "TransformComponent");
	    array_push(entityObj->Values, transform_component_to_json_value(*tc));
	}

	if (HasComponentByIdExt(ecsWorld, id, cameraId))
	{
	    CameraComponent* cc = (CameraComponent*) GetComponentByIdExt(ecsWorld, id, cameraId);
	    array_push(entityObj->Keys, "CameraComponent");
	    array_push(entityObj->Values, camera_component_to_json_value(*cc));
	}

	if (HasComponentByIdExt(ecsWorld, id, staticModelId))
	{
	    StaticModelComponent* smc = (StaticModelComponent*) GetComponentByIdExt(ecsWorld, id, staticModelId);
	    array_push(entityObj->Keys, "StaticModelComponent");
	    array_push(entityObj->Values, static_model_component_to_json_value(*smc));
	}

	if (HasComponentByIdExt(ecsWorld, id, skyboxId))
	{
	    SkyboxComponent* sc = (SkyboxComponent*)
		GetComponentByIdExt(ecsWorld, id, skyboxId);
	    array_push(entityObj->Keys, "SkyboxComponent");
	    array_push(entityObj->Values, skybox_component_to_json_value(*sc));
	}

	array_push(root->Keys, eRecord.Name);
	array_push(root->Values, JSON_OBJECT(entityObj));
    }

    vassert_not_null(root->Keys);
    vassert_not_null(root->Values);

    char* result = json_object_to_string(root, 0);
    file_write_string(fileName, result, string_builder_count(result));

    //arena_print(arena);
    //arena_destroy(arena);
}

/*
  ######################################
  REVIEW(typedef): Deserialization stuff
  ######################################
*/

i64
i64_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Int && "Wrong data type!");
    i64 result = *((i64*)value.Data);
    return result;
}

f32
f32_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Float && "Wrong data type!");
    f32 result = *((f32*)value.Data);
    return result;
}

i8
bool_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Bool && "Wrong data type!");

    i8 result = (i8) (*((i32*)value.Data));
    return result;
}

void*
null_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Null && "Wrong data type!");
    return NULL;
}

char*
string_from_json_value(JsonValue value)
{
    json_check_for_type(value, JsonValueType_String);
    char* result = (char*)value.Data;
    return result;
}

i64*
i64a_from_json_value(JsonValue value)
{
    if (value.Data == NULL)
	return NULL;

    json_check_for_type(value, JsonValueType_IntArray);
    i64* result = (i64*)value.Data;
    return result;
}

f32*
f32a_from_json_value(JsonValue value)
{
    json_check_for_type(value, JsonValueType_FloatArray);
    f32* result = (f32*)value.Data;
    return result;
}

char**
stringa_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_StringArray && "Wrong data type!");
    char** result = (char**)value.Data;
    return result;
}

static m4
m4_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_FloatArray && "Wrong data type!");
    f32* farr = (f32*) value.Data;
    m4 result = m4_copy_array(farr);

    return result;
}

static v3
v3_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;;

    vassert(string_compare(obj->Keys[0], "X"));
    vassert(string_compare(obj->Keys[1], "Y"));
    vassert(string_compare(obj->Keys[2], "Z"));

    v3 result = {
	.X = (f32) f32_from_json_value(obj->Values[0]),
	.Y = (f32) f32_from_json_value(obj->Values[1]),
	.Z = (f32) f32_from_json_value(obj->Values[2])
    };

    return result;
}

static Camera
camera_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    Camera result = {
	.View = m4_from_json_value(obj->Values[0]),
	.Projection = m4_from_json_value(obj->Values[1]),
	.ViewProjection = m4_from_json_value(obj->Values[2])
    };

    return result;
}

static CameraComponentSettings
camera_component_settings_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    CameraComponentSettings result = {
	.Type = (CameraType) i64_from_json_value(obj->Values[0]),
	.SensitivityHorizontal = f32_from_json_value(obj->Values[1]),
	.SensitivityVertical = f32_from_json_value(obj->Values[2]) ,
	.Yaw   = 0.0f,
	.Pitch = 0.0f,
	.PrevX = 0.0f,
	.PrevY = 0.0f,
	.Position = v3_from_json_value(obj->Values[3]),
	.Front = v3_from_json_value(obj->Values[4]),
	.Up = v3_from_json_value(obj->Values[5]),
	.Right = v3_from_json_value(obj->Values[6])
    };

    return result;
}

static PerspectiveSettings
perspective_settings_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    PerspectiveSettings result = {
	.Near = f32_from_json_value(obj->Values[0]),
	.Far = f32_from_json_value(obj->Values[1]),
	.AspectRatio = f32_from_json_value(obj->Values[2]),
	.Fov = f32_from_json_value(obj->Values[3])
    };

    return result;
}

static OrthographicSettings
orthographic_settings_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    OrthographicSettings result = {
	.Near = f32_from_json_value(obj->Values[0]),
	.Far = f32_from_json_value(obj->Values[1]),
	.Left = (u32) i64_from_json_value(obj->Values[2]),
	.Right = (u32) i64_from_json_value(obj->Values[3]),
	.Bot = (u32) i64_from_json_value(obj->Values[4]),
	.Top = (u32) i64_from_json_value(obj->Values[5])
    };

    return result;
}

static SpectatorSettings
spectator_settings_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    SpectatorSettings result = {
	.Speed = f32_from_json_value(obj->Values[0]),
	.Zoom = f32_from_json_value(obj->Values[1])
    };

    return result;
}

static CameraComponent
camera_component_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    CameraComponent result = {
	.Base = camera_from_json_value(obj->Values[0]),
	.IsMainCamera =  (i8) i64_from_json_value(obj->Values[1]),
	.IsCameraMoved = (i8) i64_from_json_value(obj->Values[2]),
	.Settings = camera_component_settings_from_json_value(obj->Values[3]),
	.Perspective = perspective_settings_from_json_value(obj->Values[4]),
	.Orthographic = orthographic_settings_from_json_value(obj->Values[5]),
	.Spectator = spectator_settings_from_json_value(obj->Values[6])
    };

    return result;
}

static TransformComponent
transform_component_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");
    JsonObject* obj = (JsonObject*) value.Data;

    v3 translation = v3_from_json_value(obj->Values[0]);
    v3 rotation = v3_from_json_value(obj->Values[1]);
    v3 scale = v3_from_json_value(obj->Values[2]);

    TransformComponent result = transform_component_new(translation, rotation, scale);

    return result;
}

static StaticModel
static_model_from_json_value(JsonValue value)
{
    json_check_for_type(value, JsonValueType_Object);

    JsonObject* obj = (JsonObject*) value.Data;

    /*
      ModelType_Loaded = 1 << 0,
      ModelType_Downscaled = 1 << 1,
      ModelType_Generated = 1 << 2,
      ModelType_Generated_Cube = 1 << 3
    */
    ModelType modelType = (ModelType) i64_from_json_value(obj->Values[0]);

    StaticModel staticModel;
    Texture2D* defaultTexture = (Texture2D*)
	asset_manager_get(AssetType_Texture, 0);
    if (modelType == ModelType_Loaded)
    {
	char* path = string_from_json_value(obj->Values[1]);
	// ResourceType_Texture
	staticModel = static_model_load(path);
    }
    else if (modelType & ModelType_Generated_Cube)
    {
	staticModel = static_model_cube_create();
    }
    else
    {
	vassert_break();
    }

    return staticModel;
}

static StaticModelComponent
static_model_component_from_json_value(JsonValue value)
{
    vassert(value.Type == JsonValueType_Object && "Wrong data type (not object)!");

    //ModelType type = (ModelType) i64_from_json_value(obj->Values[0]);
    //char* name = string_from_json_value(obj->Values[1]);

    StaticModelComponent result = {
	.Model = static_model_from_json_value(value)
    };

    return result;
}

JsonValue*
GetJsonValues(JsonValue compValue)					    {
    JsonObject* obj = (JsonObject*) compValue.Data;

    vguard_not_null(obj && "compValue.Data is NULL!");
#if STATIC_ANALIZER_CHECK == 1
    if (!obj) return NULL;
#endif

    vguard_not_null(obj->Values && "((JsonObject*)compValue.Data)->Values is NULL!");
#if STATIC_ANALIZER_CHECK == 1
    if (!obj->Values) return NULL;
#endif

    return obj->Values;
}


void*
ecs_serializer_read(const char* path)
{
    scene_create();
    EcsWorld* ecsWorld = (EcsWorld*) scene_get_world();

    JsonParser parser;
    json_parse_file(&parser, path);

    JsonObject* root = parser.Object;
    { // LastId, Deleted
	//vassert(string_compare(root->Keys[0], "LastId"));
	//vassert(string_compare(root->Keys[1], "Deleted"));

	//ecsWorld->EStorage.LastId = i64_from_json_value(root->Values[0]);
	//ecsWorld->EStorage.Deleted = i64a_from_json_value(root->Values[1]);
    }

    i32 i, entitiesCount = array_count(root->Keys);
    for (i = 0; i < entitiesCount; ++i)
    {
	// NOTE(typedef): key == EntityRecord.Name
	const char* key = root->Keys[i];
	// NOTE(typedef): JsonObject with keys && values
	JsonValue value = root->Values[i];
	// LEGACY(typedef): Entity entity = scene_entity_create(scene, key);
	EntityId entity = ecs_world_entity(ecsWorld, (char*)key);

	/* vassert(value.Type == JSON_VALUE_TYPE_OBJECT && "root->Values should be JSON_VALUE_TYPE_OBJECT!"); */

	JsonObject* compObj = (JsonObject*) value.Data;
	i32 c, compsCount = array_count(compObj->Keys);
	for (c = 0; c < compsCount; ++c)
	{
	    const char* compKey = compObj->Keys[c];
	    JsonValue compValue = compObj->Values[c];

	    if (string_compare(compKey, "TransformComponent"))
	    {
		TransformComponent tc =
		    transform_component_from_json_value(compValue);

		ecs_world_entity_add_component(ecsWorld, entity, TransformComponent, tc);
	    }
	    else if (string_compare(compKey, "CameraComponent"))
	    {
		JsonValue* vals = GetJsonValues(compValue);

		Camera base = camera_from_json_value(vals[0]);
		i8 isMainCamera = ((i8) *((i32*)vals[1].Data));
		i8 isCameraMoved = ((i8) *((i32*)vals[2].Data));
		CameraComponentSettings settings =
		    camera_component_settings_from_json_value(vals[3]);
		PerspectiveSettings perspective =
		    perspective_settings_from_json_value(vals[4]);
		OrthographicSettings orthographic =
		    orthographic_settings_from_json_value(vals[5]);
		SpectatorSettings spectator =
		    spectator_settings_from_json_value(vals[6]);

		CameraComponent cc = {
		    .Base = base,
		    .IsMainCamera = isMainCamera,
		    .IsCameraMoved = isCameraMoved,
		    .Settings = settings,
		    .Perspective = perspective,
		    .Orthographic = orthographic,
		    .Spectator = spectator,
		};

		ecs_world_entity_add_component(ecsWorld, entity, CameraComponent, cc);
	    }
	    else if (string_compare(compKey, "StaticModelComponent"))
	    {
		JsonValue* vals = GetJsonValues(compValue);

		StaticModelComponent smc =
		    static_model_component_from_json_value(vals[0]);

		ecs_world_entity_add_component(ecsWorld, entity, StaticModelComponent, smc);
	    }
	    /* else if (0 && string_compare(compKey, "SkyboxComponent")) */
	    /* { */
	    /*	JsonValue* vals = GetJsonValues(compValue); */

	    /*	char* path = string_from_json_value(vals[0]); */
	    /*	SkyboxComponent sc = skybox_component_new((const char*)path); */

	    /*	ecs_world_entity_add_component(ecsWorld, entity, SkyboxComponent, sc); */
	    /* } */
	}
    }

    void* result = (void*) ecsWorld;

    return result;
}
