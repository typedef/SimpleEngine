#ifndef TEST_SCENE_H
#define TEST_SCENE_H

static void
create_custom_cube_entity()
{
    TransformComponent tc = TransformComponent_Position(0,0,0);
#if 0
    StaticModelComponent staticModelComponent =
	static_model_component_create(
	    asset_model("Duck/Duck.gltf"));
#else
    StaticModelComponent staticModelComponent =
	static_model_component_create_cube();
#endif

    EcsWorld* world = scene_get_world();
    EntityId modelEntity = ecs_world_entity(world, "SomeModelForTesting");
    ecs_entity_add_component(modelEntity, StaticModelComponent, staticModelComponent);
    ecs_entity_add_component(modelEntity, TransformComponent, tc);

    //SelectedEntity = &world->EStorage.Records[GetEntityAsArrayIndex(modelEntity)];
}


#endif // TEST_SCENE_H
