#ifndef TRANSFORM_COMPONENT_H
#define TRANSFORM_COMPONENT_H

#include <Utils/Types.h>

typedef struct TransformComponent
{
    i8 ShouldBeUpdated;
    v3 Translation; // 3 * 4
    v3 Rotation;
    v3 Scale;

    m4 Matrix;
} TransformComponent;

#define TransformComponent_(position, scale, rotation) transform_component_new(position, scale, rotation)
#define TransformComponent_Position(x, y, z) transform_component_new(v3_new(x, y, z), v3_new(0, 0, 0), v3_new(1.0f, 1.0f, 1.0f))
#define TransformComponent_PositionV3(xyz) transform_component_new(xyz, v3_new(0,0,0), v3_new(1.0f, 1.0f, 1.0f))

TransformComponent transform_component_new(v3 translation, v3 rotation, v3 scale);


#endif // TRANSFORM_COMPONENT_H
