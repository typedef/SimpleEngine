#include "CameraComponent.h"

#include <Math/SimpleMath.h>
#include <Utils/SimpleStandardLibrary.h>

CameraComponentSettings
camera_component_settings_new(CameraType type)
{
    CameraComponentSettings settings = (CameraComponentSettings) {
	.Type = type,
	.Position = v3_new(0, 0, 0),
	.Front = v3_new(0, 0, -1),
	.Up    = v3_new(0, 1,  0),
	.Right = v3_new(1, 0,  0),
	.Yaw = 0,
	.Pitch = 0,
	.PrevX = 0,
	.PrevY = 0,
	.SensitivityHorizontal = 0.35f,
	.SensitivityVertical = 0.45f
    };

    return settings;
}

CameraComponent
camera_component_new(CameraType type)
{
    CameraComponentCreateInfo createInfo = {
	.IsMainCamera = 0,
	.Base = camera_new_default(),
	.Settings = camera_component_settings_new(type)
    };

    CameraComponent camera =
	camera_component_new_ext(createInfo);

    return camera;
}

CameraComponent
camera_component_new_ext(CameraComponentCreateInfo createInfo)
{
    CameraComponent camera = {
	.IsMainCamera = createInfo.IsMainCamera,
	.IsCameraMoved = createInfo.IsCameraMoved,
	.Base = createInfo.Base,
	.Settings = createInfo.Settings,
	.Perspective = createInfo.Perspective,
	.Orthographic = createInfo.Orthographic,
	.Spectator = createInfo.Spectator
    };

    camera.Base.View =
	m4_translate_identity(createInfo.Settings.Position);

    if (camera.Settings.Type & CameraType_Spectator)
    {
	camera_component_set_spectator(&camera);
	GINFO("[Camera component] Set to spectator!\n");
    }

    if (camera.Settings.Type & CameraType_Perspective)
    {
	camera_component_set_perspective(&camera);
	GINFO("[Camera component] Set to perspective!\n");
    }

    if (camera.Settings.Type & CameraType_Orthographic)
    {
	camera_component_set_orthograhic(&camera);
	GINFO("[Camera component] Set to orthograhic!\n");
    }

    return camera;
}

void
camera_component_set_perspective(CameraComponent* camera)
{
    camera->Settings.Type |=  CameraType_Perspective ;
    camera->Settings.Type &= ~CameraType_Orthographic;

    PerspectiveSettings set = camera->Perspective;
    m4 projection = perspective(set.Near, set.Far, set.AspectRatio, set.Fov);
    camera_update_projection((Camera*) &camera->Base, projection);
    // NOTE(typedef): mb camera_component_update()
}

void
camera_component_set_orthograhic(CameraComponent* camera)
{
    camera->Settings.Type |=  CameraType_Orthographic;
    camera->Settings.Type &= ~CameraType_Perspective ;

    OrthographicSettings set = camera->Orthographic;
    m4 projection = orthographic(set.Left, set.Right, set.Bot, set.Top, set.Near, set.Far);
    camera_update_projection((Camera*) &camera->Base, projection);
    // NOTE(typedef): mb camera_component_update()
}

void
camera_component_set_spectator(CameraComponent* camera)
{
    camera->Settings.Type |=  CameraType_Spectator;
    camera->Settings.Type &= ~CameraType_Arcball  ;
}

void
camera_component_update(CameraComponent* camera)
{
    CameraType type = camera->Settings.Type;
    Camera* base = (Camera*) camera;
    CameraComponentSettings set = camera->Settings;

    // NOTE(typedef): update view
    if (camera->IsCameraMoved)
    {
	if (type & CameraType_Arcball)
	{
	    //base->View = view_get2(set.Position, set.Orientation);
	    vassert_break();
	}
	else if (type == CameraType_PerspectiveSpectator)
	{
	    base->View = m4_look_at(set.Position, v3_add(set.Position, set.Front), set.Up);
	}
	else if (type == CameraType_OrthographicSpectator)
	{
	    base->View = m4_translate_identity(set.Position);
	}
	else
	{
	    vassert_break();
	}
	vguard(camera_is_valid(base) == 1);

	// NOTE(typedef): update projection
	base->ViewProjection = m4_mul(base->Projection, base->View);

	camera->IsCameraMoved = 0;
    }
}

void
camera_component_move(CameraComponent* camera, CameraMoveDirectionType direction, f32 timestep)
{
    CameraType type = camera->Settings.Type;
    if (type & CameraType_Spectator)
    {
	CameraComponentSettings* base = &camera->Settings;
	SpectatorSettings set = camera->Spectator;
	f32 velocity = set.Speed * timestep;
	camera->IsCameraMoved = 1;

	switch (direction)
	{
	case CameraMoveDirectionType_Forward:
	    base->Position = v3_add(base->Position, v3_scale(base->Front, velocity));
	    break;
	case CameraMoveDirectionType_Backward:
	    base->Position = v3_sub(base->Position, v3_scale(base->Front, velocity));
	    break;
	case CameraMoveDirectionType_Left:
	    base->Position = v3_sub(base->Position, v3_scale(base->Right, velocity));
	    break;
	case CameraMoveDirectionType_Right:
	    base->Position = v3_add(base->Position, v3_scale(base->Right, velocity));
	    break;

	case CameraMoveDirectionType_Up:
	    base->Position = v3_add(base->Position, v3_new(0.0, 1.0f * velocity, 0.0f));
	    break;
	case CameraMoveDirectionType_Down:
	    base->Position = v3_add(base->Position, v3_new(0.0, -1.0f * velocity, 0.0f));
	    break;

	default:
	    camera->IsCameraMoved = 0;
	    break;
	}
    }
    else if (type & CameraType_Arcball)
    {
	/* v3 forward = get_front_direction(camera); */
	/* v3 forwardDistance = v3_mulv(forward, 1/\* camera->Distance *\/); */
	/* camera->Position = v3_sub(camera->LookAtPoint, forwardDistance); */
    }
}

void
camera_component_rotate(CameraComponent* camera, f64 mx, f64 my, f32 timestep)
{
    CameraComponentSettings* set = &camera->Settings;

    /*
      NOTE(typedef): spectator behaviour
    */
    if (set->PrevX == 0.0f && set->PrevY == 0.0f)
    {
	set->PrevX = mx;
	set->PrevY = my;

	return;
    }

    f64 dx = mx - set->PrevX;
    f64 dy = my - set->PrevY;
    //TODO(typedef): find more accurate value
    const f32 scv = 0.1f;
    set->Yaw   = dx * set->SensitivityHorizontal * timestep * scv;
    set->Pitch = dy * set->SensitivityVertical * timestep * scv;

    set->PrevX = mx;
    set->PrevY = my;

    // GINFO("[dx, dy] [%f %f]\n", dx, dy);
    // GINFO("[mx, my] [%f %f]\n", mx, my);
    // GINFO("[px, py] [%f %f]\n", set->PrevX, set->PrevY);
    if (!f32_equal(dx, 0.0f) || !f32_equal(dy, 0.0f))
    {
	set->Up = v3_new(0, 1, 0);
	set->Right = v3_cross(set->Front, set->Up);
	quat qpitch = quat_new(-set->Pitch, set->Right);
	quat qyaw = quat_new(-set->Yaw, set->Up);
	quat q = quat_normalize(quat_mul(qpitch, qyaw));
	set->Front = quat_rotate_v3(q, set->Front);

	camera->IsCameraMoved = 1;
    }
}
