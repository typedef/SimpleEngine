#ifndef SPRITE_COMPONENT_H
#define SPRITE_COMPONENT_H

#include <Graphics/OpenGLBase.h>

typedef struct SpriteComponent
{
    i32 IsTextured;
    v4 Color;
    Texture2D Texture;
} SpriteComponent;

#define SpriteComponent(color, texture) _sprite_component(color, texture)
#define SpriteComponent_Color(color) _sprite_component_color(color)
#define SpriteComponent_ColorXYZW(x, y, z, w) _sprite_component_color(v4_new(x, y, z, w))
#define SpriteComponent_Texture(texture) _sprite_component_texture(texture)

SpriteComponent _sprite_component(v4 color, Texture2D texture);
SpriteComponent _sprite_component_color(v4 color);
SpriteComponent _sprite_component_texture(Texture2D texture);

#endif
