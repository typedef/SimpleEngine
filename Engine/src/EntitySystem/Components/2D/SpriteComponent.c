#include "SpriteComponent.h"

#include <Math/SimpleMath.h>

SpriteComponent
_sprite_component(v4 color, Texture2D texture)
{
    SpriteComponent component;
    component.IsTextured = 1;
    component.Texture = texture;
    component.Color = v4_copy(color);
    return component;
}

SpriteComponent
_sprite_component_color(v4 color)
{
    SpriteComponent component;
    component.IsTextured = 0;
    component.Texture = (Texture2D) {0};
    component.Color = v4_copy(color);
    return component;
}

SpriteComponent
_sprite_component_texture(Texture2D texture)
{
    SpriteComponent component;
    component.IsTextured = 1;
    component.Texture = texture;
    component.Color = v4_new(1.0f, 1.0f, 1.0f, 1.0f);
    return component;
}
