#ifndef BASE_LIGHT_H
#define BASE_LIGHT_H

#include <Utils/Types.h>

typedef struct BaseLight
{
    v3  Ambient;
    v3  Diffuse;
    v3  Specular;
} BaseLight;

#endif
