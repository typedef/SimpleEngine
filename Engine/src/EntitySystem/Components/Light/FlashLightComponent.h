#ifndef FLASH_LIGHT_COMPONENT_H
#define FLASH_LIGHT_COMPONENT_H

#include "BaseLight.h"

typedef struct FlashLightComponent
{
    BaseLight Base;
    v3 Color;
    v3 Direction;
    v3 Position;
    f32 Constant;
    f32 Linear;
    f32 Quadratic;
    f32 CutOff; // For FlashLight only, cos(rad(angle))
    f32 OuterCutOff; // For FlashLight only, cos(rad(angle))
} FlashLightComponent;

#endif
