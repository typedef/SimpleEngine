#ifndef MODEL_COMPONENT_H
#define MODEL_COMPONENT_H

#include <Model/StaticModelExt.h>

typedef struct StaticModelComponent
{
    StaticModel Model;
} StaticModelComponent;

StaticModelComponent static_model_component_create(const char* path);
StaticModelComponent static_model_component_create_cube();

#endif // MODEL_COMPONENT_H
