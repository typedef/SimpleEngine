#include "SkyboxComponent.h"

#include <Graphics/OpenGLBase.h>
#include <Utils/SimpleStandardLibrary.h>

SkyboxComponent
skybox_component_new(const char* path)
{
    SkyboxComponent sky = {
	.Cubemap = cube_map_create(path).ID,
	.Path = string(path)
    };

    return sky;
}
