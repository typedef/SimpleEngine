#include "StaticModelComponent.h"

#include <AssetManager/AssetManager.h>
#include <Graphics/Vulkan/VulkanSimpleApi.h>

StaticModelComponent
static_model_component_create(const char* path)
{
    StaticModelComponent result = {
	.Model = static_model_load(path)
    };
    return result;
}

StaticModelComponent
static_model_component_create_cube()
{
    StaticModelComponent result = {
	.Model = static_model_cube_create()
    };
    return result;
}
