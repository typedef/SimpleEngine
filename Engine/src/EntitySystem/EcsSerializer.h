#ifndef ECS_SERIALIZER_H
#define ECS_SERIALIZER_H

void ecs_serializer_write(void* ecsWorldAsVoidPtr, const char* fileName);
void* ecs_serializer_read(const char* path);

#endif // ECS_SERIALIZER_H
