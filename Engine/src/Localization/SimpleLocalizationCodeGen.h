#ifndef SIMPLE_LOCALIZATION_CODE_GEN_H
#define SIMPLE_LOCALIZATION_CODE_GEN_H

#include "SimpleLocalization.h"

void simple_localization_code_gen_generate(SimpleLocalization local, SimpleLocalizationData localData);

#endif
