#ifndef SIMPLE_LOCALIZATION_H
#define SIMPLE_LOCALIZATION_H

#include <Utils/Types.h>

/*
  NOTE(typedef):
  This file only for step 1.
  1. read LocalizationData (languages, localizated strs, groups and other)
  2. LocalizationData -> SimpleLocalizationCodeGenerator

  each line is a group

  ui:
  -> Russian
     English
     ...

     #include <Localization/Enums/LanguageType.h>

     char* localizationPath = path_combine_n("assets/localization/", language_type_to_locale(languageType));
     SimpleLocalization sl = simple_localization_new(localizationPath);

     asset_manager_add_asset(AssetType_Localization, sl);
 */

typedef struct LocaleInfo
{
    char* Name;
} LocaleInfo;

typedef struct LanguageInfo
{
    char* Name;
} LanguageInfo;

typedef struct GroupInfo
{
    char* Name;
} GroupInfo;

typedef struct SimpleLocalizationData
{
    WideString** LocalizedValues;
} SimpleLocalizationData;

typedef struct SimpleLocalization
{
    char** LocaleNames;
    char** LanguageNames;
    char** GroupNames;
    char*** Keys;
} SimpleLocalization;

SimpleLocalization simple_localization_new(const char* localizationPath);
SimpleLocalizationData simple_localization_load_data(SimpleLocalization localization, const char* localeFilePath);
void simple_localization_set(SimpleLocalization local, SimpleLocalizationData localData);
WideString simple_localization_get(i32 group, i32 item);
WideString* simple_localization_getp(i32 group, i32 item);

#endif // SIMPLE_LOCALIZATION_H
