#include "GamePersonNames.h"
#include <Utils/Types.h>

const char*
game_person_names_to_string(GamePersonNames gamePersonNames)
{
    switch (gamePersonNames)
    {
        case GamePersonNames_Akira: return "GamePersonNames_Akira";
        case GamePersonNames_Count: return "GamePersonNames_Count";
    }

    vassert_break();
    return "";
}

const char*
game_person_names_to_short_string(GamePersonNames gamePersonNames)
{
    switch (gamePersonNames)
    {
        case GamePersonNames_Akira: return "Akira";
        case GamePersonNames_Count: return "Count";
    }

    vassert_break();
    return "";
}