#ifndef GAME_SECOND_NAMES_H
#define GAME_SECOND_NAMES_H

typedef enum GameSecondNames
{
    GameSecondNames_Ogawa = 0,
    GameSecondNames_Ohawa = 1,
    GameSecondNames_Obama = 2,
    GameSecondNames_Count = 3
} GameSecondNames;

const char* game_second_names_to_string(GameSecondNames gamesecondnames);
const char* game_second_names_to_short_string(GameSecondNames gamesecondnames);

#endif // GAME_SECOND_NAMES_H
