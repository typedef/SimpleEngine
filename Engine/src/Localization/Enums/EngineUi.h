#ifndef ENGINE_UI_H
#define ENGINE_UI_H

typedef enum EngineUi
{
    EngineUi_PlayButton = 0,
    EngineUi_StopButton = 1,
    EngineUi_Count = 2
} EngineUi;

const char* engine_ui_to_string(EngineUi engineui);
const char* engine_ui_to_short_string(EngineUi engineui);

#endif // ENGINE_UI_H
