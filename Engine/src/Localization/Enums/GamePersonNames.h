#ifndef GAME_PERSON_NAMES_H
#define GAME_PERSON_NAMES_H

typedef enum GamePersonNames
{
    GamePersonNames_Akira = 0,
    GamePersonNames_Count = 1
} GamePersonNames;

const char* game_person_names_to_string(GamePersonNames gamepersonnames);
const char* game_person_names_to_short_string(GamePersonNames gamepersonnames);

#endif // GAME_PERSON_NAMES_H
