#ifndef LOCAL_GROUP_H
#define LOCAL_GROUP_H

typedef enum LocalGroup
{
    LocalGroup_EngineUi = 0,
    LocalGroup_GamePersonNames = 1,
    LocalGroup_GameSecondNames = 2,
    LocalGroup_Count = 3
} LocalGroup;

#endif // LOCAL_GROUP_H