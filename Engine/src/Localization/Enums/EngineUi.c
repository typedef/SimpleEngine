#include "EngineUi.h"
#include <Utils/Types.h>

const char*
engine_ui_to_string(EngineUi engineUi)
{
    switch (engineUi)
    {
        case EngineUi_PlayButton: return "EngineUi_PlayButton";
        case EngineUi_StopButton: return "EngineUi_StopButton";
        case EngineUi_Count: return "EngineUi_Count";
    }

    vassert_break();
    return "";
}

const char*
engine_ui_to_short_string(EngineUi engineUi)
{
    switch (engineUi)
    {
        case EngineUi_PlayButton: return "PlayButton";
        case EngineUi_StopButton: return "StopButton";
        case EngineUi_Count: return "Count";
    }

    vassert_break();
    return "";
}