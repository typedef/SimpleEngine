#include "GameSecondNames.h"
#include <Utils/Types.h>

const char*
game_second_names_to_string(GameSecondNames gameSecondNames)
{
    switch (gameSecondNames)
    {
        case GameSecondNames_Ogawa: return "GameSecondNames_Ogawa";
        case GameSecondNames_Ohawa: return "GameSecondNames_Ohawa";
        case GameSecondNames_Obama: return "GameSecondNames_Obama";
        case GameSecondNames_Count: return "GameSecondNames_Count";
    }

    vassert_break();
    return "";
}

const char*
game_second_names_to_short_string(GameSecondNames gameSecondNames)
{
    switch (gameSecondNames)
    {
        case GameSecondNames_Ogawa: return "Ogawa";
        case GameSecondNames_Ohawa: return "Ohawa";
        case GameSecondNames_Obama: return "Obama";
        case GameSecondNames_Count: return "Count";
    }

    vassert_break();
    return "";
}