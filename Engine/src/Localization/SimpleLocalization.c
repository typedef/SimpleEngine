#include "SimpleLocalization.h"

#include <Utils/SimpleStandardLibrary.h>

SimpleLocalization Local;
SimpleLocalizationData LocalData;

SimpleLocalization
simple_localization_new(const char* basePath)
{
    SimpleLocalization localization = {
	.LocaleNames = NULL,
	.LanguageNames = NULL,
	.GroupNames = NULL,

	.Keys = NULL
    };

    size_t textLength = 0;
    char* text = file_read_string_ext(basePath, &textLength);

    Arena* arena = arena_create(5*textLength);
    memory_set_arena(arena);

    size_t newLength;
    text = string_trim_char(text, textLength, &newLength, ' ');
    GINFO("Trimmed: %s\n", text);
    file_write_string(basePath, text, newLength);

#define ParseKeyWordWithData(ptr, key, keyLen, dataLen)			\
    ({									\
	ptr += keyLen;							\
									\
	if (*ptr != ':')						\
	{								\
	    GERROR("Ptr=%s\n", ptr);					\
	    vassert(*ptr == ':' && "After key you should use ':'!As follows: Key:Data!"); \
	}								\
									\
	++ptr;								\
									\
	char* optr = ptr;						\
	while (*optr && *optr != '\n')					\
	{								\
	    ++optr;							\
	}								\
									\
	dataLen = (i32) (((size_t)optr) - ((size_t)ptr));		\
	char* data = string_substring_length(ptr, textLength, dataLen);	\
									\
	ptr = optr;							\
									\
	data;								\
    })

    char* localesData       = NULL;
    char* languagesData     = NULL;
    char* groupsData        = NULL;
    size_t localesDataLen   = 0;
    size_t languagesDataLen = 0;
    size_t groupsDataLen    = 0;

    char* ptr = text;
    while (*ptr != '\0')
    {

	if (string_compare_length(ptr, "Locales", 7))
	{
	    localesData = ParseKeyWordWithData(ptr, "Locales", 7, localesDataLen);
	}

	if (string_compare_length(ptr, "Languages", 9))
	{
	    languagesData = ParseKeyWordWithData(ptr, "Languages", 9, languagesDataLen);
	}

	if (string_compare_length(ptr, "Groups", 6))
	{
	    groupsData = ParseKeyWordWithData(ptr, "Groups", 6, groupsDataLen);

	    vassert_not_null(localesData);
	    vassert_not_null(languagesData);
	    vassert_not_null(groupsData);
	    vassert(localesDataLen > 0 && "Wrong localesDataLen!");
	    vassert(languagesDataLen > 0 && "Wrong languagesDataLen!");
	    vassert(groupsDataLen > 0 && "Wrong groupsDataLen!");

	    memory_unbind_current_arena();
	    {

		localization.LocaleNames   = string_split_length(localesData, localesDataLen, ',');
		localization.LanguageNames = string_split_length(languagesData, languagesDataLen, ',');
		localization.GroupNames    = string_split_length(groupsData, groupsDataLen, ',');

		/* array_foreach(localization.LocaleNames, */
		/*	      GWARNING("Locale: %s\n", item);); */
		/* array_foreach(localization.LanguageNames, */
		/*	      GWARNING("Language: %s\n", item);); */
		/* array_foreach(localization.GroupNames, */
		/*	      GWARNING("Group: %s\n", item);); */

	    }
	    memory_bind_current_arena();

	    vassert(memory_get_arena() == arena && "Arena is not set as current arena!");

	    ++ptr;
	    GINFO("PTR: %s \n", ptr);

	    for (i32 i = 0; i < array_count(localization.GroupNames); ++i)
	    {
		char* groupName = localization.GroupNames[i];
		size_t groupLength = string_length(groupName);
		if (string_compare_length(ptr, groupName, groupLength))
		{
		    size_t dataLength;
		    char* groupData =
			ParseKeyWordWithData(
			    ptr, groupName, groupLength, dataLength);

		    memory_unbind_current_arena();

		    char** groupsContent = string_split_length(groupData, dataLength, ',');
		    array_push(localization.Keys, groupsContent);

		    memory_bind_current_arena();

		    /* array_foreach(groupsContent, */
		    /*		  GERROR("groupCont[%d]: %s\n", i, item);); */

		    ++ptr;
		    GINFO("PTR: %s \n", ptr);
		}

	    }


	}

	++ptr;
    }


    arena_print(arena);
    arena_destroy(arena);

    return localization;
}

SimpleLocalizationData
simple_localization_load_data(SimpleLocalization localization, const char* localeFilePath)
{
    SimpleLocalizationData localData = { .LocalizedValues = NULL };

    const char* localeFileName = path_get_name(localeFilePath);
    i32 containsInsideLocalization = array_index_of(localization.LocaleNames, string_compare(item, localeFileName)) >= 0;
    vassert(containsInsideLocalization && "Locale wasn't exist inside .slfb file!");

    size_t textLength;
    char* text = file_read_string_ext(localeFilePath, &textLength);
    vassert_not_null(text);
    vassert(textLength > 0 && "Wrong data size!");
    /* GINFO("\nLocalization Text: \n%s\n", text); */

    Arena* arena = arena_create(2 * textLength + KB(1));
    memory_set_arena(arena);

    i64 rowBegins = 0,
	currentPos = 0,
	rowLength = 0;
    char* ptr = text;
    char c = *ptr;

    GroupInfo* groupInfos = NULL;

    char* groupName = NULL;

    while (currentPos != textLength)
    {
	c = *ptr;

	switch (c)
	{
	case '(':
	{
	    char* optr = ptr;
	    while (*optr != ')')
	    {
		++optr;
	    }

	    i32 wordLength = (i32) (((size_t)optr) - ((size_t)ptr) - 1);
	    /* GINFO("Word Length: %d\n", wordLength); */
	    vassert(wordLength > 0 && wordLength <= 2);

	    rowLength = string_to_i32_length(ptr+1, wordLength);

	    /* GINFO("RowLength: %d\n", rowLength); */

	    break;
	}

	case '\n':
	{
	    rowBegins = currentPos + 1;
	    break;
	}

	case ':':
	{
	    if (rowLength == 0)
	    {
		GERROR("Row Length: %d\n", rowLength);
		vassert_break();
		break;
	    }

	    ++ptr;
	    ++currentPos;

	    //BUG(typedef): should be inside Arena

	    i64 charsCount = currentPos - rowBegins;
	    char* data = string_copy((const char*)ptr, rowLength);
	    /* GINFO("Data: %s\n", data); */

	    WideString wideStr = wide_string_utf8((const char*)data);
	    /* GWARNING("WideStr: "); */
	    /*
	      NOTE(typdef): we need use this until we have full featured
	      printf, vnsprintf()
	    */
	    wide_string_print_line(wideStr);

	    memory_unbind_current_arena();
	    {
		WideString* groupsValue = wide_string_split(wideStr, ',');
		array_push(localData.LocalizedValues, groupsValue);
	    }
	    memory_bind_current_arena();

	    string_builder_clear(groupName);

	    break;
	}

	default:
	{
	    string_builder_appendc(groupName, c);

	    break;
	}

	}

	++currentPos;
	++ptr;
    }

    //arena_print(arena);
    arena_destroy(arena);

    return localData;
}

void
simple_localization_set(SimpleLocalization local, SimpleLocalizationData localData)
{
    Local = local;
    LocalData = localData;
}

WideString
simple_localization_get(i32 group, i32 item)
{
    return LocalData.LocalizedValues[group][item];
}

WideString*
simple_localization_getp(i32 group, i32 item)
{
    return &LocalData.LocalizedValues[group][item];
}
