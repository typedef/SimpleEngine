#include "SimpleLocalizationCodeGen.h"

#include <Utils/SimpleStandardLibrary.h>

#include <Helma/HelmaEnumGenerator.h>


/*
  NOTE(typedef):

  Is this really good idea??
  typedef struct LocalizationBase
  {
      WideString* Ui;
      WideString* PersonMaleName;
      WideString* PersonFemaleName;
      WideString* PersonSecondName;
  } LocalizationBase;


  LocalizationBase localization_get_base();
  WideString  localization_get(GroupType group, i32 index);
  WideString* localization_getp(GroupType group, i32 index);

  Usage:
  LocalizationBase localizationBase = localization_get_base();

  WideString playBtnText = localization_get(GroupType_Ui, EngineUi_PlayBtn);
  WideString* exitBtnTextPtr = localization_getp(GroupType_Ui, EngineUi_ExitBtn);


*/

typedef struct LocalGroup
{
    char* DotH;
    char* DotC;
} LocalGroup;

static LocalGroup
_simple_localization_generate_localization_group(SimpleLocalization local, SimpleLocalizationData localData)
{
    char* dotHFile = NULL;
    {

	string_builder_appends(dotHFile, "#ifndef LOCAL_GROUP_H\n");
	string_builder_appends(dotHFile, "#define LOCAL_GROUP_H\n\n");
	string_builder_appends(dotHFile, "typedef enum LocalGroup\n{\n");

	i32 i, count = array_count(local.GroupNames);
	for (i = 0; i < count; ++i)
	{
	    char* groupName = local.GroupNames[i];
	    string_builder_appendf(dotHFile, "    LocalGroup_%s = %d,\n", groupName, i);
	}

	string_builder_appendf(dotHFile, "    LocalGroup_Count = %d\n} LocalGroup;\n\n#endif // LOCAL_GROUP_H", i);
    }

    char* dotCFile = string("#include \"LocalGroup.h\"");

    return (LocalGroup) { .DotH = dotHFile, .DotC = dotCFile };
}

void
simple_localization_code_gen_generate(SimpleLocalization local, SimpleLocalizationData localData)
{
    Arena* arena = arena_create_and_set(MB(1));

    char* writeDirPath = path_combine(path_get_current_directory(), "Engine/src/Localization/Enums");

    i32 i, count = array_count(local.GroupNames);
    for (i = 0; i < count; ++i)
    {
	char* groupName = local.GroupNames[i];

	HelmaGeneratedEnumContent content =
	    helma_generate_enum(groupName, local.Keys[i]);

	char* writeFileHPath = path_combine(writeDirPath, string_concat(groupName, ".h"));
	char* writeFileCPath = path_combine(writeDirPath, string_concat(groupName, ".c"));

	GINFO("Path: %s\n", writeDirPath);
	GINFO("Path: %s\n", writeFileHPath);
	GINFO("Path: %s\n", writeFileCPath);

	file_write_string(writeFileHPath, content.DotH, string_builder_count(content.DotH));
	file_write_string(writeFileCPath, content.DotC, string_builder_count(content.DotC));
    }

    LocalGroup localGroup =
	_simple_localization_generate_localization_group(
	    local, localData);

    file_write_string(path_combine(writeDirPath, "LocalGroup.h"), localGroup.DotH, string_length(localGroup.DotH));
    file_write_string(path_combine(writeDirPath, "LocalGroup.c"), localGroup.DotC, string_length(localGroup.DotC));


    arena_print(arena);
    arena_destroy(arena);
}
