#ifndef HELMA_H
#define HELMA_H

#define LOGGER_DEFINITION
#define MEMORY_ALLOCATOR_DEFINITION
#define ARRAY_DEFINITION
#define STRING_DEFINITION
#define WIDE_STRING_DEFINITION
#define STRING_BUILDER_DEFINITION
#define IO_DEFINITION
#define PATH_DEFINITION
#include <Utils/SimpleStandardLibrary.h>

char* helma_string_to_convention(const char* str, size_t length, char conventionChar);

#endif // HELMA_H
