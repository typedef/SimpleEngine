#include "Helma.h"

char*
helma_string_to_convention(const char* str, size_t length, char conventionChar)
{
    i32 upperCount = string_count_upper(str);
    if (upperCount <= 0)
    {
	vassert_break();
	return string_copy(str, length);
    }

    if (upperCount == 1)
    {
	return string_to_lower(str);
    }

    size_t newLength = length + upperCount - 1;
    char* result = memory_allocate(newLength + 1);
    memset(result, '\0', newLength + 1);

    size_t ind = 0;
    char* ptr = (char*) result;
    char* rptr = (char*)str;

    typedef enum LocalFlag {
	LocalFlag_IsFirst = 0,
	LocalFlag_IsSecondary = 1,
	LocalFlag_IsThird = 2
    } LocalFlag;

    LocalFlag flag = LocalFlag_IsFirst;
    while (ind < newLength)
    {
	char c = *rptr;

	if (char_is_upper(c) && flag != LocalFlag_IsThird)
	{
	    if (flag == LocalFlag_IsFirst)
	    {
		flag = LocalFlag_IsSecondary;
		*ptr = char_to_lower(c);
		++rptr;
	    }
	    else
	    {
		flag = LocalFlag_IsThird;
		*ptr = conventionChar;
	    }
	}
	else
	{
	    flag = LocalFlag_IsSecondary;
	    *ptr = char_to_lower(c);
	    ++rptr;
	}

	++ind;
	++ptr;
    }

    return result;
}
