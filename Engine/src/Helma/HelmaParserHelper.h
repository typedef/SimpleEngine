#ifndef HELMA_PARSER_HELPER_H
#define HELMA_PARSER_HELPER_H

#include <Utils/Types.h>

typedef enum VariableType
{
    VariableType_Void = 0,
    VariableType_I8,
    VariableType_I32,
    VariableType_U8,
    VariableType_U32,
    VariableType_F32,
    VariableType_Bool,
    VariableType_ConstCharPtr,
    VariableType_CharPtr,
    VariableType_Count
} VariableType;

static const char* DefaultType[VariableType_Count] = {
    "void",
    "i8",
    "i32",
    "u8",
    "u32",
    "f32",
    "f64",
    "bool",
    "const char*",
    //"char*"
};

const char* variable_type_to_string(VariableType variableType);

#define skip_line(ptr)				\
    while (*ptr != '\n' && *ptr != '\0') ++ptr;
#define skip_empty(ptr)				\
    while (*ptr == '\n' || *ptr == '\t' || *ptr == ' ') ++ptr;
#define skip_preproc(ptr)			\
    while (*ptr == '#') { skip_line(ptr); ++ptr; }
#define skip_to_char(ptr, ch)			\
    while (*ptr && *ptr != ch) { ++ptr; }
#define skip_to_separator(ptr)						\
    while (*ptr && (*ptr != '\n' && *ptr != ' ' && *ptr != ';')) { ++ptr; }

#define skip_comments(ptr)			\
    ({						\
	char c = *ptr;				\
	char nc = *(ptr + 1);			\
	if (c == '/' && nc == '/')		\
	{					\
	    skip_line(ptr);			\
	}					\
						\
	if (c == '/' && nc == '*')		\
	{					\
	    /**/				\
	    ptr += 2;				\
	    c = *ptr;				\
	    char pc = *(ptr - 1);		\
	    while (c != '/' || pc != '*')	\
	    {					\
		++ptr;				\
		c = *ptr;			\
		pc = *(ptr - 1);		\
	    }					\
						\
	    /* skip '/' */			\
	    ++ptr;				\
	}					\
    })

force_inline i32
_is_char(char c)
{
    if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
	return 1;
    return 0;
}

force_inline i32
_is_digit(char c)
{
    if (c >= '0' && c <= '9')
	return 1;
    return 0;
}

force_inline i32
helma_is_skipable(char c)
{
    if (c == '#'
	|| c == '\n'
	|| c == '\t'
	|| c == ' '
	|| c == '_'
	|| _is_digit(c))
    {
	return 1;
    }

    return 0;
}

/*
  NOTE(typedef): mb _is_keyword()
*/
i32 _is_typedef_keyword(char* ptr);
i32 _is_type(char* ptr, VariableType* type);
i32 _is_enum_acceptable(char c);
i32 _is_identifier(char* ptr, char** identifier);

#endif // HELMA_PARSER_HELPER_H
