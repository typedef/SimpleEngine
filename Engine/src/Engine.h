#ifndef ENGINE_H
#define ENGINE_H

#pragma GCC diagnostic ignored "-Wint-to-pointer-cast"

#include "Application/Application.h"
#include "Application/SceneBasic.h"

#include "Event/Event.h"

// Editor
#include "Editor/ViewportPanel.h"

// Utils
#include "Utils/Types.h"
#include "Utils/SimpleThread.h"
#include "Utils/WavReader.h"
#include "Utils/SoundComponent.h"

// Graphics
#include "Graphics/OpenGLBase.h"
#include "Graphics/SimpleWindow.h"
#include "Graphics/Renderer.h"
#include "Graphics/UIRenderer.h"
// Graphics -> Vulkan
#include "Graphics/Vulkan/VulkanSimpleApi.h"
#include "Graphics/Vulkan/VulkanSimpleRenderer.h"

// Input System
#include "InputSystem/SimpleInput.h"
#include "InputSystem/KeyBinding.h"

// Asset Manager
#include "AssetManager/AssetManager.h"

// Localization
#include "Localization/SimpleLocalization.h"
#include "Localization/SimpleLocalizationCodeGen.h"

// Simple Ui
#include "UI/Suif.h"

// Entity System
#include "EntitySystem/SimpleEcs.h"
#include "EntitySystem/EcsScene.h"
#include "EntitySystem/Components/AllComponents.h"
#include "EntitySystem/EcsSerializer.h"

// Models
#include "Model/StaticModel.h"

// Math
#include "Math/SimpleMath.h"
#include "Math/SimpleMathIO.h"

// NOTE(typedef): Helma part
#include "Helma/Helma.h"
#include "Helma/HelmaParserHelper.h"
#include "Helma/HelmaEnumGenerator.h"


#endif // ENGINE_H
