configurations
{
  "Debug",
  "Release",
  "Dist"
}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

project "MiniAudio"
    cdialect "C99"
    kind "StaticLib"
    language "C"
    staticruntime "on"
    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")

    files
    {
      "src/**.h",
      "src/**.c",
    }

    filter "configurations:Debug"
      symbols "On"

    filter "configurations:Release"
      buildoptions { "-O3" }
      optimize "Speed"
