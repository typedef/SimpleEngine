project "cimgui"
    kind "StaticLib"
    language "C++"
    cppdialect "C++17"
    staticruntime "on"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin/Intermidiates/" .. outputdir .. "/%{prj.name}")

    includedirs
    {
       "src/"
    }
    files
    {
       "src/cimgui.h",
       "src/cimgui.cpp",
       "src/cimguizmo.h",
       "src/cimguizmo.cpp",

       "src/imgui/imgui.h",
       "src/imgui/imgui.cpp",
       "src/imgui/imconfig.h",
       "src/imgui/imstb_rectpack.h",
       "src/imgui/imstb_textedit.h",
       "src/imgui/imstb_truetype.h",
       "src/imgui/imgui_internal.h",
       "src/imgui/imgui_draw.cpp",
       "src/imgui/imgui_widgets.cpp",
       "src/imgui/imgui_tables.cpp",
       "src/imgui/ImGuizmo.h",
       "src/imgui/ImGuizmo.cpp",
       "src/imgui/imgui_demo.cpp"
    }

    filter "system:linux"
    pic "On"
    systemversion "latest"
    staticruntime "On"

    filter "system:windows"
    systemversion "latest"
    staticruntime "On"

    filter "configurations:Debug"
      defines { "ENGINE_DEBUG" }
      files
      {
      }
      runtime "Debug"
      symbols "On"

    filter "configurations:Release"
      defines { "ENGINE_RELEASE" }
      optimize "On"
      buildoptions { "-O3" }
      runtime "Release"
      optimize "on"
