#!/bin/bash

projectName=$1
premakeFileName=$2

cd ../../


rm $2
touch $2

templateFileContent=$(cat Templates/template-project-premake.lua)
echo "${templateFileContent//%ProjectName/$1}" >> $2
