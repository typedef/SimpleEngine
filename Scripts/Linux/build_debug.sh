#!/bin/bash

cd ../..

#$1 == SandboxApp
compration=$(Scripts/Linux/compare_file_modification_time.c "assets/" "$1/assets/")
if [ $compration -eq 1 ]
then
    Scripts/Linux/copy_files.sh "$1" 0
else
    echo ""
    echo "[Skip copying data to $(pwd)/$1..]"
    echo ""
fi

startTime=$(date +%s%3N)
# for linker debugging: ld -lvulkan2 -verbose
# for linker LD_DEBUG=libs include before make (https://man7.org/linux/man-pages/man8/ld.so.8.html)
make config=debug -j16
buildResult=$?
if [ $buildResult -eq 0 ]
then
    echo ""
    echo "[Build was successful!]"
    echo ""
else
    echo ""
    echo "[Failed to build project!]"
    echo ""
fi
endTime=$(date +%s%3N)
diffTime=$(( $endTime - $startTime ))

if [ $buildResult -eq 0 ]
then
    echo ""
    echo "[Builds $1 in $diffTime ms]"
    echo ""
    echo ""
    echo "[Run project]"
    bin/Debug-linux-x86_64/$2/$2
fi
