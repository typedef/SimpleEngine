#!/bin/bash

cd ../..
glslc assets/shaders/Vulkan/Triangle.vert -o assets/shaders/Vulkan/TriangleVert.spv
glslc assets/shaders/Vulkan/Triangle.frag -o assets/shaders/Vulkan/TriangleFrag.spv

echo "Shader compilation complete!"
