#!/bin/bash

cd ../../

cp -R assets $1/
cp -R resources $1/

premake5 --file=$1 --cc=gcc codelite
codelite Engine.workspace

#kdevelop -d gdb bin/Debug-linux-x86_64/$1/$1
