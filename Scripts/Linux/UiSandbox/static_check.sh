#!/bin/bash

cd ../../../Engine/src
echo $(pwd)

cppcheck --enable=warning --inconclusive --library=posix --std=c99 . -I ../../Projects/UiSandbox/src/ -DLINUX_PLATFORM -Dvguard_not_null -Darray_foreach -Dvguard_not_null -Dvassert -Dvassert_not_null -DDO_ONES -DGINFO -Darray_foreach_ptr --suppress=incorrectStringBooleanError   --suppress=selfAssignment --suppress=syntaxError --suppress=literalWithCharPtrCompare --suppress=nullPointer
